package com.wishbox.holder.wishEditor

import androidx.fragment.app.Fragment
import com.wishbox.feature.wishEditInfo.WishManageInfoFragment
import ru.terrakok.cicerone.android.support.SupportAppScreen

class WishManageInfoScreen(
    private val wishId: Long? = null,
    private val url: String? = null
) : SupportAppScreen() {
    override fun getFragment(): Fragment {
        return WishManageInfoFragment.newInstance(wishId, url)
    }
}