package com.wishbox.holder.wishEditor.navigation

import androidx.fragment.app.FragmentActivity
import com.wishbox.core.di.screenproviders.MainScreenProvider
import ru.terrakok.cicerone.android.support.SupportAppNavigator
import ru.terrakok.cicerone.commands.Back
import ru.terrakok.cicerone.commands.Command
import ru.terrakok.cicerone.commands.Replace

/**
 * Created by Pak Evgeniy on 07.12.2018
 */

class WishManageNavigator(
    activity: FragmentActivity,
    containerId: Int,
    private val mainScreenProvider: MainScreenProvider
) : SupportAppNavigator(activity, containerId) {

    override fun applyCommand(command: Command?) {
        if (command is Back) handleBack()
        else super.applyCommand(command)
    }

    private fun handleBack() {
        if (localStackCopy.size > 0) {
            fragmentManager.popBackStack()
            localStackCopy.removeLast()
        } else {
            activity?.run {
                if (isTaskRoot) {
                    activityReplace(Replace(mainScreenProvider.getMainScreen()))
                } else {
                    activityBack()
                }
            } ?: activityBack()
        }
    }
}