package com.wishbox.holder.wishEditor

import com.wishbox.core.presentation.pictures.PicturesManager
import com.wishbox.core.presentation.pictures.PicturesManager.Companion.MAX_PICTURES_AMOUNT
import io.reactivex.BackpressureStrategy
import io.reactivex.Flowable
import io.reactivex.subjects.BehaviorSubject
import javax.inject.Inject

class PicturesManagerImpl @Inject constructor() : PicturesManager {

    private val subject = BehaviorSubject.createDefault<List<String>>(emptyList())

    override val pictures: MutableList<String> = mutableListOf()

    override fun observePictures(): Flowable<List<String>> =
        subject.toFlowable(BackpressureStrategy.BUFFER)

    override fun makePictureMain(path: String) {
        val index = pictures.indexOfFirst { it == path }
        if (index != -1) {
            val picture = pictures[index]
            pictures.removeAt(index)
            pictures.add(0, picture)
            subject.onNext(pictures)
        }
    }

    override fun addLocalPictures(vararg paths: String) {
        paths.forEach { path ->
            val picture = pictures.find { it == path }
            if (picture == null && pictures.size < MAX_PICTURES_AMOUNT) {
                pictures.add(path)
            }
        }
        subject.onNext(pictures)
    }

    override fun deletePicture(path: String) {
        val index = pictures.indexOfFirst { it == path }
        if (index != -1) {
            pictures.removeAt(index)
            subject.onNext(pictures)
        }
    }

    override fun canAddMore(): Boolean = pictures.size < MAX_PICTURES_AMOUNT
}