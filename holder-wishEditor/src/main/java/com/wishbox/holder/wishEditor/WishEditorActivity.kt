package com.wishbox.holder.wishEditor

import android.content.Intent
import com.wishbox.core.di.api.FeatureProviderApi
import com.wishbox.core.di.api.RouterApi
import com.wishbox.core.di.provider.FeatureProviderApiProvider
import com.wishbox.core.di.provider.RouterApiProvider
import com.wishbox.core.presentation.base.BaseActivity
import com.wishbox.core.presentation.di.PictureManagerApi
import com.wishbox.core.presentation.di.PictureManagerProvider
import com.wishbox.core.presentation.navigation.NavigationBackHelper
import com.wishbox.holder.wishEditor.di.WishEditorViewComponent
import javax.inject.Inject

/**
 * Created by Pak Evgeniy on 04/12/2018.
 */

class WishEditorActivity : BaseActivity(),
    FeatureProviderApiProvider,
    PictureManagerProvider,
    RouterApiProvider {

    private val component by lazy { WishEditorViewComponent.Initializer.init(this) }

    override val featureProviderApi: FeatureProviderApi
        get() = component

    override val pictureManagerApi: PictureManagerApi
        get() = component

    override val routerApi: RouterApi
        get() = component

    companion object {
        const val BUNDLE_WISH_ID = "BUNDLE_WISH_ID"
    }

    @Inject
    lateinit var navigationBackHelper: NavigationBackHelper

    @Inject
    lateinit var viewModel: WishEditorViewModel

    @Inject
    lateinit var newUrlHelper: NewUrlHelper

    override fun injectDependencies() {
        component.inject(this)
    }

    override fun layoutRes(): Int = R.layout.activity_wish_editor

    override fun viewCreated() {
        intent?.extras?.run {
            getString(Intent.EXTRA_TEXT)
                ?.let { viewModel.handleUrlIncome(it) }
                ?: viewModel.showWishEditInfo(id = getLong(BUNDLE_WISH_ID))
        } ?: viewModel.showWishEditInfo()
    }

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        intent?.extras?.getString(Intent.EXTRA_TEXT)?.let { newUrlHelper.onNewUrl(it) }
    }

    override fun onBackPressed() {
        if (navigationBackHelper.onBackPressedHandled().not()) super.onBackPressed()
    }
}