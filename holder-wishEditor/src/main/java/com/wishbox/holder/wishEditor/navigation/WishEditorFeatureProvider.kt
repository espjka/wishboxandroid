package com.wishbox.holder.wishEditor.navigation

import androidx.fragment.app.Fragment
import com.wishbox.core.di.screenproviders.EntranceScreenProvider
import com.wishbox.core.di.screenproviders.FeatureProvider
import com.wishbox.feature.managePictures.ManagePicturesFragment
import ru.terrakok.cicerone.android.support.SupportAppScreen
import javax.inject.Inject

/**
 * Created by Pak Evgeniy on 12.12.2018
 */

class WishEditorFeatureProvider @Inject constructor(
    private val entranceScreenProvider: EntranceScreenProvider
) : FeatureProvider {

    override fun getManagePictures(wishId: Long?): SupportAppScreen =
        object : SupportAppScreen() {
            override fun getScreenKey(): String = ManagePicturesFragment::class.java.name
            override fun getFragment(): Fragment = ManagePicturesFragment.newInstance(wishId)
        }

    override fun getEntrance(): SupportAppScreen {
        return entranceScreenProvider.getEntranceScreen()
    }
}