package com.wishbox.holder.wishEditor

import com.smedialink.common.base.BaseViewModel
import com.wishbox.core.di.screenproviders.FeatureProvider
import ru.terrakok.cicerone.Router
import javax.inject.Inject

/**
 * Created by Pak Evgeniy on 04/12/2018.
 */

class WishEditorViewModel @Inject constructor(
    private val router: Router,
    private val isLoggedIn: () -> Boolean,
    private val featureProvider: FeatureProvider
) : BaseViewModel() {

    fun showWishEditInfo(id: Long? = null, url: String? = null) {
        router.replaceScreen(WishManageInfoScreen(id, url))
    }

    fun handleUrlIncome(url: String) {
        if (isLoggedIn.invoke()) {
            showWishEditInfo(url = url)
        } else {
            router.replaceScreen(featureProvider.getEntrance())
        }
    }
}