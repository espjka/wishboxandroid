package com.wishbox.holder.wishEditor.di

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.wishbox.core.di.mapkeys.ViewModelKey
import com.wishbox.core.di.scope.PerActivity
import com.wishbox.core.di.screenproviders.FeatureProvider
import com.wishbox.core.di.screenproviders.MainScreenProvider
import com.wishbox.core.domain.repository.UserSessionRepository
import com.wishbox.core.presentation.navigation.NavigationBackHelper
import com.wishbox.core.presentation.pictures.PicturesManager
import com.wishbox.holder.wishEditor.PicturesManagerImpl
import com.wishbox.holder.wishEditor.R
import com.wishbox.holder.wishEditor.WishEditorActivity
import com.wishbox.holder.wishEditor.WishEditorViewModel
import com.wishbox.holder.wishEditor.navigation.WishEditorFeatureProvider
import com.wishbox.holder.wishEditor.navigation.WishManageNavigator
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.multibindings.IntoMap
import ru.terrakok.cicerone.Navigator
import ru.terrakok.cicerone.Router

/**
 * Created by Pak Evgeniy on 26/11/2018.
 */

@Module
abstract class WishEditorViewModule {

    @Binds
    abstract fun provideFeatureProvider(impl: WishEditorFeatureProvider): FeatureProvider

    @Binds
    @PerActivity
    abstract fun providePicturesManager(impl: PicturesManagerImpl): PicturesManager

    @Module
    companion object {

        @Provides
        @JvmStatic
        fun provideNavigatorBackHelper(activity: WishEditorActivity): NavigationBackHelper =
            NavigationBackHelper(activity)

        @Provides
        @JvmStatic
        fun provideNavigator(
            activity: WishEditorActivity,
            mainScreenProvider: MainScreenProvider
        ): Navigator =
            WishManageNavigator(
                activity,
                R.id.container,
                mainScreenProvider
            )

        @Provides
        @JvmStatic
        @IntoMap
        @ViewModelKey(WishEditorViewModel::class)
        fun provideViewModel(
            router: Router,
            sessionRepository: UserSessionRepository,
            featureProvider: FeatureProvider
        ): ViewModel =
            WishEditorViewModel(
                router,
                { sessionRepository.isLoggedIn },
                featureProvider
            )

        @Provides
        @JvmStatic
        fun provideWishEditorViewModel(
            activity: WishEditorActivity,
            factory: ViewModelProvider.Factory
        ): WishEditorViewModel =
            ViewModelProviders.of(activity, factory).get(WishEditorViewModel::class.java)
    }
}