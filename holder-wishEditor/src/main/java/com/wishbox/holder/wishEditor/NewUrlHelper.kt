package com.wishbox.holder.wishEditor

import androidx.fragment.app.Fragment
import com.wishbox.feature.wishmanage.wishManageInfo.helper.NewUrlListener
import javax.inject.Inject

/**
 * Created by Pak Evgeniy on 07.12.2018
 */

class NewUrlHelper @Inject constructor(private val activity: WishEditorActivity) {

    fun onNewUrl(url: String) {
        val fm = activity.supportFragmentManager
        var fragment: Fragment? = null
        val fragments = fm.fragments
        for (f in fragments) {
            if (f.isVisible) {
                fragment = f
                break
            }
        }

        (fragment as? NewUrlListener)?.onNewUrl(url)
    }
}