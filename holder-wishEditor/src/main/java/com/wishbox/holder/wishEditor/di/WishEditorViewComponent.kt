package com.wishbox.holder.wishEditor.di

import com.wishbox.core.di.api.FeatureProviderApi
import com.wishbox.core.di.api.InjectorApi
import com.wishbox.core.di.api.RouterApi
import com.wishbox.core.di.module.GlobalRouterModule
import com.wishbox.core.di.module.ViewModelModule
import com.wishbox.core.di.provider.InjectorApiProvider
import com.wishbox.core.di.scope.PerActivity
import com.wishbox.core.presentation.di.PictureManagerApi
import com.wishbox.holder.wishEditor.WishEditorActivity
import dagger.BindsInstance
import dagger.Component

/**
 * Created by Pak Evgeniy on 26/11/2018.
 */

@PerActivity
@Component(
    dependencies = [
        InjectorApi::class
    ],
    modules = [
        GlobalRouterModule::class,
        ViewModelModule::class,
        WishEditorViewModule::class
    ]
)
interface WishEditorViewComponent : FeatureProviderApi, PictureManagerApi, RouterApi {

    fun inject(activity: WishEditorActivity)

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun bind(activity: WishEditorActivity): Builder

        fun api(api: InjectorApi): Builder

        fun build(): WishEditorViewComponent
    }

    class Initializer private constructor() {
        companion object {

            fun init(activity: WishEditorActivity): WishEditorViewComponent {
                val appApi = (activity.application as InjectorApiProvider).injectorApi

                return DaggerWishEditorViewComponent.builder()
                    .bind(activity)
                    .api(appApi)
                    .build()
            }
        }
    }
}