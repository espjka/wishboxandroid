package com.wishbox.holder.entrance.navigation.screensProvider

import androidx.fragment.app.Fragment
import com.wishbox.feature.auth.auth.AuthFragment
import ru.terrakok.cicerone.android.support.SupportAppScreen
import javax.inject.Inject

class EntranceInnerScreensProvider @Inject constructor() {
    fun getAuthScreen(): SupportAppScreen =
        object : SupportAppScreen() {
            override fun getFragment(): Fragment = AuthFragment()
        }
}