package com.wishbox.holder.entrance

import com.smedialink.common.base.BaseViewModel
import com.wishbox.core.presentation.navigation.HasNavigation
import com.wishbox.core.presentation.navigation.LiveRouter
import com.wishbox.holder.entrance.navigation.AuthFlowScreen
import javax.inject.Inject

class EntranceViewModel @Inject constructor(
    private val router: LiveRouter,
    private val authFlowScreen: AuthFlowScreen
) : BaseViewModel(), HasNavigation by router {

    fun openInitialScreen() {
        router.newRootScreen(authFlowScreen)
    }
}
