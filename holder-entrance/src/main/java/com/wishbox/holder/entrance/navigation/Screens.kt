package com.wishbox.holder.entrance.navigation

import androidx.fragment.app.Fragment
import com.wishbox.feature.auth.flow.AuthFlowFragment
import ru.terrakok.cicerone.android.support.SupportAppScreen
import javax.inject.Inject

class AuthFlowScreen @Inject constructor() : SupportAppScreen() {
    override fun getFragment(): Fragment {
        return AuthFlowFragment()
    }
}

class ProfileScreen : SupportAppScreen() {
}

class SplashScreen : SupportAppScreen() {
}