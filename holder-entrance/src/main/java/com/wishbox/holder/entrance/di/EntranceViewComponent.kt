package com.wishbox.holder.entrance.di

import com.wishbox.core.di.api.InjectorApi
import com.wishbox.core.di.api.RouterApi
import com.wishbox.core.di.module.GlobalRouterModule
import com.wishbox.core.di.module.ViewModelModule
import com.wishbox.core.di.provider.InjectorApiProvider
import com.wishbox.core.di.scope.PerActivity
import com.wishbox.holder.entrance.EntranceActivity
import com.wishbox.holder.entrance.di.module.EntranceActivityModule
import dagger.BindsInstance
import dagger.Component

@PerActivity
@Component(
    dependencies = [InjectorApi::class],
    modules = [
        GlobalRouterModule::class,
        ViewModelModule::class,
        EntranceActivityModule::class
    ]
)
interface EntranceViewComponent : RouterApi {

    fun inject(startActivity: EntranceActivity)

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun bind(entranceActivity: EntranceActivity): Builder

        fun api(injectorApi: InjectorApi): Builder

        fun build(): EntranceViewComponent
    }

    object Injector {

        fun init(activity: EntranceActivity): EntranceViewComponent {
            val appApi = (activity.application as InjectorApiProvider).injectorApi

            return DaggerEntranceViewComponent.builder()
                .api(appApi)
                .bind(activity)
                .build()
        }
    }
}