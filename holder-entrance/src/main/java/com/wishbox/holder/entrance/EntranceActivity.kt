package com.wishbox.holder.entrance

import com.smedialink.common.ext.observe
import com.wishbox.core.di.api.RouterApi
import com.wishbox.core.di.provider.RouterApiProvider
import com.wishbox.core.presentation.base.BaseActivity
import com.wishbox.core.presentation.navigation.accept
import com.wishbox.holder.entrance.di.EntranceViewComponent
import javax.inject.Inject

class EntranceActivity : BaseActivity(), RouterApiProvider {

    private val component by lazy { EntranceViewComponent.Injector.init(this) }

    override val routerApi: RouterApi
        get() = component

    @Inject
    lateinit var viewModel: EntranceViewModel

    override fun layoutRes() = R.layout.activity_entrance

    override fun viewCreated() {
        viewModel.navigationEvents.observe(this, navigator::accept)
        if (supportFragmentManager.findFragmentById(R.id.container) == null) {
            viewModel.openInitialScreen()
        }
    }

    override fun injectDependencies() {
        component.inject(this)
    }

    override fun onBackPressed() {
        val fragment = supportFragmentManager.findFragmentById(R.id.container)
        if (fragment != null && fragment.childFragmentManager.backStackEntryCount > 0) {
            fragment.childFragmentManager.popBackStack()
        } else {
            super.onBackPressed()
        }
    }
}
