package com.wishbox.holder.entrance.di.module

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.wishbox.core.di.mapkeys.ViewModelKey
import com.wishbox.core.presentation.navigation.LiveRouter
import com.wishbox.feature.auth.R
import com.wishbox.holder.entrance.EntranceActivity
import com.wishbox.holder.entrance.EntranceViewModel
import com.wishbox.holder.entrance.navigation.AuthFlowScreen
import dagger.Module
import dagger.Provides
import dagger.multibindings.IntoMap
import ru.terrakok.cicerone.Navigator
import ru.terrakok.cicerone.Router
import ru.terrakok.cicerone.android.support.SupportAppNavigator

@Module
class EntranceActivityModule {

    @Provides
    fun provideNavigator(activity: EntranceActivity): Navigator = SupportAppNavigator(activity, R.id.container)

    @Provides
    @IntoMap
    @ViewModelKey(EntranceViewModel::class)
    fun provideViewModel(
        router: LiveRouter,
        authFlowScreen: AuthFlowScreen
    ): ViewModel = EntranceViewModel(router, authFlowScreen)

    @Provides
    fun provideAuthViewModel(
        activity: EntranceActivity,
        factory: ViewModelProvider.Factory
    ): EntranceViewModel = ViewModelProviders.of(activity, factory).get(EntranceViewModel::class.java)
}
