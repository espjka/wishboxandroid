package com.wishbox.feature.profile

/**
 * Created by Pak Evgeniy on 20/12/2018.
 */

data class ProfileUiModel(
    val fullName: String,
    val avatarUrl: String? = null,
    val city: String = "",
    val birthday: String = "",
    val friendAmount: Int = 0,
    val wishAmount: Int = 0,
    val hobby: String = "",
    val flower: String = "",
    val alcohol: String = "",
    val sweet: String = ""
)