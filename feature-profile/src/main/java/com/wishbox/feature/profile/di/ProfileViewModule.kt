package com.wishbox.feature.profile.di

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.smedialink.common.error.ErrorHandler
import com.wishbox.core.di.mapkeys.ViewModelKey
import com.wishbox.core.di.screenproviders.FeatureProvider
import com.wishbox.core.domain.repository.ProfileRepository
import com.wishbox.core.domain.repository.UserRepository
import com.wishbox.core.presentation.errorHandling.MainErrorHandler
import com.wishbox.feature.profile.ProfileFragment
import com.wishbox.feature.profile.ProfileViewModel
import com.wishbox.feature.profile.mapper.ProfileViewMapper
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.multibindings.IntoMap
import ru.terrakok.cicerone.Router

/**
 * Created by Pak Evgeniy on 08.12.2018
 */

@Module
abstract class ProfileViewModule {

    @Binds
    abstract fun provideErrorHandler(impl: MainErrorHandler): ErrorHandler

    @Module
    companion object {

        @Provides
        @JvmStatic
        fun provideUserId(fragment: ProfileFragment): Long? =
            fragment.arguments?.getLong(ProfileFragment.BUNDLE_USER_ID)?.takeIf { it != 0L }

        @Provides
        @IntoMap
        @ViewModelKey(ProfileViewModel::class)
        @JvmStatic
        fun provideViewModel(
            router: Router,
            errorHandler: ErrorHandler,
            userId: Long?,
            featureProvider: FeatureProvider,
            profileRepository: ProfileRepository,
            userRepository: UserRepository,
            mapper: ProfileViewMapper
        ): ViewModel =
            ProfileViewModel(
                router = router,
                errorHandler = errorHandler,
                userId = userId,
                featureProvider = featureProvider,
                requestMyProfile = { profileRepository.requestProfile() },
                observeMyProfile = { profileRepository.observeProfile() },
                getOtherProfile = { userRepository.getUser(it, true) },
                mapper = mapper
            )

        @JvmStatic
        @Provides
        fun provideProfileViewModel(
            fragment: ProfileFragment,
            factory: ViewModelProvider.Factory
        ): ProfileViewModel =
            ViewModelProviders.of(fragment, factory).get(ProfileViewModel::class.java)
    }
}