package com.wishbox.feature.profile

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.smedialink.common.base.BaseViewModel
import com.smedialink.common.delegates.rx.disposableDelegate
import com.smedialink.common.error.ErrorHandler
import com.smedialink.common.ext.applySchedulers
import com.wishbox.core.di.screenproviders.FeatureProvider
import com.wishbox.core.domain.entity.ProfileEntity
import com.wishbox.core.domain.entity.UserEntity
import com.wishbox.feature.profile.mapper.ProfileViewMapper
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Single
import io.reactivex.disposables.Disposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import ru.terrakok.cicerone.Router
import javax.inject.Inject

/**
 * Created by Pak Evgeniy on 18/12/2018.
 */

class ProfileViewModel @Inject constructor(
    private val router: Router,
    private val errorHandler: ErrorHandler,
    private val userId: Long?,
    private val featureProvider: FeatureProvider,
    private val mapper: ProfileViewMapper,
    private val requestMyProfile: () -> Completable,
    private val observeMyProfile: () -> Flowable<ProfileEntity>,
    private val getOtherProfile: (id: Long) -> Single<UserEntity>
) : BaseViewModel() {

    private val state: MutableLiveData<ProfileState> by lazy { MutableLiveData<ProfileState>() }

    private var observeProfileDisposable: Disposable? by disposableDelegate()
    private var getProfileDisposable: Disposable? by disposableDelegate()

    fun getState(): LiveData<ProfileState> = state

    init {
        userId ?: let {
            observeMyProfile()
        }
    }

    fun requestProfile() {
        userId?.let { getOtherProfile(it) }
            ?: run {
                requestMyProfile()
            }
    }

    fun handleAction() {
        userId?.let { /*navigate to chat here*/ }
            ?: run { router.navigateTo(featureProvider.getEditProfile()) }
    }

    private fun getOtherProfile(id: Long) {
        getProfileDisposable = getOtherProfile.invoke(id)
            .map { mapper.map(it) }
            .applySchedulers()
            .doOnSubscribe { state.value = ProfileState.loading() }
            .subscribeBy(
                onError = ::onError,
                onSuccess = ::onSuccess
            ).addTo(disposables)
    }

    private fun onSuccess(profile: ProfileUiModel) {
        state.value = ProfileState.data(profile)
    }

    private fun onError(throwable: Throwable) {
        errorHandler.handle(throwable).event?.let { state.value = ProfileState.error(it) }
    }

    private fun requestMyProfile() {
        getProfileDisposable = requestMyProfile.invoke()
            .applySchedulers()
            .doOnSubscribe { state.value = ProfileState.loading() }
            .subscribeBy(
                onError = ::onError
            ).addTo(disposables)
    }

    private fun observeMyProfile() {
        observeProfileDisposable = observeMyProfile.invoke()
            .map { mapper.map(it) }
            .applySchedulers()
            .subscribeBy(
                onError = ::onError,
                onNext = ::onSuccess
            ).addTo(disposables)
    }

    fun navigateBack() {
        router.exit()
    }

    fun openWishList() {
        userId?.let { router.navigateTo(featureProvider.getWishList(it)) }
    }

    fun openFriendList() {
        userId?.let { router.navigateTo(featureProvider.getFriendList(it)) }
    }
}