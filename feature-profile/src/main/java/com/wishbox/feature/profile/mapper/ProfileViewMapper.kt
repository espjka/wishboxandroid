package com.wishbox.feature.profile.mapper

import com.wishbox.core.domain.entity.ItemValue
import com.wishbox.core.domain.entity.ProfileEntity
import com.wishbox.core.domain.entity.UserEntity
import com.wishbox.core.domain.parser.DateParser
import com.wishbox.core.presentation.resources.ResourceProvider
import com.wishbox.feature.profile.ProfileUiModel
import com.wishbox.feature.profile.R
import javax.inject.Inject

/**
 * Created by Pak Evgeniy on 20.12.2018
 */

class ProfileViewMapper @Inject constructor(
    private val resourceProvider: ResourceProvider,
    private val dateParser: DateParser
) {

    private val notSpecifiedInfoString: String by lazy { resourceProvider.getString(R.string.not_specified) }

    fun map(from: ProfileEntity): ProfileUiModel =
        ProfileUiModel(
            fullName = mapName(from.lastName, from.firstName, from.patronymic),
            avatarUrl = from.avatar?.big,
            city = mapItemValue(from.city),
            birthday = dateParser.dateToString(from.birthday, DateParser.Pattern.DATE_DOTS) ?: notSpecifiedInfoString,
            friendAmount = from.friendAmount,
            wishAmount = from.wishAmount,
            hobby = mapItemValue(from.hobby),
            flower = mapItemValue(from.flower),
            alcohol = mapItemValue(from.alcohol),
            sweet = mapItemValue(from.sweet)
        )

    fun map(from: UserEntity): ProfileUiModel =
        ProfileUiModel(
            fullName = mapName(from.lastName, from.firstName, from.patronymic),
            avatarUrl = from.avatar?.big,
            city = mapItemValue(from.city),
            birthday = dateParser.dateToString(from.birthday, DateParser.Pattern.DATE_DOTS) ?: notSpecifiedInfoString,
            friendAmount = from.friendAmount,
            wishAmount = from.wishAmount,
            hobby = mapItemValue(from.hobby),
            flower = mapItemValue(from.flower),
            alcohol = mapItemValue(from.alcohol),
            sweet = mapItemValue(from.sweet)
        )

    private fun mapName(lastName: String?, firstName: String?, patronymic: String?): String =
        "${firstName.orEmpty()} ${lastName.orEmpty()} ${patronymic.orEmpty()}"
            .takeIf { it.isNotBlank() } ?: notSpecifiedInfoString

    private fun mapItemValue(item: ItemValue?): String = item?.name ?: notSpecifiedInfoString
}