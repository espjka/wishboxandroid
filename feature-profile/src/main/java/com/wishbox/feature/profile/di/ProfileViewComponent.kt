package com.wishbox.feature.profile.di

import com.wishbox.core.di.api.RouterApi
import com.wishbox.core.di.api.FeatureProviderApi
import com.wishbox.core.di.api.InjectorApi
import com.wishbox.core.di.module.ViewModelModule
import com.wishbox.core.di.provider.RouterApiProvider
import com.wishbox.core.di.provider.FeatureProviderApiProvider
import com.wishbox.core.di.provider.InjectorApiProvider
import com.wishbox.core.di.scope.PerFragment
import com.wishbox.core.di.util.lookupApiProvider
import com.wishbox.feature.profile.ProfileFragment
import dagger.BindsInstance
import dagger.Component

/**
 * Created by Pak Evgeniy on 08.12.2018
 */

@PerFragment
@Component(
    dependencies = [
        InjectorApi::class,
        RouterApi::class,
        FeatureProviderApi::class
    ],
    modules = [
        ViewModelModule::class,
        ProfileViewModule::class
    ]
)
interface ProfileViewComponent {

    fun inject(fragment: ProfileFragment)

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun bind(fragment: ProfileFragment): Builder

        fun api(api: InjectorApi): Builder

        fun api(api: RouterApi): Builder

        fun api(api: FeatureProviderApi): Builder

        fun build(): ProfileViewComponent
    }

    class Initializer private constructor() {
        companion object {

            fun inject(fragment: ProfileFragment) {
                val injectorProvider = fragment.lookupApiProvider<InjectorApiProvider>()
                val containerProvider = fragment.lookupApiProvider<RouterApiProvider>()
                val featureProviderProvider = fragment.lookupApiProvider<FeatureProviderApiProvider>()

                DaggerProfileViewComponent.builder()
                    .api(injectorProvider.injectorApi)
                    .api(containerProvider.routerApi)
                    .api(featureProviderProvider.featureProviderApi)
                    .bind(fragment)
                    .build()
                    .inject(fragment)
            }
        }
    }
}