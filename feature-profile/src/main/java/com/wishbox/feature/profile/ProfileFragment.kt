package com.wishbox.feature.profile

import android.os.Bundle
import android.view.View
import androidx.core.os.bundleOf
import com.smedialink.common.events.handle
import com.smedialink.common.ext.gone
import com.smedialink.common.ext.observe
import com.wishbox.core.glide.loadImage
import com.wishbox.core.presentation.base.BaseFragment
import com.wishbox.core.presentation.errorHandling.delegate.ToastDelegate
import com.wishbox.core.presentation.loading.ProgressLoadingDelegate
import com.wishbox.core.presentation.navigation.BackButtonListener
import com.wishbox.feature.profile.di.ProfileViewComponent
import kotlinx.android.synthetic.main.fragment_profile.*
import kotlinx.android.synthetic.main.partial_card_purple.view.*
import kotlinx.android.synthetic.main.partial_card_white.view.*
import javax.inject.Inject

/**
 * Created by Pak Evgeniy on 18/12/2018.
 */

class ProfileFragment : BaseFragment<ProfileViewModel>(), BackButtonListener {

    @Inject
    lateinit var toastDelegate: ToastDelegate

    private val progressLoadingDelegate: ProgressLoadingDelegate by lazy {
        return@lazy ProgressLoadingDelegate(context!!, "Загрузка...")
    }

    companion object {
        const val BUNDLE_USER_ID = "BUNDLE_USER_ID"
        fun newInstance(userId: Long? = null): ProfileFragment =
            ProfileFragment().apply { userId?.let { arguments = bundleOf(BUNDLE_USER_ID to it) } }
    }

    override fun injectDependencies() {
        ProfileViewComponent.Initializer.inject(this)
    }

    override fun layoutRes(): Int = R.layout.fragment_profile

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupUI()
        if (arguments?.containsKey(BUNDLE_USER_ID) == true) {
            setupForOtherProfile()
        }
        viewModel.getState().observe(this, this::render)
    }

    override fun onFirstLaunch() {
        viewModel.requestProfile()
    }

    override fun onBackPressed(): Boolean {
        viewModel.navigateBack()
        return true
    }

    private fun render(state: ProfileState) {
        progressLoadingDelegate.showLoading(state.isLoading)
        state.error?.handle { toastDelegate.show(it.errorText) }
        state.profile?.run {
            includeFriends.textAmount.text = "$friendAmount"
            includeFriends.textAmountType.text = resources.getQuantityText(R.plurals.friends, friendAmount)
            includeWishes.textAmount.text = "$wishAmount"
            includeWishes.textAmountType.text = resources.getQuantityText(R.plurals.wishes, wishAmount)

            includeHobbies.textValue.text = hobby
            includeAlcohol.textValue.text = alcohol
            includeFlowers.textValue.text = flower
            includeSweet.textValue.text = sweet

            imageAvatar.loadImage(avatarUrl, R.drawable.placeholder_avatar)

            textFullName.text = fullName
            textCity.text = city
            textBirthday.text = birthday
        }
    }

    private fun setupUI() {
        toolbar.getRightButton().setOnClickListener { viewModel.handleAction() }
        toolbar.getLeftButton().setOnClickListener { viewModel.navigateBack() }
        includeFriends.setOnClickListener { viewModel.openFriendList() }
        includeWishes.setOnClickListener { viewModel.openWishList() }

        includeFriends.textAmountType.text = resources.getQuantityText(R.plurals.friends, 0)
        includeWishes.textAmountType.text = resources.getQuantityText(R.plurals.wishes, 0)

        includeHobbies.textType.text = getString(R.string.hobby)
        includeHobbies.imageType.setImageResource(R.drawable.ic_hobby)
        includeAlcohol.textType.text = getString(R.string.alcohol)
        includeAlcohol.imageType.setImageResource(R.drawable.ic_alcohol)
        includeFlowers.textType.text = getString(R.string.flowers)
        includeFlowers.imageType.setImageResource(R.drawable.ic_flower)
        includeSweet.textType.text = getString(R.string.sweets)
        includeSweet.imageType.setImageResource(R.drawable.ic_sweet)
    }

    private fun setupForOtherProfile() {
        toolbar.getRightButton().gone()
    }
}