package com.wishbox.feature.profile

import com.smedialink.common.events.ErrorEvent

/**
 * Created by Pak Evgeniy on 20/12/2018.
 */

data class ProfileState(
    val profile: ProfileUiModel? = null,
    val error: ErrorEvent? = null,
    val isLoading: Boolean = false
) {

    companion object {
        fun initial(): ProfileState = ProfileState()
        fun loading(): ProfileState = ProfileState(isLoading = true)
        fun error(error: ErrorEvent): ProfileState = ProfileState(error = error)
        fun data(profile: ProfileUiModel): ProfileState = ProfileState(profile = profile)
    }
}