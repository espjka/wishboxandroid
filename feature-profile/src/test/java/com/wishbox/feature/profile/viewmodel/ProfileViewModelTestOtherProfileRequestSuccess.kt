package com.wishbox.feature.profile.viewmodel

import com.nhaarman.mockitokotlin2.*
import com.wishbox.core.domain.entity.UserEntity
import com.wishbox.coretesting.testObserver
import com.wishbox.feature.profile.ProfileState
import io.reactivex.Single
import org.junit.Assert
import org.junit.Before
import org.junit.Test

class ProfileViewModelTestOtherProfileRequestSuccess : BaseProfileViewModelTest() {
    private lateinit var expected: List<ProfileState>

    @Before
    override fun setUp() {
        mocksBundle = ProfileViewModelMocksBundle(
            testUserId = 1,
            getOtherProfile = mock {
                onGeneric { invoke(any()) } doReturn Single.just(UserEntity(0, "", ""))
            }
        )
        expected = listOf(
            ProfileState.loading(),
            ProfileState.data(mocksBundle.testProfileUiModel)
        )

        super.setUp()
    }

    @Test
    fun test() {
        val testObserver = subject.getState().testObserver()
        subject.requestProfile()
        verify(mocksBundle.getOtherProfile, times(1)).invoke(any())
        Assert.assertEquals(testObserver.observedValues, expected)
    }
}