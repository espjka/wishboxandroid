package com.wishbox.feature.profile.viewmodel

import com.nhaarman.mockitokotlin2.*
import org.junit.Before
import org.junit.Test
import ru.terrakok.cicerone.android.support.SupportAppScreen

class ProfileViewModelTestMyProfileOther : BaseProfileViewModelTest() {

    private val testEditProfileScreen: SupportAppScreen = object : SupportAppScreen() {}
    private val testWishListScreen: SupportAppScreen = object : SupportAppScreen() {}

    @Before
    override fun setUp() {
        mocksBundle = ProfileViewModelMocksBundle(
            featureProvider = mock {
                on { getEditProfile() } doReturn testEditProfileScreen
            }
        )
        super.setUp()
    }

    @Test
    fun testHandleAction() {
        subject.handleAction()
        verify(mocksBundle.liveRouter, times(1)).navigateTo(argThat { equals(testEditProfileScreen) })
    }

    @Test
    fun testNavigateBack() {
        subject.navigateBack()
        verify(mocksBundle.liveRouter, times(1)).exit()
    }
}