package com.wishbox.feature.profile.viewmodel

import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.mock
import com.smedialink.common.error.ErrorHandler
import com.wishbox.core.di.screenproviders.EditProfileScreenProvider
import com.wishbox.core.di.screenproviders.FeatureProvider
import com.wishbox.core.domain.entity.ProfileEntity
import com.wishbox.core.domain.entity.UserEntity
import com.wishbox.feature.profile.ProfileUiModel
import com.wishbox.feature.profile.mapper.ProfileViewMapper
import io.reactivex.BackpressureStrategy
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Single
import io.reactivex.subjects.BehaviorSubject
import ru.terrakok.cicerone.Router

class ProfileViewModelMocksBundle(

    private val testInitProfileEntity: ProfileEntity = ProfileEntity(
        1,
        "myProfileName",
        "myProfileLastName"
    ),

    private val testProfileEntity: ProfileEntity = ProfileEntity(
        2,
        "myProfileName",
        "myProfileLastName"
    ),

    val testProfileUiModel: ProfileUiModel = ProfileUiModel(
        "testFullName"
    ),

    val testInitProfileUiModel: ProfileUiModel = ProfileUiModel(
        "initProfileFullName"
    ),
    val testUserId: Long? = null,
    val errorHandler: ErrorHandler = mock(),
    val liveRouter: Router = mock(),
    val featureProvider: FeatureProvider = mock(),
    val mapper: ProfileViewMapper = mock {
        on { map(com.nhaarman.mockitokotlin2.any<ProfileEntity>()) }
            .doReturn(testInitProfileUiModel, testProfileUiModel)
        on { map(com.nhaarman.mockitokotlin2.any<UserEntity>()) }
            .doReturn(testProfileUiModel)
    },

    private val myProfileSource: BehaviorSubject<ProfileEntity> = BehaviorSubject.createDefault(testInitProfileEntity),

    var getOtherProfile: (Long) -> Single<UserEntity> = mock(),
    observeMyProfileResult: Flowable<ProfileEntity> = Flowable.just(testProfileEntity),
    requestMyProfileResult: Completable = Completable.complete()

) {
    var requestMyProfile: () -> Completable = mock {
        onGeneric { invoke() } doReturn requestMyProfileResult.doOnComplete {
            myProfileSource.onNext(testProfileEntity)
        }
    }

    val observeMyProfile: () -> Flowable<ProfileEntity> = mock {
        onGeneric { invoke() } doReturn myProfileSource
            .toFlowable(BackpressureStrategy.BUFFER)
            .flatMap {
                return@flatMap if (it == testInitProfileEntity)
                    Flowable.just(testProfileEntity)
                else
                    observeMyProfileResult
            }
    }
}
