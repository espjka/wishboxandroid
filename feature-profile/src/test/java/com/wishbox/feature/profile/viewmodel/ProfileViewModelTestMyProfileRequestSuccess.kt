package com.wishbox.feature.profile.viewmodel

import com.nhaarman.mockitokotlin2.times
import com.nhaarman.mockitokotlin2.verify
import com.wishbox.coretesting.testObserver
import com.wishbox.feature.profile.ProfileState
import org.junit.Assert
import org.junit.Before
import org.junit.Test


class ProfileViewModelTestMyProfileRequestSuccess : BaseProfileViewModelTest() {
    private lateinit var expected: List<ProfileState>

    @Before
    override fun setUp() {
        mocksBundle = ProfileViewModelMocksBundle()
        expected = listOf(
            ProfileState.data(mocksBundle.testInitProfileUiModel),
            ProfileState.loading(),
            ProfileState.data(mocksBundle.testProfileUiModel)
        )
        super.setUp()
    }

    @Test
    fun getMyProfileSuccess() {
        val testObserver = subject.getState().testObserver()
        subject.requestProfile()
        verify(mocksBundle.requestMyProfile, times(1)).invoke()
        Assert.assertEquals(testObserver.observedValues, expected)
    }
}






