package com.wishbox.feature.profile.viewmodel

import com.nhaarman.mockitokotlin2.*
import com.smedialink.common.error.ErrorState
import com.smedialink.common.events.ErrorEvent
import com.wishbox.coretesting.testObserver
import com.wishbox.feature.profile.ProfileState
import io.reactivex.Flowable
import org.junit.Assert
import org.junit.Before
import org.junit.Test

class ProfileViewModelTestMyProfileObserveError : BaseProfileViewModelTest() {
    private lateinit var expected: List<ProfileState>
    private val exception: Exception = Exception()
    private val testErrorEvent = ErrorEvent("test error")

    @Before
    override fun setUp() {
        mocksBundle = ProfileViewModelMocksBundle(
            observeMyProfileResult = Flowable.error(exception),
            errorHandler = mock {
                on { handle(any()) } doReturn ErrorState(testErrorEvent)
            }
        )
        expected = listOf(
            ProfileState.data(mocksBundle.testInitProfileUiModel),
            ProfileState.loading(),
            ProfileState.error(testErrorEvent)
        )

        super.setUp()
    }

    @Test
    fun test() {
        val testObserver = subject.getState().testObserver()
        subject.requestProfile()
        verify(mocksBundle.errorHandler, times(1)).handle(argThat {
            this == exception
        })
        Assert.assertEquals(testObserver.observedValues, expected)
    }
}