package com.wishbox.feature.profile.viewmodel

import com.nhaarman.mockitokotlin2.*
import org.junit.Before
import org.junit.Test
import ru.terrakok.cicerone.android.support.SupportAppScreen


class ProfileViewModelTestOtherProfileOther : BaseProfileViewModelTest() {

    private val testFriendListScreen: SupportAppScreen = object : SupportAppScreen() {}
    private val testWishListScreen: SupportAppScreen = object : SupportAppScreen() {}

    @Before
    override fun setUp() {
        mocksBundle = ProfileViewModelMocksBundle(
            testUserId = 1,
            featureProvider = mock {
                on { getWishList(any()) } doReturn testWishListScreen
                on { getFriendList(any()) } doReturn testFriendListScreen
            }
        )
        super.setUp()
    }

    @Test
    fun testOpenWishList() {
        subject.openWishList()
        verify(mocksBundle.liveRouter, times(1)).navigateTo(argThat { this == testWishListScreen })
    }

    @Test
    fun testOpenFriendList() {
        subject.openFriendList()
        verify(mocksBundle.liveRouter, times(1)).navigateTo(argThat { this == testFriendListScreen })
    }
}