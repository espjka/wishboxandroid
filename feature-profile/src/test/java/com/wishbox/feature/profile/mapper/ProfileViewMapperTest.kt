package com.wishbox.feature.profile.mapper

import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.mock
import com.wishbox.core.domain.entity.*
import org.junit.Before
import org.junit.Test
import java.util.*

class ProfileViewMapperTest {

    private lateinit var subject: ProfileViewMapper
    private val testDate: Date = Calendar.getInstance().time
    private val testDateString: String = "test date String"
    private val userEntity = UserEntity(
        1,
        "testFirstName",
        "testLastName",
        "testPatronymic",
        AvatarEntity(
            "testSmallUrl",
            "testMediumUrl",
            "testBigUrl"
        ),
        9,
        Gender.MALE,
        true,
        Calendar.getInstance().time,
        ItemValue(1, "testCity"),
        ItemValue(1, "testHobby"),
        ItemValue(1, "testAlcohol"),
        ItemValue(1, "testFlower"),
        ItemValue(1, "testSweet"),
        10,
        11
    )

    private val profileEntity = ProfileEntity(
        2,
        "testFirstName",
        "testLastName",
        "testPatronymic",
        AvatarEntity(
            "testSmallUrl",
            "testMediumUrl",
            "testBigUrl"
        ),
        Gender.MALE,
        Calendar.getInstance().time,
        ItemValue(1, "testCity"),
        ItemValue(1, "testHobby"),
        ItemValue(1, "testAlcohol"),
        ItemValue(1, "testFlower"),
        ItemValue(1, "testSweet"),
        10,
        11
    )


    @Before
    fun setUp() {
        subject = ProfileViewMapper(
            mock {
                on { getString(any()) } doReturn "test string"
            },
            mock {
                on { stringToDate(any(), any()) } doReturn testDate
                on { dateToString(any(), any()) } doReturn testDateString
            }
        )
    }

    @Test
    fun mapFromUserEntity() {
        val profileUiModel = subject.map(userEntity)
        profileUiModel.run {
            assert(
                fullName == "${userEntity.firstName.orEmpty()} ${userEntity.lastName.orEmpty()} ${userEntity.patronymic.orEmpty()}"
            )
            assert(avatarUrl == userEntity.avatar!!.big)
            assert(city == userEntity.city!!.name)
            assert(birthday == testDateString)
            assert(friendAmount == userEntity.friendAmount)
            assert(wishAmount == userEntity.wishAmount)
            assert(hobby == userEntity.hobby!!.name)
            assert(flower == userEntity.flower!!.name)
            assert(alcohol == userEntity.alcohol!!.name)
            assert(sweet == userEntity.sweet!!.name)
        }
    }

    @Test
    fun mapFromProfileEntity() {
        val profileUiModel = subject.map(profileEntity)
        profileUiModel.run {
            assert(
                fullName == "${profileEntity.firstName.orEmpty()} ${profileEntity.lastName.orEmpty()} ${profileEntity.patronymic.orEmpty()}"
            )
            assert(avatarUrl == profileEntity.avatar!!.big)
            assert(city == profileEntity.city!!.name)
            assert(birthday == testDateString)
            assert(friendAmount == profileEntity.friendAmount)
            assert(wishAmount == profileEntity.wishAmount)
            assert(hobby == profileEntity.hobby!!.name)
            assert(flower == profileEntity.flower!!.name)
            assert(alcohol == profileEntity.alcohol!!.name)
            assert(sweet == profileEntity.sweet!!.name)
        }
    }
}