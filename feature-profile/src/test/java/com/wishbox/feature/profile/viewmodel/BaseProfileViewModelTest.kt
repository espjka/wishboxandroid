package com.wishbox.feature.profile.viewmodel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.wishbox.coretesting.RxSchedulesRule
import com.wishbox.feature.profile.ProfileViewModel
import org.junit.Before
import org.junit.Rule

abstract class BaseProfileViewModelTest {
    @get:Rule
    val schedulerRule = RxSchedulesRule()
    //
    @get:Rule
    val taskExecutorRule = InstantTaskExecutorRule()

    lateinit var mocksBundle: ProfileViewModelMocksBundle

    lateinit var subject: ProfileViewModel

    @Before
    open fun setUp() {
        subject = ProfileViewModel(
            mocksBundle.liveRouter,
            mocksBundle.errorHandler,
            mocksBundle.testUserId,
            mocksBundle.featureProvider,
            mocksBundle.mapper,
            mocksBundle.requestMyProfile,
            mocksBundle.observeMyProfile,
            mocksBundle.getOtherProfile
        )
    }
}