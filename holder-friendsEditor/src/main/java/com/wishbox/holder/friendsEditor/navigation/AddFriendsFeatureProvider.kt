package com.wishbox.holder.friendsEditor.navigation

import androidx.fragment.app.Fragment
import com.wishbox.core.di.screenproviders.FeatureProvider
import com.wishbox.core.di.screenproviders.MainScreenProvider
import com.wishbox.feature.addFriends.AddFriendsFragment
import com.wishbox.feature.friendList.FriendListFragment
import com.wishbox.feature.profile.ProfileFragment
import com.wishbox.feature.wishDetail.WishDetailFragment
import com.wishbox.feature.wishList.WishListFragment
import ru.terrakok.cicerone.android.support.SupportAppScreen
import javax.inject.Inject

/**
 * Created by Pak Evgeniy on 09.01.2019
 */

class AddFriendsFeatureProvider @Inject constructor(
    private val mainScreenProvider: MainScreenProvider
) : FeatureProvider {
    override fun getAddFriends(isAuthFlow: Boolean?): SupportAppScreen {
        return object : SupportAppScreen() {
            override fun getScreenKey(): String = AddFriendsFragment::class.java.name
            override fun getFragment(): Fragment = AddFriendsFragment.newInstance(isAuthFlow)
        }
    }

    override fun getProfile(id: Long?): SupportAppScreen {
        return object : SupportAppScreen() {
            override fun getScreenKey(): String = ProfileFragment::class.java.name
            override fun getFragment(): Fragment = ProfileFragment.newInstance(id)
        }
    }

    override fun getFriendList(userId: Long?): SupportAppScreen =
        object : SupportAppScreen() {
            override fun getScreenKey(): String = FriendListFragment::class.java.name
            override fun getFragment(): Fragment = FriendListFragment.newInstance(userId)
        }

    override fun getWishDetail(wishId: Long, userId: Long?): SupportAppScreen =
        object : SupportAppScreen() {
            override fun getScreenKey(): String = WishDetailFragment::class.java.name
            override fun getFragment(): Fragment = WishDetailFragment.newInstance(wishId, userId)
        }

    override fun getWishList(userId: Long?): SupportAppScreen =
        object : SupportAppScreen() {
            override fun getScreenKey(): String = WishListFragment::class.java.name
            override fun getFragment(): Fragment = WishListFragment.newInstance(userId)
        }

    override fun getMain(): SupportAppScreen {
        return mainScreenProvider.getMainScreen()
    }
}