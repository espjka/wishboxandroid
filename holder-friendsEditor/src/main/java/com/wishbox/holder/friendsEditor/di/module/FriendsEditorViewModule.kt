package com.wishbox.holder.friendsEditor.di.module

import com.wishbox.core.di.screenproviders.FeatureProvider
import com.wishbox.core.presentation.navigation.NavigationBackHelper
import com.wishbox.holder.friendsEditor.FriendsEditorActivity
import com.wishbox.holder.friendsEditor.R
import com.wishbox.holder.friendsEditor.navigation.AddFriendsFeatureProvider
import dagger.Binds
import dagger.Module
import dagger.Provides
import ru.terrakok.cicerone.Navigator
import ru.terrakok.cicerone.android.support.SupportAppNavigator

@Module
abstract class FriendsEditorViewModule {

    @Binds
    abstract fun provideFeatureProvider(impl: AddFriendsFeatureProvider): FeatureProvider

    @Module
    companion object {
        @Provides
        @JvmStatic
        fun provideNavigatorBackHelper(activity: FriendsEditorActivity): NavigationBackHelper =
            NavigationBackHelper(activity)

        @Provides
        @JvmStatic
        fun provideNavigator(activity: FriendsEditorActivity): Navigator =
            SupportAppNavigator(activity, R.id.container)
    }
}
