package com.wishbox.holder.friendsEditor.di

import com.wishbox.core.di.api.FeatureProviderApi
import com.wishbox.core.di.api.InjectorApi
import com.wishbox.core.di.api.RouterApi
import com.wishbox.core.di.module.GlobalRouterModule
import com.wishbox.core.di.provider.InjectorApiProvider
import com.wishbox.core.di.scope.PerActivity
import com.wishbox.core.di.util.lookupApiProvider
import com.wishbox.holder.friendsEditor.FriendsEditorActivity
import com.wishbox.holder.friendsEditor.di.module.FriendsEditorViewModule
import dagger.BindsInstance
import dagger.Component

@PerActivity
@Component(
    dependencies = [
        InjectorApi::class
    ],
    modules = [
        GlobalRouterModule::class,
        FriendsEditorViewModule::class
    ]
)
interface FriendsEditorViewComponent : FeatureProviderApi, RouterApi {

    fun inject(startActivity: FriendsEditorActivity)

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun bind(friendsEditorActivity: FriendsEditorActivity): Builder

        fun api(injectorApi: InjectorApi): Builder

        fun build(): FriendsEditorViewComponent
    }

    object Injector {

        fun init(activity: FriendsEditorActivity): FriendsEditorViewComponent {
            val injectorProvider = activity.lookupApiProvider<InjectorApiProvider>()

            return DaggerFriendsEditorViewComponent.builder()
                .api(injectorProvider.injectorApi)
                .bind(activity)
                .build()
        }
    }
}