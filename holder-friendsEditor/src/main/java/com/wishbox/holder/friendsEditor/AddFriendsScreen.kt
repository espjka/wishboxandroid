package com.wishbox.holder.friendsEditor

import androidx.fragment.app.Fragment
import com.wishbox.feature.addFriends.AddFriendsFragment
import ru.terrakok.cicerone.android.support.SupportAppScreen

class AddFriendsScreen(private val isAuthFlow: Boolean?) : SupportAppScreen() {
    override fun getFragment(): Fragment {
        return AddFriendsFragment.newInstance(isAuthFlow)
    }
}