package com.wishbox.holder.friendsEditor

import com.wishbox.core.di.api.FeatureProviderApi
import com.wishbox.core.di.api.RouterApi
import com.wishbox.core.di.provider.FeatureProviderApiProvider
import com.wishbox.core.di.provider.RouterApiProvider
import com.wishbox.core.presentation.base.BaseActivity
import com.wishbox.core.presentation.navigation.NavigationBackHelper
import com.wishbox.holder.friendsEditor.di.FriendsEditorViewComponent
import ru.terrakok.cicerone.commands.Replace
import javax.inject.Inject

class FriendsEditorActivity : BaseActivity(), FeatureProviderApiProvider, RouterApiProvider {

    companion object {
        const val BUNDLE_AUTH_FLOW = "BUNDLE_AUTH_FLOW"
    }

    private val component by lazy { FriendsEditorViewComponent.Injector.init(this) }

    override val featureProviderApi: FeatureProviderApi
        get() = component

    override val routerApi: RouterApi
        get() = component

    @Inject
    lateinit var navigationBackHelper: NavigationBackHelper

    override fun layoutRes() = R.layout.activity_friends_editor

    override fun viewCreated() {
        if (supportFragmentManager.findFragmentById(R.id.container) == null) {
            navigator.applyCommands(arrayOf(Replace(AddFriendsScreen(intent.extras?.getBoolean(BUNDLE_AUTH_FLOW)))))
        }
    }

    override fun injectDependencies() {
        component.inject(this)
    }

    override fun onBackPressed() {
        if (navigationBackHelper.onBackPressedHandled().not()) super.onBackPressed()
    }
}
