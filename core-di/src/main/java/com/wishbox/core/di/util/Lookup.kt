package com.wishbox.core.di.util

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity

inline fun <reified T> Fragment.lookupApiProvider(): T {
    var parentFragment: Fragment? = this
    while (parentFragment != null) {
        if (parentFragment is T) {
            return parentFragment
        }

        parentFragment = parentFragment.parentFragment
    }

    val activity = this.activity
    if (activity is T) {
        return activity
    }

    val application = activity?.application
    if (application != null && application is T) {
        return application
    } else {
        throw IllegalArgumentException(
            String.format("No ${T::class.java.simpleName} was found for %s", this.javaClass.canonicalName)
        )
    }
}

inline fun <reified T> FragmentActivity.lookupApiProvider(): T {
    val application = this.application
    if (application !is T) {
        throw RuntimeException(
            String.format(
                "%s does not implement %s",
                application.javaClass.canonicalName,
                T::class.java.canonicalName
            )
        )
    }

    return application
}