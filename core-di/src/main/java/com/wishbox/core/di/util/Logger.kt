package com.wishbox.core.di.util

/**
 * Created by Oleg Sheliakin on 18.12.2018.
 * Contact me by email - olegsheliakin@gmail.com
 */
interface Logger {
    fun error(error: Throwable)
}