package com.wishbox.core.di.api

import android.content.Context
import android.content.SharedPreferences
import com.wishbox.core.di.util.Logger

/**
 * Created by Pak Evgeniy on 30/11/2018.
 */

interface AppApi {
    fun provideContext(): Context
    fun provideSharedPrefs(): SharedPreferences
    fun provideLogger() : Logger
}