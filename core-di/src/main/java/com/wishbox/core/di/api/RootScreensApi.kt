package com.wishbox.core.di.api

import com.wishbox.core.di.screenproviders.*

interface RootScreensApi {
    fun provideEntranceScreenProvider(): EntranceScreenProvider
    fun provideMainScreenProvider(): MainScreenProvider
    fun provideWishEditorScreenProvider(): WishEditorProvider
    fun provideFriendsEditorScreenProvider(): FriendsEditorProvider
    fun provideEditProfileScreenProvider(): EditProfileScreenProvider
    fun provideChangePasswordScreenProvider(): ChangePasswordScreenProvider
}