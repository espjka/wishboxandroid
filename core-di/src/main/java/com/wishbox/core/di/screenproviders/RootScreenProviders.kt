package com.wishbox.core.di.screenproviders

import ru.terrakok.cicerone.android.support.SupportAppScreen

interface MainScreenProvider {
    fun getMainScreen(): SupportAppScreen
}

interface EntranceScreenProvider {
    fun getEntranceScreen(): SupportAppScreen
}

interface WishEditorProvider {
    fun getWishEditorScreen(wishId: Long? = null): SupportAppScreen
}

interface FriendsEditorProvider {
    fun getFriendsEditorScreen(authFlow: Boolean? = null): SupportAppScreen
}

interface EditProfileScreenProvider {
    fun getEditProfileScreen(authFlow: Boolean? = null): SupportAppScreen
}

interface ChangePasswordScreenProvider {
    fun getChangePasswordScreen(): SupportAppScreen
}