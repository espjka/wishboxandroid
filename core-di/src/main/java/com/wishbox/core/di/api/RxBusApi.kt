package com.wishbox.core.di.api

import com.wishbox.core.di.rxbusprovider.RxBusProvider

interface RxBusApi {
    fun rxBusProvider(): RxBusProvider
}