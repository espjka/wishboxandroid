package com.wishbox.core.di.provider

import com.wishbox.core.di.api.RxBusApi

interface RxBusApiProvider {
    val rxBusApi: RxBusApi
}

