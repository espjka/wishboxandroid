package com.wishbox.core.di.provider

import com.wishbox.core.di.api.InjectorApi

interface InjectorApiProvider {
    val injectorApi: InjectorApi
}
