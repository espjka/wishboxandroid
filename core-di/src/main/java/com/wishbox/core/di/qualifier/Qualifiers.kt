package com.wishbox.core.di.qualifier

import javax.inject.Qualifier

/**
 * Created by Pak Evgeniy on 01.12.2018
 */

@Qualifier
annotation class Internal

@Qualifier
annotation class Flow

@Qualifier
annotation class UserId

@Qualifier
annotation class WishId