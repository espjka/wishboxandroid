package com.wishbox.core.di.scope

import javax.inject.Scope

/**
 * Created by olegshelyakin on 20.08.17.
 */
@Scope
annotation class PerActivity