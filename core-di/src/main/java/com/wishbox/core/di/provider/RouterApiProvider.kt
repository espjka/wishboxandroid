package com.wishbox.core.di.provider

import com.wishbox.core.di.api.RouterApi

/**
 * Created by Pak Evgeniy on 01.12.2018
 */

interface RouterApiProvider {
    val routerApi: RouterApi
}