package com.wishbox.core.di.screenproviders

import ru.terrakok.cicerone.android.support.SupportAppScreen

/**
 * Created by Pak Evgeniy on 12.12.2018
 */

interface FeatureProvider {
    fun getMain(): SupportAppScreen = NOT_SUPPORTED_FEATURE
    fun getEntrance(): SupportAppScreen = NOT_SUPPORTED_FEATURE
    fun getAddFriends(isAuthFlow: Boolean? = null): SupportAppScreen = NOT_SUPPORTED_FEATURE
    fun getFriendList(userId: Long? = null): SupportAppScreen = NOT_SUPPORTED_FEATURE
    fun getManagePictures(wishId: Long? = null): SupportAppScreen = NOT_SUPPORTED_FEATURE
    fun getWishDetail(wishId: Long, userId: Long? = null): SupportAppScreen = NOT_SUPPORTED_FEATURE
    fun getWishEditInfo(wishId: Long? = null): SupportAppScreen = NOT_SUPPORTED_FEATURE
    fun getWishList(userId: Long? = null): SupportAppScreen = NOT_SUPPORTED_FEATURE
    fun getCalendar(): SupportAppScreen = NOT_SUPPORTED_FEATURE
    fun getMenu(): SupportAppScreen = NOT_SUPPORTED_FEATURE
    fun getProfile(id: Long? = null): SupportAppScreen = NOT_SUPPORTED_FEATURE
    fun getSelectionScreen(selection: Selections): SupportAppScreen = NOT_SUPPORTED_FEATURE
    fun getEditProfile(): SupportAppScreen = NOT_SUPPORTED_FEATURE
    fun getChangePassword(): SupportAppScreen = NOT_SUPPORTED_FEATURE
    fun getFriendsEditor(): SupportAppScreen = NOT_SUPPORTED_FEATURE
}

val NOT_SUPPORTED_FEATURE: SupportAppScreen = object : SupportAppScreen() {}

enum class Selections {
    FLOWERS,
    CITY,
    SWEETIES,
    ALCOHOL,
    HOBBIES;
    companion object {
        fun resolve(id: Int): Selections = values()[id]
    }
}