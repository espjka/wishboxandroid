package com.wishbox.core.di.module

import dagger.Module
import dagger.Provides
import ru.terrakok.cicerone.Cicerone
import ru.terrakok.cicerone.Router

/**
 * Created by Pak Evgeniy on 09.01.2019
 */

@Module
class GlobalRouterModule {
    @Provides
    fun provideGlobalRouter(cicerone: Cicerone<Router>): Router = cicerone.router
}