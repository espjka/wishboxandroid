package com.wishbox.core.di.api

import com.wishbox.core.domain.cleaner.DataCleaner
import com.wishbox.core.domain.parser.DateParser
import com.wishbox.core.domain.repository.*

interface RepositoryApi {
    fun provideAuthRepository(): AuthRepository
    fun provideWishRepository(): WishRepository
    fun provideMetaRepository(): MetaInfoRepository
    fun provideUserRepository(): UserRepository
    fun provideUserRegistrationRepository(): UserRegistrationRepository
    fun providePinRepository(): PinRepository
    fun provideUserSessionRepository(): UserSessionRepository
    fun provideRecoveryPasswordRepository(): ResetPasswordRepository
    fun provideFriendshipRepository(): FriendshipRepository
    fun provideProfileRepository(): ProfileRepository
    fun providePictureRepository(): PictureRepository
    fun provideChangePasswordRepository(): ChangePasswordRepository

    fun provideDataCleaner(): DataCleaner
    fun provideDateParser(): DateParser
}