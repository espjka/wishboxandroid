package com.wishbox.core.di.api

interface InjectorApi : AppApi, NavigationApi, RepositoryApi, RootScreensApi