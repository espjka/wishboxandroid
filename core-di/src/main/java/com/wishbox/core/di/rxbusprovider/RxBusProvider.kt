package com.wishbox.core.di.rxbusprovider

import com.smedialink.common.rxbus.RxEventBus
import com.smedialink.common.rxbus.rxEvent.RxEvent
import com.wishbox.core.domain.entity.ItemValue
import com.wishbox.core.domain.entity.ProfileItemValueType

interface RxBusProvider {
    fun provideProfileSelectRxBus(): RxEventBus<ItemValueSelectedEvent>

    class ItemValueSelectedEvent constructor(
        val type: ProfileItemValueType? = null,
        val itemValue: ItemValue? = null
    ) : RxEvent
}

