package com.wishbox.core.di.api

import ru.terrakok.cicerone.Cicerone
import ru.terrakok.cicerone.NavigatorHolder
import ru.terrakok.cicerone.Router

/**
 * Created by Pak Evgeniy on 26/11/2018.
 */

interface NavigationApi {
    fun provideCicerone(): Cicerone<Router>
    fun provideNavigatorHolder(): NavigatorHolder
}