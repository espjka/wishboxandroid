package com.wishbox.core.di.api

import ru.terrakok.cicerone.Router

/**
 * Created by Pak Evgeniy on 01.12.2018
 */

interface RouterApi {
    fun provideRouter(): Router
}
