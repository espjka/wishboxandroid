package com.wishbox.core.di.api

import com.wishbox.core.di.screenproviders.FeatureProvider

/**
 * Created by Pak Evgeniy on 12.12.2018
 */

interface FeatureProviderApi {
    fun provideFeatureProvider(): FeatureProvider
}