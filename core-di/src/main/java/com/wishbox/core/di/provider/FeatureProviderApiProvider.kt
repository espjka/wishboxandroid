package com.wishbox.core.di.provider

import com.wishbox.core.di.api.FeatureProviderApi

/**
 * Created by Pak Evgeniy on 12.12.2018
 */

interface FeatureProviderApiProvider {
    val featureProviderApi: FeatureProviderApi
}