package com.wishbox.feature.auth.base.domain.validation

import com.wishbox.core.domain.validator.Validator
import com.wishbox.feature.auth.base.domain.exception.ValidationException
import io.reactivex.Completable
import java.util.regex.Pattern
import javax.inject.Inject

class PasswordValidator @Inject constructor() : Validator {

    companion object {
        const val REGEXP_PASSWORD =
            "^(?=.*[a-zA-Z!@#\$%^&*_+\\/=\\-?.,])[a-zA-Za-zA-Z!@#\$%^&*_+\\/=\\-?.,\\d]{6,}\$"
    }

    override fun validate(value: String): Completable =
        if (Pattern.matches(REGEXP_PASSWORD, value)) Completable.complete()
        else Completable.error(ValidationException.InvalidPasswordException)

}