package com.wishbox.feature.auth.flow

import android.os.Bundle
import android.view.View
import androidx.core.view.isInvisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import com.smedialink.common.base.BaseFragment
import com.wishbox.core.presentation.navigation.BackButtonListener
import com.wishbox.feature.auth.R
import com.wishbox.feature.auth.auth.AuthFragment
import com.wishbox.feature.auth.flow.api.AuthFlowApi
import com.wishbox.feature.auth.flow.api.AuthFlowApiProvider
import com.wishbox.feature.auth.flow.di.AuthFlowComponent
import kotlinx.android.synthetic.main.auth_flow.*

class AuthFlowFragment : BaseFragment(), AuthFlowApiProvider {

    override val flowApi: AuthFlowApi by lazy {
        return@lazy component
    }

    private lateinit var component: AuthFlowApi

    override val layoutRes = R.layout.auth_flow

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (childFragmentManager.findFragmentById(R.id.container) == null) {
            childFragmentManager.beginTransaction().replace(R.id.container, AuthFragment()).commit()
        }

        childFragmentManager.registerFragmentLifecycleCallbacks(
            object : FragmentManager.FragmentLifecycleCallbacks() {
                override fun onFragmentResumed(fm: FragmentManager, fragment: Fragment) {
                    super.onFragmentResumed(fm, fragment)
                    toolbar.getLeftButton().isInvisible = fragment is AuthFragment
                }
            }, false
        )
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        toolbar.getLeftButton().setOnClickListener {
            (childFragmentManager.fragments[childFragmentManager.fragments.size - 1] as? BackButtonListener)?.onBackPressed()
        }
    }

    override fun inject() {
        component = AuthFlowComponent.inject(this)
    }

}
