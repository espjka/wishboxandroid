package com.wishbox.feature.auth.flow.di

import com.wishbox.core.di.qualifier.Flow
import com.wishbox.feature.auth.flow.AuthFlowFragment
import com.wishbox.feature.auth.flow.api.AuthFlowApi
import com.wishbox.feature.auth.flow.di.module.AuthFlowModule
import dagger.BindsInstance
import dagger.Component

@Flow
@Component(
    modules = [
        AuthFlowModule::class
    ]
)
interface AuthFlowComponent : AuthFlowApi {

    fun inject(target: AuthFlowFragment)

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun bind(target: AuthFlowFragment): Builder

        fun build(): AuthFlowComponent
    }

    companion object {

        fun inject(fragment: AuthFlowFragment): AuthFlowApi {

            val component =
                DaggerAuthFlowComponent.builder()
                    .bind(fragment)
                    .build()

            component.inject(fragment)

            return component
        }
    }
}