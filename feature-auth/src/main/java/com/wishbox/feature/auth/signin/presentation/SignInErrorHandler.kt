package com.wishbox.feature.auth.signin.presentation

import com.smedialink.common.events.ErrorEvent
import com.wishbox.core.di.util.Logger
import com.wishbox.core.presentation.resources.ErrorTextProvider
import com.wishbox.core.presentation.resources.ResourceProvider
import com.wishbox.feature.auth.R
import com.wishbox.feature.auth.base.domain.exception.ValidationException
import retrofit2.HttpException
import javax.inject.Inject

class SignInErrorHandler @Inject constructor(
    private val logger: Logger,
    private val resourceProvider: ResourceProvider,
    private val errorTextProvider: ErrorTextProvider
) {

    fun handle(prevErrorState: SignInErrorState?, throwable: Throwable): SignInErrorState {
        logger.error(throwable)
        val prevState = prevErrorState ?: SignInErrorState(null, null, null)
        return if (throwable is HttpException && throwable.code() == 404) { //todo create consts for all http error codes
            prevState.copy(
                event = ErrorEvent(resourceProvider.getString(R.string.sign_in_wrong_data_error)),
                loginErrorText = "",
                passwordErrorText = ""
            )
        } else if (throwable is ValidationException.InvalidLoginException) {
            prevState.copy(loginErrorText = resourceProvider.getString(R.string.auth_invalid_data_format_error))
        } else if (throwable is ValidationException.InvalidPasswordException) {
            prevState.copy(passwordErrorText = resourceProvider.getString(R.string.auth_invalid_data_format_error))
        } else {
            val errorEvent = errorTextProvider.provideErrorEvent(throwable)
            prevState.copy(event = errorEvent)
        }
    }
}
