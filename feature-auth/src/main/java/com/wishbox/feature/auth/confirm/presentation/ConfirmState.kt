package com.wishbox.feature.auth.confirm.presentation

import com.smedialink.common.error.ErrorState
import com.smedialink.common.events.ErrorEvent

data class ConfirmState(
    val isLoading: Boolean,
    val errorState: ConfirmErrorState?
) {
    companion object {
        fun initial() = ConfirmState(false, null)
    }
}

data class ConfirmErrorState(
    override val event: ErrorEvent?,
    val loginErrorText: String?
) : ErrorState(event)

