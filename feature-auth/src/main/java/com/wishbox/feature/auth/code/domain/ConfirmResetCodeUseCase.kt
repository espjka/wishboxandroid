package com.wishbox.feature.auth.code.domain

import com.wishbox.core.domain.repository.ResetPasswordRepository
import com.wishbox.feature.auth.base.domain.validation.LoginValidator
import com.wishbox.feature.auth.base.domain.validation.PinCodeValidator
import io.reactivex.Completable
import io.reactivex.Single
import javax.inject.Inject

/**
 * Created by Oleg Sheliakin on 18.12.2018.
 * Contact me by email - olegsheliakin@gmail.com
 */
class ConfirmResetCodeUseCase @Inject constructor(
    private val resetPasswordRepository: ResetPasswordRepository,
    private val pinCodeValidator: PinCodeValidator,
    private val loginValidator: LoginValidator
) {

    operator fun invoke(userName: String, code: String) : Single<String> {
        return loginValidator.validate(userName)
            .andThen(pinCodeValidator.validate(code))
            .andThen(resetPasswordRepository.confirmCode(userName, code))
    }

}