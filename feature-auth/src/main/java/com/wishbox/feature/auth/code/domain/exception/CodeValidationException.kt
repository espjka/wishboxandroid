package com.wishbox.feature.auth.code.domain.exception

import java.lang.Exception

class CodeValidationException : Exception()