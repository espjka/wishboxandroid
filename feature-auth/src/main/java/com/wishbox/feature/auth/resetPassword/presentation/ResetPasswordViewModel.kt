package com.wishbox.feature.auth.resetPassword.presentation

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.smedialink.common.base.BaseViewModel
import com.smedialink.common.delegates.PropertyChangedDelegate
import com.smedialink.common.delegates.rx.disposableDelegate
import com.smedialink.common.ext.applySchedulers
import com.smedialink.common.ext.reduceState
import com.wishbox.core.presentation.navigation.HasNavigation
import com.wishbox.core.presentation.navigation.LiveRouter
import com.wishbox.feature.auth.navigation.AuthScreensProvider
import com.wishbox.feature.auth.resetPassword.domain.ResetPasswordUseCase
import io.reactivex.disposables.Disposable
import io.reactivex.rxkotlin.subscribeBy
import kotlin.properties.Delegates

class ResetPasswordViewModel(
    private val liveRouter: LiveRouter,
    private val recoveryPasswordErrorHandler: ResetPasswordErrorHandler,
    private val authScreensProvider: AuthScreensProvider,
    private val resetPasswordUseCase: ResetPasswordUseCase
) : BaseViewModel(), HasNavigation by liveRouter {

    private val internalState: MutableLiveData<RecoveryPasswordState> by lazy {
        return@lazy MutableLiveData<RecoveryPasswordState>()
    }

    private var disposable: Disposable? by disposableDelegate()

    var token: String by Delegates.notNull()

    var password: String? by PropertyChangedDelegate("") { s ->
        internalState.reduceState { it.copy(errorState = it.errorState?.copy(passwordTextError = null)) }
    }

    var confirmationPassword: String? by PropertyChangedDelegate("") { s ->
        internalState.reduceState { it.copy(errorState = it.errorState?.copy(confirmationPasswordTextError = null)) }
    }

    val state: LiveData<RecoveryPasswordState> = internalState

    init {
        internalState.value = RecoveryPasswordState.initial()
    }

    fun changePassword() {
        val params = ResetPasswordUseCase.Params(
            token = token,
            password = password ?: "",
            confirmationPassword = confirmationPassword ?: ""
        )

        internalState.reduceState {
            it.copy(errorState = null)
        }

        disposable = resetPasswordUseCase.invoke(params)
            .applySchedulers()
            .doOnSubscribe {
                internalState.reduceState {
                    it.copy(isLoading = true)
                }
            }
            .subscribeBy(
                onComplete = {
                    internalState.value = RecoveryPasswordState.initial()
                    liveRouter.backTo(authScreensProvider.getAuthScreen())
                },
                onError = { throwable ->
                    internalState.reduceState {
                        val errorState = recoveryPasswordErrorHandler.handle(it.errorState, throwable)
                        it.copy(isLoading = false, errorState = errorState)
                    }
                }
            )
    }

    fun navigateBack() {
        internalState.value = RecoveryPasswordState.initial()
        liveRouter.exit()
    }
}
