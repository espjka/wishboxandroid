package com.wishbox.feature.auth.signin.di.module

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.wishbox.core.di.mapkeys.ViewModelKey
import com.wishbox.feature.auth.base.domain.usecase.CheckAuthUseCase
import com.wishbox.feature.auth.base.domain.usecase.CheckConfirmationUseCase
import com.wishbox.core.presentation.navigation.LiveRouter
import com.wishbox.feature.auth.code.domain.SendPinCodeUseCase
import com.wishbox.feature.auth.navigation.AuthScreensProvider
import com.wishbox.feature.auth.signin.domain.LoginUseCase
import com.wishbox.feature.auth.signin.domain.SignInUseCasesFacade
import com.wishbox.feature.auth.signin.presentation.SignInErrorHandler
import com.wishbox.feature.auth.signin.presentation.SignInFragment
import com.wishbox.feature.auth.signin.presentation.SignInViewModel
import dagger.Module
import dagger.Provides
import dagger.multibindings.IntoMap

@Module
abstract class SignInModule {

    @Module
    companion object {
        @Provides
        @IntoMap
        @JvmStatic
        @ViewModelKey(SignInViewModel::class)
        fun provideViewModel(
            router: LiveRouter,
            authScreensProvider: AuthScreensProvider,
            errorHandler: SignInErrorHandler,
            signInUseCasesFacade: SignInUseCasesFacade
        ): ViewModel = SignInViewModel(
            router,
            authScreensProvider,
            signInUseCasesFacade,
            errorHandler
        )

        @Provides
        @JvmStatic
        fun provideSignInViewModel(
            fragment: SignInFragment,
            factory: ViewModelProvider.Factory
        ): SignInViewModel = ViewModelProviders.of(fragment, factory).get(
            SignInViewModel::class.java
        )
    }

}
