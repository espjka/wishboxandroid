package com.wishbox.feature.auth.auth

import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import com.wishbox.feature.auth.R
import com.wishbox.feature.auth.base.util.CanSkipAnimation
import com.wishbox.feature.auth.signin.presentation.SignInFragment
import com.wishbox.feature.auth.signup.presentation.SignUpFragment

/**
 * Created by Oleg Sheliakin on 11.12.2018.
 * Contact me by email - olegsheliakin@gmail.com
 */

class TabsSelector constructor(private val fragmentManager: FragmentManager) : LifecycleObserver {

    fun initialSelect() {
        val fragment = fragmentManager.findFragmentById(R.id.container)
        if (fragment == null) {
            fragmentManager.beginTransaction()
                .replace(R.id.container, SignInFragment())
                .commit()
        }
    }

    fun select(position: Int) {
        fragmentManager.beginTransaction().apply {
            when (position) {
                0 -> {
                    setCustomAnimations(
                        R.anim.slide_left_in, R.anim.slide_right_out
                    )
                    replace(R.id.container, SignInFragment())
                }
                1 -> {
                    setCustomAnimations(
                        R.anim.slide_right_in, R.anim.slide_left_out
                    )
                    replace(R.id.container, SignUpFragment())
                }
                else -> {
                }
            }

            commit()
        }
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    fun enableAnim() {
        fragmentManager.fragments.filterIsInstance(CanSkipAnimation::class.java).forEach {
            it.skipAnimation = false
        }
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    fun disableAnim() {
        fragmentManager.fragments.filterIsInstance(CanSkipAnimation::class.java).forEach {
            it.skipAnimation = true
        }
    }

}