package com.wishbox.feature.auth.flow.api

import ru.terrakok.cicerone.Navigator

interface HasFlowNavigator {
    val flowNavigator: Navigator
}