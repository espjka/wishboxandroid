package com.wishbox.feature.auth.signup.presentation

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.smedialink.common.base.BaseViewModel
import com.smedialink.common.delegates.rx.disposableDelegate
import com.smedialink.common.ext.applySchedulers
import com.smedialink.common.ext.reduceState
import com.wishbox.core.presentation.navigation.HasNavigation
import com.wishbox.core.presentation.navigation.LiveRouter
import com.smedialink.common.delegates.PropertyChangedDelegate
import com.wishbox.feature.auth.navigation.AuthScreensProvider
import com.wishbox.feature.auth.signup.domain.RegisterUseCase
import io.reactivex.disposables.Disposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import javax.inject.Inject

class SignUpViewModel @Inject constructor(
    private val router: LiveRouter,
    private val errorHandler: SignUpErrorHandler,
    private val authChildScreensProvider: AuthScreensProvider,
    private val registerUseCase: RegisterUseCase
) : BaseViewModel(), HasNavigation by router {

    private var disposable: Disposable? by disposableDelegate()

    private val internalState: MutableLiveData<SignUpState> = MutableLiveData()

    val state: LiveData<SignUpState> = internalState

    var login: String? by PropertyChangedDelegate("") {
        internalState.reduceState {
            it.copy(errorState = it.errorState?.copy(loginErrorText = null))
        }
    }

    var password: String? by PropertyChangedDelegate("") {
        internalState.reduceState {
            it.copy(errorState = it.errorState?.copy(passwordErrorText = null))
        }
    }

    var confirmPassword: String? by PropertyChangedDelegate("") {
        internalState.reduceState {
            it.copy(errorState = it.errorState?.copy(confirmPasswordErrorText = null))
        }
    }

    init {
        internalState.value = SignUpState.initial()
    }

    fun register() {
        val param = RegisterUseCase.Param(
            login = login ?: "",
            password = password ?: "",
            confirmationPassword = confirmPassword ?: ""
        )

        internalState.reduceState {
            it.copy(errorState = null)
        }

        disposable = registerUseCase.invoke(param)
            .applySchedulers()
            .doOnSubscribe {
                internalState.reduceState {
                    it.copy(isLoading = true)
                }
            }
            .subscribeBy(
                onSuccess = {
                    internalState.value = SignUpState.initial()
                    router.navigateTo(authChildScreensProvider.getPinScreen(it))
                },
                onError = { throwable ->
                    internalState.reduceState {
                        val error = errorHandler.handle(it.errorState, throwable)
                        it.copy(isLoading = false, errorState = error)
                    }
                }
            ).addTo(disposables)
    }

}
