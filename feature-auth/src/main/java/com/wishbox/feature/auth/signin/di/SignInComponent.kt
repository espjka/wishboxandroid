package com.wishbox.feature.auth.signin.di

import com.wishbox.core.di.api.InjectorApi
import com.wishbox.core.di.module.ViewModelModule
import com.wishbox.core.di.provider.InjectorApiProvider
import com.wishbox.core.di.scope.PerFragment
import com.wishbox.core.di.util.lookupApiProvider
import com.wishbox.feature.auth.flow.api.AuthFlowApi
import com.wishbox.feature.auth.flow.api.AuthFlowApiProvider
import com.wishbox.feature.auth.signin.di.module.SignInModule
import com.wishbox.feature.auth.signin.presentation.SignInFragment
import dagger.BindsInstance
import dagger.Component

@PerFragment
@Component(
    dependencies = [
        InjectorApi::class,
        AuthFlowApi::class],
    modules = [
        SignInModule::class,
        ViewModelModule::class
    ]
)
interface SignInComponent {

    fun inject(target: SignInFragment)

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun bind(target: SignInFragment): Builder

        fun api(authFlowApi: AuthFlowApi): Builder

        fun api(injectorApi: InjectorApi): Builder

        fun build(): SignInComponent
    }

    companion object {

        fun inject(fragment: SignInFragment) {
            val injectorApiProvider: InjectorApiProvider = fragment.lookupApiProvider()
            val authFlowApiProvider: AuthFlowApiProvider = fragment.lookupApiProvider()

            DaggerSignInComponent.builder()
                .api(injectorApiProvider.injectorApi)
                .api(authFlowApiProvider.flowApi)
                .bind(fragment)
                .build()
                .inject(fragment)
        }
    }
}