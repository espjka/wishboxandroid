package com.wishbox.feature.auth.auth

import android.os.Bundle
import android.view.View
import com.smedialink.common.base.BaseFragment
import com.wishbox.feature.auth.R
import kotlinx.android.synthetic.main.fragment_auth.*

class AuthFragment : BaseFragment() {

    override val layoutRes: Int = R.layout.fragment_auth

    private val tabsSelector: TabsSelector by lazy {
        return@lazy TabsSelector(childFragmentManager)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        tabsSelector.initialSelect()
        tabs.itemSelected = tabsSelector::select

        lifecycle.addObserver(tabsSelector)
    }

    override fun inject() {

    }

}