package com.wishbox.feature.auth.signin.domain

import com.wishbox.feature.auth.base.domain.usecase.CheckAuthUseCase
import com.wishbox.feature.auth.base.domain.usecase.CheckConfirmationUseCase
import com.wishbox.feature.auth.base.domain.usecase.CheckProfileUseCase
import com.wishbox.feature.auth.code.domain.SendPinCodeUseCase
import io.reactivex.Completable
import javax.inject.Inject


class SignInUseCasesFacade @Inject constructor(
    private val loginUseCase: LoginUseCase,
    private val sendCodeUseCase: SendPinCodeUseCase,
    private val checkConfirmationUseCase: CheckConfirmationUseCase,
    private val checkAuth: CheckAuthUseCase,
    private val checkProfileUseCase: CheckProfileUseCase
) {

    fun checkAuth(): Completable {
        return checkAuth.invoke()
            .andThen(checkConfirmation())
            .andThen(checkProfile())
    }

    fun login(params: LoginUseCase.Params): Completable {
        return loginUseCase.invoke(params)
    }

    fun checkConfirmation(): Completable {
        return checkConfirmationUseCase.invoke()
    }

    fun checkProfile(): Completable = checkProfileUseCase.invoke()

    fun sendPinCode(userId: Long): Completable {
        return sendCodeUseCase.invoke(userId)
    }
}
