package com.wishbox.feature.auth.confirm.presentation

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.smedialink.common.base.BaseViewModel
import com.smedialink.common.delegates.PropertyChangedDelegate
import com.smedialink.common.delegates.rx.disposableDelegate
import com.smedialink.common.ext.applySchedulers
import com.smedialink.common.ext.reduceState
import com.wishbox.core.presentation.navigation.HasNavigation
import com.wishbox.core.presentation.navigation.LiveRouter
import com.wishbox.feature.auth.confirm.domain.SendResetCodeUseCase
import com.wishbox.feature.auth.navigation.AuthScreensProvider
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy

class ConfirmLoginViewModel(
    private val liveRouter: LiveRouter,
    private val authScreensProvider: AuthScreensProvider,
    private val errorHandler: ConfirmLoginErrorHandler,
    private val sendResetCodeUseCase: SendResetCodeUseCase
) : BaseViewModel(), HasNavigation by liveRouter {

    private val internalState: MutableLiveData<ConfirmState> by lazy {
        return@lazy MutableLiveData<ConfirmState>()
    }

    private var disposable by disposableDelegate()

    init {
        internalState.value = ConfirmState.initial()
    }

    val state: LiveData<ConfirmState> = internalState

    var login: String? by PropertyChangedDelegate("") {
        internalState.reduceState { it.copy(errorState = null) }
    }

    fun navigateBack() {
        internalState.value = ConfirmState.initial()
        liveRouter.exit()
    }

    fun sendResetCode() {
        internalState.reduceState {
            it.copy(errorState = null)
        }

        disposable = sendResetCodeUseCase.invoke(login.orEmpty())
            .applySchedulers()
            .doOnSubscribe {
                internalState.reduceState {
                    it.copy(isLoading = true)
                }
            }
            .subscribeBy(
                onComplete = {
                    internalState.value = ConfirmState.initial()
                    liveRouter.navigateTo(authScreensProvider.getPinScreen(login.orEmpty()))
                },
                onError = { throwable ->
                    internalState.reduceState {
                        val errorState = errorHandler.handle(it.errorState, throwable)
                        it.copy(errorState = errorState, isLoading = false)
                    }
                }
            ).addTo(disposables)
    }
}
