package com.wishbox.feature.auth.confirm.di

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.smedialink.common.error.ErrorHandler
import com.wishbox.core.di.mapkeys.ViewModelKey
import com.wishbox.core.presentation.errorHandling.MainErrorHandler
import com.wishbox.core.presentation.navigation.LiveRouter
import com.wishbox.feature.auth.confirm.domain.SendResetCodeUseCase
import com.wishbox.feature.auth.navigation.AuthScreensProvider
import com.wishbox.feature.auth.confirm.presentation.ConfirmLoginErrorHandler
import com.wishbox.feature.auth.confirm.presentation.ConfirmLoginFragment
import com.wishbox.feature.auth.confirm.presentation.ConfirmLoginViewModel
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.multibindings.IntoMap

@Module
abstract class ConfirmLoginModule {

    @Module
    companion object {
        @Provides
        @IntoMap
        @JvmStatic
        @ViewModelKey(ConfirmLoginViewModel::class)
        fun provideViewModel(
            router: LiveRouter,
            authScreensProvider: AuthScreensProvider,
            errorHandler: ConfirmLoginErrorHandler,
            resetCodeUseCase: SendResetCodeUseCase
        ): ViewModel = ConfirmLoginViewModel(
            router,
            authScreensProvider,
            errorHandler,
            resetCodeUseCase
        )

        @Provides
        @JvmStatic
        fun providePinViewModel(
            fragment: ConfirmLoginFragment,
            factory: ViewModelProvider.Factory
        ): ConfirmLoginViewModel = ViewModelProviders.of(fragment, factory).get(
            ConfirmLoginViewModel::class.java
        )
    }

    @Binds
    abstract fun provideErrorHandler(impl: MainErrorHandler): ErrorHandler

}
