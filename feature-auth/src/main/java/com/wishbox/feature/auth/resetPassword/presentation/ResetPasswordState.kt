package com.wishbox.feature.auth.resetPassword.presentation

import com.smedialink.common.error.ErrorState
import com.smedialink.common.events.ErrorEvent

data class RecoveryPasswordState(
    val isLoading: Boolean,
    val errorState: RecoveryPasswordErrorState?
) {
    companion object {
        fun initial() = RecoveryPasswordState(false, null)
    }
}

data class RecoveryPasswordErrorState(
    override val event: ErrorEvent?,
    val passwordTextError: String?,
    val confirmationPasswordTextError: String?
) : ErrorState(event)

