package com.wishbox.feature.auth.flow.api

interface AuthFlowApiProvider {
    val flowApi: AuthFlowApi
}