package com.wishbox.feature.auth.code.di

import com.wishbox.core.di.api.InjectorApi
import com.wishbox.core.di.module.ViewModelModule
import com.wishbox.core.di.provider.InjectorApiProvider
import com.wishbox.core.di.scope.PerFragment
import com.wishbox.core.di.util.lookupApiProvider
import com.wishbox.feature.auth.flow.api.AuthFlowApi
import com.wishbox.feature.auth.flow.api.AuthFlowApiProvider
import com.wishbox.feature.auth.code.di.module.CodeModule
import com.wishbox.feature.auth.code.presentation.CodeFragment
import dagger.BindsInstance
import dagger.Component

@PerFragment
@Component(
    dependencies = [
        InjectorApi::class,
        AuthFlowApi::class],
    modules = [
        CodeModule::class,
        ViewModelModule::class
    ]
)
interface PinComponent {

    fun inject(target: CodeFragment)

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun bind(target: CodeFragment): Builder

        fun api(authFlowApi: AuthFlowApi): Builder

        fun api(injectorApi: InjectorApi): Builder

        fun build(): PinComponent
    }

    companion object {

        fun inject(fragment: CodeFragment) {
            val injectorApiProvider: InjectorApiProvider = fragment.lookupApiProvider()
            val authFlowApiProvider: AuthFlowApiProvider = fragment.lookupApiProvider()

            DaggerPinComponent.builder()
                .api(injectorApiProvider.injectorApi)
                .api(authFlowApiProvider.flowApi)
                .bind(fragment)
                .build()
                .inject(fragment)
        }
    }
}