package com.wishbox.feature.auth.base.domain.validation

import com.wishbox.feature.auth.base.domain.exception.ValidationException
import io.reactivex.Completable
import javax.inject.Inject

/**
 * Created by Pak Evgeniy on 17.12.2018
 */

class PasswordMatchValidator @Inject constructor() {

    fun validate(firstPass: String, secondPass: String): Completable =
        if (firstPass == secondPass) Completable.complete()
        else Completable.error(ValidationException.PasswordsNotMatchException)
}