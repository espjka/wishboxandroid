package com.wishbox.feature.auth.code.presentation

import android.graphics.Color
import android.os.Bundle
import android.view.View
import androidx.core.content.ContextCompat
import com.smedialink.common.base.BaseFragment
import com.smedialink.common.events.handle
import com.smedialink.common.ext.observe
import com.smedialink.common.ext.onTextChanged
import com.smedialink.common.util.StateHolder
import com.wishbox.core.presentation.errorHandling.delegate.ToastDelegate
import com.wishbox.core.presentation.loading.ProgressLoadingDelegate
import com.wishbox.core.presentation.navigation.BackButtonListener
import com.wishbox.core.presentation.navigation.accept
import com.wishbox.feature.auth.R
import com.wishbox.feature.auth.code.di.PinComponent
import kotlinx.android.synthetic.main.fragment_pin.*
import ru.terrakok.cicerone.Navigator
import javax.inject.Inject

/**
 * Created by Oleg Sheliakin on 11.12.2018.
 * Contact me by email - olegsheliakin@gmail.com
 */
class CodeFragment : BaseFragment(), BackButtonListener {

    private val progressLoadingDelegate: ProgressLoadingDelegate by lazy {
        return@lazy ProgressLoadingDelegate(context!!, "Загрузка...")
    }

    override val layoutRes: Int = R.layout.fragment_pin

    @Inject
    lateinit var viewModel: CodeViewModel

    @Inject
    lateinit var toastDelegate: ToastDelegate

    @Inject
    lateinit var navigator: Navigator

    companion object {
        const val USER_ID_KEY = "user_id_key"
        const val USER_NAME_KEY = "user_name_key"

        fun createWithUserId(userId: Long): CodeFragment {
            return CodeFragment().apply {
                val bundle = Bundle().apply { putLong(USER_ID_KEY, userId) }
                arguments = bundle
            }
        }

        fun createWithUserName(userName: String): CodeFragment {
            return CodeFragment().apply {
                val bundle = Bundle().apply { putString(USER_NAME_KEY, userName) }
                arguments = bundle
            }
        }
    }

    private var stateHolder: StateHolder<CodeState> = StateHolder()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initUi()
        view.requestFocus()

        arguments?.let {
            when {
                it.containsKey(USER_ID_KEY) -> viewModel.type = CodeViewModel.Type.UserId(it.getLong(USER_ID_KEY))
                it.containsKey(USER_NAME_KEY) -> viewModel.type = CodeViewModel.Type.UserName(it.getString(USER_NAME_KEY, ""))
                else -> throw IllegalStateException("User id or user name must be set")
            }
        }

        viewModel.apply {
            navigationEvents.observe(this@CodeFragment, navigator::accept)
            state.observe(this@CodeFragment, this@CodeFragment::render)
        }
    }

    override fun inject() {
        PinComponent.inject(this)
    }

    private fun initUi() {
        btnResendPin.setOnClickListener {
            viewModel.resend()
        }

        etPin.onTextChanged { s, start, before, count ->
            viewModel.code = s?.toString() ?: ""
        }
    }

    private fun render(state: CodeState) {
        stateHolder = stateHolder.newState(state)

        stateHolder.diff({ isLoading }) {
            progressLoadingDelegate.showLoading(it)
        }

        stateHolder.diffNullable({ errorState?.errorText }) {
            etPin.showError(it)
        }

        stateHolder.diff({ emptyText }) {
            etPin.setText("")
        }

        stateHolder.diffNullable({ errorState?.event }) {
            it?.handle {
                toastDelegate.show(it.errorText, ToastDelegate.ToastGravity.TOP)
            }
        }

        stateHolder.diff({ resendState }) {
            when (it) {
                ResendState.Enabled -> {
                    btnResendPin.isEnabled = true
                    btnResendPin.setTextColor(Color.WHITE)
                }
                is ResendState.Disabled -> {
                    btnResendPin.isEnabled = false
                    btnResendPin.text = it.secondsLabel
                    context?.let {
                        btnResendPin.setTextColor(ContextCompat.getColor(it, R.color.white_50))
                    }
                }
            }
        }
    }

    override fun onBackPressed(): Boolean {
        viewModel.navigateBack()
        return true
    }
}