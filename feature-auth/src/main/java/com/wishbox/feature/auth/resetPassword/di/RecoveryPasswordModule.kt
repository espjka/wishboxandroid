package com.wishbox.feature.auth.resetPassword.di

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.smedialink.common.error.ErrorHandler
import com.wishbox.core.di.mapkeys.ViewModelKey
import com.wishbox.core.presentation.errorHandling.MainErrorHandler
import com.wishbox.core.presentation.navigation.LiveRouter
import com.wishbox.feature.auth.navigation.AuthScreensProvider
import com.wishbox.feature.auth.resetPassword.domain.ResetPasswordUseCase
import com.wishbox.feature.auth.resetPassword.presentation.ResetPasswordErrorHandler
import com.wishbox.feature.auth.resetPassword.presentation.ResetPasswordFragment
import com.wishbox.feature.auth.resetPassword.presentation.ResetPasswordViewModel
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.multibindings.IntoMap

@Module
abstract class RecoveryPasswordModule {

    @Module
    companion object {
        @Provides
        @IntoMap
        @JvmStatic
        @ViewModelKey(ResetPasswordViewModel::class)
        fun provideViewModel(
            router: LiveRouter,
            recoveryPasswordErrorHandler: ResetPasswordErrorHandler,
            authScreensProvider: AuthScreensProvider,
            resetPasswordUseCase: ResetPasswordUseCase
        ): ViewModel =
            ResetPasswordViewModel(
                router,
                recoveryPasswordErrorHandler,
                authScreensProvider,
                resetPasswordUseCase
            )

        @Provides
        @JvmStatic
        fun providePinViewModel(
            fragment: ResetPasswordFragment,
            factory: ViewModelProvider.Factory
        ): ResetPasswordViewModel = ViewModelProviders.of(fragment, factory).get(
            ResetPasswordViewModel::class.java
        )
    }

    @Binds
    abstract fun provideErrorHandler(impl: MainErrorHandler): ErrorHandler

}
