package com.wishbox.feature.auth.signup.domain.exception

import java.lang.Exception

class SignUpValidationException(val type: Type) : Exception()

enum class Type {
    LOGIN,
    PASSWORDS_NOT_MATCH,
    PASSWORD
}