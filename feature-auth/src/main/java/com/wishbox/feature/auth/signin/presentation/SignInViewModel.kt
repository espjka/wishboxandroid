package com.wishbox.feature.auth.signin.presentation

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.smedialink.common.base.BaseViewModel
import com.smedialink.common.delegates.PropertyChangedDelegate
import com.smedialink.common.delegates.rx.disposableDelegate
import com.smedialink.common.ext.applySchedulers
import com.smedialink.common.ext.reduceState
import com.wishbox.core.domain.exception.ProfileNotExistException
import com.wishbox.core.domain.exception.UserNotConfirmedException
import com.wishbox.core.presentation.navigation.HasNavigation
import com.wishbox.core.presentation.navigation.LiveRouter
import com.wishbox.feature.auth.navigation.AuthScreensProvider
import com.wishbox.feature.auth.signin.domain.LoginUseCase
import com.wishbox.feature.auth.signin.domain.SignInUseCasesFacade
import io.reactivex.Completable
import io.reactivex.disposables.Disposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import javax.inject.Inject

class SignInViewModel @Inject constructor(
    private val router: LiveRouter,
    private val authScreensProvider: AuthScreensProvider,
    private val signInUseCasesFacade: SignInUseCasesFacade,
    private val errorHandler: SignInErrorHandler
) : BaseViewModel(), HasNavigation by router {

    private var disposable: Disposable? by disposableDelegate()

    private val internalState: MutableLiveData<SignInState> = MutableLiveData()

    val state: LiveData<SignInState> = internalState

    init {
        internalState.value = SignInState.initial()
    }

    var login: String? by PropertyChangedDelegate("") {
        internalState.reduceState {
            it.copy(errorState = it.errorState?.copy(loginErrorText = null))
        }
    }

    var password: String? by PropertyChangedDelegate("") {
        internalState.reduceState {
            it.copy(errorState = it.errorState?.copy(passwordErrorText = null))
        }
    }

    fun checkAuth() {
        signInUseCasesFacade.checkAuth()
            .applySchedulers()
            .subscribeBy(
                onComplete = { router.newRootScreen(authScreensProvider.getMainScreen()) },
                onError = {
                    if (it is ProfileNotExistException) {
                        router.newRootScreen(authScreensProvider.getCreateProfileScreen())
                    }
                }
            ).addTo(disposables)
    }

    fun login() {
        val params = LoginUseCase.Params(
            login ?: "",
            password ?: ""
        )

        disposable = signInUseCasesFacade.login(params)
            .andThen(signInUseCasesFacade.checkConfirmation())
            .andThen(signInUseCasesFacade.checkProfile())
            .onErrorResumeNext {
                return@onErrorResumeNext if (it is UserNotConfirmedException) {
                    signInUseCasesFacade.sendPinCode(it.userId)
                        .andThen(Completable.error(it))
                } else {
                    Completable.error(it)
                }
            }
            .applySchedulers()
            .doOnSubscribe { _ ->
                internalState.reduceState {
                   SignInState.loading()
                }
            }
            .subscribeBy(
                onComplete = {
                    internalState.value = SignInState.initial()
                    router.newRootScreen(authScreensProvider.getMainScreen())
                },
                onError = { throwable ->
                    when (throwable) {
                        is UserNotConfirmedException -> {
                            internalState.value = SignInState.initial()
                            router.navigateTo(authScreensProvider.getPinScreen(throwable.userId))
                        }
                        is ProfileNotExistException -> router.newRootScreen(authScreensProvider.getCreateProfileScreen())
                        else -> internalState.reduceState {
                            val error = errorHandler.handle(it.errorState, throwable)
                            it.copy(isLoading = false, errorState = error)
                        }
                    }
                }
            ).addTo(disposables)
    }

    fun openConfirmLoginScreen() {
        internalState.reduceState {
            it.copy(errorState = null)
        }
        router.navigateTo(authScreensProvider.getConfirmLoginScreen())
    }
}
