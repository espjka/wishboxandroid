package com.wishbox.feature.auth.base.util

interface CanSkipAnimation {
    var skipAnimation: Boolean
}