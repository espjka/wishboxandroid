package com.wishbox.feature.auth.signup.presentation

import com.smedialink.common.events.ErrorEvent
import com.wishbox.core.presentation.resources.ErrorTextProvider
import com.wishbox.core.presentation.resources.ResourceProvider
import com.wishbox.feature.auth.R
import com.wishbox.feature.auth.base.domain.exception.ValidationException
import retrofit2.HttpException
import javax.inject.Inject

class SignUpErrorHandler @Inject constructor(
    private val resourceProvider: ResourceProvider,
    private val errorTextProvider: ErrorTextProvider
) {

    fun handle(prevErrorState: SignUpErrorState?, throwable: Throwable): SignUpErrorState {
        val prevState = prevErrorState ?: SignUpErrorState(null, null, null, null)
        return if (throwable is HttpException && throwable.code() == 400) { //todo create consts for all http error codes
            prevState.copy(event = ErrorEvent(resourceProvider.getString(R.string.sign_up_user_exist_error)))
        } else if (throwable is ValidationException.InvalidLoginException) {
            prevState.copy(loginErrorText = resourceProvider.getString(R.string.auth_invalid_data_format_error))
        } else if (throwable is ValidationException.InvalidPasswordException) {
            prevState.copy(passwordErrorText = resourceProvider.getString(R.string.auth_invalid_data_format_error))
        } else if (throwable is ValidationException.PasswordsNotMatchException){
            val errorEvent = ErrorEvent(resourceProvider.getString(R.string.auth_passwords_not_match_error))
            prevState.copy(event = errorEvent, passwordErrorText = "", confirmPasswordErrorText = "")
        } else {
            val errorEvent = errorTextProvider.provideErrorEvent(throwable)
            prevState.copy(event = errorEvent)
        }
    }
}
