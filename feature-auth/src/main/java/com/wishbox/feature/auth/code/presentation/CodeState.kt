package com.wishbox.feature.auth.code.presentation

import com.smedialink.common.error.ErrorState
import com.smedialink.common.events.ErrorEvent

data class CodeState(
    val isLoading: Boolean,
    val emptyText: Boolean,
    val errorState: CodeErrorState?,
    val resendState: ResendState
) {
    companion object {
        fun initial() = CodeState(false, false, null, ResendState.Enabled)
    }
}

sealed class ResendState {
    data class Disabled(val secondsLabel: String) : ResendState()
    object Enabled : ResendState()
}

data class CodeErrorState(
    override val event: ErrorEvent?,
    val errorText: String?
) : ErrorState(event)

