package com.wishbox.feature.auth.flow.di.module

import com.wishbox.feature.auth.R
import com.wishbox.feature.auth.flow.AuthFlowFragment
import com.wishbox.feature.auth.navigation.AuthNavigator
import dagger.Module
import dagger.Provides
import ru.terrakok.cicerone.Navigator

@Module
abstract class AuthFlowModule {
    @Module
    companion object {

        @JvmStatic
        @Provides
        fun provideNavigator(fragment: AuthFlowFragment): Navigator =
            AuthNavigator(fragment.activity, fragment.childFragmentManager, R.id.container)
    }

}
