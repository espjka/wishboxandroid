package com.wishbox.feature.auth.signup.presentation

import com.smedialink.common.events.ErrorEvent

data class SignUpState(
    val isLoading: Boolean,
    val errorState: SignUpErrorState?
) {
    companion object {
        fun initial(): SignUpState {
            return SignUpState(false, null)
        }
    }
}

data class SignUpErrorState(
    val event: ErrorEvent?,
    val loginErrorText: String?,
    val passwordErrorText: String?,
    val confirmPasswordErrorText: String?
)

