package com.wishbox.feature.auth.signup.domain

import com.wishbox.core.domain.repository.UserRegistrationRepository
import com.wishbox.core.domain.repository.UserSessionRepository
import com.wishbox.feature.auth.base.domain.validation.LoginValidator
import com.wishbox.feature.auth.base.domain.validation.PasswordMatchValidator
import com.wishbox.feature.auth.base.domain.validation.PasswordValidator
import io.reactivex.Completable
import io.reactivex.Single
import javax.inject.Inject

class RegisterUseCase @Inject constructor(
    private val loginValidator: LoginValidator,
    private val passwordValidator: PasswordValidator,
    private val passwordMatchValidator: PasswordMatchValidator,
    private val userRegistrationRepository: UserRegistrationRepository,
    private val userSessionRepository: UserSessionRepository
) {

    operator fun invoke(param: Param): Single<Long> =
        loginValidator.validate(param.login)
            .andThen(passwordValidator.validate(param.password))
            .andThen(passwordMatchValidator.validate(param.password, param.confirmationPassword))
            .andThen(userRegistrationRepository.register(param.login, param.password))
            .doOnSuccess {
                userSessionRepository.userId = it
            }

    data class Param(
        val login: String,
        val password: String,
        val confirmationPassword: String
    )
}