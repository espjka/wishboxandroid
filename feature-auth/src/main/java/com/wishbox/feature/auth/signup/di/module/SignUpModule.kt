package com.wishbox.feature.auth.signup.di.module

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.smedialink.common.error.ErrorHandler
import com.wishbox.core.di.mapkeys.ViewModelKey
import com.wishbox.core.presentation.navigation.LiveRouter
import com.wishbox.feature.auth.navigation.AuthScreensProvider
import com.wishbox.feature.auth.signin.presentation.SignInErrorHandler
import com.wishbox.feature.auth.signup.domain.RegisterUseCase
import com.wishbox.feature.auth.signup.presentation.SignUpErrorHandler
import com.wishbox.feature.auth.signup.presentation.SignUpFragment
import com.wishbox.feature.auth.signup.presentation.SignUpViewModel
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.multibindings.IntoMap

@Module
abstract class SignUpModule {

    @Module
    companion object {
        @Provides
        @IntoMap
        @JvmStatic
        @ViewModelKey(SignUpViewModel::class)
        fun provideViewModel(
            router: LiveRouter,
            errorHandler: SignUpErrorHandler,
            authChildScreensProvider: AuthScreensProvider,
            registerUseCase: RegisterUseCase
        ): ViewModel = SignUpViewModel(
            router,
            errorHandler,
            authChildScreensProvider,
            registerUseCase
        )

        @Provides
        @JvmStatic
        fun provideSignUpViewModel(
            fragment: SignUpFragment,
            factory: ViewModelProvider.Factory
        ): SignUpViewModel = ViewModelProviders.of(fragment, factory).get(
            SignUpViewModel::class.java
        )
    }

}
