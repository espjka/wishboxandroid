package com.wishbox.feature.auth.resetPassword.presentation

import android.os.Bundle
import android.view.View
import com.smedialink.common.base.BaseFragment
import com.smedialink.common.delegates.bundle.bundleString
import com.smedialink.common.events.handle
import com.smedialink.common.ext.observe
import com.smedialink.common.ext.onTextChanged
import com.smedialink.common.util.StateHolder
import com.wishbox.core.presentation.errorHandling.delegate.ToastDelegate
import com.wishbox.core.presentation.loading.ProgressLoadingDelegate
import com.wishbox.core.presentation.navigation.BackButtonListener
import com.wishbox.core.presentation.navigation.accept
import com.wishbox.feature.auth.R
import com.wishbox.feature.auth.resetPassword.di.ResetPasswordComponent
import kotlinx.android.synthetic.main.fragment_recovery_password.*
import ru.terrakok.cicerone.Navigator
import javax.inject.Inject

class ResetPasswordFragment : BaseFragment(), BackButtonListener {

    @Inject
    lateinit var viewModel: ResetPasswordViewModel

    @Inject
    lateinit var navigator: Navigator

    @Inject
    lateinit var toastDelegate: ToastDelegate

    private var recoveryToken: String? by bundleString()

    private var stateHolder = StateHolder<RecoveryPasswordState>()

    private val progressLoadingDelegate: ProgressLoadingDelegate by lazy {
        return@lazy ProgressLoadingDelegate(context!!, "Загрузка...")
    }

    companion object {
        fun create(token: String): ResetPasswordFragment {
            return ResetPasswordFragment().apply {
                recoveryToken = token
            }
        }
    }

    override val layoutRes: Int = R.layout.fragment_recovery_password

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        view.requestFocus()

        val token = recoveryToken ?: throw IllegalStateException("Token cannot be null")
        viewModel.token = token

        viewModel.navigationEvents.observe(this, navigator::accept)

        viewModel.state.observe(this) {
            render(it)
        }
        etPassword.onTextChanged { s, _, _, _ ->
            viewModel.password = s?.toString() ?: ""
        }

        etConfirmPassword.onTextChanged { s, _, _, _ ->
            viewModel.confirmationPassword = s?.toString() ?: ""
        }

        btnConfirmationPassword.setOnClickListener {
            viewModel.changePassword()
        }
    }

    private fun render(state: RecoveryPasswordState) {
        stateHolder = stateHolder.newState(state)

        stateHolder.apply {
            diff({ isLoading }, progressLoadingDelegate::showLoading)
            diffNullable({ errorState?.event }) { errorEvent ->
                errorEvent?.handle {
                    toastDelegate.show(
                        it.errorText,
                        ToastDelegate.ToastGravity.TOP
                    )
                }
            }

            diffNullable({ errorState?.passwordTextError }) {
                etPassword.showError(it)
            }

            diffNullable({ errorState?.confirmationPasswordTextError }) {
                etConfirmPassword.showError(it)
            }
        }
    }

    override fun inject() {
        ResetPasswordComponent.inject(this)
    }

    override fun onBackPressed(): Boolean {
        viewModel.navigateBack()
        return true
    }
}