package com.wishbox.feature.auth.signup.di

import com.wishbox.core.di.api.InjectorApi
import com.wishbox.core.di.module.ViewModelModule
import com.wishbox.core.di.provider.InjectorApiProvider
import com.wishbox.core.di.scope.PerFragment
import com.wishbox.core.di.util.lookupApiProvider
import com.wishbox.feature.auth.flow.api.AuthFlowApi
import com.wishbox.feature.auth.flow.api.AuthFlowApiProvider
import com.wishbox.feature.auth.signup.di.module.SignUpModule
import com.wishbox.feature.auth.signup.presentation.SignUpFragment
import dagger.BindsInstance
import dagger.Component

@PerFragment
@Component(
    dependencies = [
        InjectorApi::class,
        AuthFlowApi::class],
    modules = [
        SignUpModule::class,
        ViewModelModule::class
    ]
)
interface SignUpComponent {

    fun inject(target: SignUpFragment)

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun bind(target: SignUpFragment): Builder

        fun api(injectorApi: InjectorApi): Builder

        fun api(authFlowApi: AuthFlowApi): Builder

        fun build(): SignUpComponent
    }

    companion object {

        fun inject(fragment: SignUpFragment) {
            val injectorApiProvider: InjectorApiProvider = fragment.lookupApiProvider()
            val authFlowApiProvider: AuthFlowApiProvider = fragment.lookupApiProvider()

            DaggerSignUpComponent.builder()
                .api(injectorApiProvider.injectorApi)
                .api(authFlowApiProvider.flowApi)
                .bind(fragment)
                .build()
                .inject(fragment)
        }
    }
}