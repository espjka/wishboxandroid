package com.wishbox.feature.auth.signup.presentation

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.View
import android.view.animation.Animation
import com.smedialink.common.base.BaseFragment
import com.smedialink.common.events.handle
import com.smedialink.common.ext.observe
import com.smedialink.common.ext.onTextChanged
import com.smedialink.common.ext.trimmedString
import com.smedialink.common.util.StateHolder
import com.wishbox.core.presentation.errorHandling.delegate.ToastDelegate
import com.wishbox.core.presentation.loading.ProgressLoadingDelegate
import com.wishbox.core.presentation.navigation.accept
import com.wishbox.feature.auth.R
import com.wishbox.feature.auth.base.util.CanSkipAnimation
import com.wishbox.feature.auth.base.util.SkipAnimationDelegate
import com.wishbox.feature.auth.signup.di.SignUpComponent
import kotlinx.android.synthetic.main.fragment_sign_up.*
import ru.terrakok.cicerone.Navigator
import javax.inject.Inject

@SuppressLint("ValidFragment")
class SignUpFragment constructor(private val skipAnimationDelegate: SkipAnimationDelegate) :
    BaseFragment(),
    View.OnClickListener,
    CanSkipAnimation by skipAnimationDelegate {

    override val layoutRes: Int = R.layout.fragment_sign_up

    @Inject
    lateinit var toastDelegate: ToastDelegate

    @Inject
    lateinit var viewModel: SignUpViewModel

    @Inject
    lateinit var navigator: Navigator

    private var stateHolder = StateHolder<SignUpState>()

    private val progressLoadingDelegate: ProgressLoadingDelegate by lazy {
        return@lazy ProgressLoadingDelegate(context!!, "Загрузка...")
    }
    
    constructor() : this(SkipAnimationDelegate())

    override fun inject() {
        SignUpComponent.inject(this)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        skipAnimationDelegate.fragment = this
        view.requestFocus()
        btnSignUp.setOnClickListener(this)

        etLogin.onTextChanged { _, _, _, _ ->
            viewModel.login = etLogin.trimmedString()
        }

        etPassword.onTextChanged { _, _, _, _ ->
            viewModel.password = etPassword.trimmedString()
        }

        etConfirmPassword.onTextChanged { _, _, _, _ ->
            viewModel.confirmPassword = etConfirmPassword.trimmedString()
        }

        viewModel.navigationEvents.observe(this, navigator::accept)
        viewModel.state.observe(this, this::render)
    }

    override fun onClick(v: View?) {
        viewModel.register()
    }

    override fun onCreateAnimation(transit: Int, enter: Boolean, nextAnim: Int): Animation? {
        return skipAnimationDelegate.createAnim(transit, enter, nextAnim)
    }

    private fun render(state: SignUpState) {
        stateHolder = stateHolder.newState(state)

        stateHolder.apply {
            diff({ isLoading }) {
                progressLoadingDelegate.showLoading(it)
            }

            diffNullable({ errorState?.event }) { errorEvent ->
                errorEvent?.handle { toastDelegate.show(it.errorText, ToastDelegate.ToastGravity.TOP) }
            }

            diffNullable({ errorState?.loginErrorText }) {
                etLogin.showError(it)
            }

            diffNullable({ errorState?.passwordErrorText }) {
                etPassword.showError(it)
            }

            diffNullable({ errorState?.confirmPasswordErrorText }) {
                etConfirmPassword.showError(it)
            }

        }
    }

}
