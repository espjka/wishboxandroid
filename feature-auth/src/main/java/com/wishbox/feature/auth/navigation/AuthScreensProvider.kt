package com.wishbox.feature.auth.navigation

import androidx.fragment.app.Fragment
import com.wishbox.core.di.screenproviders.EditProfileScreenProvider
import com.wishbox.core.di.screenproviders.MainScreenProvider
import com.wishbox.feature.auth.auth.AuthFragment
import com.wishbox.feature.auth.code.presentation.CodeFragment
import com.wishbox.feature.auth.confirm.presentation.ConfirmLoginFragment
import com.wishbox.feature.auth.resetPassword.presentation.ResetPasswordFragment
import ru.terrakok.cicerone.android.support.SupportAppScreen
import javax.inject.Inject

class AuthScreensProvider @Inject constructor(
    private val mainScreenProvider: MainScreenProvider,
    private val editProfileScreenProvider: EditProfileScreenProvider
) {

    fun getMainScreen(): SupportAppScreen {
        return mainScreenProvider.getMainScreen()
    }

    fun getCreateProfileScreen(): SupportAppScreen {
        return editProfileScreenProvider.getEditProfileScreen(true)
    }

    fun getAuthScreen(): SupportAppScreen {
        return object : SupportAppScreen() {
            override fun getFragment(): Fragment {
                return AuthFragment()
            }

            override fun getScreenKey(): String = AuthFragment::class.java.name
        }
    }

    fun getConfirmLoginScreen(): SupportAppScreen {
        return object : SupportAppScreen() {
            override fun getFragment(): Fragment {
                return ConfirmLoginFragment()
            }

            override fun getScreenKey(): String = ConfirmLoginFragment::class.java.name
        }
    }

    fun getRecoveryScreen(token: String): SupportAppScreen {
        return object : SupportAppScreen() {
            override fun getFragment(): Fragment {
                return ResetPasswordFragment.create(token)
            }

            override fun getScreenKey(): String = ResetPasswordFragment::class.java.name
        }
    }

    fun getPinScreen(userName: String): SupportAppScreen {
        return object : SupportAppScreen() {
            override fun getFragment(): Fragment {
                return CodeFragment.createWithUserName(userName)
            }

            override fun getScreenKey(): String = CodeFragment::class.java.name
        }
    }

    fun getPinScreen(userId: Long): SupportAppScreen {
        return object : SupportAppScreen() {
            override fun getFragment(): Fragment {
                return CodeFragment.createWithUserId(userId)
            }

            override fun getScreenKey(): String = CodeFragment::class.java.name
        }
    }
}