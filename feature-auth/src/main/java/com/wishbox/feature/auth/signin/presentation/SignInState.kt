package com.wishbox.feature.auth.signin.presentation

import com.smedialink.common.events.ErrorEvent

/**
 * Created by Pak Evgeniy on 06.12.2018
 */
data class SignInState(
    val isLoading: Boolean,
    val errorState: SignInErrorState?
) {
    companion object {
        fun initial() : SignInState {
            return SignInState(false, null)
        }

        fun loading() : SignInState {
            return SignInState(true, null)
        }
    }
}

data class SignInErrorState(
    val event: ErrorEvent?,
    val loginErrorText: String?,
    val passwordErrorText: String?
)