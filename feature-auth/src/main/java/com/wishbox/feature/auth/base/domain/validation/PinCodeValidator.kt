package com.wishbox.feature.auth.base.domain.validation

import com.wishbox.core.domain.validator.Validator
import com.wishbox.feature.auth.base.domain.exception.ValidationException
import io.reactivex.Completable
import java.util.regex.Pattern
import javax.inject.Inject

class PinCodeValidator @Inject constructor() : Validator {

    companion object {
        const val REGEXP_PINCODE = "^\\d{6}\$"
    }

    override fun validate(value: String): Completable {
        return if (Pattern.matches(REGEXP_PINCODE, value)) Completable.complete()
        else Completable.error(ValidationException.InvalidCodeException)
    }
}