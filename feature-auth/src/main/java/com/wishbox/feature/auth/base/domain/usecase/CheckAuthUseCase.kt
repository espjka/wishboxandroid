package com.wishbox.feature.auth.base.domain.usecase

import com.wishbox.core.domain.exception.UserNotLoggedInException
import com.wishbox.core.domain.repository.UserSessionRepository
import io.reactivex.Completable
import javax.inject.Inject

//todo add to Splash screen
class CheckAuthUseCase @Inject constructor(
    private val userSessionRepository: UserSessionRepository
) {

    operator fun invoke(): Completable = Completable.fromCallable {
        if (!userSessionRepository.isLoggedIn) {
            throw UserNotLoggedInException()
        }
    }
}