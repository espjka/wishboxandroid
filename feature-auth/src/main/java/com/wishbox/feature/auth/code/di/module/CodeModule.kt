package com.wishbox.feature.auth.code.di.module

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.smedialink.common.error.ErrorHandler
import com.wishbox.core.di.mapkeys.ViewModelKey
import com.wishbox.core.presentation.errorHandling.MainErrorHandler
import com.wishbox.core.presentation.navigation.LiveRouter
import com.wishbox.feature.auth.base.domain.usecase.CheckProfileUseCase
import com.wishbox.feature.auth.code.domain.ConfirmPinCodeUseCase
import com.wishbox.feature.auth.code.domain.ConfirmResetCodeUseCase
import com.wishbox.feature.auth.code.domain.SendPinCodeUseCase
import com.wishbox.feature.auth.code.presentation.CodeErrorHandler
import com.wishbox.feature.auth.code.presentation.CodeFragment
import com.wishbox.feature.auth.code.presentation.CodeTimer
import com.wishbox.feature.auth.code.presentation.CodeViewModel
import com.wishbox.feature.auth.confirm.domain.SendResetCodeUseCase
import com.wishbox.feature.auth.navigation.AuthScreensProvider
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.multibindings.IntoMap

@Module
abstract class CodeModule {

    @Module
    companion object {
        @Provides
        @IntoMap
        @JvmStatic
        @ViewModelKey(CodeViewModel::class)
        fun provideViewModel(
            router: LiveRouter,
            pinErrorStateMapper: CodeErrorHandler,
            sendPinUseCase: SendPinCodeUseCase,
            authScreensProvider: AuthScreensProvider,
            pinTimer: CodeTimer,
            sendResetCodeUseCase: SendResetCodeUseCase,
            confirmResetCodeUseCase: ConfirmPinCodeUseCase,
            confirmPinCodeUseCase: ConfirmResetCodeUseCase,
            checkProfileUseCase: CheckProfileUseCase
        ): ViewModel = CodeViewModel(
            router,
            pinErrorStateMapper,
            pinTimer,
            sendPinUseCase,
            sendResetCodeUseCase,
            confirmResetCodeUseCase,
            confirmPinCodeUseCase,
            checkProfileUseCase,
            authScreensProvider
        )

        @Provides
        @JvmStatic
        fun providePinViewModel(
            fragment: CodeFragment,
            factory: ViewModelProvider.Factory
        ): CodeViewModel = ViewModelProviders.of(fragment, factory).get(
            CodeViewModel::class.java
        )

    }

    @Binds
    abstract fun provideErrorHandler(impl: MainErrorHandler): ErrorHandler

}
