package com.wishbox.feature.auth.resetPassword.domain

import com.wishbox.core.domain.repository.ResetPasswordRepository
import com.wishbox.feature.auth.base.domain.validation.PasswordMatchValidator
import com.wishbox.feature.auth.base.domain.validation.PasswordValidator
import io.reactivex.Completable
import javax.inject.Inject

/**
 * Created by Oleg Sheliakin on 18.12.2018.
 * Contact me by email - olegsheliakin@gmail.com
 */
class ResetPasswordUseCase @Inject constructor(
    private val resetPasswordRepository: ResetPasswordRepository,
    private val passwordMatchValidator: PasswordMatchValidator,
    private val passwordValidator: PasswordValidator
) {

    operator fun invoke(params: Params): Completable {
        return passwordValidator.validate(params.password)
            .andThen(passwordMatchValidator.validate(params.password, params.confirmationPassword))
            .andThen(resetPasswordRepository.resetPassword(params.password, params.token))
    }

    data class Params(
        val token: String,
        val password: String,
        val confirmationPassword: String)
}