package com.wishbox.feature.auth.base.util

import android.view.animation.Animation
import android.view.animation.AnimationUtils
import androidx.fragment.app.Fragment
import com.wishbox.feature.auth.R
import javax.inject.Inject

class SkipAnimationDelegate @Inject constructor() : CanSkipAnimation {

    lateinit var fragment: Fragment

    override var skipAnimation: Boolean = false

    private val noneAnim: Animation by lazy {
        return@lazy AnimationUtils.loadAnimation(fragment.activity, R.anim.none)
    }

    fun createAnim(transit: Int, enter: Boolean, nextAnim: Int): Animation? {
        return if (skipAnimation) noneAnim else null
    }

}