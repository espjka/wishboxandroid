package com.wishbox.feature.auth.code.domain

import com.wishbox.core.domain.repository.PinRepository
import com.wishbox.core.domain.repository.ProfileRepository
import com.wishbox.core.domain.repository.ResetPasswordRepository
import com.wishbox.feature.auth.base.domain.validation.PinCodeValidator
import com.wishbox.feature.auth.code.domain.exception.CodeValidationException
import io.reactivex.Completable
import io.reactivex.Single
import java.util.regex.Pattern
import javax.inject.Inject

class ConfirmPinCodeUseCase @Inject constructor(
    private val pinRepository: PinRepository,
    private val profileRepository: ProfileRepository,
    private val pinCodeValidator: PinCodeValidator
) {

    operator fun invoke(userId: Long, code: String): Completable {
        return pinCodeValidator.validate(code)
            .andThen(pinRepository.confirm(userId, code))
            .andThen(profileRepository.checkExistence())
    }
}