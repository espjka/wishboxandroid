package com.wishbox.feature.auth.resetPassword.presentation

import com.smedialink.common.events.ErrorEvent
import com.wishbox.core.di.util.Logger
import com.wishbox.core.presentation.resources.ErrorTextProvider
import com.wishbox.core.presentation.resources.ResourceProvider
import com.wishbox.feature.auth.R
import com.wishbox.feature.auth.base.domain.exception.ValidationException
import javax.inject.Inject
import retrofit2.HttpException

class ResetPasswordErrorHandler @Inject constructor(
    private val logger: Logger,
    private val resourceProvider: ResourceProvider,
    private val errorTextProvider: ErrorTextProvider
) {

    fun handle(prevErrorState: RecoveryPasswordErrorState?, throwable: Throwable): RecoveryPasswordErrorState {
        logger.error(throwable)
        return if (throwable is HttpException && throwable.code() == 400) { //todo create consts for all http error codes
            RecoveryPasswordErrorState(
                ErrorEvent(resourceProvider.getString(R.string.confirm_login_invalid_login_error)),
                prevErrorState?.passwordTextError,
                prevErrorState?.confirmationPasswordTextError
            )
        } else if (throwable is ValidationException.PasswordsNotMatchException) {
            RecoveryPasswordErrorState(
                prevErrorState?.event,
                prevErrorState?.passwordTextError,
                resourceProvider.getString(R.string.auth_passwords_not_match_error)
            )
        } else if (throwable is ValidationException.InvalidPasswordException) {
            RecoveryPasswordErrorState(
                prevErrorState?.event,
                resourceProvider.getString(R.string.auth_invalid_data_format_error),
                prevErrorState?.confirmationPasswordTextError
            )
        } else {
            val errorEvent = errorTextProvider.provideErrorEvent(throwable)
            RecoveryPasswordErrorState(
                errorEvent, prevErrorState?.passwordTextError,
                prevErrorState?.confirmationPasswordTextError
            )
        }
    }

}