package com.wishbox.feature.auth.base.domain.usecase

import com.wishbox.core.domain.repository.ProfileRepository
import io.reactivex.Completable
import javax.inject.Inject

/**
 * Created by Pak Evgeniy on 29.12.2018
 */

class CheckProfileUseCase @Inject constructor(
    private val repository: ProfileRepository
) {
    operator fun invoke(): Completable = repository.checkExistence()
}