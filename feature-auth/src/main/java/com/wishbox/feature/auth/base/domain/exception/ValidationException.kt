package com.wishbox.feature.auth.base.domain.exception

import java.lang.Exception

object ValidationException {
    object InvalidLoginException : Exception()
    object InvalidPasswordException : Exception()
    object PasswordsNotMatchException : Exception()
    object InvalidCodeException : Exception()
}

