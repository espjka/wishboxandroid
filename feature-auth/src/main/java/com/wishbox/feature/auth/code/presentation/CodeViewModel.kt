package com.wishbox.feature.auth.code.presentation

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.smedialink.common.base.BaseViewModel
import com.smedialink.common.delegates.PropertyChangedDelegate
import com.smedialink.common.delegates.rx.disposableDelegate
import com.smedialink.common.ext.applySchedulers
import com.smedialink.common.ext.reduceState
import com.wishbox.core.domain.exception.ProfileNotExistException
import com.wishbox.core.presentation.navigation.HasNavigation
import com.wishbox.core.presentation.navigation.LiveRouter
import com.wishbox.feature.auth.base.domain.usecase.CheckProfileUseCase
import com.wishbox.feature.auth.code.domain.ConfirmPinCodeUseCase
import com.wishbox.feature.auth.code.domain.ConfirmResetCodeUseCase
import com.wishbox.feature.auth.code.domain.SendPinCodeUseCase
import com.wishbox.feature.auth.confirm.domain.SendResetCodeUseCase
import com.wishbox.feature.auth.navigation.AuthScreensProvider
import io.reactivex.disposables.Disposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import javax.inject.Inject
import kotlin.properties.Delegates

class CodeViewModel @Inject constructor(
    private val router: LiveRouter,
    private val pinErrorStateMapper: CodeErrorHandler,
    private val pinTimer: CodeTimer,
    private val sendPinUseCase: SendPinCodeUseCase,
    private val sendResetCodeUseCase: SendResetCodeUseCase,
    private val confirmPinCodeUseCase: ConfirmPinCodeUseCase,
    private val confirmResetCodeUseCase: ConfirmResetCodeUseCase,
    private val checkProfileUseCase: CheckProfileUseCase,
    private val authScreensProvider: AuthScreensProvider
) : BaseViewModel(), HasNavigation by router {

    private var disposable: Disposable? by disposableDelegate()
    private var timerDisposable: Disposable? by disposableDelegate()

    private val internalUiModel: MutableLiveData<CodeState> = MutableLiveData()

    var type: Type by Delegates.notNull()

    val state: LiveData<CodeState> = internalUiModel

    var code: String? by PropertyChangedDelegate("") { s ->
        internalUiModel.reduceState {
            it.copy(errorState = CodeErrorState(null, null))
        }

        if (s != null && s.length == CODE_LENGTH) {
            val type = this.type
            when (type) {
                is Type.UserId -> confirmPin(type.userId, s)
                is Type.UserName -> confirmResetCode(type.userName, s)
            }
        }
    }

    companion object {
        const val TIMER_VALUE = 60L
        const val CODE_LENGTH = 6
    }

    init {
        internalUiModel.value = CodeState.initial()
        startTimer()
    }

    fun resend() {
        val type = this.type
        disposable = when (type) {
            is Type.UserId -> sendPinUseCase.invoke(type.userId)
            is Type.UserName -> sendResetCodeUseCase.invoke(type.userName)
        }.applySchedulers()
            .doOnSubscribe { loading() }
            .subscribeBy(
                onComplete = {
                    internalUiModel.reduceState { it.copy(isLoading = false, errorState = null) }
                    startTimer()
                },
                onError = this@CodeViewModel::onError
            ).addTo(disposables)
    }

    private fun confirmPin(userId: Long, code: String) {
        disposable = confirmPinCodeUseCase.invoke(userId, code)
            .andThen(checkProfileUseCase.invoke())
            .applySchedulers()
            .doOnSubscribe { loading() }
            .subscribeBy(
                onComplete = {
                    success()
                    router.newRootScreen(authScreensProvider.getMainScreen())
                },
                onError = {
                    if (it is ProfileNotExistException) {
                        router.newRootScreen(authScreensProvider.getCreateProfileScreen())
                    } else {
                        this@CodeViewModel.onError(it)
                    }
                }
            ).addTo(disposables)
    }

    private fun confirmResetCode(userName: String, code: String) {
        disposable = confirmResetCodeUseCase.invoke(userName, code)
            .applySchedulers()
            .doOnSubscribe { loading() }
            .subscribeBy(
                onSuccess = {
                    success()
                    router.navigateTo(authScreensProvider.getRecoveryScreen(it))
                },
                onError = this@CodeViewModel::onError
            )
    }

    private fun startTimer() {
        timerDisposable = pinTimer.timer()
            .subscribeBy(
                onNext = { label ->
                    internalUiModel.reduceState {
                        it.copy(resendState = ResendState.Disabled(label))
                    }
                },
                onComplete = {
                    internalUiModel.reduceState {
                        it.copy(resendState = ResendState.Enabled)
                    }
                },
                onError = { /*ignored*/ }
            ).addTo(disposables)
    }

    private fun success() {
        internalUiModel.reduceState {
            it.copy(isLoading = false, emptyText = true, errorState = null)
        }
    }

    private fun loading() {
        internalUiModel.reduceState {
            it.copy(isLoading = true, errorState = null)
        }
    }

    private fun onError(throwable: Throwable) {
        internalUiModel.reduceState {
            val error = pinErrorStateMapper.handle(it.errorState, throwable)
            it.copy(isLoading = false, emptyText = true, errorState = error)
        }
    }

    fun navigateBack() {
        internalUiModel.value = CodeState.initial()
        router.exit()
    }

    sealed class Type {
        data class UserId(val userId: Long) : Type()
        data class UserName(val userName: String) : Type()
    }

}