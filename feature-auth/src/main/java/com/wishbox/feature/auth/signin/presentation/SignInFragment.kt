package com.wishbox.feature.auth.signin.presentation

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.View
import android.view.animation.Animation
import com.smedialink.common.events.handle
import com.smedialink.common.ext.*
import com.smedialink.common.util.StateHolder
import com.wishbox.core.presentation.base.BaseFragment
import com.wishbox.core.presentation.errorHandling.delegate.ToastDelegate
import com.wishbox.core.presentation.loading.ProgressLoadingDelegate
import com.wishbox.core.presentation.navigation.accept
import com.wishbox.feature.auth.R
import com.wishbox.feature.auth.base.util.CanSkipAnimation
import com.wishbox.feature.auth.base.util.SkipAnimationDelegate
import com.wishbox.feature.auth.signin.di.SignInComponent
import kotlinx.android.synthetic.main.fragment_sign_in.*
import ru.terrakok.cicerone.Navigator
import javax.inject.Inject

@SuppressLint("ValidFragment")
class SignInFragment constructor(private val skipAnimationDelegate: SkipAnimationDelegate) :
    BaseFragment<SignInViewModel>(),
    CanSkipAnimation by skipAnimationDelegate {

    @Inject
    lateinit var toastDelegate: ToastDelegate

    @Inject
    lateinit var navigator: Navigator

    constructor() : this(SkipAnimationDelegate())

    private var stateHolder = StateHolder<SignInState>()

    private val progressLoadingDelegate: ProgressLoadingDelegate by lazy {
        return@lazy ProgressLoadingDelegate(context!!, "Загрузка...")
    }

    override fun layoutRes(): Int = R.layout.fragment_sign_in

    override fun injectDependencies() {
        SignInComponent.inject(this)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        skipAnimationDelegate.fragment = this
        view.requestFocus()
        initUi()
    }

    override fun onCreateAnimation(transit: Int, enter: Boolean, nextAnim: Int): Animation? {
        return skipAnimationDelegate.createAnim(transit, enter, nextAnim)
    }

    private fun initUi() {
        btnLogin.setOnClickListener {
            viewModel.login()
        }

        tvForgotPassword.setOnClickListener {
            viewModel.openConfirmLoginScreen()
        }

        etLogin.onTextChanged { _, _, _, _ ->
            viewModel.login = etLogin.trimmedString()
        }

        etPassword.onTextChanged {  _, _, _, _ ->
            viewModel.password = etPassword.trimmedString()
        }

        viewModel.apply {
            navigationEvents.observe(this@SignInFragment, navigator::accept)
            state.observe(this@SignInFragment) { render(it) }
            checkAuth()
        }
    }

    private fun render(state: SignInState) {
        stateHolder = stateHolder.newState(state)

        stateHolder.apply {

            diff({ isLoading }) {
                progressLoadingDelegate.showLoading(it)
            }

            diffNullable({ errorState?.event }) { errorEvent ->
                errorEvent?.handle { toastDelegate.show(it.errorText, ToastDelegate.ToastGravity.TOP) }
            }

            diffNullable({ errorState?.loginErrorText }) {
                etLogin.showError(it)
            }

            diffNullable({ errorState?.passwordErrorText }) {
                etPassword.showError(it)
            }

        }
    }
}
