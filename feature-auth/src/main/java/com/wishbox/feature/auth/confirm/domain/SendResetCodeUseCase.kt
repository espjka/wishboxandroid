package com.wishbox.feature.auth.confirm.domain

import com.wishbox.core.domain.repository.ResetPasswordRepository
import com.wishbox.feature.auth.base.domain.validation.LoginValidator
import io.reactivex.Completable
import javax.inject.Inject

class SendResetCodeUseCase @Inject constructor(
    private val resetPasswordRepository: ResetPasswordRepository,
    private val loginValidator: LoginValidator
) {

    operator fun invoke(login: String): Completable {
        return loginValidator.validate(login)
            .andThen(resetPasswordRepository.sendResetCode(login))
    }
}