package com.wishbox.feature.auth.confirm.presentation

import com.smedialink.common.events.ErrorEvent
import com.wishbox.core.di.util.Logger
import com.wishbox.core.presentation.resources.ErrorTextProvider
import com.wishbox.core.presentation.resources.ResourceProvider
import com.wishbox.feature.auth.R
import com.wishbox.feature.auth.base.domain.exception.ValidationException
import retrofit2.HttpException
import javax.inject.Inject

class ConfirmLoginErrorHandler @Inject constructor(
    private val logger: Logger,
    private val resourceProvider: ResourceProvider,
    private val errorTextProvider: ErrorTextProvider
) {

    fun handle(prevState: ConfirmErrorState?, throwable: Throwable): ConfirmErrorState {
        logger.error(throwable)
        val state = prevState ?: ConfirmErrorState(null, null)
        return if (throwable is HttpException && throwable.code() == 429) { //todo create consts for all http error codes
            ConfirmErrorState(event = ErrorEvent(resourceProvider.getString(R.string.confirm_login_throttle_error)), loginErrorText = "")
        } else if (throwable is HttpException && throwable.code() == 400) { //todo create consts for all http error codes
            ConfirmErrorState(event = ErrorEvent(resourceProvider.getString(R.string.confirm_login_invalid_login_error)), loginErrorText = "")
        } else if (throwable is ValidationException.InvalidLoginException) {
            state.copy(loginErrorText = resourceProvider.getString(R.string.auth_invalid_data_format_error))
        } else {
            val errorEvent = errorTextProvider.provideErrorEvent(throwable)
            state.copy(event = errorEvent)
        }
    }

}