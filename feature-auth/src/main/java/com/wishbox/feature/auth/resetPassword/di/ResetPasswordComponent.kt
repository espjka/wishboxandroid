package com.wishbox.feature.auth.resetPassword.di

import com.wishbox.core.di.api.InjectorApi
import com.wishbox.core.di.module.ViewModelModule
import com.wishbox.core.di.provider.InjectorApiProvider
import com.wishbox.core.di.scope.PerFragment
import com.wishbox.core.di.util.lookupApiProvider
import com.wishbox.feature.auth.flow.api.AuthFlowApi
import com.wishbox.feature.auth.flow.api.AuthFlowApiProvider
import com.wishbox.feature.auth.resetPassword.presentation.ResetPasswordFragment
import dagger.BindsInstance
import dagger.Component

@PerFragment
@Component(
    dependencies = [
        InjectorApi::class,
        AuthFlowApi::class],
    modules = [
        RecoveryPasswordModule::class,
        ViewModelModule::class
    ]
)
interface ResetPasswordComponent {

    fun inject(target: ResetPasswordFragment)

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun bind(target: ResetPasswordFragment): Builder

        fun api(authFlowApi: AuthFlowApi): Builder

        fun api(injectorApi: InjectorApi): Builder

        fun build(): ResetPasswordComponent
    }

    companion object {

        fun inject(fragment: ResetPasswordFragment) {
            val injectorApiProvider: InjectorApiProvider = fragment.lookupApiProvider()
            val authFlowApiProvider: AuthFlowApiProvider = fragment.lookupApiProvider()

            DaggerResetPasswordComponent.builder()
                .api(injectorApiProvider.injectorApi)
                .api(authFlowApiProvider.flowApi)
                .bind(fragment)
                .build()
                .inject(fragment)
        }
    }
}