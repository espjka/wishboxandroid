package com.wishbox.feature.auth.code.presentation

import com.smedialink.common.events.ErrorEvent
import com.wishbox.core.di.util.Logger
import com.wishbox.core.presentation.resources.ErrorTextProvider
import com.wishbox.core.presentation.resources.ResourceProvider
import com.wishbox.feature.auth.R
import com.wishbox.feature.auth.code.domain.exception.CodeValidationException
import javax.inject.Inject
import retrofit2.HttpException

class CodeErrorHandler @Inject constructor(
    private val logger: Logger,
    private val resourceProvider: ResourceProvider,
    private val errorTextProvider: ErrorTextProvider
) {

    fun handle(prevState: CodeErrorState?, error: Throwable): CodeErrorState {
        logger.error(error)
        val state = prevState ?: CodeErrorState(null, null)
        return if (error is HttpException && error.code() == 400) { //todo create consts for all http error codes
            CodeErrorState(ErrorEvent(resourceProvider.getString(R.string.pin_incorrect_code_error)), "")
        } else if (error is CodeValidationException) {
            state.copy(errorText = resourceProvider.getString(R.string.pin_invalid_code_error))
        } else {
            val errorEvent = errorTextProvider.provideErrorEvent(error)
            state.copy(event = errorEvent)
        }
    }

}