package com.wishbox.feature.auth.code.domain

import com.wishbox.core.domain.repository.PinRepository
import io.reactivex.Completable
import javax.inject.Inject

class SendPinCodeUseCase @Inject constructor(
    private val pinRepository: PinRepository
) {

    operator fun invoke(userId: Long): Completable {
        return pinRepository.resend(userId)
    }

}