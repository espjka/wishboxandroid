package com.wishbox.feature.auth.signin.domain

import com.wishbox.core.domain.repository.AuthRepository
import com.wishbox.feature.auth.base.domain.validation.LoginValidator
import com.wishbox.feature.auth.base.domain.validation.PasswordValidator
import io.reactivex.Completable
import javax.inject.Inject

class LoginUseCase @Inject constructor(
    private val authRepository: AuthRepository,
    private val validator: LoginValidator,
    private val passwordValidator: PasswordValidator
) {

    operator fun invoke(params: Params): Completable =
        validator.validate(params.username)
            .andThen(passwordValidator.validate(params.password))
            .andThen(authRepository.login(params.username, params.password))

    data class Params(val username: String, val password: String)
}