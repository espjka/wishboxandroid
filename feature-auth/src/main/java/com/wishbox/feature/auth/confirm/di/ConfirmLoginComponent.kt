package com.wishbox.feature.auth.confirm.di

import com.wishbox.core.di.api.InjectorApi
import com.wishbox.core.di.module.ViewModelModule
import com.wishbox.core.di.provider.InjectorApiProvider
import com.wishbox.core.di.scope.PerFragment
import com.wishbox.core.di.util.lookupApiProvider
import com.wishbox.feature.auth.confirm.presentation.ConfirmLoginFragment
import com.wishbox.feature.auth.flow.api.AuthFlowApi
import com.wishbox.feature.auth.flow.api.AuthFlowApiProvider
import dagger.BindsInstance
import dagger.Component

@PerFragment
@Component(
    dependencies = [
        InjectorApi::class,
        AuthFlowApi::class],
    modules = [
        ConfirmLoginModule::class,
        ViewModelModule::class
    ]
)
interface ConfirmLoginComponent {

    fun inject(target: ConfirmLoginFragment)

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun bind(target: ConfirmLoginFragment): Builder

        fun api(authFlowApi: AuthFlowApi): Builder

        fun api(injectorApi: InjectorApi): Builder

        fun build(): ConfirmLoginComponent
    }

    companion object {

        fun inject(fragment: ConfirmLoginFragment) {
            val injectorApiProvider: InjectorApiProvider = fragment.lookupApiProvider()
            val authFlowApiProvider: AuthFlowApiProvider = fragment.lookupApiProvider()

            DaggerConfirmLoginComponent.builder()
                .api(injectorApiProvider.injectorApi)
                .api(authFlowApiProvider.flowApi)
                .bind(fragment)
                .build()
                .inject(fragment)
        }
    }
}