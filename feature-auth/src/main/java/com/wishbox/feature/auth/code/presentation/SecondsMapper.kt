package com.wishbox.feature.auth.code.presentation

import com.wishbox.core.presentation.resources.ResourceProvider
import com.wishbox.feature.auth.R
import javax.inject.Inject

class SecondsMapper @Inject constructor(
    private val resourceProvider: ResourceProvider
) {

    private val label : String by lazy {
        return@lazy resourceProvider.getString(R.string.sign_up_pin_resend_title_button)
    }

    fun map(seconds: Long): String {
        return if (seconds <= 0L) {
            label
        } else {
            label.plus(" ($seconds)")
        }
    }
}