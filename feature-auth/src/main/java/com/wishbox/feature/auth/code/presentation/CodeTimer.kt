package com.wishbox.feature.auth.code.presentation

import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import java.util.*
import java.util.concurrent.TimeUnit
import javax.inject.Inject

/**
 * Created by Oleg Sheliakin on 18.12.2018.
 * Contact me by email - olegsheliakin@gmail.com
 */
class CodeTimer @Inject constructor(
    private val secondsMapper: SecondsMapper
) {

    fun timer(): Observable<String> {
        return Observable
            .intervalRange(1, CodeViewModel.TIMER_VALUE, 0, 1, TimeUnit.SECONDS)
            .observeOn(AndroidSchedulers.mainThread())
            .map { secondsMapper.map(CodeViewModel.TIMER_VALUE - it) }
    }
}