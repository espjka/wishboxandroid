package com.wishbox.feature.auth.base.domain.usecase

import com.wishbox.core.domain.exception.UserNotConfirmedException
import com.wishbox.core.domain.repository.UserSessionRepository
import io.reactivex.Completable
import javax.inject.Inject

class CheckConfirmationUseCase @Inject constructor(
    private val userSessionRepository: UserSessionRepository
) {

    operator fun invoke(): Completable =
        Completable.fromAction {
            val userId = userSessionRepository.userId
            if (!userSessionRepository.isConfirmed && userId != null) {
                throw UserNotConfirmedException(userId)
            } else if (userId == null) {
                throw IllegalStateException("User id is null")
            }
        }
}