package com.wishbox.feature.auth.confirm.presentation

import android.os.Bundle
import android.view.View
import com.smedialink.common.base.BaseFragment
import com.smedialink.common.events.handle
import com.smedialink.common.ext.observe
import com.smedialink.common.ext.onTextChanged
import com.smedialink.common.util.StateHolder
import com.wishbox.core.presentation.errorHandling.delegate.ToastDelegate
import com.wishbox.core.presentation.loading.ProgressLoadingDelegate
import com.wishbox.core.presentation.navigation.BackButtonListener
import com.wishbox.core.presentation.navigation.accept
import com.wishbox.feature.auth.R
import com.wishbox.feature.auth.confirm.di.ConfirmLoginComponent
import kotlinx.android.synthetic.main.fragment_confirm_login.*
import ru.terrakok.cicerone.Navigator
import javax.inject.Inject

class ConfirmLoginFragment : BaseFragment(), BackButtonListener {

    @Inject
    lateinit var viewModel: ConfirmLoginViewModel

    @Inject
    lateinit var navigator: Navigator

    @Inject
    lateinit var toastDelegate: ToastDelegate

    override val layoutRes: Int = R.layout.fragment_confirm_login

    private var stateHolder: StateHolder<ConfirmState> = StateHolder()

    private val progressLoadingDelegate: ProgressLoadingDelegate by lazy {
        return@lazy ProgressLoadingDelegate(context!!, "Загрузка...")
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        view.requestFocus()
        btnConfirmLogin.setOnClickListener {
            viewModel.sendResetCode()
        }

        etLogin.onTextChanged { s, start, before, count ->
            viewModel.login = s?.toString() ?: ""
        }

        viewModel.navigationEvents.observe(this, navigator::accept)

        viewModel.state.observe(this) {
            render(it)
        }
    }

    override fun inject() {
        ConfirmLoginComponent.inject(this)
    }

    private fun render(state: ConfirmState) {
        stateHolder = stateHolder.newState(state)
        stateHolder.apply {

            diff({ isLoading }, progressLoadingDelegate::showLoading)

            diffNullable({ errorState?.event }) { errorState ->
                errorState?.handle { toastDelegate.show(it.errorText, ToastDelegate.ToastGravity.TOP) }
            }
            diffNullable({ errorState?.loginErrorText }) {
                etLogin.showError(it)
            }

        }
    }

    override fun onBackPressed(): Boolean {
        viewModel.navigateBack()
        return true
    }
}