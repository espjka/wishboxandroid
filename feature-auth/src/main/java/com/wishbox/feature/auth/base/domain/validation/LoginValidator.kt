package com.wishbox.feature.auth.base.domain.validation

import com.wishbox.core.domain.validator.Validator
import com.wishbox.feature.auth.base.domain.exception.ValidationException
import io.reactivex.Completable
import java.util.regex.Pattern
import javax.inject.Inject

class LoginValidator @Inject constructor() : Validator {

    companion object {
        private const val REGEX_EMAIL = "^[a-zA-Z0-9_+&*-]+(?:\\." +
                "[a-zA-Z0-9_+&*-]+)*@" +
                "(?:[a-zA-Z0-9-]+\\.)+[a-z" +
                "A-Z]{2,7}$"

        private const val REGEX_PHONE = "^((\\+7|8)+([0-9]){10})\$"
    }

    override fun validate(value: String): Completable {
        val result = ((!value.isEmpty() && Pattern.compile(REGEX_EMAIL).matcher(value).matches())
                || Pattern.compile(REGEX_PHONE).matcher(value).matches())
        return if (result) Completable.complete()
        else Completable.error(ValidationException.InvalidLoginException)
    }
}