package com.wishbox.feature.auth.signin.domain

import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import com.wishbox.core.domain.repository.AuthRepository
import com.wishbox.core.domain.repository.ProfileRepository
import com.wishbox.feature.auth.base.domain.validation.LoginValidator
import com.wishbox.feature.auth.base.domain.validation.PasswordValidator
import io.reactivex.Completable
import org.junit.Test

class LoginUseCaseTest {

    private val validator = LoginValidator()
    private val profileRepository: ProfileRepository = mock()
    private val passwordValidator = PasswordValidator()
    private val authRepo: AuthRepository = mock()

    private val subject: LoginUseCase = LoginUseCase(authRepo, validator, passwordValidator)

    @Test
    fun test() {
        whenever(authRepo.login(any(), any())).thenReturn(Completable.complete())
        whenever(profileRepository.checkExistence()).thenReturn(Completable.complete())
        subject.invoke(LoginUseCase.Params("test@test.com", "test1234"))
            .test().assertNoErrors()
    }

}
