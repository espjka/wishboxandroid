package com.wishbox.feature.auth.signin

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.mock
import com.wishbox.core.presentation.navigation.LiveRouter
import com.wishbox.coretesting.RxSchedulesRule
import com.wishbox.feature.auth.navigation.AuthScreensProvider
import com.wishbox.feature.auth.signin.domain.SignInUseCasesFacade
import com.wishbox.feature.auth.signin.presentation.SignInErrorHandler
import com.wishbox.feature.auth.signin.presentation.SignInErrorState
import com.wishbox.feature.auth.signin.presentation.SignInViewModel
import io.reactivex.Completable
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import ru.terrakok.cicerone.android.support.SupportAppScreen

class SignInViewModelTest {

    @get:Rule
    val schedulerRule = RxSchedulesRule()

    @get:Rule
    val taskExecutorRule = InstantTaskExecutorRule()

    private val authChildScreensProvider: AuthScreensProvider = mock {
        on { getAuthScreen() } doReturn object : SupportAppScreen() {}
        on { getPinScreen(any<Long>()) } doReturn object : SupportAppScreen() {}
        on { getPinScreen(any<String>()) } doReturn object : SupportAppScreen() {}
        on { getRecoveryScreen(any()) } doReturn object : SupportAppScreen() {}
        on { getMainScreen() } doReturn object : SupportAppScreen() {}
    }

    private val errorHandler: SignInErrorHandler = mock {
        on { handle(any(), any()) } doReturn SignInErrorState(null, null, null)
    }

    private val signInUseCasesFacade: SignInUseCasesFacade = mock {
        on { checkAuth() } doReturn Completable.complete()
        on { login(any()) } doReturn Completable.complete()
        on { checkConfirmation() } doReturn Completable.complete()
        on { sendPinCode(any()) } doReturn Completable.complete()
    }

    private lateinit var subject: SignInViewModel

    @Before
    fun setUp() {
        subject = SignInViewModel(
            LiveRouter(),
            authChildScreensProvider,
            signInUseCasesFacade,
            errorHandler
        )
    }

    @Test
    fun shouldLogin() {
//        val expected = listOf(
//            SignInState.initial(),
//            SignInState(isLoading = true, errorState = null),
//            SignInState.initial()
//        )
//
//        val testObserver = subject.state.testObserver()
//
//        subject.login()
//
//        Assert.assertEquals(expected, testObserver.observedValues)
    }
}