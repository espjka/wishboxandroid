package com.wishbox.feature.friendList

import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.smedialink.common.delegates.bundle.bundleLong
import com.smedialink.common.events.handle
import com.smedialink.common.ext.gone
import com.smedialink.common.ext.observe
import com.smedialink.common.ext.visible
import com.smedialink.common.recyclerview.itemDecoration.SpaceItemDecoration
import com.wishbox.core.presentation.base.BaseDialog
import com.wishbox.core.presentation.base.BaseFragment
import com.wishbox.core.presentation.errorHandling.delegate.ToastDelegate
import com.wishbox.core.presentation.navigation.BackButtonListener
import com.wishbox.core.presentation.pagination.Paginator
import com.wishbox.core.presentation.widget.ScrollingButtonHelper
import com.wishbox.feature.friendList.adapter.FriendAdapter
import com.wishbox.feature.friendList.adapter.FriendAdapterViewType
import com.wishbox.feature.friendList.di.FriendListViewComponent
import kotlinx.android.synthetic.main.fragment_friend_list.*
import javax.inject.Inject

/**
 * Created by Pak Evgeniy on 08.12.2018
 */

class FriendListFragment : BaseFragment<FriendListViewModel>(), BackButtonListener {

    @Inject
    lateinit var errorDelegate: ToastDelegate

    @Inject
    lateinit var adapter: FriendAdapter

    @Inject
    lateinit var paginator: Paginator

    @Inject
    lateinit var scrollButtonHelper: ScrollingButtonHelper

    companion object {
        fun newInstance(userId: Long? = null): FriendListFragment =
            FriendListFragment().apply { this.userId = userId }
    }

    var userId: Long? by bundleLong()
        private set

    val isNotMine: Boolean by lazy { userId.takeIf { it != 0L } != null }

    override fun injectDependencies() {
        FriendListViewComponent.Initializer.init(this)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        with(recyclerFriends) {
            adapter = this@FriendListFragment.adapter
            layoutManager = LinearLayoutManager(this@FriendListFragment.context)
            paginator.withRecyclerView(this, adapter?.itemCount == 0)
            val offset = resources.getDimension(R.dimen.margin_small)
            addItemDecoration(SpaceItemDecoration(SpaceItemDecoration.Offset(offset.toInt(), 0), SpaceItemDecoration.each()))
        }

        setupForNotMineWishes()

        layoutRefresh.setOnRefreshListener { viewModel.loadNextPage(true) }

        textRetry.setOnClickListener { viewModel.loadNextPage() }
        toolbar.getLeftButton().setOnClickListener { viewModel.navigateBack() }
        buttonAdd.setOnClickListener { viewModel.openFriendsEditor() }

        viewModel.getState().observe(this, this::render)
        viewModel.requestUser()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        paginator.onSaveInstanceState(outState)
        adapter.onSaveInstanceState(outState)
        scrollButtonHelper.onSaveInstanceState(outState)
    }

    override fun onViewStateRestored(savedInstanceState: Bundle?) {
        super.onViewStateRestored(savedInstanceState)
        paginator.onViewStateRestored(savedInstanceState)
        adapter.onRestoreInstanceState(savedInstanceState)
        scrollButtonHelper.onViewStateRestored(savedInstanceState)
    }

    override fun onBackPressed(): Boolean {
        viewModel.navigateBack()
        return true
    }

    override fun layoutRes(): Int = R.layout.fragment_friend_list

    private fun render(state: FriendListState) {
        if (state.hasLoadPage) paginator.hasLoadPage()
        layoutRefresh.isRefreshing = state.isInitialLoading
        state.friendList.let { adapter.items = it }
        if (state.isEmpty) {
            if (isNotMine) {
                textNotMineEmpty.visible()
            } else {
                layoutEmpty.visible()
                imageArrow.visible()
            }
        } else {
            layoutEmpty.gone()
            imageArrow.gone()
            textNotMineEmpty.gone()
        }
        if (state.emptyListError) layoutError.visible() else layoutError.gone()
        state.userName?.let { toolbar.getDescriptionText().text = it }
        state.event?.handle { errorDelegate.show(it.errorText) }
    }

    fun openDeleteDialog(friend: FriendAdapterViewType.Friend) {
        BaseDialog.show(
            fragmentManager = childFragmentManager,
            title = getString(R.string.delete_friend_question, friend.name),
            positiveText = getString(R.string.delete),
            okClick = { viewModel.breakFriendship(friend.id) }
        )
    }

    private fun setupForNotMineWishes() {
        if (isNotMine) {
            toolbar.getLeftButton().visible()
            toolbar.getLeftButton().setImageResource(R.drawable.ic_arrow_back)
            toolbar.getTitleText().text = getString(R.string.title_friends)
            toolbar.getDescriptionText().visible()
            buttonAdd.gone()
        } else {
            scrollButtonHelper.withRecyclerView(recyclerFriends, buttonAdd)
        }
    }
}