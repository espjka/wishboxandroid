package com.wishbox.feature.friendList.adapter.delegates

import android.os.Bundle

/**
 * Created by Pak Evgeniy on 08.12.2018
 */

interface DelegateWithInstanceState {
    fun onSaveInstanceState(outState: Bundle?)
    fun onRestoreInstanceState(savedInstanceState: Bundle?)
}