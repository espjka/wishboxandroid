package com.wishbox.feature.friendList.adapter.delegates

import android.os.Bundle
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.chauthai.swipereveallayout.ViewBinderHelper
import com.hannesdorfmann.adapterdelegates4.AbsListItemAdapterDelegate
import com.smedialink.common.ext.inflate
import com.wishbox.core.glide.loadImage
import com.wishbox.feature.friendList.R
import com.wishbox.feature.friendList.adapter.FriendAdapterViewType
import kotlinx.android.synthetic.main.item_friend.view.*

/**
 * Created by Pak Evgeniy on 08.12.2018
 */

class FriendAdapterDelegate(
    private val isNotMine: Boolean,
    private val friendClick: (FriendAdapterViewType.Friend) -> Unit,
    private val friendDeleteClick: (friend: FriendAdapterViewType.Friend) -> Unit,
    private val binderHelper: ViewBinderHelper
) :
    AbsListItemAdapterDelegate<FriendAdapterViewType.Friend, FriendAdapterViewType, FriendAdapterDelegate.ViewHolder>(),
    DelegateWithInstanceState {

    override fun onSaveInstanceState(outState: Bundle?) {
        binderHelper.saveStates(outState)
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle?) {
        binderHelper.restoreStates(savedInstanceState)
    }

    override fun onCreateViewHolder(parent: ViewGroup): ViewHolder =
        ViewHolder(parent.inflate(R.layout.item_friend), isNotMine, friendClick, friendDeleteClick)

    override fun isForViewType(
        item: FriendAdapterViewType,
        items: MutableList<FriendAdapterViewType>,
        position: Int
    ): Boolean = item is FriendAdapterViewType.Friend

    override fun onBindViewHolder(
        item: FriendAdapterViewType.Friend,
        holder: ViewHolder,
        payloads: MutableList<Any>
    ) {
        holder.bind(item, binderHelper)
    }

    class ViewHolder(
        view: View,
        private val isNotMine: Boolean,
        private val friendClick: (FriendAdapterViewType.Friend) -> Unit,
        private val friendDeleteClick: (friend: FriendAdapterViewType.Friend) -> Unit
    ) : RecyclerView.ViewHolder(view) {

        fun bind(item: FriendAdapterViewType.Friend, binderHelper: ViewBinderHelper) =
            with(itemView) {
                imageAvatar.loadImage(item.avatarUrl, placeholder = R.drawable.placeholder_avatar)
                if (isNotMine) {
                    layoutSwipeReveal.setLockDrag(true)
                } else {
                    binderHelper.bind(layoutSwipeReveal, "${item.id}")
                }
                imageAvatar.setOnClickListener { friendClick.invoke(item) }
                layoutItem.setOnClickListener { friendClick.invoke(item) }
                if (!isNotMine) layoutDelete.setOnClickListener { friendDeleteClick.invoke(item) }
                textName.text = item.name
            }
    }
}