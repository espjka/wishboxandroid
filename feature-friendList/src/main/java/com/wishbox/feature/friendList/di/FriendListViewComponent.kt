package com.wishbox.feature.friendList.di

import com.wishbox.core.di.api.FeatureProviderApi
import com.wishbox.core.di.api.InjectorApi
import com.wishbox.core.di.api.RouterApi
import com.wishbox.core.di.module.ViewModelModule
import com.wishbox.core.di.provider.FeatureProviderApiProvider
import com.wishbox.core.di.provider.InjectorApiProvider
import com.wishbox.core.di.provider.RouterApiProvider
import com.wishbox.core.di.scope.PerFragment
import com.wishbox.core.di.util.lookupApiProvider
import com.wishbox.feature.friendList.FriendListFragment
import dagger.BindsInstance
import dagger.Component

/**
 * Created by Pak Evgeniy on 08.12.2018
 */

@PerFragment
@Component(
    dependencies = [
        InjectorApi::class,
        RouterApi::class,
        FeatureProviderApi::class
    ],
    modules = [
        ViewModelModule::class,
        FriendListViewModule::class
    ]
)
interface FriendListViewComponent {

    fun inject(fragment: FriendListFragment)

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun bind(fragment: FriendListFragment): Builder

        fun api(api: InjectorApi): Builder

        fun api(api: RouterApi): Builder

        fun api(api: FeatureProviderApi): Builder

        fun build(): FriendListViewComponent
    }

    class Initializer private constructor() {
        companion object {

            fun init(fragment: FriendListFragment) {
                val injectorProvider = fragment.lookupApiProvider<InjectorApiProvider>()
                val routerProvider = fragment.lookupApiProvider<RouterApiProvider>()
                val featureProviderProvider = fragment.lookupApiProvider<FeatureProviderApiProvider>()

                DaggerFriendListViewComponent.builder()
                    .api(injectorProvider.injectorApi)
                    .api(routerProvider.routerApi)
                    .api(featureProviderProvider.featureProviderApi)
                    .bind(fragment)
                    .build()
                    .inject(fragment)
            }
        }
    }
}