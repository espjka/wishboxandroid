package com.wishbox.feature.friendList

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.smedialink.common.base.BaseViewModel
import com.smedialink.common.delegates.rx.disposableDelegate
import com.smedialink.common.error.ErrorHandler
import com.smedialink.common.ext.applySchedulers
import com.wishbox.core.di.screenproviders.FeatureProvider
import com.wishbox.core.domain.exception.LastPageException
import com.wishbox.feature.friendList.mapper.FriendViewMapper
import com.wishbox.feature.friendList.useCase.FriendListInteractor
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import ru.terrakok.cicerone.Router
import javax.inject.Inject

/**
 * Created by Pak Evgeniy on 08.12.2018
 */

class FriendListViewModel @Inject constructor(
    private val userId: Long?,
    private val router: Router,
    private val mapper: FriendViewMapper,
    private val errorHandler: ErrorHandler,
    private val interactor: FriendListInteractor,
    private val featureProvider: FeatureProvider
) : BaseViewModel() {

    private val state: MutableLiveData<FriendListState> by lazy { MutableLiveData<FriendListState>() }

    private var requestFriendListDisposable: Disposable? by disposableDelegate()
    private var observeFriendListDisposable: Disposable? by disposableDelegate()
    private var deleteFriendDisposable: Disposable? by disposableDelegate()
    private var getUserDisposable: Disposable? by disposableDelegate()

    fun getState(): LiveData<FriendListState> = state

    fun requestUser() {
        getUserDisposable = userId?.let { interactor.getUser(it) }
            ?.map { mapper.mapName(it) }
            ?.subscribeOn(Schedulers.io())
            ?.observeOn(AndroidSchedulers.mainThread())
            ?.subscribeBy(
                onError = { reduceState(FriendListStateChanges.Error(errorHandler.handle(it))) },
                onSuccess = { reduceState(FriendListStateChanges.UserName(it)) }
            )
            ?.addTo(disposables)
    }

    private fun observeMyFriendList() {
        if (observeFriendListDisposable != null) return
        observeFriendListDisposable = interactor.observeMyFriendList()
            .map { mapper.mapToViewTypeList(it) }
            .map { FriendListStateChanges.Data(it) }
            .applySchedulers()
            .subscribeBy(
                onError = { reduceState(FriendListStateChanges.Error(errorHandler.handle(it))) },
                onNext = { reduceState(it) }
            ).addTo(disposables)
    }

    fun loadNextPage(needToDrop: Boolean = false) {
        userId?.let { getFriendList(it, needToDrop) }
        ?: run { requestMyFriendList(needToDrop) }
    }

    private fun getFriendList(userId: Long, needToDrop: Boolean = false) {
        requestFriendListDisposable = interactor.getFriendList(userId, needToDrop)
            .map { mapper.mapToViewTypeList(it) }
            .map { FriendListStateChanges.Data(it) }
            .applySchedulers()
            .doOnSubscribe { reduceState(FriendListStateChanges.Loading(needToDrop)) }
            .subscribeBy(
                onError = { reduceState(FriendListStateChanges.Error(errorHandler.handle(it), needToDrop)) },
                onSuccess = { reduceState(it) }
            ).addTo(disposables)
    }

    private fun requestMyFriendList(needToDrop: Boolean = false) {
        requestFriendListDisposable = interactor.requestMyFriendList(needToDrop)
            .applySchedulers()
            .doOnSubscribe { reduceState(FriendListStateChanges.Loading(needToDrop)) }
            .subscribeBy(
                onError = {
                    when (it) {
                        is LastPageException -> reduceState(FriendListStateChanges.Success(false))
                        else -> reduceState(FriendListStateChanges.Error(errorHandler.handle(it), needToDrop))
                    }
                    observeMyFriendList()
                },
                onComplete = {
                    reduceState(FriendListStateChanges.Success(true))
                    observeMyFriendList()
                }
            ).addTo(disposables)
    }

    fun openProfile(id: Long) {
        router.navigateTo(featureProvider.getProfile(id))
    }

    fun openFriendsEditor() {
        router.navigateTo(featureProvider.getFriendsEditor())
    }

    private fun reduceState(changes: FriendListStateChanges) {
        val prevState = state.value ?: FriendListState()
        state.value = when (changes) {
            is FriendListStateChanges.Data -> changes.reduceState(prevState)
            is FriendListStateChanges.Error -> changes.reduceState(prevState)
            is FriendListStateChanges.Loading -> changes.reduceState(prevState)
            is FriendListStateChanges.Success -> changes.reduceState(prevState)
            is FriendListStateChanges.UserName -> changes.reduceState(prevState)
        }
    }

    fun navigateBack() {
        router.exit()
    }

    fun breakFriendship(id: Long) {
        deleteFriendDisposable = interactor.breakFriendship(id)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeBy(
                onError = { reduceState(FriendListStateChanges.Error(errorHandler.handle(it))) },
                onComplete = { }
            ).addTo(disposables)
    }
}