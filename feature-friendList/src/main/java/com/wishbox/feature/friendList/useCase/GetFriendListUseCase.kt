package com.wishbox.feature.friendList.useCase

import com.wishbox.core.domain.entity.UserEntity
import com.wishbox.core.domain.repository.UserRepository
import com.wishbox.core.domain.repository.UserSessionRepository
import com.wishbox.core.presentation.pagination.Paginator
import io.reactivex.Single
import javax.inject.Inject

/**
 * Created by Pak Evgeniy on 08.12.2018
 */

class GetFriendListUseCase @Inject constructor(
    private val repository: UserRepository,
    private val userSessionRepository: UserSessionRepository
) {

    var offset = 0

    operator fun invoke(userId:Long? = null, dropOffset: Boolean = false): Single<List<UserEntity>> {
        val id = userId ?: userSessionRepository.userId ?: throw IllegalArgumentException("No userId specified")
        if (dropOffset) offset = 0
        return repository.getFriendList(id, Paginator.LIMIT, offset)
            .doOnSuccess { offset += Paginator.LIMIT }
    }
}