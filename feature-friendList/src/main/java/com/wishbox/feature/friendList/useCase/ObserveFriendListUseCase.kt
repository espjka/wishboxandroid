package com.wishbox.feature.friendList.useCase

import com.wishbox.core.domain.entity.UserEntity
import com.wishbox.core.domain.repository.UserRepository
import io.reactivex.Flowable
import javax.inject.Inject

/**
 * Created by Pak Evgeniy on 19/12/2018.
 */

class ObserveFriendListUseCase @Inject constructor(
    private val repository: UserRepository
) {
    operator fun invoke(): Flowable<List<UserEntity>> {
        return repository.observeMyFriendList()
    }
}