package com.wishbox.feature.friendList.di

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.chauthai.swipereveallayout.ViewBinderHelper
import com.smedialink.common.error.ErrorHandler
import com.wishbox.core.di.mapkeys.ViewModelKey
import com.wishbox.core.di.screenproviders.FeatureProvider
import com.wishbox.core.presentation.errorHandling.MainErrorHandler
import com.wishbox.core.presentation.pagination.Paginator
import com.wishbox.feature.friendList.FriendListFragment
import com.wishbox.feature.friendList.FriendListViewModel
import com.wishbox.feature.friendList.adapter.FriendAdapter
import com.wishbox.feature.friendList.adapter.FriendDiffCallback
import com.wishbox.feature.friendList.mapper.FriendViewMapper
import com.wishbox.feature.friendList.useCase.FriendListInteractor
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.multibindings.IntoMap
import ru.terrakok.cicerone.Router

/**
 * Created by Pak Evgeniy on 08.12.2018
 */

@Module
abstract class FriendListViewModule {

    @Binds
    abstract fun provideErrorHandler(impl: MainErrorHandler): ErrorHandler

    @Module
    companion object {

        @Provides
        @IntoMap
        @ViewModelKey(FriendListViewModel::class)
        @JvmStatic
        fun provideViewModel(
            fragment: FriendListFragment,
            router: Router,
            mapper: FriendViewMapper,
            errorHandler: ErrorHandler,
            interactor: FriendListInteractor,
            featureProvider: FeatureProvider
        ): ViewModel =
            FriendListViewModel(
                userId = fragment.userId?.takeIf { it != 0L },
                interactor = interactor,
                router = router,
                errorHandler = errorHandler,
                mapper = mapper,
                featureProvider = featureProvider
            )

        @JvmStatic
        @Provides
        fun provideFriendListViewModel(
            fragment: FriendListFragment,
            factory: ViewModelProvider.Factory
        ): FriendListViewModel =
            ViewModelProviders.of(fragment, factory).get(FriendListViewModel::class.java)

        @Provides
        @JvmStatic
        fun provideBinderHelper(): ViewBinderHelper =
            ViewBinderHelper()
                .apply { setOpenOnlyOne(true) }

        @Provides
        @JvmStatic
        fun provideAdapter(
            diffCallback: FriendDiffCallback,
            viewModel: FriendListViewModel,
            fragment: FriendListFragment,
            binderHelper: ViewBinderHelper
        ): FriendAdapter =
            FriendAdapter(
                diffCallback = diffCallback,
                isNotMine = fragment.isNotMine,
                friendClick = { viewModel.openProfile(it.id) },
                friendDeleteClick = { fragment.openDeleteDialog(it) },
                errorClick = { viewModel.loadNextPage() },
                binderHelper = binderHelper
            )

        @Provides
        @JvmStatic
        fun providePaginationScrollDelegate(viewModel: FriendListViewModel): Paginator =
            Paginator { viewModel.loadNextPage() }
    }
}