package com.wishbox.feature.friendList.adapter

import android.os.Bundle
import com.chauthai.swipereveallayout.ViewBinderHelper
import com.hannesdorfmann.adapterdelegates4.AsyncListDifferDelegationAdapter
import com.wishbox.core.presentation.pagination.PaginationErrorAdapterDelegate
import com.wishbox.core.presentation.pagination.PaginationProgressAdapterDelegate
import com.wishbox.feature.friendList.adapter.delegates.DelegateWithInstanceState
import com.wishbox.feature.friendList.adapter.delegates.FriendAdapterDelegate


/**
 * Created by Pak Evgeniy on 08.12.2018
 */

class FriendAdapter(
    diffCallback: FriendDiffCallback,
    isNotMine: Boolean,
    friendClick: (friend: FriendAdapterViewType.Friend) -> Unit,
    friendDeleteClick: (friend: FriendAdapterViewType.Friend) -> Unit,
    errorClick: () -> Unit,
    binderHelper: ViewBinderHelper
) : AsyncListDifferDelegationAdapter<FriendAdapterViewType>(diffCallback) {

    init {
        delegatesManager
            .addDelegate(FriendAdapterDelegate(isNotMine, friendClick, friendDeleteClick, binderHelper))
            .addDelegate(PaginationProgressAdapterDelegate<FriendAdapterViewType.Loading, FriendAdapterViewType> { it is FriendAdapterViewType.Loading })
            .addDelegate(
                PaginationErrorAdapterDelegate<FriendAdapterViewType.Error, FriendAdapterViewType>(
                    isForViewType = { it is FriendAdapterViewType.Error },
                    errorClick = errorClick
                )
            )
    }

    fun onSaveInstanceState(outState: Bundle?) {
        getDelegateForState(0)?.onSaveInstanceState(outState)
    }

    fun onRestoreInstanceState(savedInstanceState: Bundle?) {
        getDelegateForState(0)?.onRestoreInstanceState(savedInstanceState)
    }

    private fun getDelegateForState(viewType: Int): DelegateWithInstanceState? =
        delegatesManager.getDelegateForViewType(viewType) as? DelegateWithInstanceState
}