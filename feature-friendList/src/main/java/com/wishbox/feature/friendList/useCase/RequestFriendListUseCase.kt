package com.wishbox.feature.friendList.useCase

import com.wishbox.core.domain.repository.UserRepository
import com.wishbox.core.presentation.pagination.Paginator
import io.reactivex.Completable
import javax.inject.Inject

/**
 * Created by Pak Evgeniy on 08.12.2018
 */

class RequestFriendListUseCase @Inject constructor(
    private val repository: UserRepository
) {

    var offset = 0

    operator fun invoke(dropOffset: Boolean = false): Completable {
        if (dropOffset) offset = 0
        return repository.requestMyFriendList(Paginator.LIMIT, offset)
            .doOnComplete { offset += Paginator.LIMIT }
    }
}