package com.wishbox.feature.friendList.useCase

import com.wishbox.core.domain.entity.UserEntity
import com.wishbox.core.domain.repository.FriendshipRepository
import com.wishbox.core.domain.repository.UserRepository
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Single
import javax.inject.Inject

/**
 * Created by Pak Evgeniy on 29.12.2018
 */

class FriendListInteractor @Inject constructor(
    private val get: GetFriendListUseCase,
    private val request: RequestFriendListUseCase,
    private val observe: ObserveFriendListUseCase,
    private val userRepository: UserRepository,
    private val friendshipRepository: FriendshipRepository
) {

    fun getFriendList(userId: Long? = null, dropOffset: Boolean = false): Single<List<UserEntity>> =
            get.invoke(userId, dropOffset)

    fun observeMyFriendList(): Flowable<List<UserEntity>> = observe.invoke()

    fun requestMyFriendList(dropOffset: Boolean = false): Completable = request.invoke(dropOffset)

    fun getUser(userId: Long): Single<UserEntity> = userRepository.getUser(userId)

    fun breakFriendship(userId: Long): Completable = friendshipRepository.breakFriendship(userId)
}