package com.wishbox.feature.friendList

import com.smedialink.common.error.ErrorState
import com.smedialink.common.events.ErrorEvent
import com.wishbox.feature.friendList.adapter.FriendAdapterViewType

/**
 * Created by Pak Evgeniy on 08.12.2018
 */

data class FriendListState(
    val friendList: List<FriendAdapterViewType> = emptyList(),
    val isEmpty: Boolean = false,
    val userName: String? = null,
    val isInitialLoading: Boolean = false,
    val hasLoadPage: Boolean = false,
    val event: ErrorEvent? = null,
    val emptyListError: Boolean = false
)

sealed class FriendListStateChanges {
    class Loading(val isForceLoading: Boolean) : FriendListStateChanges()
    class Success(val hasLoadPage: Boolean) : FriendListStateChanges()
    class Data(val friendList: List<FriendAdapterViewType>) : FriendListStateChanges()
    class Error(val error: ErrorState, val isForceLoading: Boolean = false) :
        FriendListStateChanges()
    class UserName(val userName: String) : FriendListStateChanges()
}

fun FriendListStateChanges.Data.reduceState(prevState: FriendListState): FriendListState =
    FriendListState(
        friendList = friendList,
        isEmpty = friendList.isEmpty() && prevState.emptyListError.not(),
        emptyListError = friendList.isEmpty() && prevState.emptyListError
    )

fun FriendListStateChanges.UserName.reduceState(prevState: FriendListState): FriendListState =
    prevState.copy(userName = userName)


fun FriendListStateChanges.Error.reduceState(prevState: FriendListState): FriendListState =
    prevState.copy(
        friendList = prevState.friendList.toMutableList()
            .apply {
                remove(FriendAdapterViewType.Loading)
                if (isForceLoading.not() && isNotEmpty() && !contains(FriendAdapterViewType.Error)) {
                    add(FriendAdapterViewType.Error)
                }
            },
        event = error.event,
        emptyListError = prevState.friendList.isNullOrEmpty(),
        isEmpty = false,
        isInitialLoading = false,
        hasLoadPage = false
    )

fun FriendListStateChanges.Loading.reduceState(prevState: FriendListState): FriendListState =
    if (isForceLoading || prevState.friendList.isNullOrEmpty()) {
        prevState.copy(isInitialLoading = true, hasLoadPage = false,
            friendList = prevState.friendList.toMutableList()
                .apply {
                    remove(FriendAdapterViewType.Loading)
                })
    } else {
        prevState.copy(friendList = prevState.friendList.toMutableList()
            .apply {
                remove(FriendAdapterViewType.Error)
                if (!contains(FriendAdapterViewType.Loading)) add(FriendAdapterViewType.Loading)
            })
    }

fun FriendListStateChanges.Success.reduceState(prevState: FriendListState): FriendListState =
    prevState.copy(
        isInitialLoading = false,
        emptyListError = false,
        hasLoadPage = hasLoadPage,
        friendList = prevState.friendList.toMutableList()
            .apply { remove(FriendAdapterViewType.Loading) },
        isEmpty = prevState.friendList.isEmpty()
    )