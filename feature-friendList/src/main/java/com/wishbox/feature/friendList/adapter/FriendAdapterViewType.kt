package com.wishbox.feature.friendList.adapter

import android.os.Parcel
import android.os.Parcelable

/**
 * Created by Pak Evgeniy on 08.12.2018
 */

sealed class FriendAdapterViewType {

    data class Friend(
        val id: Long,
        val name: String,
        val avatarUrl: String? = null
    ): FriendAdapterViewType(), Parcelable {

        constructor(parcel: Parcel) : this(
            parcel.readLong(),
            parcel.readString() ?: "",
            parcel.readString()
        )

        override fun writeToParcel(dest: Parcel?, flags: Int) {
            dest?.run {
                writeLong(id)
                writeString(name)
                writeString(avatarUrl)
            }
        }

        override fun describeContents(): Int = 0

        companion object CREATOR : Parcelable.Creator<Friend> {
            override fun createFromParcel(parcel: Parcel): Friend {
                return Friend(parcel)
            }

            override fun newArray(size: Int): Array<Friend?> {
                return arrayOfNulls(size)
            }
        }
    }

    object Loading : FriendAdapterViewType()

    object Error : FriendAdapterViewType()
}