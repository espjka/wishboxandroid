package com.wishbox.feature.friendList.mapper

import android.content.Context
import com.wishbox.core.domain.entity.UserEntity
import com.wishbox.feature.friendList.R
import com.wishbox.feature.friendList.adapter.FriendAdapterViewType
import javax.inject.Inject

/**
 * Created by Pak Evgeniy on 08.12.2018
 */

class FriendViewMapper @Inject constructor(
    private val context: Context
) {

    fun mapToViewTypeList(from: List<UserEntity>): List<FriendAdapterViewType> =
        from.map { mapToViewType(it) }

    private fun mapToViewType(from: UserEntity): FriendAdapterViewType =
        FriendAdapterViewType.Friend(
            id = from.id,
            name = manageName(from.firstName, from.lastName),
            avatarUrl = from.avatar?.small
        )

    fun mapName(user: UserEntity): String = manageName(user.firstName, user.lastName)

    private fun manageName(firstName: String?, lastName: String?): String =
        "${firstName.orEmpty()} ${lastName.orEmpty()}"
            .takeIf { it.isNotBlank() } ?: context.getString(R.string.not_specified)
}