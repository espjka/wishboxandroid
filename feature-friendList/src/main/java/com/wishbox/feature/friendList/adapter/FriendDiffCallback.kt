package com.wishbox.feature.friendList.adapter

import androidx.recyclerview.widget.DiffUtil
import javax.inject.Inject

/**
 * Created by Pak Evgeniy on 08.12.2018
 */

class FriendDiffCallback @Inject constructor() : DiffUtil.ItemCallback<FriendAdapterViewType>() {

    override fun areItemsTheSame(
        oldItem: FriendAdapterViewType,
        newItem: FriendAdapterViewType
    ): Boolean {
        if (oldItem.javaClass != newItem.javaClass) return false
        return when (oldItem) {
            is FriendAdapterViewType.Friend -> oldItem.id == (newItem as FriendAdapterViewType.Friend).id
            FriendAdapterViewType.Loading -> true
            FriendAdapterViewType.Error -> true
        }
    }

    override fun areContentsTheSame(
        oldItem: FriendAdapterViewType,
        newItem: FriendAdapterViewType
    ): Boolean = oldItem == newItem
}