package com.wishbox.feature.managePictures.adapter.delegates

import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.hannesdorfmann.adapterdelegates4.AbsListItemAdapterDelegate
import com.smedialink.common.ext.inflate
import com.wishbox.core.glide.loadImage
import com.wishbox.feature.managePictures.R
import com.wishbox.feature.managePictures.adapter.PictureAdapterViewType
import kotlinx.android.synthetic.main.item_picture_card.view.*

/**
 * Created by Pak Evgeniy on 12.12.2018
 */

class PictureAdapterDelegate(
    private val click: (PictureAdapterViewType.Picture) -> Unit
) :
    AbsListItemAdapterDelegate<PictureAdapterViewType.Picture, PictureAdapterViewType, PictureAdapterDelegate.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup): ViewHolder =
        ViewHolder(parent.inflate(R.layout.item_picture_card), click)

    override fun isForViewType(
        item: PictureAdapterViewType,
        items: MutableList<PictureAdapterViewType>,
        position: Int
    ): Boolean = item is PictureAdapterViewType.Picture

    override fun onBindViewHolder(
        item: PictureAdapterViewType.Picture,
        holder: ViewHolder,
        payloads: MutableList<Any>
    ) {
        holder.bind(item)
    }

    class ViewHolder(
        view: View,
        private val click: (PictureAdapterViewType.Picture) -> Unit
    ) : RecyclerView.ViewHolder(view) {

        fun bind(item: PictureAdapterViewType.Picture) =
            with(itemView) {
                layoutCard.setOnClickListener { click.invoke(item) }
                imagePhoto.loadImage(item.link)
            }
    }
}