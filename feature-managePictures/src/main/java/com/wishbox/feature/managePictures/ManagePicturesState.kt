package com.wishbox.feature.managePictures

import com.smedialink.common.error.ErrorState
import com.wishbox.feature.managePictures.adapter.PictureAdapterViewType

/**
 * Created by Pak Evgeniy on 13.12.2018
 */

data class ManagePicturesState(
    val isLoading: Boolean = false,
    val payload: List<PictureAdapterViewType> = emptyList(),
    val errorState: ErrorState? = null
) {
    companion object {
        fun initial(): ManagePicturesState = ManagePicturesState()
        fun data(payload: List<PictureAdapterViewType>): ManagePicturesState = ManagePicturesState(payload = payload)
    }
}