package com.wishbox.feature.managePictures.mapper

import com.wishbox.core.domain.entity.PictureEntity
import com.wishbox.core.presentation.pictures.PicturesManager.Companion.MAX_PICTURES_AMOUNT
import com.wishbox.feature.managePictures.adapter.PictureAdapterViewType
import javax.inject.Inject

/**
 * Created by Pak Evgeniy on 22.12.2018
 */

class ManagePicturesViewMapper @Inject constructor() {

    fun mapListFromEntity(from: List<PictureEntity>): List<PictureAdapterViewType> {
        val outPictures: MutableList<PictureAdapterViewType> = mutableListOf()
        outPictures.addAll(from.map { map(it) })
        if (from.size < MAX_PICTURES_AMOUNT) outPictures.add(PictureAdapterViewType.AddPicture)
        return outPictures
    }

    fun mapListFromString(from: List<String>): List<PictureAdapterViewType> {
        val outPictures: MutableList<PictureAdapterViewType> = mutableListOf()
        outPictures.addAll(from.map {
            PictureAdapterViewType.Picture(
                key = it,
                link = it
            )
        })
        if (from.size < MAX_PICTURES_AMOUNT) outPictures.add(PictureAdapterViewType.AddPicture)
        return outPictures
    }


    fun map(from: PictureEntity): PictureAdapterViewType.Picture =
        PictureAdapterViewType.Picture(
            key = from.key,
            link = from.medium
        )
}