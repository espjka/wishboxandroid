package com.wishbox.feature.managePictures.viewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.smedialink.common.base.BaseViewModel
import com.smedialink.common.delegates.rx.disposableDelegate
import com.wishbox.core.presentation.pictures.PhotoHelper
import com.wishbox.feature.managePictures.ManagePicturesState
import io.reactivex.disposables.Disposable
import ru.terrakok.cicerone.Router

/**
 * Created by Pak Evgeniy on 24/12/2018.
 */

abstract class ManagePicturesViewModel(
    private val router: Router
) : BaseViewModel() {

    protected var photoHelper: PhotoHelper? = null

    protected var deletePictureDisposable: Disposable? by disposableDelegate()
    protected var makePictureMainDisposable: Disposable? by disposableDelegate()
    protected var getPictureDisposable: Disposable? by disposableDelegate()
    protected var observePicturesDisposable: Disposable? by disposableDelegate()

    fun attachPhotoHelper(photoHelper: PhotoHelper) {
        this.photoHelper = photoHelper
    }

    protected val state: MutableLiveData<ManagePicturesState> by lazy {
        MutableLiveData<ManagePicturesState>().apply { value = ManagePicturesState.initial() }
    }

    fun getState(): LiveData<ManagePicturesState> = state

    fun navigateBack() {
        router.exit()
    }

    abstract fun observePictures()
    abstract fun getPictureFromCamera()
    abstract fun getPicturesFromGallery()
    abstract fun deletePicture(key: String)
    abstract fun makePictureMain(key: String)
}