package com.wishbox.feature.managePictures

import android.os.Bundle
import android.view.View
import androidx.core.os.bundleOf
import androidx.recyclerview.widget.GridLayoutManager
import com.smedialink.common.events.handle
import com.smedialink.common.ext.observe
import com.wishbox.core.presentation.base.BaseBottomSheetDialog
import com.wishbox.core.presentation.base.BaseFragment
import com.wishbox.core.presentation.errorHandling.delegate.ToastDelegate
import com.wishbox.core.presentation.loading.ProgressLoadingDelegate
import com.wishbox.core.presentation.pictures.PhotoHelper
import com.wishbox.feature.managePictures.adapter.PictureAdapter
import com.wishbox.feature.managePictures.di.ManagePicturesViewComponent
import com.wishbox.feature.managePictures.viewModel.ManagePicturesViewModel
import kotlinx.android.synthetic.main.fragment_manage_pictures.*
import javax.inject.Inject

/**
 * Created by Pak Evgeniy on 12.12.2018
 */

class ManagePicturesFragment : BaseFragment<ManagePicturesViewModel>() {

    @Inject
    lateinit var toastDelegate: ToastDelegate

    @Inject
    lateinit var adapter: PictureAdapter

    @Inject
    lateinit var photoHelper: PhotoHelper

    private val progressLoadingDelegate: ProgressLoadingDelegate by lazy {
        return@lazy ProgressLoadingDelegate(context!!, "Сохранение...")
    }

    companion object {
        const val BUNDLE_WISH_ID = "BUNDLE_WISH_ID"

        fun newInstance(wishId: Long? = null): ManagePicturesFragment =
                ManagePicturesFragment()
                    .apply { arguments = bundleOf(BUNDLE_WISH_ID to wishId) }
    }

    override fun injectDependencies() {
        ManagePicturesViewComponent.inject(this)
    }

    override fun layoutRes(): Int = R.layout.fragment_manage_pictures

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        toolbar.getLeftButton().setOnClickListener { viewModel.navigateBack() }
        initRecyclerView()
        viewModel.attachPhotoHelper(photoHelper)
        viewModel.getState().observe(this, this::render)
        viewModel.observePictures()
    }

    private fun render(state: ManagePicturesState) {
        progressLoadingDelegate.showLoading(state.isLoading)
        adapter.items = state.payload
        state.errorState?.event?.handle { toastDelegate.show(it.errorText) }
    }

    fun openAddPhotoDialog() {
        BaseBottomSheetDialog.show(
            childFragmentManager,
            getString(R.string.dialog_camera),
            getString(R.string.dialog_gallery),
            {
                viewModel.getPictureFromCamera()
                it.dismiss()
            },
            {
                viewModel.getPicturesFromGallery()
                it.dismiss()
            }
        )
    }

    fun openManagePictureDialog(url: String) {
        BaseBottomSheetDialog.show(
            childFragmentManager,
            getString(R.string.delete),
            getString(R.string.dialog_make_main),
            {
                viewModel.deletePicture(url)
                it.dismiss()
            },
            {
                viewModel.makePictureMain(url)
                it.dismiss()
            }
        )
    }

    private fun initRecyclerView() {
        with(recyclerPictures) {
            adapter = this@ManagePicturesFragment.adapter
            layoutManager = GridLayoutManager(this@ManagePicturesFragment.context, 2)
        }
    }
}