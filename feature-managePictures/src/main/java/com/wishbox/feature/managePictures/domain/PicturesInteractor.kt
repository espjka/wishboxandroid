package com.wishbox.feature.managePictures.domain

import com.wishbox.core.domain.entity.PictureEntity
import com.wishbox.core.domain.repository.PictureRepository
import com.wishbox.core.domain.repository.WishRepository
import io.reactivex.Completable
import io.reactivex.Flowable
import java.io.File
import javax.inject.Inject

/**
 * Created by Pak Evgeniy on 22.12.2018
 */

class PicturesInteractor @Inject constructor(
    private val wishId: Long,
    private val wishRepository: WishRepository,
    private val repository: PictureRepository
) {

    fun observePictures(): Flowable<List<PictureEntity>> =
        repository.observePictures(wishId)

    fun uploadPicture(file: File): Completable =
        repository.uploadPicture(wishId, file)
            .flatMapCompletable {
                repository.getMainPictureKey(wishId)
                    ?.let { Completable.complete() }
                    ?: wishRepository.updateMainPicture(wishId, it)
            }

    fun makePictureMain(key: String): Completable =
        repository.makePictureMain(wishId, key)
            .andThen(wishRepository.updateMainPicture(wishId, key))

    fun deletePicture(key: String): Completable =
        repository.deletePicture(wishId, key)
            .toSingle { repository.getMainPictureKey(wishId).orEmpty() }
            .flatMapCompletable {
                it.takeIf { it.isNotBlank() }?.let { Completable.complete() }
                    ?: wishRepository.updateMainPicture(
                        wishId,
                        repository.getFristPictureKey(wishId)
                    )
            }
}