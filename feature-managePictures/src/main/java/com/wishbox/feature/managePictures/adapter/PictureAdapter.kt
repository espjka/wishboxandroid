package com.wishbox.feature.managePictures.adapter

import com.hannesdorfmann.adapterdelegates4.AsyncListDifferDelegationAdapter
import com.wishbox.feature.managePictures.adapter.delegates.AddPictureAdapterDelegate
import com.wishbox.feature.managePictures.adapter.delegates.PictureAdapterDelegate
import javax.inject.Inject

/**
 * Created by Pak Evgeniy on 12.12.2018
 */

class PictureAdapter @Inject constructor(
    diffCallback: PictureDiffCallback,
    pictureClick: (picture: PictureAdapterViewType.Picture) -> Unit,
    addPictureClick: () -> Unit
) : AsyncListDifferDelegationAdapter<PictureAdapterViewType>(diffCallback) {

    init {
        delegatesManager
            .addDelegate(PictureAdapterDelegate(pictureClick))
            .addDelegate(AddPictureAdapterDelegate(addPictureClick))
    }
}