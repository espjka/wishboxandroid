package com.wishbox.feature.managePictures.viewModel

import com.smedialink.common.error.ErrorHandler
import com.smedialink.common.ext.applySchedulers
import com.smedialink.common.ext.reduceState
import com.wishbox.feature.managePictures.ManagePicturesState
import com.wishbox.feature.managePictures.domain.PicturesInteractor
import com.wishbox.feature.managePictures.mapper.ManagePicturesViewMapper
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import ru.terrakok.cicerone.Router
import java.io.File
import javax.inject.Inject

/**
 * Created by Pak Evgeniy on 12.12.2018
 */

class ManagePicturesEditViewModel @Inject constructor(
    router: Router,
    private val mapper: ManagePicturesViewMapper,
    private val errorHandler: ErrorHandler,
    private val interactor: PicturesInteractor
) : ManagePicturesViewModel(router) {

    override fun observePictures() {
        observePicturesDisposable = interactor.observePictures()
            .map { mapper.mapListFromEntity(it) }
            .applySchedulers()
            .subscribeBy(
                onError = { throwable ->
                    state.reduceState {
                        ManagePicturesState(
                            payload = it.payload,
                            errorState = errorHandler.handle(throwable)
                        )
                    }
                },
                onNext = { state.value = ManagePicturesState.data(it) }
            ).addTo(disposables)
    }

    override fun getPictureFromCamera() {
        getPictureDisposable = photoHelper?.getFromCamera().uploadPicture()
    }

    override fun getPicturesFromGallery() {
        getPictureDisposable = photoHelper?.getFromGallery().uploadPicture()
    }

    private fun Observable<File>?.uploadPicture(): Disposable? =
        this?.firstElement()
            ?.applySchedulers()
            ?.doOnSuccess { state.reduceState { it.copy(isLoading = true, errorState = null) } }
            ?.observeOn(Schedulers.io())
            ?.flatMapCompletable(interactor::uploadPicture)
            ?.observeOn(AndroidSchedulers.mainThread())
            ?.subscribeBy(
                onError = { throwable ->
                    state.reduceState {
                        ManagePicturesState(
                            payload = it.payload,
                            errorState = errorHandler.handle(throwable)
                        )
                    }
                },
                onComplete = { state.reduceState { it.copy(isLoading = false, errorState = null) } }
            )?.addTo(disposables)

    override fun deletePicture(key: String) {
        deletePictureDisposable = interactor.deletePicture(key)
            .applySchedulers()
            .doOnSubscribe { state.reduceState { it.copy(isLoading = true, errorState = null) } }
            .subscribeBy(
                onError = { throwable ->
                    state.reduceState {
                        ManagePicturesState(
                            payload = it.payload,
                            errorState = errorHandler.handle(throwable)
                        )
                    }
                },
                onComplete = { state.reduceState { it.copy(isLoading = false, errorState = null) } }
            ).addTo(disposables)
    }

    override fun makePictureMain(key: String) {
        makePictureMainDisposable = interactor.makePictureMain(key)
            .applySchedulers()
            .doOnSubscribe { state.reduceState { it.copy(isLoading = true, errorState = null) } }
            .subscribeBy(
                onError = { throwable ->
                    state.reduceState {
                        ManagePicturesState(
                            payload = it.payload,
                            errorState = errorHandler.handle(throwable)
                        )
                    }
                },
                onComplete = { state.reduceState { it.copy(isLoading = false, errorState = null) } }
            ).addTo(disposables)
    }
}