package com.wishbox.feature.managePictures.viewModel

import com.smedialink.common.error.ErrorHandler
import com.smedialink.common.ext.applySchedulers
import com.smedialink.common.ext.reduceState
import com.wishbox.core.presentation.pictures.PicturesManager
import com.wishbox.feature.managePictures.ManagePicturesState
import com.wishbox.feature.managePictures.mapper.ManagePicturesViewMapper
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import ru.terrakok.cicerone.Router
import javax.inject.Inject

/**
 * Created by Pak Evgeniy on 12.12.2018
 */

class ManagePicturesCreateViewModel @Inject constructor(
    router: Router,
    private val mapper: ManagePicturesViewMapper,
    private val picturesManager: PicturesManager,
    private val errorHandler: ErrorHandler
) : ManagePicturesViewModel(router) {

    override fun observePictures() {
        observePicturesDisposable = picturesManager.observePictures()
            .map { mapper.mapListFromString(it) }
            .applySchedulers()
            .subscribeBy(
                onError = { throwable ->
                    state.reduceState {
                        ManagePicturesState(
                            payload = it.payload,
                            errorState = errorHandler.handle(throwable)
                        )
                    }
                },
                onNext = { state.value = ManagePicturesState.data(it) }
            ).addTo(disposables)
    }

    override fun getPictureFromCamera() {
        if (picturesManager.canAddMore().not()) return
        getPictureDisposable = photoHelper?.getFromCamera()
            ?.applySchedulers()
            ?.subscribeBy(
                onError = { throwable ->
                    state.reduceState {
                        ManagePicturesState(
                            payload = it.payload,
                            errorState = errorHandler.handle(throwable)
                        )
                    }
                },
                onNext = { picturesManager.addLocalPictures(it.path) }
            )?.addTo(disposables)
    }

    override fun getPicturesFromGallery() {
        if (picturesManager.canAddMore().not()) return
        getPictureDisposable = photoHelper?.getFromGalleryMultiple()
            ?.applySchedulers()
            ?.subscribeBy(
                onError = { throwable ->
                    state.reduceState {
                        ManagePicturesState(
                            payload = it.payload,
                            errorState = errorHandler.handle(throwable)
                        )
                    }
                },
                onNext = { picturesManager.addLocalPictures(*it.map { it.path }.toTypedArray()) }
            )?.addTo(disposables)
    }

    override fun deletePicture(key: String) {
        picturesManager.deletePicture(key)
    }

    override fun makePictureMain(key: String) {
        picturesManager.makePictureMain(key)
    }
}