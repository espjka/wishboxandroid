package com.wishbox.feature.managePictures.di

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.smedialink.common.error.ErrorHandler
import com.smedialink.common.ext.orZero
import com.wishbox.core.di.mapkeys.ViewModelKey
import com.wishbox.core.domain.repository.PictureRepository
import com.wishbox.core.domain.repository.WishRepository
import com.wishbox.core.presentation.pictures.PhotoHelper
import com.wishbox.core.presentation.pictures.PicturesManager
import com.wishbox.feature.managePictures.ManagePicturesFragment
import com.wishbox.feature.managePictures.adapter.PictureAdapter
import com.wishbox.feature.managePictures.adapter.PictureDiffCallback
import com.wishbox.feature.managePictures.domain.PicturesInteractor
import com.wishbox.feature.managePictures.error.ManagePicturesErrorHandler
import com.wishbox.feature.managePictures.mapper.ManagePicturesViewMapper
import com.wishbox.feature.managePictures.viewModel.ManagePicturesCreateViewModel
import com.wishbox.feature.managePictures.viewModel.ManagePicturesEditViewModel
import com.wishbox.feature.managePictures.viewModel.ManagePicturesViewModel
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.multibindings.IntoMap
import ru.terrakok.cicerone.Router

/**
 * Created by Pak Evgeniy on 12.12.2018
 */

@Module
abstract class ManagePicturesViewModule {

    @Binds
    abstract fun provideErrorHandler(impl: ManagePicturesErrorHandler): ErrorHandler

    @Module
    companion object {

        @Provides
        @JvmStatic
        fun provideWishId(fragment: ManagePicturesFragment): Long? =
            fragment.arguments?.getLong(ManagePicturesFragment.BUNDLE_WISH_ID)?.takeIf { it != 0L }

        @Provides
        @JvmStatic
        fun providePhotoHelper(fragment: ManagePicturesFragment): PhotoHelper = PhotoHelper(fragment)

        @Provides
        @JvmStatic
        fun provideAdapter(
            diffCallback: PictureDiffCallback,
            fragment: ManagePicturesFragment
        ): PictureAdapter = PictureAdapter(
            diffCallback = diffCallback,
            pictureClick = { fragment.openManagePictureDialog(it.key) },
            addPictureClick = { fragment.openAddPhotoDialog() }
        )

        @Provides
        @JvmStatic
        fun providePictureInteractor(
            wishId: Long?,
            wishRepository: WishRepository,
            picturesRepository: PictureRepository
        ): PicturesInteractor =
            PicturesInteractor(
                wishId.orZero(),
                wishRepository,
                picturesRepository
            )

        @Provides
        @IntoMap
        @ViewModelKey(ManagePicturesEditViewModel::class)
        @JvmStatic
        fun provideManagePictureEditViewModel(
            router: Router,
            errorHandler: ErrorHandler,
            mapper: ManagePicturesViewMapper,
            picturesInteractor: PicturesInteractor
        ): ViewModel =
            ManagePicturesEditViewModel(
                router,
                mapper,
                errorHandler,
                picturesInteractor
            )

        @Provides
        @IntoMap
        @ViewModelKey(ManagePicturesCreateViewModel::class)
        @JvmStatic
        fun provideManagePictureCreateViewModel(
            router: Router,
            mapper: ManagePicturesViewMapper,
            picturesManager: PicturesManager,
            errorHandler: ErrorHandler
        ): ViewModel =
            ManagePicturesCreateViewModel(
                router,
                mapper,
                picturesManager,
                errorHandler
            )

        @Provides
        @JvmStatic
        fun provideViewModel(
            wishId: Long?,
            fragment: ManagePicturesFragment,
            factory: ViewModelProvider.Factory
        ): ManagePicturesViewModel =
            if (wishId == null) {
                ViewModelProviders.of(fragment, factory)
                    .get(ManagePicturesCreateViewModel::class.java)
            } else {
                ViewModelProviders.of(fragment, factory)
                    .get(ManagePicturesEditViewModel::class.java)
            }
    }
}