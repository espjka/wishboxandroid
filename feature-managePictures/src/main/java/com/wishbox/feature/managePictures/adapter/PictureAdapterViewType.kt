package com.wishbox.feature.managePictures.adapter

/**
 * Created by Pak Evgeniy on 12.12.2018
 */

sealed class PictureAdapterViewType {
    data class Picture(val key: String,
                       val link: String): PictureAdapterViewType()
    object AddPicture: PictureAdapterViewType()
}