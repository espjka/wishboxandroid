package com.wishbox.feature.managePictures.error

import com.wishbox.core.presentation.errorHandling.MainErrorHandler
import com.wishbox.core.presentation.pictures.PermissionsDeniedException
import com.wishbox.core.presentation.resources.ResourceProvider
import com.wishbox.feature.managePictures.R
import javax.inject.Inject

/**
 * Created by Pak Evgeniy on 09.01.2019
 */

class ManagePicturesErrorHandler @Inject constructor(
    resourceProvider: ResourceProvider
) : MainErrorHandler(resourceProvider) {

    init {
        addError(PermissionsDeniedException::class.java) { createDefaultState(R.string.error_permissions) }
    }
}