package com.wishbox.feature.managePictures.di

import com.wishbox.core.di.provider.InjectorApiProvider
import com.wishbox.core.di.api.InjectorApi
import com.wishbox.core.di.api.RouterApi
import com.wishbox.core.di.module.ViewModelModule
import com.wishbox.core.di.provider.RouterApiProvider
import com.wishbox.core.di.scope.PerFragment
import com.wishbox.core.di.util.lookupApiProvider
import com.wishbox.core.presentation.di.PictureManagerApi
import com.wishbox.core.presentation.di.PictureManagerProvider
import com.wishbox.feature.managePictures.ManagePicturesFragment
import dagger.BindsInstance
import dagger.Component

/**
 * Created by Pak Evgeniy on 12.12.2018
 */

@PerFragment
@Component(
    dependencies = [
        InjectorApi::class,
        RouterApi::class,
        PictureManagerApi::class
    ],
    modules = [
        ViewModelModule::class,
        ManagePicturesViewModule::class
    ]
)
interface ManagePicturesViewComponent {

    fun inject(fragment: ManagePicturesFragment)

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun bind(fragment: ManagePicturesFragment): Builder

        fun api(api: InjectorApi): Builder

        fun api(api: RouterApi): Builder

        fun api(api: PictureManagerApi): Builder

        fun build() : ManagePicturesViewComponent
    }

    companion object {
        fun inject(fragment: ManagePicturesFragment) {
            val injectorProvider = fragment.lookupApiProvider<InjectorApiProvider>()
            val routerProvider = fragment.lookupApiProvider<RouterApiProvider>()
            val pictureManagerProvider = fragment.lookupApiProvider<PictureManagerProvider>()

            DaggerManagePicturesViewComponent.builder()
                .api(injectorProvider.injectorApi)
                .api(routerProvider.routerApi)
                .api(pictureManagerProvider.pictureManagerApi)
                .bind(fragment)
                .build()
                .inject(fragment)
        }
    }
}