package com.wishbox.feature.managePictures.adapter

import androidx.recyclerview.widget.DiffUtil
import javax.inject.Inject

/**
 * Created by Pak Evgeniy on 12.12.2018
 */

class PictureDiffCallback @Inject constructor() : DiffUtil.ItemCallback<PictureAdapterViewType>() {

    override fun areItemsTheSame(
        oldItem: PictureAdapterViewType,
        newItem: PictureAdapterViewType
    ): Boolean {
        if (oldItem.javaClass != newItem.javaClass) return false
        return when (oldItem) {
            is PictureAdapterViewType.Picture -> oldItem.key == (newItem as PictureAdapterViewType.Picture).key
            PictureAdapterViewType.AddPicture -> true
        }
    }

    override fun areContentsTheSame(
        oldItem: PictureAdapterViewType,
        newItem: PictureAdapterViewType
    ): Boolean = oldItem == newItem
}