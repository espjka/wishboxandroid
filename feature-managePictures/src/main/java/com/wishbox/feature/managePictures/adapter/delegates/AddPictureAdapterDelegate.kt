package com.wishbox.feature.managePictures.adapter.delegates

import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.hannesdorfmann.adapterdelegates4.AbsListItemAdapterDelegate
import com.smedialink.common.ext.inflate
import com.wishbox.feature.managePictures.R
import com.wishbox.feature.managePictures.adapter.PictureAdapterViewType

/**
 * Created by Pak Evgeniy on 12.12.2018
 */

class AddPictureAdapterDelegate(
    private val click: () -> Unit
) :
    AbsListItemAdapterDelegate<PictureAdapterViewType.AddPicture, PictureAdapterViewType, AddPictureAdapterDelegate.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup): ViewHolder =
        ViewHolder(
            parent.inflate(R.layout.item_add_picture),
            click
        )

    override fun isForViewType(
        item: PictureAdapterViewType,
        items: MutableList<PictureAdapterViewType>,
        position: Int
    ): Boolean = item is PictureAdapterViewType.AddPicture

    override fun onBindViewHolder(
        item: PictureAdapterViewType.AddPicture,
        holder: ViewHolder,
        payloads: MutableList<Any>
    ) {
        holder.bind()
    }

    class ViewHolder(
        view: View,
        private val click: () -> Unit
    ) : RecyclerView.ViewHolder(view) {
        fun bind() = itemView.setOnClickListener { click.invoke() }
    }
}