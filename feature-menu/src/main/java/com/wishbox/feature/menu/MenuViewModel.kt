package com.wishbox.feature.menu

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.smedialink.common.base.BaseViewModel
import com.smedialink.common.delegates.rx.disposableDelegate
import com.smedialink.common.ext.applySchedulers
import com.wishbox.core.di.screenproviders.FeatureProvider
import com.wishbox.core.domain.entity.ProfileEntity
import com.wishbox.feature.menu.mapper.MenuProfileViewMapper
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.disposables.Disposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import ru.terrakok.cicerone.Router

/**
 * Created by Pak Evgeniy on 21.12.2018
 */

class MenuViewModel(
    private val router: Router,
    private val mapper: MenuProfileViewMapper,
    private val featureProvider: FeatureProvider,
    private val observeProfile: () -> Flowable<ProfileEntity>,
    private val logout: () -> Completable
): BaseViewModel() {

    private val state: MutableLiveData<MenuState> by lazy { MutableLiveData<MenuState>() }
    fun getState(): LiveData<MenuState> = state

    private var getProfileDisposable: Disposable? by disposableDelegate()
    private var logoutDisposable: Disposable? by disposableDelegate()

    fun observeProfile() {
        getProfileDisposable = observeProfile.invoke()
            .map { mapper.map(it) }
            .applySchedulers()
            .subscribeBy(
                onError = {  },
                onNext = { state.value = MenuState.data(it) }
            ).addTo(disposables)
    }

    fun openChangePasswordScreen() {
        router.navigateTo(featureProvider.getChangePassword())
    }

    fun openMyProfile() {
        router.navigateTo(featureProvider.getProfile())
    }

    fun logout() {
        logoutDisposable = logout.invoke()
            .applySchedulers()
            .subscribeBy(
                onError = { it.printStackTrace() },
                onComplete = { router.newRootScreen(featureProvider.getEntrance()) }
            ).addTo(disposables)
    }

    fun navigateBack() {
        router.exit()
    }
}