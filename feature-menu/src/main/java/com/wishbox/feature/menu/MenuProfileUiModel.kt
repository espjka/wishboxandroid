package com.wishbox.feature.menu

/**
 * Created by Pak Evgeniy on 21/12/2018.
 */

data class MenuProfileUiModel(
    val fullName: String,
    val avatarUrl: String? = null,
    val birthday: String
)