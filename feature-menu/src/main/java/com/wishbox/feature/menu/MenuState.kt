package com.wishbox.feature.menu

/**
 * Created by Pak Evgeniy on 21/12/2018.
 */

data class MenuState(
    val profile: MenuProfileUiModel? = null
) {

    companion object {
        fun initial(): MenuState = MenuState()
        fun data(profile: MenuProfileUiModel): MenuState = MenuState(profile = profile)
    }
}