package com.wishbox.feature.menu.di

import com.wishbox.core.di.api.RouterApi
import com.wishbox.core.di.api.FeatureProviderApi
import com.wishbox.core.di.api.InjectorApi
import com.wishbox.core.di.module.ViewModelModule
import com.wishbox.core.di.provider.RouterApiProvider
import com.wishbox.core.di.provider.FeatureProviderApiProvider
import com.wishbox.core.di.provider.InjectorApiProvider
import com.wishbox.core.di.scope.PerFragment
import com.wishbox.core.di.util.lookupApiProvider
import com.wishbox.feature.menu.MenuFragment
import dagger.BindsInstance
import dagger.Component

/**
 * Created by Pak Evgeniy on 21.12.2018
 */

@PerFragment
@Component(
    dependencies = [
        InjectorApi::class,
        RouterApi::class,
        FeatureProviderApi::class
    ],
    modules = [
        ViewModelModule::class,
        MenuViewModule::class
    ]
)
interface MenuViewComponent {

    fun inject(fragment: MenuFragment)

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun bind(fragment: MenuFragment): Builder

        fun api(api: InjectorApi): Builder

        fun api(api: RouterApi): Builder

        fun api(api: FeatureProviderApi): Builder

        fun build(): MenuViewComponent
    }

    class Initializer private constructor() {
        companion object {

            fun inject(fragment: MenuFragment) {
                val injectorProvider = fragment.lookupApiProvider<InjectorApiProvider>()
                val featureProviderProvider = fragment.lookupApiProvider<FeatureProviderApiProvider>()
                val containerProvider = fragment.lookupApiProvider<RouterApiProvider>()

                DaggerMenuViewComponent.builder()
                    .api(injectorProvider.injectorApi)
                    .api(containerProvider.routerApi)
                    .api(featureProviderProvider.featureProviderApi)
                    .bind(fragment)
                    .build()
                    .inject(fragment)
            }
        }
    }
}