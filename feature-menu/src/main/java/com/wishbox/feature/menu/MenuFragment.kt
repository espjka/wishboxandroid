package com.wishbox.feature.menu

import android.os.Bundle
import android.view.View
import com.smedialink.common.ext.observe
import com.smedialink.common.util.StateHolder
import com.wishbox.core.glide.loadImage
import com.wishbox.core.presentation.base.BaseFragment
import com.wishbox.core.presentation.navigation.BackButtonListener
import com.wishbox.feature.menu.di.MenuViewComponent
import kotlinx.android.synthetic.main.fragment_menu.*

/**
 * Created by Pak Evgeniy on 21.12.2018
 */

class MenuFragment : BaseFragment<MenuViewModel>(), BackButtonListener {

    private var stateHolder = StateHolder<MenuState>()

    override fun injectDependencies() {
        MenuViewComponent.Initializer.inject(this)
    }

    override fun layoutRes(): Int = R.layout.fragment_menu

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        stateHolder = StateHolder()
        layoutProfile.setOnClickListener { viewModel.openMyProfile() }
        textChangePassword.setOnClickListener { viewModel.openChangePasswordScreen() }
        textLogout.setOnClickListener { viewModel.logout() }

        with(viewModel) {
            getState().observe(this@MenuFragment, this@MenuFragment::render)
            observeProfile()
        }
    }

    override fun onBackPressed(): Boolean {
        viewModel.navigateBack()
        return true
    }

    private fun render(state: MenuState) {
        stateHolder = stateHolder.newState(state)

        state.profile?.run {
            with (stateHolder) {
                diff({ fullName }) { textName.text = it }
                diff({ birthday }) { textBirthday.text = it }
                diffNullable({ avatarUrl }) { imageAvatar.loadImage(it, R.drawable.placeholder_avatar) }
            }
        }
    }
}