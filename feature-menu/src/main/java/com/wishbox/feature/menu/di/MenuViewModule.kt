package com.wishbox.feature.menu.di

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.smedialink.common.error.ErrorHandler
import com.wishbox.core.di.mapkeys.ViewModelKey
import com.wishbox.core.di.screenproviders.FeatureProvider
import com.wishbox.core.domain.cleaner.DataCleaner
import com.wishbox.core.domain.repository.ProfileRepository
import com.wishbox.core.presentation.errorHandling.MainErrorHandler
import com.wishbox.feature.menu.MenuFragment
import com.wishbox.feature.menu.MenuViewModel
import com.wishbox.feature.menu.mapper.MenuProfileViewMapper
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.multibindings.IntoMap
import ru.terrakok.cicerone.Router

/**
 * Created by Pak Evgeniy on 08.12.2018
 */

@Module
abstract class MenuViewModule {

    @Binds
    abstract fun provideErrorHandler(impl: MainErrorHandler): ErrorHandler

    @Module
    companion object {

        @Provides
        @IntoMap
        @ViewModelKey(MenuViewModel::class)
        @JvmStatic
        fun provideViewModel(
            router: Router,
            featureProvider: FeatureProvider,
            dataCleaner: DataCleaner,
            profileRepository: ProfileRepository,
            mapper: MenuProfileViewMapper
        ): ViewModel =
            MenuViewModel(
                router = router,
                featureProvider = featureProvider,
                logout = { dataCleaner.clear() },
                mapper = mapper,
                observeProfile = { profileRepository.observeProfile() }
            )

        @JvmStatic
        @Provides
        fun provideMenuViewModel(
            fragment: MenuFragment,
            factory: ViewModelProvider.Factory
        ): MenuViewModel =
            ViewModelProviders.of(fragment, factory).get(MenuViewModel::class.java)
    }
}