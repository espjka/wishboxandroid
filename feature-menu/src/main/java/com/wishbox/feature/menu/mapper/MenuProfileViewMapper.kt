package com.wishbox.feature.menu.mapper

import com.wishbox.core.domain.entity.ProfileEntity
import com.wishbox.core.domain.parser.DateParser
import com.wishbox.core.presentation.resources.ResourceProvider
import com.wishbox.feature.menu.MenuProfileUiModel
import com.wishbox.feature.menu.R
import javax.inject.Inject

/**
 * Created by Pak Evgeniy on 20.12.2018
 */

class MenuProfileViewMapper @Inject constructor(
    private val resourceProvider: ResourceProvider,
    private val dateParser: DateParser
) {

    private val notSpecifiedInfoString: String by lazy { resourceProvider.getString(R.string.not_specified) }

    fun map(from: ProfileEntity): MenuProfileUiModel =
        MenuProfileUiModel(
            fullName = mapName(from.lastName, from.firstName),
            avatarUrl = from.avatar?.small,
            birthday = dateParser.dateToString(from.birthday, DateParser.Pattern.DATE_DOTS) ?: notSpecifiedInfoString
        )

    private fun mapName(lastName: String?, firstName: String?): String =
        "${firstName.orEmpty()} ${lastName.orEmpty()}"
            .takeIf { it.isNotBlank() } ?: notSpecifiedInfoString
}