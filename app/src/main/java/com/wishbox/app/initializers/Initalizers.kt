package com.wishbox.app.initializers

import android.app.Application
import com.crashlytics.android.Crashlytics
import com.miguelbcr.ui.rx_paparazzo2.RxPaparazzo
import com.wishbox.app.R
import io.fabric.sdk.android.Fabric
import uk.co.chrisjenx.calligraphy.CalligraphyConfig
import javax.inject.Inject

/**
 * Created by Oleg Sheliakin on 04.12.2018.
 * Contact me by email - olegsheliakin@gmail.com
 */

class CrashlyticsInitializer @Inject constructor() : AppInitializer {
    override fun init(app: Application) {
        Fabric.with(app, Crashlytics())
    }
}

class FontInitializer @Inject constructor() : AppInitializer {
    override fun init(app: Application) {
        val config = CalligraphyConfig.Builder()
            .setDefaultFontPath(app.resources.getString(R.string.path_to_default_font))
            .setFontAttrId(R.attr.fontPath)
            .build()
        CalligraphyConfig.initDefault(config)
    }
}

class RxPaparazzoInitializer @Inject constructor() : AppInitializer {

    companion object {
        private const val PATH_IMAGES = "WishBox/"
        private const val AUTHORITY = "com.wishbox.app.fileProvider"
    }

    override fun init(app: Application) {
        RxPaparazzo.register(app)
            .withFileProviderPath(PATH_IMAGES)
            .withFileProviderAuthority(AUTHORITY)
    }
}