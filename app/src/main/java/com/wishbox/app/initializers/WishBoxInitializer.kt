package com.wishbox.app.initializers

import android.app.Application
import javax.inject.Inject

/**
 * Created by Oleg Sheliakin on 04.12.2018.
 * Contact me by email - olegsheliakin@gmail.com
 */
class WishBoxInitializer @Inject constructor(
    private val initializers: Set<@JvmSuppressWildcards AppInitializer>
) : AppInitializer {
    override fun init(app: Application) {
        initializers.forEach { it.init(app) }
    }

}