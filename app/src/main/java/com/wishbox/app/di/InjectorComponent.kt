package com.wishbox.app.di

import com.wishbox.core.presentation.navigation.di.NavigationComponent
import com.wishbox.app.WishBoxInjector
import com.wishbox.core.data.di.RepositoryComponent
import com.wishbox.core.di.api.*
import com.wishbox.core.di.scope.PerApplication
import com.wishbox.di.RootScreensComponent
import dagger.Component

@PerApplication
@Component(
    dependencies = [
        RootScreensApi::class,
        NavigationApi::class,
        RepositoryApi::class,
        AppApi::class
    ],
    modules = [
        InitializersModule::class
    ]
)
interface InjectorComponent : InjectorApi {

    fun inject(app: WishBoxInjector)

    @Component.Builder
    interface Builder {

        fun api(api: AppApi): Builder

        fun api(api: NavigationApi): Builder

        fun api(api: RepositoryApi): Builder

        fun api(api: RootScreensApi): Builder

        fun build(): InjectorComponent
    }

    class Initializer private constructor() {
        companion object {
            fun init(app: WishBoxInjector): InjectorComponent {

                val appApi = AppComponent.Initializer.init(app)

                val navigationApi = NavigationComponent.Initializer.init()

                val repositoryApi = RepositoryComponent.Initializer.init(appApi)

                val rootScreensApi = RootScreensComponent.Initializer.init()

                return DaggerInjectorComponent.builder()
                    .api(appApi)
                    .api(navigationApi)
                    .api(repositoryApi)
                    .api(rootScreensApi)
                    .build()
            }
        }
    }
}
