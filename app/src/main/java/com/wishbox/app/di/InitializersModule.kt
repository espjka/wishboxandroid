package com.wishbox.app.di

import com.wishbox.app.initializers.CrashlyticsInitializer
import com.wishbox.app.initializers.FontInitializer
import com.wishbox.app.initializers.AppInitializer
import com.wishbox.app.initializers.RxPaparazzoInitializer
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoSet

/**
 * Created by Oleg Sheliakin on 04.12.2018.
 * Contact me by email - olegsheliakin@gmail.com
 */
@Module
interface InitializersModule {

    @IntoSet
    @Binds
    fun provideCrashlyticInits(impl: CrashlyticsInitializer) : AppInitializer

    @IntoSet
    @Binds
    fun provideFontInits(impl: FontInitializer) : AppInitializer

    @IntoSet
    @Binds
    fun provideRxPaparazzoInitializer(impl: RxPaparazzoInitializer): AppInitializer
}