package com.wishbox.app.di

import android.content.Context
import android.content.SharedPreferences
import com.wishbox.app.BuildConfig
import com.wishbox.app.WishBoxInjector
import com.wishbox.core.di.scope.PerApplication
import com.wishbox.core.di.util.Logger
import dagger.Module
import dagger.Provides


@Module
class AppModule {

    @Provides
    fun provideContext(app: WishBoxInjector): Context = app

    @PerApplication
    @Provides
    fun provideLogger(): Logger = object: Logger {
        override fun error(error: Throwable) {
            if(BuildConfig.DEBUG) {
                error.printStackTrace()
            }
        }

    }

    @Provides
    fun provideSharedPrefs(context: Context): SharedPreferences =
        context.getSharedPreferences(WishBoxInjector::class.java.simpleName, Context.MODE_PRIVATE)
}
