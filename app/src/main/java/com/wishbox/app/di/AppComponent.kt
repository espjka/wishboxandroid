package com.wishbox.app.di

import com.wishbox.app.WishBoxInjector
import com.wishbox.core.di.api.AppApi
import com.wishbox.core.di.scope.PerApplication
import dagger.BindsInstance
import dagger.Component

/**
 * Created by Pak Evgeniy on 30/11/2018.
 */

@PerApplication
@Component(
    modules = [
        AppModule::class
    ]
)
interface AppComponent : AppApi {

    @Component.Builder
    interface Builder {

        @BindsInstance
        fun bind(app: WishBoxInjector): Builder

        fun build(): AppComponent
    }

    class Initializer private constructor() {
        companion object {
            fun init(app: WishBoxInjector): AppApi =
                DaggerAppComponent.builder()
                    .bind(app)
                    .build()
        }
    }
}