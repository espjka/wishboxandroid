package com.wishbox.app

import android.app.Application
import com.wishbox.app.di.InjectorComponent
import com.wishbox.app.initializers.WishBoxInitializer
import com.wishbox.core.di.api.InjectorApi
import com.wishbox.core.di.provider.InjectorApiProvider
import javax.inject.Inject

class WishBoxInjector : Application(), InjectorApiProvider {

    @Inject
    lateinit var wishBoxInitializer: WishBoxInitializer

    private val component: InjectorComponent by lazy { InjectorComponent.Initializer.init(this@WishBoxInjector) }

    override val injectorApi: InjectorApi
        get() = component

    override fun onCreate() {
        super.onCreate()
        component.inject(this)
        wishBoxInitializer.init(this)
    }
}
