package com.smedialink.common.recyclerview

import androidx.recyclerview.widget.DiffUtil

class ItemsDiffCallback private constructor() : DiffUtil.ItemCallback<ItemViewType>() {

    override fun areItemsTheSame(oldItem: ItemViewType, newItem: ItemViewType): Boolean {
        return oldItem.viewType == newItem.viewType
    }

    override fun areContentsTheSame(oldItem: ItemViewType, newItem: ItemViewType): Boolean {
        return oldItem == newItem
    }

    companion object {
        val ITEMS_DIFF_CALLBACK = ItemsDiffCallback()
    }

}
