package com.smedialink.common.recyclerview.itemDecoration

import android.graphics.Rect
import android.view.View
import androidx.recyclerview.widget.RecyclerView

/**
 * Created by Pak Evgeniy on 04/12/2018.
 */

class SpaceItemDecoration(
    private val offset: Offset,
    private val positionSelector: (Int, RecyclerView) -> Boolean = noLast()
) : RecyclerView.ItemDecoration() {

    companion object {
        fun each(): (Int, RecyclerView) -> Boolean = { _, _ -> true }
        fun noLast(): (Int, RecyclerView) -> Boolean =
            { position, rv -> position != rv.adapter?.itemCount?.minus(1) }

        fun noFirst(): (Int, RecyclerView) -> Boolean = { position, _ -> position != 0 }
    }

    override fun getItemOffsets(
        outRect: Rect,
        view: View,
        parent: RecyclerView,
        state: RecyclerView.State
    ) {
        val position = parent.getChildAdapterPosition(view)
        if (positionSelector(position, parent)) {
            outRect.set(offset.left, offset.top, offset.right, offset.bottom)
        } else {
            outRect.setEmpty()
        }
    }

    data class Offset(
        val top: Int = 0,
        val bottom: Int = 0,
        val right: Int = 0,
        val left: Int = 0
    ) {
        constructor(square: Int): this(square, square, square, square)
        constructor(height: Int, width: Int) : this(height, height, width, width)
    }
}
