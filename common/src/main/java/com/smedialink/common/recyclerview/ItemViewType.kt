package com.smedialink.common.recyclerview

interface ItemViewType {
    val viewType: Int
}
