package com.smedialink.common.events

/**
 * Created by Oleg Sheliakin on 27.10.2018.
 * Contact me by email - olegsheliakin@gmail.com
 */
data class MessageEvent(val text: String) : LiveEvent()

