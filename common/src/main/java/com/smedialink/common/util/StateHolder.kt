package com.smedialink.common.util

data class StateHolder<T>(
    private val prev: T? = null,
    private val current: T? = null
) {

    fun newState(state: T): StateHolder<T> {
        return StateHolder(prev = this.current, current = state)
    }

    fun <V> diffNullable(valueSelector: T.() -> V?, action: (V?) -> Unit) {
        val prevValue = if (prev == null) null else valueSelector(prev)
        val currentValue = if (current == null) null else valueSelector(current)
        if (prevValue != currentValue) {
            action.invoke(currentValue)
        }
    }

    fun <V : Any> diff(valueSelector: T.() -> V, action: (V) -> Unit) {
        val prevValue = if (prev == null) null else valueSelector(prev)
        val currentValue = if (current == null) null else valueSelector(current)
        if (prevValue != currentValue && currentValue != null) {
            action(currentValue)
        }
    }
}