package com.smedialink.common.rxbus

import com.smedialink.common.rxbus.rxEvent.RxEvent
import io.reactivex.Observable
import io.reactivex.subjects.BehaviorSubject

abstract class RxEventBus<T : RxEvent> {
    private val valueInternal = BehaviorSubject.createDefault(this.defultEvent())

    val value: Observable<T> = valueInternal

    fun post(event: T) {
        valueInternal.onNext(event)
    }

    abstract fun defultEvent(): T
}