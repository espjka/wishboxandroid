package com.smedialink.common.delegates.bundle

import android.os.Parcelable
import androidx.fragment.app.Fragment
import kotlin.properties.ReadWriteProperty

fun bundleInt(): ReadWriteProperty<Fragment, Int?> = IntGetter

fun bundleLong(): ReadWriteProperty<Fragment, Long?> = LongGetter

fun bundleFloat(): ReadWriteProperty<Fragment, Float?> = FloatGetter

fun bundleDouble(): ReadWriteProperty<Fragment, Double?> = DoubleGetter

fun bundleByte(): ReadWriteProperty<Fragment, Byte?> = ByteGetter

fun bundleString(): ReadWriteProperty<Fragment, String?> = StringGetter

fun bundleBoolean(): ReadWriteProperty<Fragment, Boolean?> = BooleanGetter

fun bundleChar(): ReadWriteProperty<Fragment, Char?> = CharGetter

fun <T : Parcelable> bundleParcelable(): ReadWriteProperty<Fragment, T?> = ParcelableGetter()
