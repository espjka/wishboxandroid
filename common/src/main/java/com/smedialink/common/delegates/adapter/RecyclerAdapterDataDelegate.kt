package com.smedialink.common.delegates.adapter

import androidx.recyclerview.widget.RecyclerView
import kotlin.properties.ReadWriteProperty
import kotlin.reflect.KProperty

/**
 * Created by Pak Evgeniy on 16.10.2018.
 */

class RecyclerAdapterDataDelegate<O, T>(
    private val adapter: RecyclerView.Adapter<*>
) : ReadWriteProperty<O, List<T>> {

    var data: MutableList<T> = mutableListOf()

    override fun getValue(thisRef: O, property: KProperty<*>): List<T> = data

    override fun setValue(thisRef: O, property: KProperty<*>, value: List<T>) {
        with (data) {
            clear()
            addAll(value)
        }
        adapter.notifyDataSetChanged()
    }
}