package com.smedialink.common.delegates

import kotlin.properties.ReadWriteProperty
import kotlin.reflect.KProperty

class PropertyChangedDelegate<T> (initial: T?, private val onChanged: (T?) -> Unit): ReadWriteProperty<Any, T?> {

    private var value: T? = initial

    override fun getValue(thisRef: Any, property: KProperty<*>): T? {
        return value
    }

    override fun setValue(thisRef: Any, property: KProperty<*>, value: T?) {
        val prevValue = this.value
        if (prevValue != value) {
            this.value = value
            onChanged(this.value)
        }
    }

}