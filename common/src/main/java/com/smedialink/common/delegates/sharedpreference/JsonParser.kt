package com.smedialink.common.delegates.sharedpreference

import android.content.SharedPreferences
import com.google.gson.Gson

interface JsonParser<T> {
    fun fromJson(json: String): T
    fun toJson(value: T): String
}

inline fun <reified T : Any> Gson.asParser(): JsonParser<T> {
    return object : JsonParser<T> {
        override fun fromJson(json: String): T {
            return this@asParser.fromJson(json, T::class.java)
        }

        override fun toJson(value: T): String {
            return this@asParser.toJson(value)
        }

    }
}

inline fun <reified T : Any> SharedPreferences.delegate(parser: JsonParser<T>) =
    SharedPrefDelegate(T::class.java.name, parser, this)

