package com.smedialink.common.delegates.bundle

import android.os.Parcelable
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import com.smedialink.common.ext.plusAssign
import kotlin.properties.ReadWriteProperty
import kotlin.reflect.KProperty


abstract class BaseReadWriteProperty<T : Any> : ReadWriteProperty<Fragment, T?> {

    abstract override fun getValue(thisRef: Fragment, property: KProperty<*>): T?

    override fun setValue(thisRef: Fragment, property: KProperty<*>, value: T?) {
        thisRef.arguments?.let {
            it += Pair(getKey(property), value)
        } ?: run { thisRef.arguments = bundleOf(Pair(getKey(property), value)) }
    }

    protected fun getKey(property: KProperty<*>): String = "key_${property.name}"

}

object IntGetter : BaseReadWriteProperty<Int>() {
    override fun getValue(thisRef: Fragment, property: KProperty<*>): Int? =
        thisRef.arguments?.getInt(getKey(property))
}

object LongGetter : BaseReadWriteProperty<Long>() {
    override fun getValue(thisRef: Fragment, property: KProperty<*>): Long? =
        thisRef.arguments?.getLong(getKey(property))
}

object FloatGetter : BaseReadWriteProperty<Float>() {
    override fun getValue(thisRef: Fragment, property: KProperty<*>): Float? =
        thisRef.arguments?.getFloat(getKey(property))
}

object DoubleGetter : BaseReadWriteProperty<Double>() {
    override fun getValue(thisRef: Fragment, property: KProperty<*>): Double? =
        thisRef.arguments?.getDouble(getKey(property))
}

object ByteGetter : BaseReadWriteProperty<Byte>() {
    override fun getValue(thisRef: Fragment, property: KProperty<*>): Byte? =
        thisRef.arguments?.getByte(getKey(property))
}

object StringGetter : BaseReadWriteProperty<String>() {
    override fun getValue(thisRef: Fragment, property: KProperty<*>): String? =
        thisRef.arguments?.getString(getKey(property))
}

object BooleanGetter : BaseReadWriteProperty<Boolean>() {
    override fun getValue(thisRef: Fragment, property: KProperty<*>): Boolean? =
        thisRef.arguments?.getBoolean(getKey(property))
}

object CharGetter : BaseReadWriteProperty<Char>() {
    override fun getValue(thisRef: Fragment, property: KProperty<*>): Char? =
        thisRef.arguments?.getChar(getKey(property))
}

class ParcelableGetter<T : Parcelable> : BaseReadWriteProperty<T>() {
    override fun getValue(thisRef: Fragment, property: KProperty<*>): T? =
        thisRef.arguments?.getParcelable(getKey(property))
}
