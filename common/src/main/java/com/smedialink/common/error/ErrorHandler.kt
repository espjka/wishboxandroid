package com.smedialink.common.error

interface ErrorHandler {
    fun handle(throwable: Throwable): ErrorState
}