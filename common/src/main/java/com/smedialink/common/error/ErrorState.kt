package com.smedialink.common.error

import com.smedialink.common.events.ErrorEvent

open class ErrorState(open val event: ErrorEvent? = null)