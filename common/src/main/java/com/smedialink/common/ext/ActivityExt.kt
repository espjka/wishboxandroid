@file:JvmName("ActivityExt")

package com.smedialink.common.ext

import android.app.Activity
import android.content.Context
import android.view.inputmethod.InputMethodManager

/**
 * Created by Pak Evgeniy on 11.01.2019
 */

fun Activity.hideKeyboard() {
    (getSystemService(Context.INPUT_METHOD_SERVICE) as? InputMethodManager)
        ?.hideSoftInputFromWindow(currentFocus?.windowToken ?: return, 0)
}