package com.smedialink.common.ext

import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.TextView
import androidx.annotation.LayoutRes

fun View.gone() {
    this.visibility = View.GONE
}

fun View.visible() {
    this.visibility = View.VISIBLE
}

fun View.invisible() {
    this.visibility = View.INVISIBLE
}

fun ViewGroup.inflate(@LayoutRes layoutId: Int, attachToRoot: Boolean = false): View =
    LayoutInflater.from(context).inflate(layoutId, this, attachToRoot)

fun TextView.trimmedString(): String = text.toString().trim()

fun EditText.fillText(text: String?) {
    val prevText = getText().toString()
    if (prevText != text) setText(text)
    setSelection(text?.length ?: 0)
}

fun EditText.onBeforeTextChanged(
    func: (s: CharSequence?, start: Int, count: Int, after: Int) -> Unit
) {
    this.textWatcher(beforeTextChanged = func)
}

fun EditText.onAfterTextChanged(
    func: (s: Editable?) -> Unit
) {
    this.textWatcher(onAfterTextChanged = func)
}

fun EditText.onTextChanged(
    onTextChanged: (s: CharSequence?, start: Int, before: Int, count: Int) -> Unit
) {
    this.textWatcher(onTextChanged = onTextChanged)
}

fun EditText.textWatcher(
    onTextChanged: ((s: CharSequence?, start: Int, before: Int, count: Int) -> Unit)? = null,
    onAfterTextChanged: ((s: Editable?) -> Unit)? = null,
    beforeTextChanged: ((s: CharSequence?, start: Int, count: Int, after: Int) -> Unit)? = null
) {
    this.addTextChangedListener(
        object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                onAfterTextChanged?.invoke(s)
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                beforeTextChanged?.invoke(s, start, count, after)
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                onTextChanged?.invoke(s, start, before, count)
            }

        }
    )
}