package com.smedialink.common.ext

import java.lang.ref.WeakReference

/**
 * Created by Pak Evgeniy on 03/12/2018.
 */

fun <T> T.weak() = WeakReference(this)