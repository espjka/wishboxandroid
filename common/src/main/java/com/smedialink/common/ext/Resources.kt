package com.smedialink.common.ext

import android.content.res.Resources

private const val STATUS_BAR_RESOURCE_NAME = "status_bar_height"
private const val NAVIGATION_BAR_RESOURCE_NAME = "navigation_bar_height"

fun Resources.statusBarHeight(): Int = getHeightInternal(STATUS_BAR_RESOURCE_NAME)

fun Resources.navigationBarHeight(): Int = getHeightInternal(NAVIGATION_BAR_RESOURCE_NAME)

private fun Resources.getHeightInternal(resourceName: String): Int {
    val resourceId = getIdentifier(resourceName, "dimen", "android")
    return if (resourceId > 0) {
        getDimensionPixelSize(resourceId)
    } else 0
}