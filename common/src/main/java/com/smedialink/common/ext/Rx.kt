package com.smedialink.common.ext

import io.reactivex.*
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

/**
 * Created by Oleg Sheliakin on 18.12.2018.
 * Contact me by email - olegsheliakin@gmail.com
 */

fun <T> Observable<T>.applySchedulers(
    subscribeScheduler: Scheduler = Schedulers.io(),
    observeScheduler: Scheduler = AndroidSchedulers.mainThread()
): Observable<T> {
    return subscribeOn(subscribeScheduler)
        .observeOn(observeScheduler)
}

fun <T> Single<T>.applySchedulers(
    subscribeScheduler: Scheduler = Schedulers.io(),
    observeScheduler: Scheduler = AndroidSchedulers.mainThread()
): Single<T> {
    return subscribeOn(subscribeScheduler)
        .observeOn(observeScheduler)
}

fun Completable.applySchedulers(
    subscribeScheduler: Scheduler = Schedulers.io(),
    observeScheduler: Scheduler = AndroidSchedulers.mainThread()
): Completable {
    return subscribeOn(subscribeScheduler)
        .observeOn(observeScheduler)
}

fun <T> Maybe<T>.applySchedulers(
    subscribeScheduler: Scheduler = Schedulers.io(),
    observeScheduler: Scheduler = AndroidSchedulers.mainThread()
): Maybe<T> {
    return subscribeOn(subscribeScheduler)
        .observeOn(observeScheduler)
}

fun <T> Flowable<T>.applySchedulers(
    subscribeScheduler: Scheduler = Schedulers.io(),
    observeScheduler: Scheduler = AndroidSchedulers.mainThread()
): Flowable<T> {
    return subscribeOn(subscribeScheduler)
        .observeOn(observeScheduler)
}