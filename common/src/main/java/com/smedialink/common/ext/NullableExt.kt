package com.smedialink.common.ext

/**
 * Created by Pak Evgeniy on 28/11/2018.
 */

fun Int?.orZero(): Int = this ?: 0

fun Long?.orZero(): Long = this ?: 0

fun Boolean?.orFalse(): Boolean = this ?: false