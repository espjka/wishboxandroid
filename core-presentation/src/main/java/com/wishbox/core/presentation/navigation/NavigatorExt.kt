package com.wishbox.core.presentation.navigation

import com.smedialink.common.events.handle
import com.wishbox.core.presentation.base.NavigationEvent
import ru.terrakok.cicerone.Navigator

fun Navigator.accept(event: NavigationEvent) {
    event.handle { applyCommands(it.commands) }
}
