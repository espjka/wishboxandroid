package com.wishbox.core.presentation.view

import android.content.Context
import android.graphics.drawable.Drawable
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.ImageButton
import android.widget.LinearLayout
import android.widget.TextView
import com.smedialink.common.ext.gone
import com.smedialink.common.ext.visible
import com.wishbox.core.presentation.R
import kotlinx.android.synthetic.main.wishbox_toolbar.view.*

class WishBoxToolbar @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : LinearLayout(context, attrs, defStyleAttr) {

    var leftIcon: Drawable? = null
    var rightIcon: Drawable? = null
    var title: String? = null
    var description: String? = null

    fun getLeftButton(): ImageButton = buttonLeft
    fun getRightButton(): ImageButton = buttonRight
    fun getTitleText(): TextView = textToolbarTitle
    fun getDescriptionText(): TextView = textDescription

    init {
        LayoutInflater.from(context).inflate(R.layout.wishbox_toolbar, this, true)
        initAttrs(context, attrs)
        initPaint()
    }

    private fun initAttrs(context: Context, attrs: AttributeSet?) {
        val array = context.obtainStyledAttributes(attrs, R.styleable.WishBoxToolbar)
        leftIcon = array.getDrawable(R.styleable.WishBoxToolbar_leftIcon)
        rightIcon = array.getDrawable(R.styleable.WishBoxToolbar_rightIcon)
        title = array.getString(R.styleable.WishBoxToolbar_textTitle)
        description = array.getString(R.styleable.WishBoxToolbar_textDescription)
        array.recycle()
    }

    private fun initPaint() {
        leftIcon?.let {
            buttonLeft.visible()
            buttonLeft.setImageDrawable(it)
        } ?: run { buttonLeft.gone() }

        rightIcon?.let {
            buttonRight.visible()
            buttonRight.setImageDrawable(it)
        } ?: run { buttonRight.gone() }

        title?.let { textToolbarTitle.text = it }

        description?.let {
            textDescription.text = it
        } ?: run { textDescription.gone() }
    }
}