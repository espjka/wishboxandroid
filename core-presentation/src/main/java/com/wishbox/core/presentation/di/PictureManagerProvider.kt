package com.wishbox.core.presentation.di

/**
 * Created by Pak Evgeniy on 24/12/2018.
 */

interface PictureManagerProvider {
    val pictureManagerApi: PictureManagerApi
}