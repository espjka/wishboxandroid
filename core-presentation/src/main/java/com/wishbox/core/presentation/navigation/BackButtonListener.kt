package com.wishbox.core.presentation.navigation

/**
 * Created by Pak Evgeniy on 01.12.2018
 */
interface BackButtonListener {
    fun onBackPressed(): Boolean
}