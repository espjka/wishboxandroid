package com.wishbox.core.presentation.pictures

import io.reactivex.Flowable

interface PicturesManager {

    companion object {
        const val MAX_PICTURES_AMOUNT = 10
    }

    val pictures: List<String>

    fun observePictures(): Flowable<List<String>>

    fun makePictureMain(path: String)

    fun addLocalPictures(vararg paths: String)

    fun deletePicture(path: String)

    fun canAddMore(): Boolean
}
