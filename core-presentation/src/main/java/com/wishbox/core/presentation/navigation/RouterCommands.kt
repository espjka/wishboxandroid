package com.wishbox.core.presentation.navigation

import ru.terrakok.cicerone.android.support.SupportAppScreen
import ru.terrakok.cicerone.commands.Command

/**
 * Created by Pak Evgeniy on 27/11/2018.
 */

class OpenAndKeep(val screen: SupportAppScreen) : Command

class OpenUrl(val url: String) :Command