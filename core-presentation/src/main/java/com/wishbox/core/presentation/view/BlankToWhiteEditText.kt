package com.wishbox.core.presentation.view

import android.content.Context
import android.graphics.Rect
import android.os.Bundle
import android.os.Parcelable
import android.util.AttributeSet
import androidx.annotation.ColorRes
import androidx.annotation.DrawableRes
import androidx.appcompat.widget.AppCompatEditText
import androidx.core.content.ContextCompat
import com.smedialink.common.ext.trimmedString
import com.wishbox.core.presentation.R

class BlankToWhiteEditText : AppCompatEditText {

    private var shouldShowErrorBackground: Boolean = false

    companion object {
        private const val ERROR_TEXT_STATE = "error_text_state"
        private const val SUPER_STATE_KEY = "super_state"
    }

    constructor(context: Context?) : super(context)
    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs)
    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr)

    override fun onFocusChanged(focused: Boolean, direction: Int, previouslyFocusedRect: Rect?) {
        super.onFocusChanged(focused, direction, previouslyFocusedRect)
        applyState()
    }

    override fun onSaveInstanceState(): Parcelable? {
        val bundle = Bundle()
        bundle.putParcelable(SUPER_STATE_KEY, super.onSaveInstanceState())
        bundle.putBoolean(ERROR_TEXT_STATE, shouldShowErrorBackground)
        return bundle
    }

    override fun onRestoreInstanceState(state: Parcelable?) {
        var _state = state
        if (_state is Bundle) {
            shouldShowErrorBackground = _state.getBoolean(ERROR_TEXT_STATE, false)
            _state = _state.getParcelable(SUPER_STATE_KEY)
        }
        super.onRestoreInstanceState(_state)
        applyState()
    }

    fun showError(errorText: String?) {
        error = if (errorText.isNullOrBlank()) null else errorText
        shouldShowErrorBackground = errorText != null && errorText.length >= 0
        applyState()
    }

    private fun applyState() {
        State.resolve(
            trimmedString(),
            isFocused,
            shouldShowErrorBackground
        ).let {
            setHintTextColor(ContextCompat.getColor(context, it.colorRes))
            setBackgroundResource(it.backgroundRes)
        }
    }

    enum class State(@DrawableRes val backgroundRes: Int, @ColorRes val colorRes: Int) {
        FOCUSED(
            R.drawable.bg_blank_to_white_edit_filled,
            R.color.colorAccent50
        ),
        NOT_FOCUSED(
            R.drawable.bg_blank_to_white_edit_empty,
            R.color.hint_white
        ),
        ERROR(
            R.drawable.bg_blank_to_white_edit_error,
            R.color.colorAccent50
        );

        companion object {

            fun resolve(text: String?, hasFocus: Boolean, shouldShowErrorDrawable: Boolean): State {
                return if (shouldShowErrorDrawable) {
                    ERROR
                } else if (text.isNullOrBlank() && !hasFocus) NOT_FOCUSED else FOCUSED
            }
        }
    }
}