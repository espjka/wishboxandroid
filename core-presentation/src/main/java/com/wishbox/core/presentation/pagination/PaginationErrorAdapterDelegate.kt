package com.wishbox.core.presentation.pagination

import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.hannesdorfmann.adapterdelegates4.AbsListItemAdapterDelegate
import com.wishbox.core.presentation.R
import com.smedialink.common.ext.inflate
import kotlinx.android.synthetic.main.item_pagination_error.view.*

/**
 * Created by Pak Evgeniy on 29/11/2018.
 */

class PaginationErrorAdapterDelegate<I : T, T>(
    private val isForViewType: (item: T) -> Boolean,
    private val errorClick: () -> Unit
) : AbsListItemAdapterDelegate<I, T, PaginationErrorAdapterDelegate.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup): ViewHolder =
        ViewHolder(parent.inflate(R.layout.item_pagination_error), errorClick)

    override fun isForViewType(item: T, items: MutableList<T>, position: Int): Boolean =
        isForViewType.invoke(item)

    override fun onBindViewHolder(item: I, holder: ViewHolder, payloads: MutableList<Any>) {
        holder.bind()
    }

    class ViewHolder(
        view: View,
        private val errorClick: () -> Unit
    ) : RecyclerView.ViewHolder(view) {

        fun bind() {
            with(itemView) {
                textRetry.setOnClickListener { errorClick.invoke() }
            }
        }
    }
}