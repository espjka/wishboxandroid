package com.wishbox.core.presentation.di

import com.wishbox.core.presentation.pictures.PicturesManager

/**
 * Created by Pak Evgeniy on 24/12/2018.
 */

interface PictureManagerApi {
    fun providePictureManager(): PicturesManager
}