package com.wishbox.core.presentation.resources

import com.smedialink.common.events.ErrorEvent
import com.wishbox.core.presentation.R
import com.wishbox.core.presentation.pictures.PermissionsDeniedException
import java.io.IOException
import java.net.SocketTimeoutException
import java.net.UnknownHostException
import javax.inject.Inject
import javax.net.ssl.SSLHandshakeException
import kotlin.reflect.KClass

@Suppress("UNCHECKED_CAST")
class ErrorTextProvider @Inject constructor(
    private val resourceProvider: ResourceProvider
) {

    private val errorMappers: MutableMap<KClass<out Throwable>, ((Throwable) -> String)> = mutableMapOf()

    init {
        addMapper(SocketTimeoutException::class) {
            resourceProvider.getString(R.string.error_timeout)
        }

        addMapper(IOException::class) {
            resourceProvider.getString(R.string.error_no_internet)
        }

        addMapper(UnknownHostException::class) {
            resourceProvider.getString(R.string.error_no_internet)
        }

        addMapper(SSLHandshakeException::class) {
            resourceProvider.getString(R.string.error_ssl_handshake)
        }

        addMapper(PermissionsDeniedException::class) {
            resourceProvider.getString(R.string.error_permissions)
        }
    }

    fun provide(error: Throwable): String {
        return errorMappers[error::class]?.invoke(error) ?: resourceProvider.getString(R.string.error_unexpected)
    }

    fun provideErrorEvent(error: Throwable): ErrorEvent {
        val text = errorMappers[error::class]?.invoke(error)
        return if (text != null) {
            ErrorEvent(text)
        } else {
            ErrorEvent(resourceProvider.getString(R.string.error_unexpected))
        }
    }

    fun <T : Throwable> addMapper(error: KClass<T>, mapper: (T) -> String) {
        errorMappers[error] = mapper as (Throwable) -> String

    }

}