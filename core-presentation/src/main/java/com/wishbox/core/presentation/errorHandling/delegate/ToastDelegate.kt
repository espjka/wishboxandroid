package com.wishbox.core.presentation.errorHandling.delegate

import android.content.Context
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import com.smedialink.common.events.ErrorEvent
import com.smedialink.common.events.LiveEvent
import com.smedialink.common.events.handle
import com.wishbox.core.presentation.R
import kotlinx.android.synthetic.main.layout_toast.view.*
import javax.inject.Inject

/**
 * Created by Oleg Sheliakin on 04.12.2018.
 * Contact me by email - olegsheliakin@gmail.com
 */
class ToastDelegate @Inject constructor(private val context: Context) : Observer<LiveEvent> {

    private val marginTop: Int by lazy {
        return@lazy context.resources.getDimensionPixelSize(R.dimen.margin_high)
    }

    private var currentToast: Toast? = null

    override fun onChanged(event: LiveEvent?) {
        when (event) {
            is ErrorEvent -> event.handle { show(it.errorText) }
        }
    }

    fun show(text: String, gravity: ToastGravity = ToastGravity.BOTTOM, type: ToastType = ToastType.ERROR) {
        val toastView = createView(text, type)
        showInternal(gravity, toastView)
    }

    private fun showInternal(gravity: ToastGravity, toastView: View) {
        currentToast?.cancel()

        currentToast = Toast(context).apply {
            view = toastView
            setGravity(gravity.value, 0, marginTop)
        }

        currentToast?.show()
    }

    private fun createView(text: String, type: ToastType): View =
        (context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater).inflate(
            R.layout.layout_toast, null as ViewGroup?
        ).apply {
            tvErrorMessage.setTextColor(ContextCompat.getColor(context, type.colorRes))
            tvErrorMessage.setCompoundDrawablesWithIntrinsicBounds(type.iconRes, 0, 0, 0)
            tvErrorMessage.text = text
        }

    enum class ToastGravity(val value: Int) {
        TOP(Gravity.TOP or Gravity.FILL_HORIZONTAL),
        BOTTOM(Gravity.BOTTOM or Gravity.FILL_HORIZONTAL)
    }

    enum class ToastType(val iconRes: Int, val colorRes: Int) {
        ERROR(R.drawable.ic_error, R.color.text_red),
        MESSAGE(R.drawable.ic_success, R.color.text_green)
    }
}
