package com.wishbox.core.presentation.navigation

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import com.wishbox.core.presentation.navigation.BackButtonListener
import javax.inject.Inject

/**
 * Created by Pak Evgeniy on 03/12/2018.
 */

class NavigationBackHelper @Inject constructor(private val activity: FragmentActivity) {

    fun onBackPressedHandled(): Boolean {
        val fm = activity.supportFragmentManager
        var fragment: Fragment? = null
        val fragments = fm.fragments
        for (f in fragments) {
            if (f.isVisible) {
                fragment = f
                break
            }
        }

        return (fragment as? BackButtonListener)?.onBackPressed() == true
    }
}