package com.wishbox.core.presentation.base

import android.content.Context
import android.os.Bundle
import android.os.Parcelable
import android.view.LayoutInflater
import android.view.ViewGroup
import android.view.View
import androidx.annotation.LayoutRes
import androidx.fragment.app.Fragment
import com.smedialink.common.base.BaseViewModel
import com.wishbox.core.presentation.view.WishBoxTabs

import javax.inject.Inject

abstract class BaseFragment<ViewModel : BaseViewModel> : Fragment() {

    @Inject
    lateinit var viewModel: ViewModel
    private var isFirstStart: Boolean = true

    override fun onAttach(context: Context?) {
        injectDependencies()
        super.onAttach(context)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(layoutRes(), container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if(isFirstStart) {
            onFirstLaunch()
            isFirstStart = false
        }
    }

    protected abstract fun injectDependencies()

    @LayoutRes
    protected abstract fun layoutRes(): Int

    override fun onSaveInstanceState(outState: Bundle) {
        outState.putBoolean(FIRST_START_KEY, isFirstStart)
        super.onSaveInstanceState(outState)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        if (savedInstanceState != null) {
            isFirstStart = savedInstanceState.getBoolean(FIRST_START_KEY, true)
        }
    }

    open fun onFirstLaunch() {

    }

    companion object {
        private const val FIRST_START_KEY = "first_start"
    }
}
