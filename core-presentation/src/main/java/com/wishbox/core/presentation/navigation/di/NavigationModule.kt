package com.wishbox.core.presentation.navigation.di

import com.wishbox.core.di.scope.PerApplication
import dagger.Module
import dagger.Provides
import ru.terrakok.cicerone.Cicerone
import ru.terrakok.cicerone.NavigatorHolder
import ru.terrakok.cicerone.Router

/**
 * Created by Pak Evgeniy on 26/11/2018.
 */

@Module
class NavigationModule {
    
    @Provides
    @PerApplication
    fun provideCicerone(): Cicerone<Router> = Cicerone.create()

    @Provides
    @PerApplication
    fun provideNavigatorHolder(cicerone: Cicerone<Router>): NavigatorHolder = cicerone.navigatorHolder
}