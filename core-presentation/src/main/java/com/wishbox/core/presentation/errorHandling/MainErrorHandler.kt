package com.wishbox.core.presentation.errorHandling

import com.smedialink.common.error.ErrorHandler
import com.smedialink.common.error.ErrorState
import com.smedialink.common.events.ErrorEvent
import com.wishbox.core.presentation.BuildConfig
import com.wishbox.core.presentation.R
import com.wishbox.core.presentation.resources.ResourceProvider
import java.io.IOException
import java.net.SocketTimeoutException
import java.net.UnknownHostException
import java.util.*
import javax.inject.Inject
import javax.net.ssl.SSLHandshakeException

open class MainErrorHandler @Inject constructor(
    private val resources: ResourceProvider
) : ErrorHandler {

    private val errorHandlers = HashMap<Class<out Throwable>, (Throwable) -> ErrorState?>()

    init {
        addError(SocketTimeoutException::class.java) { createDefaultState(R.string.error_timeout) }
        addError(IOException::class.java) { createDefaultState(R.string.error_no_internet) }
        addError(UnknownHostException::class.java) { createDefaultState(R.string.error_no_internet) }
        addError(SSLHandshakeException::class.java) { createDefaultState(R.string.error_ssl_handshake) }
    }

    fun addError(clazz: Class<out Throwable>, handleFunc: (Throwable) -> ErrorState?) {
        errorHandlers[clazz] = handleFunc
    }

    override fun handle(throwable: Throwable): ErrorState {
        if (BuildConfig.DEBUG) {
            throwable.printStackTrace()
        }
        return errorHandlers[throwable.javaClass]?.invoke(throwable) ?: createDefaultState(R.string.error_unexpected)
    }

    protected fun createDefaultState(resId: Int) = ErrorState(createEvent(resId))
    protected fun createEvent(resId: Int) = ErrorEvent(resources.getString(resId))
}