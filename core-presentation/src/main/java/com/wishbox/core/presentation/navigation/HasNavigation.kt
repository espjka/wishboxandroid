package com.wishbox.core.presentation.navigation

import androidx.lifecycle.LiveData
import com.wishbox.core.presentation.base.NavigationEvent

interface HasNavigation {
    val navigationEvents: LiveData<NavigationEvent>
}