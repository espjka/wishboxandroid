package com.wishbox.core.presentation.view

import android.content.Context
import android.util.AttributeSet
import android.view.WindowInsets
import android.widget.LinearLayout
import androidx.core.content.ContextCompat
import com.wishbox.core.presentation.R

class InsetsLayout : LinearLayout {

    private var customInsets: Insets = Insets()

    constructor(context: Context?) : this(context, null)
    constructor(context: Context?, attrs: AttributeSet?) : this(context, attrs, 0)
    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : this(context, attrs, defStyleAttr, 0)
    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int, defStyleRes: Int) : super(
        context,
        attrs,
        defStyleAttr,
        defStyleRes
    ) {
        val a = context?.theme?.obtainStyledAttributes(
            attrs,
            R.styleable.InsetsLayout,
            0, 0
        )

        a?.let { typedArray ->
            val top = typedArray.getDimensionPixelSize(R.styleable.InsetsLayout_insetTop, 0)
            val bottom = typedArray.getDimensionPixelSize(R.styleable.InsetsLayout_insetBottom, -1).takeIf { it > -1 }
            val start = typedArray.getDimensionPixelSize(R.styleable.InsetsLayout_insetStart, -1).takeIf { it > -1 }
            val end = typedArray.getDimensionPixelSize(R.styleable.InsetsLayout_insetEnd, -1).takeIf { it > -1 }

            customInsets = Insets(
                top = top,
                bottom = bottom,
                start = start,
                end = end
            )
        }.also {
            a?.recycle()
        }

        isClickable = true
        if (background == null) {
            context?.let { setBackgroundColor(ContextCompat.getColor(it, android.R.color.white)) }
        }
    }

    override fun onApplyWindowInsets(insets: WindowInsets?): WindowInsets {
        val newInsets = insets?.replaceSystemWindowInsets(
            customInsets.start ?: insets.systemWindowInsetLeft,
            customInsets.top ?: insets.systemWindowInsetTop,
            customInsets.end ?: insets.systemWindowInsetRight,
            customInsets.bottom ?: insets.systemWindowInsetBottom
        )
        return super.onApplyWindowInsets(newInsets)
    }

    data class Insets(
        val top: Int? = null,
        val bottom: Int? = null,
        val end: Int? = null,
        val start: Int? = null
    )
}