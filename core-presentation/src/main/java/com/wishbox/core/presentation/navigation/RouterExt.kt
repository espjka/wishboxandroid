package com.wishbox.core.presentation.navigation

import ru.terrakok.cicerone.Router
import ru.terrakok.cicerone.android.support.SupportAppScreen

/**
 * Created by Pak Evgeniy on 27/11/2018.
 */

fun Router.openAndKeep(screen: SupportAppScreen) {
    executeCommands(OpenAndKeep(screen))
}

fun Router.openUrl(url: String) {
    executeCommands(OpenUrl(url))
}