package com.wishbox.core.presentation.navigation

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.wishbox.core.presentation.base.NavigationEvent
import ru.terrakok.cicerone.Screen
import ru.terrakok.cicerone.android.support.SupportAppScreen
import ru.terrakok.cicerone.commands.*
import javax.inject.Inject

class LiveRouter @Inject constructor() : HasNavigation {

    private val navigationEventsInternal: MutableLiveData<NavigationEvent> by lazy {
        return@lazy MutableLiveData<NavigationEvent>()
    }

    override val navigationEvents: LiveData<NavigationEvent> = navigationEventsInternal

    fun navigateTo(screen: SupportAppScreen) {
        executeCommands(Forward(screen))
    }

    fun newRootScreen(screen: Screen) {
        executeCommands(
            BackTo(null),
            Replace(screen)
        )
    }

    fun replaceScreen(screen: Screen) {
        executeCommands(Replace(screen))
    }

    fun backTo(screen: Screen) {
        executeCommands(BackTo(screen))
    }

    fun newChain(vararg screens: Screen) {
        val commands = emptyArray<Command>()
        for (i in commands.indices) {
            commands[i] = Forward(screens[i])
        }
        executeCommands(*commands)
    }

    fun newRootChain(vararg screens: Screen) {
        val commands = emptyArray<Command>()
        commands[0] = BackTo(null)
        if (screens.isNotEmpty()) {
            commands[1] = Replace(screens[0])
            for (i in 1 until screens.size) {
                commands[i + 1] = Forward(screens[i])
            }
        }
        executeCommands(*commands)
    }

    fun finishChain() {
        executeCommands(
            BackTo(null),
            Back()
        )
    }

    fun exit() {
        executeCommands(Back())
    }

    private fun executeCommands(vararg commands: Command) {
        navigationEventsInternal.value = NavigationEvent(*commands)
    }

}
