package com.wishbox.core.presentation.view

import android.content.Context
import android.util.AttributeSet
import androidx.core.content.ContextCompat
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.wishbox.core.presentation.R

/**
 * Created by Pak Evgeniy on 11.12.2018
 */

class WishBoxSwipeRefreshLayout @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null
) : SwipeRefreshLayout(context, attrs) {

    init {
        val colorAccent = ContextCompat.getColor(context, R.color.colorAccent)
        val colorPrimaryDark = ContextCompat.getColor(context, R.color.colorPrimaryDark)
        val colorPrimary = ContextCompat.getColor(context, R.color.colorPrimary)
        setColorSchemeColors(colorAccent, colorPrimaryDark, colorPrimary)
    }
}