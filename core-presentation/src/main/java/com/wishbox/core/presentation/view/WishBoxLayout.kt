package com.wishbox.core.presentation.view

import android.app.Activity
import android.content.Context
import android.graphics.Color
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.WindowManager
import android.widget.FrameLayout
import androidx.fragment.app.Fragment
import com.smedialink.common.ext.navigationBarHeight
import com.wishbox.core.presentation.R
import kotlinx.android.synthetic.main.base_wish_box_layout.view.*

class WishBoxLayout @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : FrameLayout(context, attrs, defStyleAttr) {

    init {
        LayoutInflater.from(context).inflate(R.layout.base_wish_box_layout, this, true)
        val params = navigationBar.layoutParams
        params.height = resources.navigationBarHeight()
        navigationBar.requestLayout()

        val a = context.theme?.obtainStyledAttributes(
            attrs,
            R.styleable.WishBoxLayout,
            0, 0
        )

        a?.let { typedArray ->
            typedArray.takeIf { a.hasValue(R.styleable.WishBoxLayout_navigationBarBackgroundDrawable) }
                ?.getDrawable(R.styleable.WishBoxLayout_navigationBarBackgroundDrawable)?.apply {
                    navigationBar.background = this
                }
        }.also {
            a?.recycle()
        }
    }

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        val ctx = context
        when (ctx) {
            is Activity -> ctx.window
            is Fragment -> ctx.activity?.window
            else -> null
        }?.apply {
            addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
            clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
            statusBarColor = Color.TRANSPARENT
        }
    }

}