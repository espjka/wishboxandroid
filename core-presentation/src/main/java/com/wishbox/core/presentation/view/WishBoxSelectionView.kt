package com.wishbox.core.presentation.view

import android.content.Context
import android.os.Bundle
import android.os.Parcelable
import android.text.Editable
import android.text.TextWatcher
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.LinearLayout
import android.widget.TextView
import androidx.annotation.ColorRes
import androidx.annotation.DrawableRes
import androidx.core.content.ContextCompat
import com.smedialink.common.ext.trimmedString
import com.smedialink.common.ext.visible
import com.wishbox.core.presentation.R
import kotlinx.android.synthetic.main.text_with_arrow.view.*

class WishBoxSelectionView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
): LinearLayout(context, attrs, defStyleAttr) {

    private var shouldShowErrorBackground: Boolean = false

    companion object {
        private const val ERROR_TEXT_STATE = "error_text_state"
        private const val SUPER_STATE_KEY = "super_state"
    }

    init {
        LayoutInflater.from(context).inflate(R.layout.text_with_arrow, this, true)

        val a = context.theme?.obtainStyledAttributes(
            attrs,
            R.styleable.WishBoxSelectionView,
            0, 0
        )

        a?.run {
            getBoolean(R.styleable.WishBoxSelectionView_needArrow, false).apply {
                if (this) arrow.visible()
            }

            getString(R.styleable.WishBoxSelectionView_hint)?.apply {
                text.hint = this
            }
            recycle()
        }

        text.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {}
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                applyState()
            }
        })
    }

    fun getText(): TextView = text

    override fun onSaveInstanceState(): Parcelable? {
        val bundle = Bundle()
        bundle.putParcelable(SUPER_STATE_KEY, super.onSaveInstanceState())
        bundle.putBoolean(ERROR_TEXT_STATE, shouldShowErrorBackground)
        return bundle
    }

    override fun onRestoreInstanceState(state: Parcelable?) {
        var _state = state
        if (_state is Bundle) {
            shouldShowErrorBackground = _state.getBoolean(ERROR_TEXT_STATE, false)
            _state = _state.getParcelable(SUPER_STATE_KEY)
        }
        super.onRestoreInstanceState(_state)
        applyState()
    }

    fun showError(errorText: String?) {
        text.error = if (errorText.isNullOrBlank()) null else errorText
        shouldShowErrorBackground = errorText != null && errorText.length >= 0
        applyState()
    }

    private fun applyState() {
        State.resolve(
            text.trimmedString(),
            isFocused,
            shouldShowErrorBackground
        ).let {
            text.setTextColor(ContextCompat.getColor(context, it.colorRes))
            setBackgroundResource(it.backgroundRes)
            elevation = it.elevation
        }
    }

    enum class State(
        @DrawableRes val backgroundRes: Int,
        @ColorRes val colorRes: Int,
        val elevation: Float
    ) {
        FILLED(
            R.drawable.wish_box_edit_text_bg_filled,
            R.color.text_bold,
            8f
        ),
        EMPTY(
            R.drawable.wish_box_edit_bg,
            R.color.text_bold_50,
            0f
        ),
        ERROR(
            R.drawable.edit_text_bg_error,
            R.color.text_bold,
            8f
        );

        companion object {

            fun resolve(text: String?, hasFocus: Boolean, shouldShowErrorDrawable: Boolean): State {
                return if (shouldShowErrorDrawable) {
                    ERROR
                } else if (text.isNullOrBlank() && !hasFocus) EMPTY else FILLED
            }
        }
    }
}