package com.wishbox.core.presentation.navigation.di

import com.wishbox.core.di.api.NavigationApi
import com.wishbox.core.di.scope.PerApplication
import dagger.Component

/**
 * Created by Pak Evgeniy on 26/11/2018.
 */

@PerApplication
@Component(
    modules = [
        NavigationModule::class
    ]
)
interface NavigationComponent : NavigationApi {

    class Initializer private constructor() {
        companion object {
            fun init(): NavigationApi =
                DaggerNavigationComponent.builder()
                    .build()
        }
    }
}