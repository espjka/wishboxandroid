package com.wishbox.core.presentation.base

import android.app.AlertDialog
import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.FragmentManager
import com.smedialink.common.delegates.bundle.bundleString
import com.smedialink.common.ext.gone
import com.wishbox.core.presentation.R
import kotlinx.android.synthetic.main.base_dialog.view.*

/**
 * Created by Pak Evgeniy on 03/12/2018.
 */

class BaseDialog : DialogFragment() {

    var okClick: (() -> Unit)? = null

    private var title: String? by bundleString()
    private var description: String? by bundleString()
    private var positiveText: String? by bundleString()
    private var negativeText: String? by bundleString()

    companion object {
        fun show(
            fragmentManager: FragmentManager,
            title: String,
            description: String? = null,
            positiveText: String? = null,
            negativeText: String? = null,
            okClick: (() -> Unit)? = null
        ) {
            val dialog: BaseDialog =
                (fragmentManager.findFragmentByTag(BaseDialog::class.java.name) as? BaseDialog)
                    ?: BaseDialog()
                        .apply {
                            this.title = title
                            this.description = description
                            this.positiveText = positiveText
                            this.negativeText = negativeText
                            this.okClick = okClick
                        }

            if (!dialog.isAdded) {
                dialog.show(fragmentManager, BaseDialog::class.java.name)
            }
        }
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val view = LayoutInflater.from(context).inflate(R.layout.base_dialog, (null as? ViewGroup?), false)
            ?.also { view ->
                with(view) {
                    textToolbarTitle.text = title ?: ""

                    description?.takeIf { it.isNotBlank() }
                        ?.let { textDescription.text = it }
                        ?: run {
                            textToolbarTitle.setTextColor(ContextCompat.getColor(context, R.color.text_normal))
                            textToolbarTitle.background = null
                            textDescription.gone()
                        }

                    textNegative.text = negativeText ?: getString(R.string.cancel)
                    textNegative.setOnClickListener { dismiss() }

                    textPositive.text = positiveText ?: getString(R.string.ok)
                    textPositive.setOnClickListener {
                        okClick?.invoke()
                        dismiss()
                    }
                }
            }
        return AlertDialog.Builder(context!!)
            .setView(view)
            .create()
            .also {
                it.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            }
    }
}