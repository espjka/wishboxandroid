package com.wishbox.core.presentation.loading

import android.app.ProgressDialog
import android.content.Context
import javax.inject.Inject

class ProgressLoadingDelegate @Inject constructor(
    private val context: Context,
    private val message: String = ""
) {

    //temporary
    private val progressDialog: ProgressDialog by lazy {
        return@lazy ProgressDialog(context).apply {
            setCancelable(false)
            setMessage(message)
        }
    }

    fun showLoading(isLoading: Boolean) {
        if (isLoading && progressDialog.isShowing.not()) {
            progressDialog.show()
        } else if (isLoading.not()) {
            progressDialog.dismiss()
        }
    }

}