package com.wishbox.core.presentation.pagination

import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.hannesdorfmann.adapterdelegates4.AbsListItemAdapterDelegate
import com.wishbox.core.presentation.R
import com.smedialink.common.ext.inflate

/**
 * Created by Pak Evgeniy on 29/11/2018.
 */

class PaginationProgressAdapterDelegate<I : T, T>(
    private val isForViewType: (item: T) -> Boolean
) : AbsListItemAdapterDelegate<I, T, PaginationProgressAdapterDelegate.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup): ViewHolder =
        ViewHolder(parent.inflate(R.layout.item_pagination_progress))

    override fun isForViewType(item: T, items: MutableList<T>, position: Int): Boolean =
        isForViewType.invoke(item)

    override fun onBindViewHolder(item: I, holder: ViewHolder, payloads: MutableList<Any>) {}

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view)
}