package com.wishbox.core.presentation.base

import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.FragmentManager
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.smedialink.common.delegates.bundle.bundleString
import com.wishbox.core.presentation.R
import kotlinx.android.synthetic.main.base_bottom_sheet.view.*

/**
 * Created by Pak Evgeniy on 03/12/2018.
 */

class BaseBottomSheetDialog : DialogFragment() {

    var firstClick: ((dialog: Dialog) -> Unit)? = null
    var secondClick: ((dialog: Dialog) -> Unit)? = null

    private var firstText: String? by bundleString()
    private var secondText: String? by bundleString()

    companion object {
        fun show(
            fragmentManager: FragmentManager,
            firstText: String,
            secondText: String,
            firstClick: ((dialog: Dialog) -> Unit)? = null,
            secondClick: ((dialog: Dialog) -> Unit)? = null
        ) {
            val dialog: BaseBottomSheetDialog =
                (fragmentManager.findFragmentByTag(BaseBottomSheetDialog::class.java.name) as? BaseBottomSheetDialog)
                    ?: BaseBottomSheetDialog()
                        .apply {
                            this.firstText = firstText
                            this.secondText = secondText
                            this.firstClick = firstClick
                            this.secondClick = secondClick
                        }

            if (!dialog.isAdded) {
                dialog.show(fragmentManager, BaseBottomSheetDialog::class.java.name)
            }
        }
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog =
        BottomSheetDialog(context!!, theme)

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.base_bottom_sheet, container, false)
            ?.also {
                with(it) {
                    textFirst.text = firstText
                    textSecond.text = secondText
                    textFirst.setOnClickListener { firstClick?.invoke(dialog) }
                    textSecond.setOnClickListener { secondClick?.invoke(dialog) }
                    buttonClose.setOnClickListener { dismiss() }
                }
            }
    }
}

