package com.wishbox.core.presentation.widget

import android.animation.Animator

/**
 * Created by Pak Evgeniy on 11.01.2019
 */

interface EmptyAnimatorListener : Animator.AnimatorListener {
    override fun onAnimationRepeat(animation: Animator?) {}
    override fun onAnimationEnd(animation: Animator?) {}
    override fun onAnimationCancel(animation: Animator?) {}
    override fun onAnimationStart(animation: Animator?) {}
}