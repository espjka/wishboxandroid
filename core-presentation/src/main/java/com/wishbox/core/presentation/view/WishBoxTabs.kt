package com.wishbox.core.presentation.view

import android.content.Context
import android.os.Bundle
import android.os.Parcelable
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.ViewTreeObserver
import android.widget.LinearLayout
import android.widget.TextView
import com.wishbox.core.presentation.R
import kotlinx.android.synthetic.main.wish_box_tabs.view.*

class WishBoxTabs : LinearLayout {

    companion object {
        private const val SCALE_MIN = 0.66f
        private const val SCALE_MAX = 1f
        private const val ALPHA_MAX = 1f
        private const val ALPHA_MIN = 0.4f

        private const val CURRENT_ITEM_KEY = "current_item"
        private const val SUPER_STATE_KEY = "super_state"
        private const val FIRST_START_KEY = "first_start"
    }

    private var currentItem: Int = 0
    private var isFirstStart: Boolean = true
    private var rightTransitionX = 0f

    var itemSelected: ((position: Int) -> Unit)? = null

    constructor(context: Context?) : this(context, null)
    constructor(context: Context?, attrs: AttributeSet?) : this(context, attrs, 0)
    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : this(context, attrs, defStyleAttr, 0)
    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int, defStyleRes: Int) : super(
        context,
        attrs,
        defStyleAttr,
        defStyleRes
    ) {
        init()

        val a = context?.theme?.obtainStyledAttributes(
            attrs,
            R.styleable.WishBoxTabs,
            0, 0
        )

        a?.let { typedArray ->
            typedArray.takeIf { a.hasValue(R.styleable.WishBoxTabs_textLeft) }
                ?.getText(R.styleable.WishBoxTabs_textLeft)?.apply {
                    tvLeft.text = this
                }

            typedArray.takeIf { a.hasValue(R.styleable.WishBoxTabs_textRight) }
                ?.getText(R.styleable.WishBoxTabs_textRight)?.apply {
                    tvRight.text = this
                }
        }.also {
            a?.recycle()
        }
    }

    override fun onSaveInstanceState(): Parcelable? {
        val bundle = Bundle()
        bundle.putParcelable(SUPER_STATE_KEY, super.onSaveInstanceState())
        bundle.putInt(CURRENT_ITEM_KEY, currentItem)
        bundle.putBoolean(FIRST_START_KEY, isFirstStart)
        return bundle
    }

    override fun onRestoreInstanceState(state: Parcelable?) {
        var _state = state
        if (_state is Bundle) {
            currentItem = _state.getInt(CURRENT_ITEM_KEY)
            isFirstStart = _state.getBoolean(FIRST_START_KEY)
            _state = _state.getParcelable(SUPER_STATE_KEY)
        }

        super.onRestoreInstanceState(_state)
    }

    private fun init() {
        LayoutInflater.from(context).inflate(R.layout.wish_box_tabs, this, true)

        tvLeft.setOnClickListener {
            if (currentItem == 0) return@setOnClickListener
            animateSelection(tvLeft, Selection.SELECTED, 0f)
            animateSelection(tvRight, Selection.UNSELECTED, 0f)
            currentItem = 0
            itemSelected?.invoke(0)
        }

        tvRight.setOnClickListener {
            if (currentItem == 1) return@setOnClickListener
            animateSelection(tvRight, Selection.SELECTED, -rightTransitionX)
            animateSelection(tvLeft, Selection.UNSELECTED, 0f)
            currentItem = 1
            itemSelected?.invoke(1)
        }

        viewTreeObserver.addOnGlobalLayoutListener(object : ViewTreeObserver.OnGlobalLayoutListener {
            override fun onGlobalLayout() {
                tvLeft.pivotX = 0f
                tvRight.pivotX = 0f
                tvLeft.pivotY = (tvLeft.height / 2).toFloat()
                tvRight.pivotY = (tvRight.height / 2).toFloat()

                rightTransitionX = tvLeft.width * (1 - SCALE_MIN)
                initialSelect(currentItem)
                viewTreeObserver.removeOnGlobalLayoutListener(this)
            }
        })
    }

    private fun initialSelect(position: Int) {
        if (position == 0) {
            select(tvLeft, Selection.SELECTED, 0f)
            select(tvRight, Selection.UNSELECTED, 0f)
        } else {
            select(tvRight, Selection.SELECTED, -rightTransitionX)
            select(tvLeft, Selection.UNSELECTED, 0f)
        }
    }

    private fun select(tv: TextView, selection: Selection, moveX: Float) {
        tv.scaleX = selection.scaleFactor
        tv.scaleY = selection.scaleFactor
        tv.translationX = moveX
        tv.alpha = selection.alpha
    }

    private fun animateSelection(tv: TextView, selection: Selection, moveX: Float) {
        tv.animate()
            .scaleX(selection.scaleFactor)
            .scaleY(selection.scaleFactor)
            .translationX(moveX)
            .alpha(selection.alpha)
            .start()
    }

    enum class Selection(
        val scaleFactor: Float,
        val alpha: Float
    ) {
        SELECTED(SCALE_MAX, ALPHA_MAX),
        UNSELECTED(SCALE_MIN, ALPHA_MIN)
    }

}