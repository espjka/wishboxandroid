package com.wishbox.core.presentation.view

import android.content.Context
import android.util.AttributeSet
import com.google.android.material.appbar.AppBarLayout
import com.smedialink.common.ext.statusBarHeight

class WishBoxAppBar : AppBarLayout {

    constructor(context: Context?) : this(context, null)
    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs) {
        init()
    }

    private fun init() {
        setPadding(paddingLeft, paddingTop + resources.statusBarHeight(), paddingRight, paddingEnd)
    }
}