package com.wishbox.core.presentation.base

import com.smedialink.common.events.LiveEvent
import ru.terrakok.cicerone.commands.Command

class NavigationEvent(vararg val commands: Command) : LiveEvent()