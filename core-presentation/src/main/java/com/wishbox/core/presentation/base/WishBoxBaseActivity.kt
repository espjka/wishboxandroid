package com.wishbox.core.presentation.base

import android.content.Context
import com.smedialink.common.base.BaseActivity
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper

abstract class WishBoxBaseActivity : BaseActivity() {

    override fun attachBaseContext(newBase: Context?) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase))
    }

}