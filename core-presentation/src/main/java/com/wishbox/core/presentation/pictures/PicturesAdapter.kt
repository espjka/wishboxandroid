package com.wishbox.core.presentation.pictures

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.viewpager.widget.PagerAdapter
import com.smedialink.common.delegates.adapter.ViewPagerDataDelegate
import com.wishbox.core.glide.loadImage
import com.wishbox.core.presentation.R

/**
 * Created by Pak Evgeniy on 14.12.2018
 */

class PicturesAdapter(private val context: Context) : PagerAdapter() {

    var urls: List<String> by ViewPagerDataDelegate(this)

    override fun isViewFromObject(view: View, `object`: Any): Boolean = view == `object`

    override fun getCount(): Int = urls.size

    override fun getItemPosition(`object`: Any): Int = PagerAdapter.POSITION_NONE

    override fun instantiateItem(collection: ViewGroup, position: Int): Any {
        val image = (LayoutInflater.from(context).inflate(R.layout.item_picture, collection, false) as ImageView)
        image.loadImage(model = urls[position], placeholder = R.drawable.placeholder_wish)
        collection.addView(image)
        return image
    }

    override fun destroyItem(collection: ViewGroup, position: Int, view: Any) {
        collection.removeView(view as View)
    }
}