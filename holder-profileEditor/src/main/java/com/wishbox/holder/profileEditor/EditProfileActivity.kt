package com.wishbox.holder.profileEditor

import com.wishbox.core.di.api.FeatureProviderApi
import com.wishbox.core.di.api.RouterApi
import com.wishbox.core.di.api.RxBusApi
import com.wishbox.core.di.provider.FeatureProviderApiProvider
import com.wishbox.core.di.provider.RouterApiProvider
import com.wishbox.core.di.provider.RxBusApiProvider
import com.wishbox.core.presentation.base.BaseActivity
import com.wishbox.featurecreateprofile.R
import com.wishbox.holder.profileEditor.di.EditProfileFlowComponent
import ru.terrakok.cicerone.commands.Replace

class EditProfileActivity : BaseActivity(), RxBusApiProvider, FeatureProviderApiProvider, RouterApiProvider {

    private lateinit var component: EditProfileFlowComponent

    companion object {
        const val BUNDLE_AUTH_FLOW = "BUNDLE_AUTH_FLOW"
    }

    override val rxBusApi: RxBusApi by lazy {
        return@lazy component
    }

    override val featureProviderApi: FeatureProviderApi by lazy {
        return@lazy component
    }

    override val routerApi: RouterApi by lazy {
        return@lazy component
    }

    override fun injectDependencies() {
        component = EditProfileFlowComponent.inject(this)
    }

    override fun layoutRes(): Int = R.layout.activity_edit_profile

    override fun viewCreated() {
        if (supportFragmentManager.findFragmentById(R.id.container) == null) {
            navigator.applyCommands(arrayOf(Replace(EditProfileScreen(intent.extras?.getBoolean(BUNDLE_AUTH_FLOW)))))
        }
    }
}
