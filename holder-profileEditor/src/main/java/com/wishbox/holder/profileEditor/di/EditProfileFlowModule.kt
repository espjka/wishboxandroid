package com.wishbox.holder.profileEditor.di

import com.wishbox.core.di.rxbusprovider.RxBusProvider
import com.wishbox.core.di.scope.PerActivity
import com.wishbox.core.di.screenproviders.FeatureProvider
import com.wishbox.featurecreateprofile.R
import com.wishbox.holder.profileEditor.EditProfileActivity
import com.wishbox.holder.profileEditor.SelectionBusProvider
import com.wishbox.holder.profileEditor.navigation.EditProfileFeatureProvider
import dagger.Binds
import dagger.Module
import dagger.Provides
import ru.terrakok.cicerone.Navigator
import ru.terrakok.cicerone.android.support.SupportAppNavigator

@Module
abstract class EditProfileFlowModule {

    @Binds
    @PerActivity
    abstract fun provideSelectionBusProvider(impl: SelectionBusProvider): RxBusProvider

    @Binds
    abstract fun provideFeatureProvider(impl: EditProfileFeatureProvider): FeatureProvider

    @Module
    companion object {

        @JvmStatic
        @Provides
        @PerActivity
        fun provideNavigator(activity: EditProfileActivity): Navigator =
            SupportAppNavigator(activity, activity.supportFragmentManager, R.id.container)
    }
}
