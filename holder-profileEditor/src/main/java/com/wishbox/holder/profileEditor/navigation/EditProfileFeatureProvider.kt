package com.wishbox.holder.profileEditor.navigation

import androidx.fragment.app.Fragment
import com.wishbox.core.di.screenproviders.FeatureProvider
import com.wishbox.core.di.screenproviders.FriendsEditorProvider
import com.wishbox.core.di.screenproviders.Selections
import com.wishbox.feature.profileSelectItemValue.presentation.SelectionFragment
import ru.terrakok.cicerone.android.support.SupportAppScreen
import javax.inject.Inject

/**
 * Created by Pak Evgeniy on 09.01.2019
 */

class EditProfileFeatureProvider @Inject constructor(
    private val friendsEditorProvider: FriendsEditorProvider
) : FeatureProvider {

    override fun getSelectionScreen(selection: Selections): SupportAppScreen {
        return object : SupportAppScreen() {
            override fun getFragment(): Fragment = SelectionFragment.createWithSelectionType(selection)
        }
    }

    override fun getFriendsEditor(): SupportAppScreen {
        return friendsEditorProvider.getFriendsEditorScreen(true)
    }
}