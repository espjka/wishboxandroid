package com.wishbox.holder.profileEditor.di

import com.wishbox.core.di.api.FeatureProviderApi
import com.wishbox.core.di.api.InjectorApi
import com.wishbox.core.di.api.RouterApi
import com.wishbox.core.di.api.RxBusApi
import com.wishbox.core.di.module.GlobalRouterModule
import com.wishbox.core.di.module.ViewModelModule
import com.wishbox.core.di.provider.InjectorApiProvider
import com.wishbox.core.di.scope.PerActivity
import com.wishbox.core.di.util.lookupApiProvider
import com.wishbox.holder.profileEditor.EditProfileActivity
import dagger.BindsInstance
import dagger.Component

@Component(
    dependencies = [InjectorApi::class],
    modules = [
        GlobalRouterModule::class,
        ViewModelModule::class,
        EditProfileFlowModule::class
    ]
)
@PerActivity
interface EditProfileFlowComponent : RxBusApi, FeatureProviderApi, RouterApi {

    fun inject(target: EditProfileActivity)

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun bind(activity: EditProfileActivity): Builder

        fun api(api: InjectorApi): Builder

        fun build(): EditProfileFlowComponent
    }

    companion object {

        fun inject(activity: EditProfileActivity): EditProfileFlowComponent {
            val injectorProvider = activity.lookupApiProvider<InjectorApiProvider>()

            val component = DaggerEditProfileFlowComponent.builder()
                .bind(activity)
                .api(injectorProvider.injectorApi)
                .build()

            component.inject(activity)
            return component
        }
    }

}