package com.wishbox.holder.profileEditor

import androidx.fragment.app.Fragment
import com.wishbox.featurecreateprofile.edit.presentation.EditProfileFragment
import ru.terrakok.cicerone.android.support.SupportAppScreen

class EditProfileScreen(private val isCreation: Boolean?) : SupportAppScreen() {
    override fun getFragment(): Fragment {
        return EditProfileFragment.newInstance(isCreation)
    }
}