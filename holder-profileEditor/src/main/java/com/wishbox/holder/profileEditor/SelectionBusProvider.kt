package com.wishbox.holder.profileEditor

import com.smedialink.common.rxbus.RxEventBus
import com.wishbox.core.di.rxbusprovider.RxBusProvider
import com.wishbox.core.di.scope.PerActivity
import javax.inject.Inject

/**
 * Created by Pak Evgeniy on 29.12.2018
 */

@PerActivity
class SelectionBusProvider @Inject constructor() : RxBusProvider {

    private val rxBus: RxEventBus<RxBusProvider.ItemValueSelectedEvent> by lazy {
        object : RxEventBus<RxBusProvider.ItemValueSelectedEvent>() {
            override fun defultEvent(): RxBusProvider.ItemValueSelectedEvent {
                return RxBusProvider.ItemValueSelectedEvent()
            }
        }
    }

    override fun provideProfileSelectRxBus(): RxEventBus<RxBusProvider.ItemValueSelectedEvent> = rxBus

}