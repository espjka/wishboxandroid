package com.wishbox.featurecreateprofile.selection.presentation

import com.wishbox.core.presentation.resources.ErrorTextProvider
import com.wishbox.feature.profileSelectItemValue.presentation.SelectionErrorState
import javax.inject.Inject

class SelectionErrorStateHandler @Inject constructor(
    private val errorTextProvider: ErrorTextProvider
) {

    fun handle(prevErrorState: SelectionErrorState?, throwable: Throwable): SelectionErrorState {
        val prevState = prevErrorState ?: SelectionErrorState()
        val errorEvent = errorTextProvider.provideErrorEvent(throwable)
        return prevState.copy(errorEvent = errorEvent)
    }
}