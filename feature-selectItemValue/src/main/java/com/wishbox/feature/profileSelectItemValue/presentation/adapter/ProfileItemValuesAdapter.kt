package com.wishbox.featurecreateprofile.selection.presentation.adapter

import com.hannesdorfmann.adapterdelegates4.ListDelegationAdapter
import com.wishbox.core.presentation.pagination.PaginationErrorAdapterDelegate
import com.wishbox.core.presentation.pagination.PaginationProgressAdapterDelegate
import com.wishbox.feature.profileSelectItemValue.presentation.adapter.SelectionListViewType
import javax.inject.Inject

class ProfileItemValuesAdapter @Inject constructor(
    itemValueClick: (wish: SelectionListViewType.ItemValue) -> Unit,
    errorClick: () -> Unit
) : ListDelegationAdapter<List<SelectionListViewType>>() {

    init {
        delegatesManager.addDelegate(ProfileItemValuesAdapterDelegate(itemValueClick))
            .addDelegate(PaginationProgressAdapterDelegate<SelectionListViewType.Loading, SelectionListViewType> { it is SelectionListViewType.Loading })
            .addDelegate(
                PaginationErrorAdapterDelegate<SelectionListViewType.Error, SelectionListViewType>(
                    isForViewType = { it is SelectionListViewType.Error },
                    errorClick = errorClick
                )
            )
    }

    fun addLoadingItem() {
        items = items.toMutableList().apply {
            if (items.contains(SelectionListViewType.Loading).not())
                add(SelectionListViewType.Loading)
        }

    }

    fun removeLoadingItem() {
        items = items.toMutableList().apply {
            if (contains(SelectionListViewType.Loading))
                remove(SelectionListViewType.Loading)
        }
    }

    fun addErrorItem() {
        items = items.toMutableList().apply {
            if (contains(SelectionListViewType.Error).not())
                add(SelectionListViewType.Error)
        }
    }

    fun removeErrorItem() {
        items = items?.toMutableList()?.apply {
            if (contains(SelectionListViewType.Error))
                remove(SelectionListViewType.Error)
        }
    }


}