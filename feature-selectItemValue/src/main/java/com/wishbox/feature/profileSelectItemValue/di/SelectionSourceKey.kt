package com.wishbox.feature.profileSelectItemValue.di

import com.wishbox.core.di.screenproviders.Selections
import dagger.MapKey

@Target(AnnotationTarget.FUNCTION, AnnotationTarget.PROPERTY_GETTER, AnnotationTarget.PROPERTY_SETTER)
@kotlin.annotation.Retention
@MapKey
annotation class SelectionSourceKey(val value: Selections) {
}