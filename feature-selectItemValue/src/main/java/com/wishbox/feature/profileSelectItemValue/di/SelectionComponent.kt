package com.wishbox.feature.profileSelectItemValue.di

import com.wishbox.core.di.api.FeatureProviderApi
import com.wishbox.core.di.api.InjectorApi
import com.wishbox.core.di.api.RouterApi
import com.wishbox.core.di.api.RxBusApi
import com.wishbox.core.di.module.ViewModelModule
import com.wishbox.core.di.provider.FeatureProviderApiProvider
import com.wishbox.core.di.provider.InjectorApiProvider
import com.wishbox.core.di.provider.RouterApiProvider
import com.wishbox.core.di.provider.RxBusApiProvider
import com.wishbox.core.di.util.lookupApiProvider
import com.wishbox.feature.profileSelectItemValue.presentation.SelectionFragment
import dagger.BindsInstance
import dagger.Component

@Component(
    dependencies = [InjectorApi::class, RxBusApi::class, FeatureProviderApi::class, RouterApi::class],
    modules = [SelectionModule::class, ViewModelModule::class]
)
interface SelectionComponent {

    fun inject(target: SelectionFragment)

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun bind(fragment: SelectionFragment): Builder

        fun api(api: InjectorApi): Builder

        fun api(api: RxBusApi): Builder

        fun api(api: FeatureProviderApi): Builder

        fun api(api: RouterApi): Builder

        fun build(): SelectionComponent
    }

    companion object {

        fun inject(fragment: SelectionFragment) {
            val injectorApiProvider: InjectorApiProvider = fragment.lookupApiProvider()
            val routerApiProvider: RouterApiProvider = fragment.lookupApiProvider()
            val featureProviderApiProvider: FeatureProviderApiProvider = fragment.lookupApiProvider()
            val rxBusApiProvider: RxBusApiProvider = fragment.lookupApiProvider()

            DaggerSelectionComponent.builder()
                .api(injectorApiProvider.injectorApi)
                .api(rxBusApiProvider.rxBusApi)
                .api(featureProviderApiProvider.featureProviderApi)
                .api(routerApiProvider.routerApi)
                .bind(fragment)
                .build()
                .inject(fragment)
        }
    }

}