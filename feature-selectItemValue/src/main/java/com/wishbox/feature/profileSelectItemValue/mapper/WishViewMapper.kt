package com.wishbox.feature.profileSelectItemValue.mapper

import android.content.Context
import com.wishbox.core.domain.entity.ItemValue
import com.wishbox.feature.R
import com.wishbox.feature.profileSelectItemValue.presentation.adapter.SelectionListViewType
import javax.inject.Inject

/**
 * Created by Pak Evgeniy on 28/11/2018.
 */

class SelectionMapper @Inject constructor(
    private val context: Context
) {

    private val defaultValue: String by lazy { context.getString(R.string.not_specified) }

    fun mapToViewTypeList(
        from: List<ItemValue>,
        addDefault: Boolean = false
    ): List<SelectionListViewType> =
        from.map { mapToViewType(it) }.toMutableList()
            .apply { if (addDefault) add(0, SelectionListViewType.ItemValue(0, defaultValue)) }

    private fun mapToViewType(from: ItemValue): SelectionListViewType =
        SelectionListViewType.ItemValue(
            from.id,
            from.name
        )

    fun mapToItemValue(from: SelectionListViewType.ItemValue): ItemValue =
        ItemValue(
            id = from.id,
            name = from.name
        )
}