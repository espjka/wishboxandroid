package com.wishbox.feature.profileSelectItemValue.presentation

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.smedialink.common.base.BaseViewModel
import com.smedialink.common.delegates.rx.disposableDelegate
import com.smedialink.common.ext.applySchedulers
import com.smedialink.common.ext.reduceState
import com.smedialink.common.rxbus.RxEventBus
import com.wishbox.core.di.rxbusprovider.RxBusProvider
import com.wishbox.core.domain.entity.ProfileItemValueType
import com.wishbox.core.presentation.pagination.Paginator
import com.wishbox.feature.profileSelectItemValue.mapper.SelectionMapper
import com.wishbox.feature.profileSelectItemValue.presentation.adapter.SelectionListViewType
import com.wishbox.featurecreateprofile.selection.domain.GetProfileItemValuesUseCase
import com.wishbox.featurecreateprofile.selection.presentation.SelectionErrorStateHandler
import io.reactivex.disposables.Disposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import ru.terrakok.cicerone.Router

class SelectionViewModel constructor(
    private val router: Router,
    private val getProfileItemValuesUseCase: GetProfileItemValuesUseCase,
    private val selectionMapper: SelectionMapper,
    private val errorHandler: SelectionErrorStateHandler,
    private val profileItemValueType: ProfileItemValueType,
    private val profileItemValueSelectedEventBus: RxEventBus<RxBusProvider.ItemValueSelectedEvent>
) : BaseViewModel() {

    private var disposable: Disposable? by disposableDelegate()

    private val internalState by lazy {
        MutableLiveData<SelectionViewState>()
    }

    init {
        internalState.value = SelectionViewState(
            loadingState = LoadingState.NONE,
            errorState = null,
            items = emptyList()
        )
    }

    val state: LiveData<SelectionViewState> = internalState

    fun requestProfileItemValues(queryString: String? = null, needToDrop: Boolean = false) {
        disposable = getProfileItemValuesUseCase.invoke(queryString, needToDrop = needToDrop)
            .applySchedulers()
            .map {
                selectionMapper.mapToViewTypeList(
                    it,
                    profileItemValueType != ProfileItemValueType.CITY && internalState.value?.items.isNullOrEmpty()
                )
            }
            .doOnSubscribe {
                internalState.reduceState { prevState ->
                    prevState.copy(
                        loadingState = if (needToDrop || prevState.items.isNullOrEmpty()) {
                            LoadingState.INITIAL
                        } else {
                            LoadingState.LOAD_MORE
                        },
                        isEmpty = false,
                        emptyListError = false,
                        hasLoadPage = false
                    )
                }
            }
            .subscribeBy(
                onSuccess = { data ->
                    internalState.reduceState {
                        it.copy(
                            loadingState = LoadingState.NONE,
                            hasLoadPage = (data.size < Paginator.LIMIT).not(),
                            isEmpty = data.isEmpty(),
                            emptyListError = false,
                            items = data
                        )
                    }
                },
                onError = { throwable ->
                    internalState.reduceState {
                        it.copy(
                            loadingState = LoadingState.NONE,
                            hasLoadPage = true,
                            isEmpty = false,
                            emptyListError = it.items.isNullOrEmpty(),
                            errorState = if (it.loadingState == LoadingState.LOAD_MORE) {
                                SelectionErrorState(false, null)
                            } else {
                                errorHandler.handle(it.errorState, throwable)
                            }
                        )
                    }
                }
            )
            .addTo(disposables)
    }

    fun returnSelection(it: SelectionListViewType.ItemValue) {
        profileItemValueSelectedEventBus.post(
            RxBusProvider.ItemValueSelectedEvent(
                profileItemValueType,
                selectionMapper.mapToItemValue(it)
            )
        )
        router.exit()
    }

    fun navigateBack() {
        router.exit()
    }
}