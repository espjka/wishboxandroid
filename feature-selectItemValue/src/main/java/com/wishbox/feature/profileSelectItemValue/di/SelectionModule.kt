package com.wishbox.feature.profileSelectItemValue.di

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.wishbox.core.di.mapkeys.ViewModelKey
import com.wishbox.core.di.rxbusprovider.RxBusProvider
import com.wishbox.core.di.screenproviders.Selections
import com.wishbox.core.domain.entity.ProfileItemValueType
import com.wishbox.core.presentation.pagination.Paginator
import com.wishbox.feature.profileSelectItemValue.mapper.SelectionMapper
import com.wishbox.feature.profileSelectItemValue.presentation.SelectionFragment
import com.wishbox.feature.profileSelectItemValue.presentation.SelectionViewModel
import com.wishbox.featurecreateprofile.selection.domain.GetProfileItemValuesUseCase
import com.wishbox.featurecreateprofile.selection.presentation.SelectionErrorStateHandler
import com.wishbox.featurecreateprofile.selection.presentation.adapter.ProfileItemValuesAdapter
import dagger.Module
import dagger.Provides
import dagger.multibindings.IntoMap
import ru.terrakok.cicerone.Router

@Module
abstract class SelectionModule {

    @Module
    companion object {
        @Provides
        @IntoMap
        @JvmStatic
        @ViewModelKey(SelectionViewModel::class)
        fun provideViewModel(
            router: Router,
            getProfileItemValuesUseCase: GetProfileItemValuesUseCase,
            selectionMapper: SelectionMapper,
            rxBusProvider: RxBusProvider,
            profileItemValueType: ProfileItemValueType,
            errorHandler: SelectionErrorStateHandler
        ): ViewModel =
            SelectionViewModel(
                router,
                getProfileItemValuesUseCase,
                selectionMapper,
                errorHandler,
                profileItemValueType,
                rxBusProvider.provideProfileSelectRxBus()
            )

        @Provides
        @JvmStatic
        fun provideEditProfileViewModel(
            fragment: SelectionFragment,
            factory: ViewModelProvider.Factory
        ): SelectionViewModel = ViewModelProviders.of(fragment, factory).get(
            SelectionViewModel::class.java
        )

        @Provides
        @JvmStatic
        fun provideSelection(selectionFragment: SelectionFragment): Selections =
            Selections.resolve(
                selectionFragment.selectionType!!
            )

        @Provides
        @IntoMap
        @JvmStatic
        @SelectionSourceKey(Selections.CITY)
        fun provideCityItemValueType(): ProfileItemValueType =
            ProfileItemValueType.CITY

        @Provides
        @IntoMap
        @JvmStatic
        @SelectionSourceKey(Selections.HOBBIES)
        fun provideHobbyItemValueType(): ProfileItemValueType =
            ProfileItemValueType.HOBBY

        @Provides
        @IntoMap
        @JvmStatic
        @SelectionSourceKey(Selections.FLOWERS)
        fun provideItemValueType(): ProfileItemValueType =
            ProfileItemValueType.FLOWERS

        @Provides
        @IntoMap
        @JvmStatic
        @SelectionSourceKey(Selections.ALCOHOL)
        fun provideAlcoholItemValueType(): ProfileItemValueType =
            ProfileItemValueType.ALCOHOL

        @Provides
        @IntoMap
        @JvmStatic
        @SelectionSourceKey(Selections.SWEETIES)
        fun provideSweetsItemValueType(): ProfileItemValueType =
            ProfileItemValueType.SWEETS

        @Provides
        @JvmStatic
        fun provideCurrentProfileItemValueType(
            types: MutableMap<Selections, ProfileItemValueType>,
            selection: Selections
        ): ProfileItemValueType {
            return types[selection] ?: throw IllegalArgumentException("Can't detect item value type")
        }

        @Provides
        @JvmStatic
        fun providePaginator(viewModel: SelectionViewModel): Paginator {
            return Paginator {
                viewModel.requestProfileItemValues()
            }
        }

        @Provides
        @JvmStatic
        fun provideProfileSelectionAdapter(viewModel: SelectionViewModel): ProfileItemValuesAdapter =
            ProfileItemValuesAdapter(
                itemValueClick = { viewModel.returnSelection(it) },
                errorClick = {
                    viewModel.requestProfileItemValues(needToDrop = true)
                }
            )
    }

}