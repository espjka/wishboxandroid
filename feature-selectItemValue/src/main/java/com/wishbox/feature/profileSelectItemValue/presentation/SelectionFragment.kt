package com.wishbox.feature.profileSelectItemValue.presentation

import android.os.Bundle
import android.view.View
import androidx.core.view.isVisible
import androidx.recyclerview.widget.LinearLayoutManager
import com.jakewharton.rxbinding3.widget.textChanges
import com.smedialink.common.base.BaseFragment
import com.smedialink.common.delegates.bundle.bundleInt
import com.smedialink.common.events.handle
import com.smedialink.common.ext.gone
import com.smedialink.common.ext.invisible
import com.smedialink.common.ext.observe
import com.smedialink.common.ext.visible
import com.smedialink.common.util.StateHolder
import com.wishbox.core.di.screenproviders.Selections
import com.wishbox.core.presentation.errorHandling.delegate.ToastDelegate
import com.wishbox.core.presentation.pagination.Paginator
import com.wishbox.feature.R
import com.wishbox.feature.profileSelectItemValue.di.SelectionComponent
import com.wishbox.featurecreateprofile.selection.presentation.adapter.ProfileItemValuesAdapter
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import kotlinx.android.synthetic.main.fragment_selection.*
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class SelectionFragment : BaseFragment() {

    @Inject
    lateinit var viewModel: SelectionViewModel

    @Inject
    lateinit var paginator: Paginator

    @Inject
    lateinit var errorDelegate: ToastDelegate

    @Inject
    lateinit var profileSelectionAdapter: ProfileItemValuesAdapter

    private var rxViewCompositeDisposable = CompositeDisposable()

    override val layoutRes: Int = R.layout.fragment_selection

    var selectionType: Int? by bundleInt()

    private var stateHolder = StateHolder<SelectionViewState>()

    companion object {
        fun createWithSelectionType(selection: Selections): SelectionFragment {
            return SelectionFragment().apply {
                selectionType = selection.ordinal
            }
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        Selections.resolve(selectionType ?: 0).apply(::handleSelection)

        initRecyclerView()
        initUi()
    }

    private fun handleSelection(selection: Selections) {
        val title = when (selection) {
            Selections.FLOWERS -> R.string.selection_flower_title
            Selections.CITY -> R.string.selection_city_title
            Selections.SWEETIES -> R.string.selection_sweet_title
            Selections.ALCOHOL -> R.string.selection_alcohol_title
            Selections.HOBBIES -> R.string.selection_hobby_title
        }
        toolbar.getTitleText().text = getText(title)
        if (selection == Selections.CITY) {
            etSearch.visible()
            etSearch.setHint(R.string.selection_city_hint)
        }
    }

    private fun initUi() {
        etSearch.textChanges()
            .skipInitialValue()
            .debounce(400, TimeUnit.MILLISECONDS)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeBy {
                viewModel.requestProfileItemValues(it.toString(), true)
            }.addTo(rxViewCompositeDisposable)

        textRetry.setOnClickListener {
            paginator.load()
        }
        toolbar.getLeftButton().setOnClickListener { viewModel.navigateBack() }

        viewModel.state.observe(this) { state ->
            render(state)
        }
    }

    private fun initRecyclerView() {
        rvResults.run {
            adapter = profileSelectionAdapter
            layoutManager = LinearLayoutManager(this@SelectionFragment.context)
        }
        paginator.withRecyclerView(rvResults)
    }


    private fun render(state: SelectionViewState) {
        stateHolder = stateHolder.newState(state)

        stateHolder.run {
            diff({ loadingState }, { it ->
                when (it) {
                    LoadingState.INITIAL -> {
                        progress.visible()
                        rvResults.invisible()
                        profileSelectionAdapter.removeErrorItem()
                    }

                    LoadingState.LOAD_MORE -> {
                        profileSelectionAdapter.addLoadingItem()
                        profileSelectionAdapter.removeErrorItem()
                    }

                    LoadingState.NONE -> {
                        progress.gone()
                        rvResults.visible()
                        profileSelectionAdapter.removeLoadingItem()
                    }
                }
            })

            diffNullable({ errorState }, { errorState ->
                errorState?.let {
                    if (!it.isInitialLoadingError)
                        profileSelectionAdapter.addErrorItem()
                }

                errorState?.errorEvent?.handle {
                    errorDelegate.show(it.errorText)
                }
            })

            diff({ emptyListError }, {
                layoutError.isVisible = it
            })

            diff({ items }, {
                profileSelectionAdapter.items = it
                profileSelectionAdapter.notifyDataSetChanged()
            })

            diff({ hasLoadPage }, {
                if (it) { paginator.isEnabled = it }
            })

            diff({ isEmpty }) {
                if (it) textEmpty.visible() else textEmpty.gone()
            }
        }
    }


    override fun inject() {
        SelectionComponent.inject(this)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        rxViewCompositeDisposable.clear()
    }
}


