package com.wishbox.feature.profileSelectItemValue.presentation.adapter

sealed class SelectionListViewType {

    data class ItemValue(
        val id: Long,
        val name: String
    ) : SelectionListViewType()


    object Loading : SelectionListViewType()
    object Error : SelectionListViewType()
}