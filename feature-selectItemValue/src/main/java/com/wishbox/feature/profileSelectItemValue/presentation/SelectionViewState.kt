package com.wishbox.feature.profileSelectItemValue.presentation

import com.smedialink.common.events.ErrorEvent
import com.wishbox.feature.profileSelectItemValue.presentation.adapter.SelectionListViewType

data class SelectionViewState(
    val items: List<SelectionListViewType>,
    val loadingState: LoadingState = LoadingState.NONE,
    val errorState: SelectionErrorState? = null,
    val hasLoadPage: Boolean = true,
    val isEmpty: Boolean = false,
    val emptyListError: Boolean = false
)

data class SelectionErrorState(
    val isInitialLoadingError : Boolean = true,
    val errorEvent: ErrorEvent? = null
)

enum class LoadingState {
    NONE ,
    INITIAL,
    LOAD_MORE,
}