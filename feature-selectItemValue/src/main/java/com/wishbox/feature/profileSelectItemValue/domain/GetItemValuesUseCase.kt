package com.wishbox.featurecreateprofile.selection.domain

import com.wishbox.core.domain.entity.ItemValue
import com.wishbox.core.domain.entity.ProfileItemValueType
import com.wishbox.core.domain.repository.ProfileRepository
import com.wishbox.core.presentation.pagination.Paginator
import io.reactivex.Single
import javax.inject.Inject

class GetProfileItemValuesUseCase @Inject constructor(
    private val type: ProfileItemValueType,
    private val profileRepository: ProfileRepository
) {

    private var offset = 0

    operator fun invoke(query: String? = null, needToDrop: Boolean): Single<List<ItemValue>> {
        if (needToDrop)
            offset = 0

        return profileRepository.requestProfileItemValues(type, query, Paginator.LIMIT, offset)
            .doOnSuccess {
                offset += Paginator.LIMIT
            }
    }
}

