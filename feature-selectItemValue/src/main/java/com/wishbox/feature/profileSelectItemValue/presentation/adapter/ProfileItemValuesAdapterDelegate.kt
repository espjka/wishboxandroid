package com.wishbox.featurecreateprofile.selection.presentation.adapter

import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.hannesdorfmann.adapterdelegates4.AbsListItemAdapterDelegate
import com.smedialink.common.ext.inflate
import com.wishbox.feature.R
import com.wishbox.feature.profileSelectItemValue.presentation.adapter.SelectionListViewType
import kotlinx.android.synthetic.main.item_selection.view.*

class ProfileItemValuesAdapterDelegate(private val selectionClick: (SelectionListViewType.ItemValue) -> Unit) :
    AbsListItemAdapterDelegate<SelectionListViewType.ItemValue, SelectionListViewType, ProfileItemValuesAdapterDelegate.ViewHolder>() {

    override fun isForViewType(
        item: SelectionListViewType,
        items: MutableList<SelectionListViewType>,
        position: Int
    ): Boolean = item is SelectionListViewType.ItemValue

    override fun onBindViewHolder(
        item: SelectionListViewType.ItemValue,
        holder: ViewHolder,
        payloads: MutableList<Any>
    ) {
        holder.bind(item)
    }

    override fun onCreateViewHolder(parent: ViewGroup): ViewHolder =
        ViewHolder(
            parent.inflate(R.layout.item_selection),
            selectionClick
        )

    class ViewHolder(
        view: View,
        private val selectionClick: (SelectionListViewType.ItemValue) -> Unit
    ) : RecyclerView.ViewHolder(view) {

        fun bind(item: SelectionListViewType.ItemValue) =
            with(itemView) {
                setOnClickListener { selectionClick.invoke(item) }
                tvSelectionName.text = item.name
            }
    }

}
