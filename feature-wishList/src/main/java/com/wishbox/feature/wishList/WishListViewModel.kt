package com.wishbox.feature.wishList

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.smedialink.common.base.BaseViewModel
import com.smedialink.common.delegates.rx.disposableDelegate
import com.smedialink.common.error.ErrorHandler
import com.smedialink.common.ext.applySchedulers
import com.wishbox.core.di.screenproviders.FeatureProvider
import com.wishbox.core.domain.entity.UserEntity
import com.wishbox.core.domain.exception.LastPageException
import com.wishbox.feature.wishList.mapper.UserViewMapper
import com.wishbox.feature.wishList.mapper.WishViewMapper
import com.wishbox.feature.wishList.useCase.ObserveWishListUseCase
import com.wishbox.feature.wishList.useCase.RequestWishListUseCase
import io.reactivex.Completable
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import ru.terrakok.cicerone.Router
import javax.inject.Inject

/**
 * Created by Pak Evgeniy on 28/11/2018.
 */

class WishListViewModel @Inject constructor(
    private val router: Router,
    private val userId: Long?,
    private val mapper: WishViewMapper,
    private val userMapper: UserViewMapper,
    private val errorHandler: ErrorHandler,
    private val requestWishList: RequestWishListUseCase,
    private val getUser: (userId: Long) -> Single<UserEntity>,
    private val observeWishList: ObserveWishListUseCase,
    private val deleteWish: (id: Long) -> Completable,
    private val featureProvider: FeatureProvider
) : BaseViewModel() {

    private val state: MutableLiveData<WishListState> by lazy { MutableLiveData<WishListState>() }

    private var wishListDisposable: Disposable? by disposableDelegate()
    private var observeWishListDisposable: Disposable? by disposableDelegate()
    private var deleteWishDisposable: Disposable? by disposableDelegate()
    private var getUserDisposable: Disposable? by disposableDelegate()

    fun getState(): LiveData<WishListState> = state

    private fun observeWishList() {
        if (observeWishListDisposable != null) return
        observeWishListDisposable = observeWishList.invoke(userId)
            .map { mapper.mapToViewTypeList(it) }
            .map { WishListStateChanges.Data(it) }
            .applySchedulers()
            .subscribeBy(
                onError = { reduceState(WishListStateChanges.Error(errorHandler.handle(it))) },
                onNext = { reduceState(it) }
            ).addTo(disposables)
    }

    fun requestUser() {
        getUserDisposable = userId?.let(getUser)
            ?.map { userMapper.mapName(it) }
            ?.subscribeOn(Schedulers.io())
            ?.observeOn(AndroidSchedulers.mainThread())
            ?.subscribeBy(
                onError = { reduceState(WishListStateChanges.Error(errorHandler.handle(it))) },
                onSuccess = { reduceState(WishListStateChanges.UserName(it)) }
            )
            ?.addTo(disposables)
    }

    fun requestWishList(needToDrop: Boolean = false) {
        wishListDisposable = requestWishList.invoke(userId, needToDrop)
            .applySchedulers()
            .doOnSubscribe { reduceState(WishListStateChanges.Loading(needToDrop)) }
            .subscribeBy(
                onError = {
                    when (it) {
                        is LastPageException -> reduceState(WishListStateChanges.Success(false))
                        else -> reduceState(
                            WishListStateChanges.Error(
                                errorHandler.handle(it),
                                needToDrop
                            )
                        )
                    }
                    observeWishList()
                },
                onComplete = {
                    reduceState(WishListStateChanges.Success(true))
                    observeWishList()
                }
            ).addTo(disposables)
    }

    fun openWishInfo(id: Long) {
        router.navigateTo(featureProvider.getWishDetail(id, userId))
    }

    fun openWishEdit() {
        router.navigateTo(featureProvider.getWishEditInfo())
    }

    private fun reduceState(changes: WishListStateChanges) {
        val prevState = state.value ?: WishListState()
        state.value = changes.reduceState(prevState)
    }

    fun navigateBack() {
        router.exit()
    }

    fun deleteWish(id: Long) {
        deleteWishDisposable = deleteWish.invoke(id)
            .applySchedulers()
            .subscribeBy(
                onError = { reduceState(WishListStateChanges.Error(errorHandler.handle(it))) },
                onComplete = { }
            ).addTo(disposables)
    }
}