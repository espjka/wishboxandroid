package com.wishbox.feature.wishList.mapper

import com.wishbox.core.domain.entity.WishEntity
import com.wishbox.feature.wishList.adapter.WishAdapterViewType
import javax.inject.Inject

/**
 * Created by Pak Evgeniy on 28/11/2018.
 */

class WishViewMapper @Inject constructor() {

    fun mapToViewTypeList(from: List<WishEntity>): List<WishAdapterViewType> =
            from.map { mapToViewType(it) }

    private fun mapToViewType(from: WishEntity): WishAdapterViewType =
        WishAdapterViewType.Wish(
            id = from.id,
            name = from.name,
            comment = from.comment,
            url = from.pictures?.firstOrNull()?.medium
        )
}