package com.wishbox.feature.wishList.adapter

import android.os.Parcel
import android.os.Parcelable

/**
 * Created by Pak Evgeniy on 28/11/2018.
 */

sealed class WishAdapterViewType {

    data class Wish(
        val id: Long,
        val name: String,
        val comment: String? = null,
        val url: String? = null
    ): WishAdapterViewType(), Parcelable {

        constructor(parcel: Parcel) : this(
            parcel.readLong(),
            parcel.readString() ?: "",
            parcel.readString(),
            parcel.readString()
        )

        override fun writeToParcel(dest: Parcel?, flags: Int) {
            dest?.run {
                writeLong(id)
                writeString(name)
                writeString(comment)
                writeString(url)
            }
        }

        override fun describeContents(): Int = 0

        companion object CREATOR : Parcelable.Creator<Wish> {
            override fun createFromParcel(parcel: Parcel): Wish {
                return Wish(parcel)
            }

            override fun newArray(size: Int): Array<Wish?> {
                return arrayOfNulls(size)
            }
        }

    }

    object Loading : WishAdapterViewType()

    object Error : WishAdapterViewType()
}