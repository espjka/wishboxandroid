package com.wishbox.feature.wishList.adapter

import androidx.recyclerview.widget.DiffUtil
import javax.inject.Inject

/**
 * Created by Pak Evgeniy on 28/11/2018.
 */

class WishDiffCallback @Inject constructor() : DiffUtil.ItemCallback<WishAdapterViewType>() {

    override fun areItemsTheSame(
        oldItem: WishAdapterViewType,
        newItem: WishAdapterViewType
    ): Boolean {
        if (oldItem.javaClass != newItem.javaClass) return false
        return when (oldItem) {
            is WishAdapterViewType.Wish -> oldItem.id == (newItem as WishAdapterViewType.Wish).id
            WishAdapterViewType.Loading -> true
            WishAdapterViewType.Error -> true
        }
    }

    override fun areContentsTheSame(
        oldItem: WishAdapterViewType,
        newItem: WishAdapterViewType
    ): Boolean = oldItem == newItem
}