package com.wishbox.feature.wishList.mapper

import android.content.Context
import com.wishbox.core.domain.entity.UserEntity
import com.wishbox.core.presentation.R
import javax.inject.Inject

/**
 * Created by Pak Evgeniy on 09.12.2018
 */

class UserViewMapper @Inject constructor(
    private val context: Context
) {

    fun mapName(from: UserEntity): String = manageName(from.firstName, from.lastName)

    private fun manageName(firstName: String?, lastName: String?): String =
        "${lastName.orEmpty()} ${firstName.orEmpty()}"
            .takeIf { it.isNotBlank() } ?: context.getString(R.string.not_specified)
}