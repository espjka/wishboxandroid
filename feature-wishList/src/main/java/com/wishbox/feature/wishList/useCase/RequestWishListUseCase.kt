package com.wishbox.feature.wishList.useCase

import com.wishbox.core.domain.exception.UserNotLoggedInException
import com.wishbox.core.domain.repository.UserSessionRepository
import com.wishbox.core.domain.repository.WishRepository
import com.wishbox.core.presentation.pagination.Paginator
import io.reactivex.Completable
import javax.inject.Inject

/**
 * Created by Pak Evgeniy on 30/11/2018.
 */

class RequestWishListUseCase @Inject constructor(
    private val repository: WishRepository,
    private val sessionRepository: UserSessionRepository
) {

    private var offset = 0

    operator fun invoke(userId: Long?, dropOffset: Boolean = false): Completable {
        if (dropOffset) offset = 0
        val id = userId ?: sessionRepository.userId ?: throw UserNotLoggedInException()
        return repository.requestWishList(id, Paginator.LIMIT, offset)
            .doOnComplete { offset += Paginator.LIMIT }
    }
}