package com.wishbox.feature.wishList.adapter.delegates

import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.hannesdorfmann.adapterdelegates4.AbsListItemAdapterDelegate
import com.smedialink.common.ext.inflate
import com.smedialink.feature.wishlist.R
import com.wishbox.core.glide.loadImage
import com.wishbox.feature.wishList.adapter.WishAdapterViewType
import kotlinx.android.synthetic.main.item_wish.view.*

/**
 * Created by Pak Evgeniy on 28/11/2018.
 */

class WishAdapterDelegate(
    private val isNotMine: Boolean,
    private val wishClick: (WishAdapterViewType.Wish) -> Unit,
    private val wishLongClick: (wish: WishAdapterViewType.Wish) -> Unit
) :
    AbsListItemAdapterDelegate<WishAdapterViewType.Wish, WishAdapterViewType, WishAdapterDelegate.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup): ViewHolder =
        ViewHolder(parent.inflate(R.layout.item_wish), isNotMine, wishClick, wishLongClick)

    override fun isForViewType(
        item: WishAdapterViewType,
        items: MutableList<WishAdapterViewType>,
        position: Int
    ): Boolean = item is WishAdapterViewType.Wish

    override fun onBindViewHolder(
        item: WishAdapterViewType.Wish,
        holder: ViewHolder,
        payloads: MutableList<Any>
    ) {
        holder.bind(item)
    }

    class ViewHolder(
        view: View,
        private val isNotMine: Boolean,
        private val wishClick: (WishAdapterViewType.Wish) -> Unit,
        private val wishLongClick: (wish: WishAdapterViewType.Wish) -> Unit
    ) : RecyclerView.ViewHolder(view) {

        fun bind(item: WishAdapterViewType.Wish) =
            with(itemView) {
                itemView.setOnClickListener { wishClick.invoke(item) }
                if (!isNotMine) itemView.setOnLongClickListener {
                    wishLongClick.invoke(item)
                    true
                }
                imagePhoto.loadImage(item.url, placeholder = R.drawable.placeholder_wish)
                textWishName.text = item.name
            }
    }
}