package com.wishbox.feature.wishList.di

import com.wishbox.core.di.api.FeatureProviderApi
import com.wishbox.core.di.api.InjectorApi
import com.wishbox.core.di.api.RouterApi
import com.wishbox.core.di.module.ViewModelModule
import com.wishbox.core.di.provider.FeatureProviderApiProvider
import com.wishbox.core.di.provider.InjectorApiProvider
import com.wishbox.core.di.provider.RouterApiProvider
import com.wishbox.core.di.scope.PerFragment
import com.wishbox.core.di.util.lookupApiProvider
import com.wishbox.feature.wishList.WishListFragment
import dagger.BindsInstance
import dagger.Component

/**
 * Created by Pak Evgeniy on 28/11/2018.
 */

@PerFragment
@Component(
    dependencies = [
        InjectorApi::class,
        FeatureProviderApi::class,
        RouterApi::class
    ],
    modules = [
        ViewModelModule::class,
        WishListViewModule::class
    ]
)
interface WishListViewComponent {

    fun inject(fragment: WishListFragment)

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun bind(fragment: WishListFragment): Builder

        fun api(api: InjectorApi): Builder

        fun api(api: FeatureProviderApi): Builder

        fun api(api: RouterApi): Builder

        fun build(): WishListViewComponent
    }

    class Initializer private constructor() {
        companion object {

            fun init(fragment: WishListFragment) {
                val injectorProvider = fragment.lookupApiProvider<InjectorApiProvider>()
                val featureProviderProvider = fragment.lookupApiProvider<FeatureProviderApiProvider>()
                val routerProvider = fragment.lookupApiProvider<RouterApiProvider>()

                DaggerWishListViewComponent.builder()
                    .api(injectorProvider.injectorApi)
                    .api(featureProviderProvider.featureProviderApi)
                    .api(routerProvider.routerApi)
                    .bind(fragment)
                    .build()
                    .inject(fragment)
            }
        }
    }
}