package com.wishbox.feature.wishList

import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.GridLayoutManager
import com.smedialink.common.delegates.bundle.bundleLong
import com.smedialink.common.events.handle
import com.smedialink.common.ext.gone
import com.smedialink.common.ext.observe
import com.smedialink.common.ext.visible
import com.smedialink.feature.wishlist.R
import com.wishbox.core.presentation.base.BaseDialog
import com.wishbox.core.presentation.base.BaseFragment
import com.wishbox.core.presentation.errorHandling.delegate.ToastDelegate
import com.wishbox.core.presentation.navigation.BackButtonListener
import com.wishbox.core.presentation.pagination.Paginator
import com.wishbox.core.presentation.widget.ScrollingButtonHelper
import com.wishbox.feature.wishList.adapter.WishAdapter
import com.wishbox.feature.wishList.adapter.WishAdapterViewType
import com.wishbox.feature.wishList.di.WishListViewComponent
import kotlinx.android.synthetic.main.fragment_wish_list.*
import javax.inject.Inject

/**
 * Created by Pak Evgeniy on 28/11/2018.
 */

class WishListFragment : BaseFragment<WishListViewModel>(), BackButtonListener {

    companion object {
        fun newInstance(userId: Long? = null): WishListFragment =
            WishListFragment().apply { this.userId = userId }
    }

    var userId: Long? by bundleLong()
        private set

    val isNotMine: Boolean by lazy { userId.takeIf { it != 0L } != null }

    @Inject
    lateinit var errorDelegate: ToastDelegate

    @Inject
    lateinit var adapter: WishAdapter

    @Inject
    lateinit var paginator: Paginator

    @Inject
    lateinit var scrollButtonHelper: ScrollingButtonHelper

    override fun injectDependencies() {
        WishListViewComponent.Initializer.init(this)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initRecyclerView()
        setupForNotMineWishes()
        setupClicks()

        layoutRefresh.setOnRefreshListener { viewModel.requestWishList(true) }

        viewModel.getState().observe(this, this::render)
        viewModel.requestUser()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        paginator.onSaveInstanceState(outState)
        scrollButtonHelper.onSaveInstanceState(outState)
    }

    override fun onViewStateRestored(savedInstanceState: Bundle?) {
        super.onViewStateRestored(savedInstanceState)
        paginator.onViewStateRestored(savedInstanceState)
        scrollButtonHelper.onViewStateRestored(savedInstanceState)
    }

    override fun onBackPressed(): Boolean {
        viewModel.navigateBack()
        return true
    }

    override fun layoutRes(): Int = R.layout.fragment_wish_list

    fun openDeleteDialog(wish: WishAdapterViewType.Wish) {
        BaseDialog.show(
            fragmentManager = childFragmentManager,
            title = getString(R.string.delete_wish_question, wish.name),
            description = wish.comment,
            positiveText = getString(R.string.delete),
            okClick = { viewModel.deleteWish(wish.id) }
        )
    }

    private fun render(state: WishListState) {
        if (state.hasLoadPage) paginator.hasLoadPage()
        layoutRefresh.isRefreshing = state.isInitialLoading
        state.wishList.let { adapter.items = it }
        if (state.isEmpty) {
            if (isNotMine) {
                textNotMineEmpty.visible()
            } else {
                layoutEmpty.visible()
                imageArrow.visible()
            }
        } else {
            layoutEmpty.gone()
            imageArrow.gone()
            textNotMineEmpty.gone()
        }
        if (state.emptyListError) layoutError.visible() else layoutError.gone()
        state.event?.handle { errorDelegate.show(it.errorText) }
        state.userName?.let { toolbar.getDescriptionText().text = it }
    }

    private fun setupClicks() {
        toolbar.getLeftButton().setOnClickListener { viewModel.navigateBack() }
        toolbar.getRightButton().setOnClickListener { }
        textRetry.setOnClickListener { viewModel.requestWishList() }
        buttonAdd.setOnClickListener { viewModel.openWishEdit() }
    }

    private fun initRecyclerView() {
        with(recyclerWishes) {
            adapter = this@WishListFragment.adapter
            layoutManager = GridLayoutManager(this@WishListFragment.context, 2)
                .apply {
                    spanSizeLookup = object : GridLayoutManager.SpanSizeLookup() {
                        override fun getSpanSize(position: Int): Int {
                            val viewType = this@WishListFragment.adapter.getItemViewType(position)
                            return if (viewType == 0) 1 else 2
                        }
                    }
                }
            paginator.withRecyclerView(this, adapter?.itemCount == 0)
        }
    }

    private fun setupForNotMineWishes() {
        if (isNotMine) {
            toolbar.getLeftButton().visible()
            toolbar.getLeftButton().setImageResource(R.drawable.ic_arrow_back)
            toolbar.getTitleText().text = getString(R.string.title_wishes)
            toolbar.getDescriptionText().visible()
            buttonAdd.gone()
        } else {
            scrollButtonHelper.withRecyclerView(recyclerWishes, buttonAdd)
        }
    }
}