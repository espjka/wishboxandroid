package com.wishbox.feature.wishList.di

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.smedialink.common.error.ErrorHandler
import com.wishbox.core.di.mapkeys.ViewModelKey
import com.wishbox.core.di.screenproviders.FeatureProvider
import com.wishbox.core.domain.repository.UserRepository
import com.wishbox.core.domain.repository.WishRepository
import com.wishbox.core.presentation.errorHandling.MainErrorHandler
import com.wishbox.core.presentation.pagination.Paginator
import com.wishbox.feature.wishList.WishListFragment
import com.wishbox.feature.wishList.WishListViewModel
import com.wishbox.feature.wishList.adapter.WishAdapter
import com.wishbox.feature.wishList.adapter.WishDiffCallback
import com.wishbox.feature.wishList.mapper.UserViewMapper
import com.wishbox.feature.wishList.mapper.WishViewMapper
import com.wishbox.feature.wishList.useCase.ObserveWishListUseCase
import com.wishbox.feature.wishList.useCase.RequestWishListUseCase
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.multibindings.IntoMap
import ru.terrakok.cicerone.Router

/**
 * Created by Pak Evgeniy on 28/11/2018.
 */

@Module
abstract class WishListViewModule {

    @Binds
    abstract fun provideErrorHandler(impl: MainErrorHandler): ErrorHandler

    @Module
    companion object {

        @Provides
        @JvmStatic
        fun provideUserId(fragment: WishListFragment): Long? =
            fragment.userId.takeIf { it != 0L }

        @Provides
        @IntoMap
        @ViewModelKey(WishListViewModel::class)
        @JvmStatic
        fun provideViewModel(
            router: Router,
            userId: Long?,
            mapper: WishViewMapper,
            userViewMapper: UserViewMapper,
            errorHandler: ErrorHandler,
            requestUseCase: RequestWishListUseCase,
            observeUseCase: ObserveWishListUseCase,
            repository: WishRepository,
            userRepository: UserRepository,
            featureProvider: FeatureProvider
        ): ViewModel =
            WishListViewModel(
                router = router,
                userId = userId,
                requestWishList = requestUseCase,
                errorHandler = errorHandler,
                mapper = mapper,
                userMapper = userViewMapper,
                getUser = { userRepository.getUser(it) },
                observeWishList = observeUseCase,
                deleteWish = { repository.deleteWish(it) },
                featureProvider = featureProvider
            )

        @JvmStatic
        @Provides
        fun provideWishListViewModel(
            fragment: WishListFragment,
            factory: ViewModelProvider.Factory
        ): WishListViewModel =
            ViewModelProviders.of(fragment, factory).get(WishListViewModel::class.java)

        @Provides
        @JvmStatic
        fun provideAdapter(
            diffCallback: WishDiffCallback,
            viewModel: WishListViewModel,
            fragment: WishListFragment
        ): WishAdapter =
            WishAdapter(
                diffCallback = diffCallback,
                isNotMine = fragment.isNotMine,
                wishClick = { viewModel.openWishInfo(it.id) },
                wishLongClick = { fragment.openDeleteDialog(it) },
                errorClick = { viewModel.requestWishList() }
            )

        @Provides
        @JvmStatic
        fun providePaginationScrollDelegate(viewModel: WishListViewModel): Paginator =
            Paginator { viewModel.requestWishList() }
    }
}