package com.wishbox.feature.wishList

import com.smedialink.common.error.ErrorState
import com.smedialink.common.events.ErrorEvent
import com.wishbox.feature.wishList.adapter.WishAdapterViewType

/**
 * Created by Pak Evgeniy on 28/11/2018.
 */

data class WishListState(
    val wishList: List<WishAdapterViewType> = emptyList(),
    val userName: String? = null,
    val isEmpty: Boolean = false,
    val isInitialLoading: Boolean = false,
    val hasLoadPage: Boolean = false,
    val event: ErrorEvent? = null,
    val emptyListError: Boolean = false
)

sealed class WishListStateChanges {
    class UserName(val userName: String) : WishListStateChanges()
    class Loading(val isForceLoading: Boolean) : WishListStateChanges()
    class Success(val hasLoadPage: Boolean) : WishListStateChanges()
    class Data(val wishList: List<WishAdapterViewType>) : WishListStateChanges()
    class Error(val error: ErrorState, val isForceLoading: Boolean = false) : WishListStateChanges()
}

fun WishListStateChanges.reduceState(prevState: WishListState) =
    when (this) {
        is WishListStateChanges.Data -> reduceState(prevState)
        is WishListStateChanges.Error -> reduceState(prevState)
        is WishListStateChanges.Loading -> reduceState(prevState)
        is WishListStateChanges.Success -> reduceState(prevState)
        is WishListStateChanges.UserName -> reduceState(prevState)
    }

fun WishListStateChanges.UserName.reduceState(prevState: WishListState): WishListState =
    prevState.copy(userName = userName)

fun WishListStateChanges.Data.reduceState(prevState: WishListState): WishListState =
    prevState.copy(
        wishList = wishList,
        isEmpty = wishList.isEmpty() && prevState.emptyListError.not(),
        emptyListError = wishList.isEmpty() && prevState.emptyListError,
        isInitialLoading = false,
        event = null
    )

fun WishListStateChanges.Error.reduceState(prevState: WishListState): WishListState =
    prevState.copy(
        wishList = prevState.wishList.toMutableList()
            .apply {
                remove(WishAdapterViewType.Loading)
                if (isForceLoading.not() && isNotEmpty()) add(WishAdapterViewType.Error)
            },
        event = error.event,
        emptyListError = prevState.wishList.isNullOrEmpty(),
        isEmpty = false,
        isInitialLoading = false,
        hasLoadPage = false
    )

fun WishListStateChanges.Loading.reduceState(prevState: WishListState): WishListState =
    if (isForceLoading || prevState.wishList.isNullOrEmpty()) {
        prevState.copy(
            isInitialLoading = true,
            hasLoadPage = false,
            wishList = prevState.wishList.toMutableList()
                .apply {
                    remove(WishAdapterViewType.Loading)
                },
            emptyListError = false
        )
    } else {
        prevState.copy(
            wishList = prevState.wishList.toMutableList()
                .apply {
                    remove(WishAdapterViewType.Error)
                    if (!contains(WishAdapterViewType.Loading)) add(WishAdapterViewType.Loading)
                },
            hasLoadPage = false,
            emptyListError = false
        )
    }

fun WishListStateChanges.Success.reduceState(prevState: WishListState): WishListState =
    prevState.copy(
        isInitialLoading = false,
        emptyListError = false,
        hasLoadPage = hasLoadPage,
        wishList = prevState.wishList.toMutableList()
            .apply { remove(WishAdapterViewType.Loading) },
        isEmpty = prevState.wishList.isEmpty()
    )