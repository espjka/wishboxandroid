package com.wishbox.feature.wishList.adapter

import com.hannesdorfmann.adapterdelegates4.AsyncListDifferDelegationAdapter
import com.wishbox.core.presentation.pagination.PaginationErrorAdapterDelegate
import com.wishbox.core.presentation.pagination.PaginationProgressAdapterDelegate
import com.wishbox.feature.wishList.adapter.delegates.WishAdapterDelegate

/**
 * Created by Pak Evgeniy on 28/11/2018.
 */

class WishAdapter(
    diffCallback: WishDiffCallback,
    isNotMine: Boolean,
    wishClick: (wish: WishAdapterViewType.Wish) -> Unit,
    wishLongClick: (wish: WishAdapterViewType.Wish) -> Unit,
    errorClick: () -> Unit
) : AsyncListDifferDelegationAdapter<WishAdapterViewType>(diffCallback) {

    init {
        delegatesManager
            .addDelegate(WishAdapterDelegate(isNotMine, wishClick, wishLongClick))
            .addDelegate(PaginationProgressAdapterDelegate<WishAdapterViewType.Loading, WishAdapterViewType> { it is WishAdapterViewType.Loading })
            .addDelegate(
                PaginationErrorAdapterDelegate<WishAdapterViewType.Error, WishAdapterViewType>(
                    isForViewType = { it is WishAdapterViewType.Error },
                    errorClick = errorClick
                )
            )
    }
}