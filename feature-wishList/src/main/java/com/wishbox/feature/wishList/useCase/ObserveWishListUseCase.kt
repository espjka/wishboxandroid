package com.wishbox.feature.wishList.useCase

import com.wishbox.core.domain.entity.WishEntity
import com.wishbox.core.domain.repository.AuthRepository
import com.wishbox.core.domain.repository.UserSessionRepository
import com.wishbox.core.domain.repository.WishRepository
import com.wishbox.core.presentation.pagination.Paginator
import io.reactivex.Completable
import io.reactivex.Flowable
import javax.inject.Inject

/**
 * Created by Pak Evgeniy on 11/12/2018.
 */

class ObserveWishListUseCase @Inject constructor(
    private val repository: WishRepository,
    private val userSessionRepository: UserSessionRepository
) {
    operator fun invoke(userId: Long?): Flowable<List<WishEntity>> {
        val id = userId ?: userSessionRepository.userId ?: throw IllegalArgumentException("No userId specified")
        return repository.observeWishList(id)
    }
}