package com.wishbox.featurecreateprofile.edit.di

import com.wishbox.core.di.api.FeatureProviderApi
import com.wishbox.core.di.api.InjectorApi
import com.wishbox.core.di.api.RouterApi
import com.wishbox.core.di.api.RxBusApi
import com.wishbox.core.di.module.ViewModelModule
import com.wishbox.core.di.provider.FeatureProviderApiProvider
import com.wishbox.core.di.provider.InjectorApiProvider
import com.wishbox.core.di.provider.RouterApiProvider
import com.wishbox.core.di.provider.RxBusApiProvider
import com.wishbox.core.di.util.lookupApiProvider
import com.wishbox.featurecreateprofile.edit.presentation.EditProfileFragment
import dagger.BindsInstance
import dagger.Component

@Component(
    dependencies = [InjectorApi::class, RxBusApi::class, FeatureProviderApi::class, RouterApi::class],
    modules = [EditProfileModule::class, ViewModelModule::class]
)
interface EditProfileComponent {

    fun inject(target: EditProfileFragment)

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun bind(fragment: EditProfileFragment): Builder

        fun api(api: InjectorApi): Builder

        fun api(api: RxBusApi): Builder

        fun api(api: FeatureProviderApi): Builder

        fun api(api: RouterApi): Builder

        fun build(): EditProfileComponent
    }

    companion object {

        fun inject(fragment: EditProfileFragment) {
            val injectorApiProvider: InjectorApiProvider = fragment.lookupApiProvider()
            val routerApiProvider: RouterApiProvider = fragment.lookupApiProvider()
            val featureProviderApiProvider: FeatureProviderApiProvider = fragment.lookupApiProvider()
            val rxBusApiProvider: RxBusApiProvider = fragment.lookupApiProvider()

            return DaggerEditProfileComponent.builder()
                .api(injectorApiProvider.injectorApi)
                .api(rxBusApiProvider.rxBusApi)
                .api(featureProviderApiProvider.featureProviderApi)
                .api(routerApiProvider.routerApi)
                .bind(fragment)
                .build()
                .inject(fragment)
        }
    }

}