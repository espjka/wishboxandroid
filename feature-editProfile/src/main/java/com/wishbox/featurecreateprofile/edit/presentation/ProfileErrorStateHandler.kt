package com.wishbox.featurecreateprofile.edit.presentation

import com.smedialink.common.events.ErrorEvent
import com.wishbox.core.presentation.resources.ErrorTextProvider
import com.wishbox.core.presentation.resources.ResourceProvider
import com.wishbox.featurecreateprofile.R
import com.wishbox.featurecreateprofile.edit.domain.exception.ValidationException
import javax.inject.Inject

class ProfileErrorStateHandler @Inject constructor(
    private val resourceProvider: ResourceProvider,
    private val errorTextProvider: ErrorTextProvider
) {

    fun handle(prevErrorState: ProfileErrorState?, throwable: Throwable): ProfileErrorState {
        val prevState = prevErrorState ?: ProfileErrorState(null, null)
        return if (throwable is ValidationException) {
            prevState.copy(errorEvent = ErrorEvent(resourceProvider.getString(R.string.edit_validation_error)), validationException = throwable)
        } else {
            val errorEvent = errorTextProvider.provideErrorEvent(throwable)
            prevState.copy(errorEvent = errorEvent)
        }
    }
}