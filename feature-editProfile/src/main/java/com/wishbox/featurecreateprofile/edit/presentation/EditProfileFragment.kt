package com.wishbox.featurecreateprofile.edit.presentation

import android.os.Bundle
import android.view.View
import androidx.core.os.bundleOf
import com.smedialink.common.base.BaseFragment
import com.smedialink.common.events.handle
import com.smedialink.common.ext.*
import com.smedialink.common.util.StateHolder
import com.wishbox.core.di.screenproviders.Selections
import com.wishbox.core.domain.entity.Gender
import com.wishbox.core.glide.loadImage
import com.wishbox.core.presentation.base.BaseBottomSheetDialog
import com.wishbox.core.presentation.errorHandling.delegate.ToastDelegate
import com.wishbox.core.presentation.loading.ProgressLoadingDelegate
import com.wishbox.core.presentation.pictures.PhotoHelper
import com.wishbox.core.presentation.view.SwitchView
import com.wishbox.featurecreateprofile.R
import com.wishbox.featurecreateprofile.edit.di.EditProfileComponent
import kotlinx.android.synthetic.main.avatar_view.view.*
import kotlinx.android.synthetic.main.content_edit_profile.*
import kotlinx.android.synthetic.main.fragment_edit_profile.*
import javax.inject.Inject

class EditProfileFragment : BaseFragment() {

    @Inject
    lateinit var viewModel: EditProfileViewModel

    @Inject
    lateinit var toastDelegate: ToastDelegate

    @Inject
    lateinit var datePickerDelegate: DatePickerDelegate

    @Inject
    lateinit var photoHelper: PhotoHelper

    private val progressLoadingDelegate: ProgressLoadingDelegate by lazy {
        return@lazy ProgressLoadingDelegate(context!!, "Сохранение...")
    }

    override val layoutRes: Int = R.layout.fragment_edit_profile

    private var stateHolder = StateHolder<ProfileState>()

    override fun inject() {
        EditProfileComponent.inject(this)
    }

    companion object {
        private const val BUNDLE_CREATION = "BUNDLE_CREATION"

        fun newInstance(createProfile: Boolean? = null): EditProfileFragment =
            EditProfileFragment()
                .apply { arguments = bundleOf(BUNDLE_CREATION to createProfile) }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        stateHolder = StateHolder() // need to refresh state holder, cause it is not recreated when we navigate back

        if (arguments?.getBoolean(BUNDLE_CREATION) == true) {
            setupForCreation()
        }

        svGender.setText(
            arrayOf(
                getString(R.string.gender_male),
                getString(R.string.gender_female)
            )
        )

//        svPrivacy.setText(
//            arrayOf(
//                getString(R.string.privacy_open),
//                getString(R.string.privacy_close)
//            )
//        )

        datePickerDelegate.selectedDate = {
            viewModel.birthDay = it
        }

        etFirstName.onTextChanged { _, _, _, _ ->
            viewModel.firstName = etFirstName.trimmedString()
        }

        etSecondName.onTextChanged { _, _, _, _ ->
            viewModel.lastName = etSecondName.trimmedString()
        }

        etMiddleName.onTextChanged { _, _, _, _ ->
            viewModel.patronymic = etMiddleName.trimmedString()
        }

        etBirthday.setOnClickListener {
            activity?.hideKeyboard()
            datePickerDelegate.show()
        }

        etCity.setOnClickListener {
            viewModel.openSelection(Selections.CITY)
        }

        etHobbies.setOnClickListener {
            viewModel.openSelection(Selections.HOBBIES)
        }

        etAlcohol.setOnClickListener {
            viewModel.openSelection(Selections.ALCOHOL)
        }

        etSweeties.setOnClickListener {
            viewModel.openSelection(Selections.SWEETIES)
        }

        etFlowers.setOnClickListener {
            viewModel.openSelection(Selections.FLOWERS)
        }

        toolbar.getLeftButton().setOnClickListener { viewModel.navigateBack() }
        btnCreate.setOnClickListener { viewModel.update(true) }
        toolbar.getRightButton().setOnClickListener { viewModel.update(false) }

        includeAvatar.layoutAdd.setOnClickListener { openAddPhotoDialog() }
        includeAvatar.ibAddAvatar.setOnClickListener { openAddPhotoDialog() }
        includeAvatar.buttonEditAvatar.setOnClickListener { openAddPhotoDialog() }

        svGender.setOnSwitchListener(object : SwitchView.OnSwitchListener {
            override fun onSwitch(position: Int, tabText: String) {
                if (position == 0) {
                    viewModel.gender = Gender.MALE
                } else {
                    viewModel.gender = Gender.FEMALE
                }
            }
        })

//        svPrivacy.setOnSwitchListener(object : SwitchView.OnSwitchListener {
//            override fun onSwitch(position: Int, tabText: String) {
//
//            }
//        }nfr

        viewModel.attachPhotoHelper(photoHelper)
        viewModel.state.observe(this, this::render)
    }

    private fun setupForCreation() {
        btnCreate.visible()
        toolbar.getRightButton().gone()
        toolbar.getLeftButton().gone()
        toolbar.getTitleText().setText(R.string.title_profile)
    }

    private fun openAddPhotoDialog() {
        BaseBottomSheetDialog.show(
            childFragmentManager,
            getString(R.string.dialog_camera),
            getString(R.string.dialog_gallery),
            {
                viewModel.getPictureFromCamera()
                it.dismiss()
            },
            {
                viewModel.getPicturesFromGallery()
                it.dismiss()
            }
        )
    }

    private fun render(state: ProfileState) {
        stateHolder = stateHolder.newState(state)

        stateHolder.run {

            diff({ isLoading }, progressLoadingDelegate::showLoading)

            state.errorState?.errorEvent?.handle {
                toastDelegate.show(it.errorText, ToastDelegate.ToastGravity.TOP)
            }

            diffNullable({ errorState?.validationException?.firstNameErrorText }) { etFirstName.showError(it) }

            diffNullable({ errorState?.validationException?.lastNameErrorText }) { etSecondName.showError(it) }

            diffNullable({ errorState?.validationException?.patronymicErrorText }) { etMiddleName.showError(it) }

            diffNullable({ errorState?.validationException?.birthdayErrorText }) { etBirthday.showError(it) }

            diffNullable({ errorState?.validationException?.cityErrorText }) { etCity.showError(it) }

            diffNullable({ profile.firstName }) { etFirstName.fillText(it) }

            diffNullable({ profile.lastName }) { etSecondName.fillText(it) }

            diffNullable({ profile.patronymic }) { etMiddleName.fillText(it) }

            diffNullable({ profile.city }) { etCity.getText().text = it?.name }

            diff({ isSaveActive }) { btnCreate.isEnabled = it }

            state.profile.avatarUrl
                ?.let {
                    includeAvatar.layoutAdd.gone()
                    includeAvatar.buttonEditAvatar.visible()
                    includeAvatar.imageAvatar.visible()
                    includeAvatar.imageAvatar.loadImage(it, R.drawable.placeholder_avatar)
                }
                ?: run {
                    includeAvatar.layoutAdd.visible()
                    includeAvatar.buttonEditAvatar.gone()
                    includeAvatar.imageAvatar.gone()
                }

            diff({ profile.gender }) {
                if (it == Gender.FEMALE) {
                    svGender.setSelectedTab(1)
                } else {
                    svGender.setSelectedTab(0)
                }
            }

            diffNullable({ profile.birthday }) { etBirthday.getText().text = it }

            diffNullable({ profile.alcohol }) { etAlcohol.getText().text = it?.name }

            diffNullable({ profile.sweet }) { etSweeties.getText().text = it?.name }

            diffNullable({ profile.hobby }) { etHobbies.getText().text = it?.name }

            diffNullable({ profile.flower }) { etFlowers.getText().text = it?.name }
        }
    }

}
