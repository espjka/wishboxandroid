package com.wishbox.featurecreateprofile.edit.presentation

import android.app.DatePickerDialog
import android.graphics.drawable.Drawable
import android.widget.DatePicker
import androidx.core.content.ContextCompat
import androidx.fragment.app.FragmentActivity
import com.wishbox.core.domain.parser.DateParser
import com.wishbox.featurecreateprofile.R
import java.util.*
import javax.inject.Inject

class DatePickerDelegate @Inject constructor(
    private val activity: FragmentActivity,
    private val dateParser: DateParser
) :
    DatePickerDialog.OnDateSetListener {

    var selectedDate: ((String) -> Unit)? = null

    private val dateAndTime: Calendar by lazy {
        return@lazy Calendar.getInstance()
    }

    private val colorAccent: Int by lazy {
        return@lazy ContextCompat.getColor(activity, R.color.colorAccent)
    }

    private val background: Drawable by lazy {
        return@lazy activity.getDrawable(R.drawable.date_picker_day_of_week_bg)
    }

    private val dialog: DatePickerDialog by lazy {
        return@lazy DatePickerDialog(
            activity, this,
            dateAndTime.get(Calendar.YEAR),
            dateAndTime.get(Calendar.MONTH),
            dateAndTime.get(Calendar.DAY_OF_MONTH)
        ).apply {
            datePicker.maxDate = dateAndTime.timeInMillis
            getButton(DatePickerDialog.BUTTON_NEGATIVE)?.setTextColor(colorAccent)
            getButton(DatePickerDialog.BUTTON_POSITIVE)?.setTextColor(colorAccent)
            window?.setBackgroundDrawable(background)
        }
    }

    fun show() {
        dialog.takeUnless { it.isShowing }?.show()
    }


    override fun onDateSet(view: DatePicker?, year: Int, month: Int, dayOfMonth: Int) {
        selectedDate?.let {
            val date = Calendar.getInstance()
                .apply { set(Calendar.YEAR, year) }
                .apply { set(Calendar.MONTH, month) }
                .apply { set(Calendar.DAY_OF_MONTH, dayOfMonth) }
                .let { it.time }
            dateParser.dateToString(date, DateParser.Pattern.DATE_DOTS)?.let(it)
        }
    }
}