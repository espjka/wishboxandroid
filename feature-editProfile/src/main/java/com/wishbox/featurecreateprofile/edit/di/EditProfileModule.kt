package com.wishbox.featurecreateprofile.edit.di

import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.wishbox.core.di.mapkeys.ViewModelKey
import com.wishbox.core.di.rxbusprovider.RxBusProvider
import com.wishbox.core.di.screenproviders.FeatureProvider
import com.wishbox.core.domain.repository.ProfileRepository
import com.wishbox.core.presentation.pictures.PhotoHelper
import com.wishbox.featurecreateprofile.edit.domain.GetCurrentProfileUseCase
import com.wishbox.featurecreateprofile.edit.domain.ProfileValidator
import com.wishbox.featurecreateprofile.edit.domain.UpdateProfileUseCase
import com.wishbox.featurecreateprofile.edit.presentation.EditProfileFragment
import com.wishbox.featurecreateprofile.edit.presentation.EditProfileViewModel
import com.wishbox.featurecreateprofile.edit.presentation.ProfileErrorStateHandler
import com.wishbox.featurecreateprofile.edit.presentation.mapper.ProfileViewMapper
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.multibindings.IntoMap
import ru.terrakok.cicerone.Router

@Module
abstract class EditProfileModule {

    @Module
    companion object {

        @Provides
        @JvmStatic
        fun provideActivity(
            fragment: EditProfileFragment
        ): FragmentActivity = fragment.activity!!

        @Provides
        @JvmStatic
        fun providePhotoHelper(fragment: EditProfileFragment): PhotoHelper = PhotoHelper(fragment)

        @Provides
        @IntoMap
        @JvmStatic
        @ViewModelKey(EditProfileViewModel::class)
        fun provideViewModel(
            router: Router,
            profileErrorStateHandler: ProfileErrorStateHandler,
            featureProvider: FeatureProvider,
            getCurrentProfileUseCase: GetCurrentProfileUseCase,
            updateProfileUseCase: UpdateProfileUseCase,
            rxBusProvider: RxBusProvider,
            mapper: ProfileViewMapper,
            profileRepository: ProfileRepository
        ): ViewModel =
            EditProfileViewModel(
                router,
                profileErrorStateHandler,
                getCurrentProfileUseCase,
                updateProfileUseCase,
                featureProvider,
                rxBusProvider.provideProfileSelectRxBus(),
                mapper,
                uploadAvatar = { profileRepository.uploadAvatar(it).map { it.big }.toMaybe() }
            )

        @Provides
        @JvmStatic
        fun provideEditProfileViewModel(
            fragment: EditProfileFragment,
            factory: ViewModelProvider.Factory
        ): EditProfileViewModel = ViewModelProviders.of(fragment, factory).get(EditProfileViewModel::class.java)
    }

    @Binds
    abstract fun provideProfileValidator(impl: ProfileValidatorImpl): ProfileValidator

}