package com.wishbox.featurecreateprofile.edit.presentation.mapper

import com.wishbox.core.domain.entity.ProfileEntity
import com.wishbox.core.domain.entity.UpdateProfileEntity
import com.wishbox.core.domain.parser.DateParser
import com.wishbox.featurecreateprofile.edit.presentation.ProfileUiModel
import javax.inject.Inject

/**
 * Created by Pak Evgeniy on 20.12.2018
 */

class ProfileViewMapper @Inject constructor(
    private val dateParser: DateParser
) {

    fun map(from: ProfileEntity): ProfileUiModel =
        ProfileUiModel(
            firstName = from.firstName,
            lastName = from.lastName,
            patronymic = from.patronymic,
            avatarUrl = from.avatar?.big,
            city = from.city,
            birthday = dateParser.dateToString(from.birthday, DateParser.Pattern.DATE_DOTS),
            hobby = from.hobby,
            flower = from.flower,
            alcohol = from.alcohol,
            sweet = from.sweet,
            gender = from.gender
        )

    fun map(from: ProfileUiModel): UpdateProfileEntity =
        UpdateProfileEntity(
            firstName = from.firstName,
            lastName = from.lastName,
            patronymic = from.patronymic,
            city = from.city,
            birthday = dateParser.stringToDate(from.birthday, DateParser.Pattern.DATE_DOTS),
            hobby = from.hobby,
            flower = from.flower,
            alcohol = from.alcohol,
            sweet = from.sweet,
            gender = from.gender
        )
}