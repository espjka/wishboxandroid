package com.wishbox.featurecreateprofile.edit.presentation

import com.smedialink.common.events.ErrorEvent
import com.wishbox.featurecreateprofile.edit.domain.exception.ValidationException

data class ProfileState(
    val isLoading: Boolean,
    val profile: ProfileUiModel,
    val errorState: ProfileErrorState?,
    val isSaveActive: Boolean = false
) {

    fun reduceErrorState(setter: (ValidationException) -> ValidationException?): ProfileState {
        return this.copy(
            errorState = this.errorState?.copy(
                validationException = this.errorState.validationException?.let(setter)
            )
        )
    }
}

data class ProfileErrorState(
    val errorEvent: ErrorEvent?,
    val validationException: ValidationException?
)
