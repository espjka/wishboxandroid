package com.wishbox.featurecreateprofile.edit.di

import com.wishbox.core.domain.entity.UpdateProfileEntity
import com.wishbox.core.presentation.resources.ResourceProvider
import com.wishbox.featurecreateprofile.R
import com.wishbox.featurecreateprofile.edit.domain.ProfileValidator
import com.wishbox.featurecreateprofile.edit.domain.exception.ValidationException
import javax.inject.Inject

class ProfileValidatorImpl @Inject constructor(
    private val resourceProvider: ResourceProvider
) : ProfileValidator {

    companion object {
        const val FIRST_NAME_LENGTH = 30
        const val LAST_NAME_LENGTH = 150
        const val PATRONYMIC_LENGTH = 150
    }

    override fun validate(profile: UpdateProfileEntity): UpdateProfileEntity {
        var exception: ValidationException? = null
        if (profile.firstName.isNullOrBlank()) {
            exception = resourceProvider.getString(R.string.edit_profile_required_field_invalid_error).let {
                return@let exception?.copy(firstNameErrorText = it) ?: ValidationException(firstNameErrorText = it)
            }
        }

        if (profile.firstName?.length?.compareTo(FIRST_NAME_LENGTH) == 1) {
            exception = resourceProvider.getString(R.string.validation_length, FIRST_NAME_LENGTH).let {
                return@let exception?.copy(firstNameErrorText = it) ?: ValidationException(firstNameErrorText = it)
            }
        }

        if (profile.lastName.isNullOrBlank()) {
            exception = resourceProvider.getString(R.string.edit_profile_required_field_invalid_error).let {
                return@let exception?.copy(lastNameErrorText = it) ?: ValidationException(lastNameErrorText = it)
            }
        }

        if (profile.lastName?.length?.compareTo(LAST_NAME_LENGTH) == 1) {
            exception = resourceProvider.getString(R.string.validation_length, LAST_NAME_LENGTH).let {
                return@let exception?.copy(lastNameErrorText = it) ?: ValidationException(lastNameErrorText = it)
            }
        }

        if (profile.patronymic?.length?.compareTo(PATRONYMIC_LENGTH) == 1) {
            exception = resourceProvider.getString(R.string.validation_length, PATRONYMIC_LENGTH).let {
                return@let exception?.copy(patronymicErrorText = it) ?: ValidationException(patronymicErrorText = it)
            }
        }

        if (profile.birthday == null) {
            exception = resourceProvider.getString(R.string.edit_profile_required_field_invalid_error).let {
                return@let exception?.copy(birthdayErrorText = it) ?: ValidationException(birthdayErrorText = it)
            }
        }

        if (profile.city?.name.isNullOrBlank()) {
            exception = resourceProvider.getString(R.string.edit_profile_required_field_invalid_error).let {
                return@let exception?.copy(cityErrorText = it) ?: ValidationException(cityErrorText = it)
            }
        }

        if (exception != null) {
            throw exception
        }

        return profile
    }

}