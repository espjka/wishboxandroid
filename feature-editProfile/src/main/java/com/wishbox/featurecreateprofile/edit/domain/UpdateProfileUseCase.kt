package com.wishbox.featurecreateprofile.edit.domain

import com.wishbox.core.domain.entity.ProfileEntity
import com.wishbox.core.domain.entity.UpdateProfileEntity
import com.wishbox.core.domain.repository.ProfileRepository
import io.reactivex.Single
import javax.inject.Inject

class UpdateProfileUseCase @Inject constructor(
    private val profileRepository: ProfileRepository,
    private val profileValidator: ProfileValidator
) {

    operator fun invoke(profile: UpdateProfileEntity): Single<ProfileEntity> {
        return Single.fromCallable {
            profileValidator.validate(profile)
        }.flatMap {
            profileRepository.updateProfile(it)
        }
    }

}