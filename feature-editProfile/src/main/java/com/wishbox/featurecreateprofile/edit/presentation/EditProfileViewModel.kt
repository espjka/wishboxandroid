package com.wishbox.featurecreateprofile.edit.presentation

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.smedialink.common.base.BaseViewModel
import com.smedialink.common.delegates.PropertyChangedDelegate
import com.smedialink.common.delegates.rx.disposableDelegate
import com.smedialink.common.ext.applySchedulers
import com.smedialink.common.ext.reduceState
import com.smedialink.common.rxbus.RxEventBus
import com.wishbox.core.di.rxbusprovider.RxBusProvider
import com.wishbox.core.di.screenproviders.FeatureProvider
import com.wishbox.core.di.screenproviders.Selections
import com.wishbox.core.domain.entity.Gender
import com.wishbox.core.domain.entity.ItemValue
import com.wishbox.core.domain.entity.ProfileItemValueType
import com.wishbox.core.presentation.pictures.PhotoHelper
import com.wishbox.featurecreateprofile.edit.domain.GetCurrentProfileUseCase
import com.wishbox.featurecreateprofile.edit.domain.UpdateProfileUseCase
import com.wishbox.featurecreateprofile.edit.presentation.mapper.ProfileViewMapper
import io.reactivex.Maybe
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import ru.terrakok.cicerone.Router
import java.io.File

class EditProfileViewModel constructor(
    private val router: Router,
    private val profileErrorStateHandler: ProfileErrorStateHandler,
    getCurrentProfileUseCase: GetCurrentProfileUseCase,
    private val editProfileUseCase: UpdateProfileUseCase,
    private val featureProvider: FeatureProvider,
    selectionRxEventBus: RxEventBus<RxBusProvider.ItemValueSelectedEvent>,
    private val mapper: ProfileViewMapper,
    private val uploadAvatar: (picture: File) -> Maybe<String>
) : BaseViewModel() {

    private var disposable: Disposable? by disposableDelegate()
    private var getPictureDisposable: Disposable? by disposableDelegate()

    private var photoHelper: PhotoHelper? = null

    fun attachPhotoHelper(photoHelper: PhotoHelper) {
        this.photoHelper = photoHelper
    }

    private val internalState: MutableLiveData<ProfileState> by lazy {
        return@lazy MutableLiveData<ProfileState>()
    }

    private var profile: ProfileUiModel = mapper.map(getCurrentProfileUseCase.profile)
        set(value) {
            field = value
            internalState.reduceState { state ->
                state.copy(profile = value, isSaveActive = value.checkActiveSaveButton())
            }
        }

    init {
        internalState.value = ProfileState(isLoading = false, profile = profile, errorState = null)
        selectionRxEventBus.value
            .subscribeBy(
                onNext = {
                    when (it.type) {
                        ProfileItemValueType.CITY -> city = it.itemValue?.takeIf { it.id != 0L }
                        ProfileItemValueType.HOBBY -> hobby = it.itemValue?.takeIf { it.id != 0L }
                        ProfileItemValueType.FLOWERS -> flowers = it.itemValue?.takeIf { it.id != 0L }
                        ProfileItemValueType.ALCOHOL -> alcohol = it.itemValue?.takeIf { it.id != 0L }
                        ProfileItemValueType.SWEETS -> sweet = it.itemValue?.takeIf { it.id != 0L }
                    }
                }
            )
            .addTo(disposables)
    }

    val state: LiveData<ProfileState> = internalState

    var firstName: String? by PropertyChangedDelegate(profile.firstName) { s ->
        profile = profile.copy(firstName = s)
        internalState.reduceState { state ->
            state.reduceErrorState {
                it.copy(firstNameErrorText = null)
            }
        }
    }

    var lastName: String? by PropertyChangedDelegate(profile.lastName) {
        profile = profile.copy(lastName = it)
        internalState.reduceState { state ->
            state.reduceErrorState {
                it.copy(lastNameErrorText = null)
            }
        }
    }

    var patronymic: String? by PropertyChangedDelegate(profile.patronymic) {
        profile = profile.copy(patronymic = it)
    }

    var birthDay: String? by PropertyChangedDelegate(profile.birthday) {
        profile = profile.copy(birthday = it)
        internalState.reduceState { state ->
            state.reduceErrorState {
                it.copy(birthdayErrorText = null)
            }
        }
    }

    var city: ItemValue? by PropertyChangedDelegate(profile.city) {
        profile = profile.copy(city = it)
        internalState.reduceState { state ->
            state.reduceErrorState {
                it.copy(cityErrorText = null)
            }
        }
    }

    var hobby: ItemValue? by PropertyChangedDelegate(profile.hobby) {
        profile = profile.copy(hobby = it)
    }

    var flowers: ItemValue? by PropertyChangedDelegate(profile.flower) {
        profile = profile.copy(flower = it)
    }

    var sweet: ItemValue? by PropertyChangedDelegate(profile.sweet) {
        profile = profile.copy(sweet = it)
    }

    var alcohol: ItemValue? by PropertyChangedDelegate(profile.alcohol) {
        profile = profile.copy(alcohol = it)
    }

    var gender: Gender? by PropertyChangedDelegate(profile.gender) {
        profile = profile.copy(gender = it ?: Gender.NONE)
    }

    fun navigateBack() {
        router.exit()
    }

    fun openSelection(selection: Selections) {
        router.navigateTo(featureProvider.getSelectionScreen(selection))
    }

    fun update(authFlow: Boolean) {
        disposable = editProfileUseCase.invoke(mapper.map(profile))
            .applySchedulers()
            .doOnSubscribe { _ ->
                internalState.reduceState {
                    it.copy(isLoading = true).reduceErrorState { null }
                }
            }
            .subscribeBy(
                onSuccess = { _ ->
                    internalState.reduceState {
                        it.copy(isLoading = false).reduceErrorState { null }
                    }
                    if (authFlow) {
                        router.newRootScreen(featureProvider.getFriendsEditor())
                    } else {
                        router.exit()
                    }
                },
                onError = { throwable ->
                    internalState.reduceState {
                        val errorState = profileErrorStateHandler.handle(it.errorState, throwable)
                        it.copy(isLoading = false, errorState = errorState)
                    }
                }
            ).addTo(disposables)
    }

    fun getPictureFromCamera() {
        getPictureDisposable = photoHelper?.getFromCamera().uploadPicture()
    }

    fun getPicturesFromGallery() {
        getPictureDisposable = photoHelper?.getFromGallery().uploadPicture()
    }

    private fun Observable<File>?.uploadPicture(): Disposable? =
        this?.firstElement()
            ?.applySchedulers()
            ?.doOnSuccess { internalState.reduceState { it.copy(isLoading = true, errorState = null) } }
            ?.observeOn(Schedulers.io())
            ?.flatMap { uploadAvatar.invoke(it) }
            ?.observeOn(AndroidSchedulers.mainThread())
            ?.subscribeBy(
                onError = { throwable ->
                    internalState.reduceState {
                        val errorState = profileErrorStateHandler.handle(it.errorState, throwable)
                        it.copy(isLoading = false, errorState = errorState)
                    }
                },
                onSuccess = {
                    internalState.reduceState { it.copy(isLoading = false, errorState = null) }
                    profile = profile.copy(avatarUrl = it)
                }
            )?.addTo(disposables)
}