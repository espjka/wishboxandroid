package com.wishbox.featurecreateprofile.edit.domain.exception

data class ValidationException(
    var birthdayErrorText: String? = null,
    var firstNameErrorText: String? = null,
    var lastNameErrorText: String? = null,
    var patronymicErrorText: String? = null,
    var cityErrorText: String? = null
) : Exception()

