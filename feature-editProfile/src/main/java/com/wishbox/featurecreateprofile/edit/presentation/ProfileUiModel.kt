package com.wishbox.featurecreateprofile.edit.presentation

import com.wishbox.core.domain.entity.Gender
import com.wishbox.core.domain.entity.ItemValue

/**
 * Created by Pak Evgeniy on 20/12/2018.
 */

data class ProfileUiModel(
    val firstName: String?,
    val lastName: String?,
    val patronymic: String?,
    val avatarUrl: String?,
    val city: ItemValue?,
    val gender: Gender,
    val birthday: String?,
    val hobby: ItemValue?,
    val flower: ItemValue?,
    val alcohol: ItemValue?,
    val sweet: ItemValue?
) {
    fun checkActiveSaveButton(): Boolean =
        firstName.isNullOrBlank().not() and lastName.isNullOrBlank().not() and (city != null) and birthday.isNullOrBlank().not()
}