package com.wishbox.featurecreateprofile.edit.domain

import com.wishbox.core.domain.entity.UpdateProfileEntity
import com.wishbox.featurecreateprofile.edit.domain.exception.ValidationException

interface ProfileValidator {
    @Throws(ValidationException::class)
    fun validate(profile: UpdateProfileEntity): UpdateProfileEntity
}
