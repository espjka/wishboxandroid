package com.wishbox.featurecreateprofile.edit.domain

import com.wishbox.core.domain.entity.ProfileEntity
import com.wishbox.core.domain.repository.ProfileRepository
import com.wishbox.core.domain.repository.UserSessionRepository
import javax.inject.Inject

class GetCurrentProfileUseCase @Inject constructor(
    profileRepository: ProfileRepository,
    userSessionRepository: UserSessionRepository
) {

    val profile: ProfileEntity by lazy {
        val userId = userSessionRepository.userId ?: throw IllegalStateException("User id cannot be null")
        return@lazy profileRepository.profile ?: ProfileEntity(
            id = userId,
            alcohol = null,
            avatar = null,
            birthday = null,
            city = null,
            firstName = null,
            flower = null,
            friendAmount = 0,
            hobby = null,
            lastName = null,
            patronymic = null,
            sweet = null,
            wishAmount = 0
        )
    }

}