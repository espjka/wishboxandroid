package com.wishbox.featurecreateprofile.edit.presentation

import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.mock
import com.smedialink.common.rxbus.RxEventBus
import com.wishbox.core.di.rxbusprovider.RxBusProvider
import com.wishbox.core.di.screenproviders.FeatureProvider
import com.wishbox.core.domain.entity.Gender
import com.wishbox.core.domain.entity.ItemValue
import com.wishbox.core.domain.entity.ProfileEntity
import com.wishbox.core.domain.entity.UpdateProfileEntity
import com.wishbox.featurecreateprofile.edit.domain.GetCurrentProfileUseCase
import com.wishbox.featurecreateprofile.edit.domain.UpdateProfileUseCase
import com.wishbox.featurecreateprofile.edit.presentation.mapper.ProfileViewMapper
import io.reactivex.Maybe
import io.reactivex.Observable
import io.reactivex.Single
import ru.terrakok.cicerone.Router
import java.io.File
import java.util.*


data class EditProfileViewModelMocksBundle(
    val testProfileUiModel: ProfileUiModel = ProfileUiModel(
        "testFirstName",
        "testLastName",
        "testPatronymic",
        "testAvatarUrl",
        ItemValue(0, "testCity"),
        Gender.MALE,
        "test date string",
        ItemValue(1, "testHobby"),
        ItemValue(2, "testFlower"),
        ItemValue(3, "testAlcohol"),
        ItemValue(4, "testSweet")
    ),
    val testUpdateProfileEntity: UpdateProfileEntity = UpdateProfileEntity(
        "testFirstName",
        "testLastName",
        "testPatronymic",
        Gender.MALE,
        Calendar.getInstance().time,
        ItemValue(0, "testCity"),
        ItemValue(1, "testHobby"),
        ItemValue(2, "testFlower"),
        ItemValue(3, "testAlcohol"),
        ItemValue(4, "testSweet")
    ),
    val router: Router = mock(),
    val errorHandler: ProfileErrorStateHandler = mock(),
    val getCurrentProfileUseCase: GetCurrentProfileUseCase = mock {
        on { profile } doReturn ProfileEntity(0,"","")
    },
    val updateProfileUseCase: UpdateProfileUseCase = mock {
        onGeneric { invoke(any()) } doReturn Single.just(ProfileEntity(0,"",""))
    },
    val featureProvider: FeatureProvider = mock(),
    val selectionRxEventBus: RxEventBus<RxBusProvider.ItemValueSelectedEvent> = mock {
        on { value } doReturn Observable.just(RxBusProvider.ItemValueSelectedEvent())
    },
    val profileViewMapper: ProfileViewMapper = mock {
        on { map(any<ProfileEntity>()) } doReturn testProfileUiModel
        on { map(any<ProfileUiModel>()) } doReturn testUpdateProfileEntity
    },

    val uploadAvatar: (picture: File) -> Maybe<String> = mock()
)