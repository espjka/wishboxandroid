package com.wishbox.featurecreateprofile.edit.presentation

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.wishbox.coretesting.RxSchedulesRule
import com.wishbox.coretesting.testObserver
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class EditProfileViewModelTest {

    @get:Rule
    val schedulerRule = RxSchedulesRule()
    //
    @get:Rule
    val taskExecutorRule = InstantTaskExecutorRule()

    private lateinit var subject: EditProfileViewModel
    private val mocksBundle = EditProfileViewModelMocksBundle()
    lateinit var expected: List<ProfileState>

    @Before
    fun setUp() {
        expected = listOf(
            ProfileState(false, mocksBundle.testProfileUiModel, null),
            ProfileState(true, mocksBundle.testProfileUiModel, null),
            ProfileState(false, mocksBundle.testProfileUiModel, null)
        )

        subject = EditProfileViewModel(
            mocksBundle.router,
            mocksBundle.errorHandler,
            mocksBundle.getCurrentProfileUseCase,
            mocksBundle.updateProfileUseCase,
            mocksBundle.featureProvider,
            mocksBundle.selectionRxEventBus,
            mocksBundle.profileViewMapper,
            mocksBundle.uploadAvatar
        )
    }

    @Test
    fun updateUser() {
        val testObserver = subject.state.testObserver()
        subject.update(true)
        Assert.assertEquals(expected, testObserver.observedValues)
    }
}