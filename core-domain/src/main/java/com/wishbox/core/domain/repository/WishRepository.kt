package com.wishbox.core.domain.repository

import com.wishbox.core.domain.entity.WishEntity
import com.wishbox.core.domain.entity.WishRequestParams
import com.wishbox.core.domain.entity.WishWithLinks
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Single

/**
 * Created by Pak Evgeniy on 28/11/2018.
 */

interface WishRepository {
    fun requestWishList(userId: Long, limit: Int, offset: Int): Completable
    fun observeWishList(userId: Long): Flowable<List<WishEntity>>

    fun getWish(id: Long): Single<WishEntity>
    fun requestWish(id: Long): Completable
    fun observeWish(id: Long): Flowable<WishEntity>
    fun deleteWish(id: Long): Completable
    fun createWish(params: WishRequestParams): Single<WishWithLinks>
    fun updateWish(id: Long, params: WishRequestParams): Completable
    fun updateMainPicture(id: Long, key: String?): Completable
}