package com.wishbox.core.domain.entity

/**
 * Created by Pak Evgeniy on 24/12/2018.
 */

data class WishWithLinks(
    val id: Long,
    val links: List<String>
)