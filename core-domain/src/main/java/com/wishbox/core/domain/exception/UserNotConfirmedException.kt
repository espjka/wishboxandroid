package com.wishbox.core.domain.exception

class UserNotConfirmedException(val userId: Long) : Exception()