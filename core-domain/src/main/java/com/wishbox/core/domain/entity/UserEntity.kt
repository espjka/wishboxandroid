package com.wishbox.core.domain.entity

import java.util.*

/**
 * Created by Pak Evgeniy on 08.12.2018
 */

data class UserEntity(
    val id: Long,
    val firstName: String?,
    val lastName: String?,
    val patronymic: String? = null,
    val avatar: AvatarEntity? = null,
    val friendshipId: Long? = null,
    val gender: Gender = Gender.NONE,
    val isFriend: Boolean = false,
    val birthday: Date? = null,
    val city: ItemValue? = null,
    val hobby: ItemValue? = null,
    val alcohol: ItemValue? = null,
    val flower: ItemValue? = null,
    val sweet: ItemValue? = null,
    val friendAmount: Int = 0,
    val wishAmount: Int = 0
)

data class AvatarEntity(
    val small: String,
    val medium: String,
    val big: String
)