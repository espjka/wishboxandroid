package com.wishbox.core.domain.entity

import java.util.*

/**
 * Created by Pak Evgeniy on 20.12.2018
 */

data class ProfileEntity(
    val id: Long,
    val firstName: String?,
    val lastName: String?,
    val patronymic: String? = null,
    val avatar: AvatarEntity? = null,
    val gender: Gender = Gender.MALE, //default
    val birthday: Date? = null,
    val city: ItemValue? = null,
    val hobby: ItemValue? = null,
    val alcohol: ItemValue? = null,
    val flower: ItemValue? = null,
    val sweet: ItemValue? = null,
    val friendAmount: Int = 0,
    val wishAmount: Int = 0
)