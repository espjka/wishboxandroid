package com.wishbox.core.domain.entity

/**
 * Created by Pak Evgeniy on 20/12/2018.
 */

enum class Gender(val value: String? = null) {
    NONE,
    MALE("M"),
    FEMALE("F");

    companion object {
        fun getGender(str: String?): Gender =
            when (str) {
                "M" -> MALE
                "F" -> FEMALE
                else -> NONE
            }
    }
}