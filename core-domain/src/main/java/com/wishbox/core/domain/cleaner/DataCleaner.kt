package com.wishbox.core.domain.cleaner

import io.reactivex.Completable

/**
 * Created by Pak Evgeniy on 29/11/2018.
 */

interface DataCleaner {
    fun clear(): Completable
}