package com.wishbox.core.domain.entity

import java.util.*

/**
 * Created by Pak Evgeniy on 22.12.2018
 */

data class UpdateProfileEntity(
    val firstName: String?,
    val lastName: String?,
    val patronymic: String?,
    val gender: Gender,
    val birthday: Date?,
    val city: ItemValue?,
    val hobby: ItemValue?,
    val alcohol: ItemValue?,
    val flower: ItemValue?,
    val sweet: ItemValue?
)