package com.wishbox.core.domain.repository

import io.reactivex.Completable

/**
 * Created by Pak Evgeniy on 19/12/2018.
 */

interface FriendshipRepository {
    fun requestFriendship(id: Long): Completable
    fun acceptFriendship(id: Long): Completable
    fun breakFriendship(id: Long): Completable
}