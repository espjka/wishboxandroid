package com.wishbox.core.domain.repository

import com.wishbox.core.domain.entity.PictureEntity
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Single
import java.io.File

/**
 * Created by Pak Evgeniy on 22.12.2018
 */

interface PictureRepository {
    fun observePictures(wishId: Long): Flowable<List<PictureEntity>>
    fun uploadPicture(wishId: Long, file: File): Single<String>
    fun deletePicture(wishId: Long, key: String): Completable
    fun makePictureMain(wishId: Long, key: String): Completable
    fun initialUpload(
        wishId: Long,
        links: List<String>,
        localPaths: List<String>
    ): Single<String>

    fun getMainPictureKey(wishId: Long): String?
    fun getFristPictureKey(wishId: Long): String?
}