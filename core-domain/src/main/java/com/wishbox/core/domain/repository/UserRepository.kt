package com.wishbox.core.domain.repository

import com.wishbox.core.domain.entity.UserEntity
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Single

/**
 * Created by Pak Evgeniy on 08.12.2018
 */

interface UserRepository {

    fun requestMyFriendList(limit: Int, offset: Int): Completable
    fun observeMyFriendList(): Flowable<List<UserEntity>>
    fun getMyNotFriendList(limit: Int, offset: Int): Single<List<UserEntity>>

    fun getFriendList(userId: Long, limit: Int, offset: Int): Single<List<UserEntity>>

    fun requestUser(id: Long): Completable
    fun observeUser(id: Long): Flowable<UserEntity>

    fun getUser(id: Long, isFresh: Boolean = false): Single<UserEntity>
}