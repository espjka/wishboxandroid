package com.wishbox.core.domain.repository

import com.wishbox.core.domain.entity.*
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Single
import java.io.File

/**
 * Created by Pak Evgeniy on 20/12/2018.
 */

interface ProfileRepository {
    val profile: ProfileEntity?
    fun requestProfile(): Completable
    fun observeProfile(): Flowable<ProfileEntity>

    /**
     * @throws com.wishbox.core.domain.exception.ProfileNotExistException
     */
    fun checkExistence(): Completable

    fun updateProfile(profile: UpdateProfileEntity): Single<ProfileEntity>

    fun uploadAvatar(file: File): Single<AvatarEntity>

    fun requestProfileItemValues(
        type: ProfileItemValueType,
        queryString: String?,
        limit: Int,
        offset: Int
    ): Single<List<ItemValue>>

}