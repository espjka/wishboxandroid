package com.wishbox.core.domain.repository

import io.reactivex.Completable

interface AuthRepository {
    fun login(userName: String, password: String): Completable
}