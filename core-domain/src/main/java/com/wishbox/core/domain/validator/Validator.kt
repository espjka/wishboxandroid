package com.wishbox.core.domain.validator

import io.reactivex.Completable

interface Validator {
    fun validate(value: String): Completable
}