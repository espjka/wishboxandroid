package com.wishbox.core.domain.repository

import com.wishbox.core.domain.entity.UserCredentials
/*
* Should be used only after successful authorization
* */
interface UserSessionRepository {
    var userCredentials: UserCredentials? //todo change it with only properties
    var userId: Long?
    val isLoggedIn: Boolean
    val isConfirmed: Boolean
}