package com.wishbox.core.domain.entity

/**
 * Created by Pak Evgeniy on 28/11/2018.
 */

data class WishEntity(val id: Long,
                      val name: String,
                      val link: String? = null,
                      val comment: String? = null,
                      val address: String? = null,
                      val cost: String? = null,
                      val pictures: List<PictureEntity>? = null)