package com.wishbox.core.domain.entity

/**
 * Created by Pak Evgeniy on 20/12/2018.
 */

data class ItemValue(
    val id: Long,
    val name: String
)

enum class ProfileItemValueType(val value: String) {
    CITY("city"),
    HOBBY("hobby"),
    FLOWERS("flowers"),
    ALCOHOL("alcohol"),
    SWEETS("sweets");

    override fun toString(): String = value
}