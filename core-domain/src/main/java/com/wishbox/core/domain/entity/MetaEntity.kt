package com.wishbox.core.domain.entity

/**
 * Created by Pak Evgeniy on 04.12.2018
 */

data class MetaEntity(
    val title: String?,
    val imageLink: String?
)