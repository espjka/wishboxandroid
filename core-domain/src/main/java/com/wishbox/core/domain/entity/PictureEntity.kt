package com.wishbox.core.domain.entity

/**
 * Created by Pak Evgeniy on 22.12.2018
 */

data class PictureEntity(
    val key: String,
    val isMain: Boolean,
    val small: String,
    val medium: String,
    val big: String
)