package com.wishbox.core.domain.repository

import io.reactivex.Completable

interface ChangePasswordRepository {
    fun changePassword(old: String, new: String): Completable
}