package com.wishbox.core.domain.parser

import java.util.*

/**
 * Created by Pak Evgeniy on 22.12.2018
 */

interface DateParser {

    enum class Pattern(val pattern: String) {
        DATE("yyyy-MM-dd"),
        DATE_TIME("yyyy-MM-dd hh:mm:ss"),

        DATE_DOTS("dd.MM.yyyy")
    }

    fun stringToDate(from: String?, pattern: Pattern): Date?

    fun dateToString(from: Date?, pattern: Pattern): String?
}