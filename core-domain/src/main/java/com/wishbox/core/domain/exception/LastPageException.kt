package com.wishbox.core.domain.exception

/**
 * Created by Pak Evgeniy on 30.11.2018
 */

class LastPageException : Exception()