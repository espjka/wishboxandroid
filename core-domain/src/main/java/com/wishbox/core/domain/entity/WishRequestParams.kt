package com.wishbox.core.domain.entity

data class WishRequestParams(
    val name: String,
    val link: String?,
    val cost: String?,
    val comment: String?,
    val shopAddress: String?
)
