package com.wishbox.core.domain.repository

import io.reactivex.Completable

interface PinRepository {
    fun confirm(userId: Long, code: String): Completable

    fun resend(userId: Long): Completable
}