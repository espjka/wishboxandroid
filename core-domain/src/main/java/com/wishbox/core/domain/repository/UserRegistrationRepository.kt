package com.wishbox.core.domain.repository

import io.reactivex.Single

interface UserRegistrationRepository {

    fun register(userName: String, password: String): Single<Long>

}