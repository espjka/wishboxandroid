package com.wishbox.core.domain.repository

import com.wishbox.core.domain.entity.MetaEntity
import io.reactivex.Single

/**
 * Created by Pak Evgeniy on 03/12/2018.
 */

interface MetaInfoRepository {
    fun getMetaInfo(url: String): Single<MetaEntity>
}