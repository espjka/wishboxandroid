package com.wishbox.core.domain.exception

/**
 * Created by Pak Evgeniy on 19/12/2018.
 */

class ProfileNotExistException : Exception()