package com.wishbox.core.domain.entity

data class UserCredentials(
    val id: Long,
    val isConfirmed: Boolean
)