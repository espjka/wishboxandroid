package com.wishbox.core.domain.exception

class UserNotLoggedInException : Exception()