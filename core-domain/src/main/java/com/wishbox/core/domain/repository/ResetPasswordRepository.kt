package com.wishbox.core.domain.repository

import io.reactivex.Completable
import io.reactivex.Single

interface ResetPasswordRepository {

    fun sendResetCode(userName: String): Completable

    /**
     * @return Token to reset password
     * */
    fun confirmCode(username: String, code: String): Single<String>

    fun resetPassword(password: String, token: String): Completable

}