package com.wishbox

import android.content.Context
import android.content.Intent
import com.wishbox.core.di.screenproviders.*
import com.wishbox.feature.changePassword.ChangePasswordActivity
import com.wishbox.holder.entrance.EntranceActivity
import com.wishbox.holder.friendsEditor.FriendsEditorActivity
import com.wishbox.holder.main.MainActivity
import com.wishbox.holder.profileEditor.EditProfileActivity
import com.wishbox.holder.wishEditor.WishEditorActivity
import ru.terrakok.cicerone.android.support.SupportAppScreen
import javax.inject.Inject

/**
 * Created by Pak Evgeniy on 30.11.2018
 */

class MainScreenProviderImpl @Inject constructor() : MainScreenProvider {

    override fun getMainScreen(): SupportAppScreen {
        return object : SupportAppScreen() {
            override fun getScreenKey(): String = MainActivity::class.java.name
            override fun getActivityIntent(context: Context): Intent =
                Intent(context, MainActivity::class.java)
        }
    }
}

class EntranceScreenProviderImpl @Inject constructor() : EntranceScreenProvider {
    override fun getEntranceScreen(): SupportAppScreen {
        return object : SupportAppScreen() {
            override fun getScreenKey(): String = EntranceActivity::class.java.name
            override fun getActivityIntent(context: Context): Intent =
                Intent(context, EntranceActivity::class.java)
        }
    }
}

class WishEditorProviderImpl @Inject constructor() : WishEditorProvider {
    override fun getWishEditorScreen(wishId: Long?): SupportAppScreen {
        return object : SupportAppScreen() {
            override fun getScreenKey(): String = WishEditorActivity::class.java.name
            override fun getActivityIntent(context: Context): Intent =
                Intent(context, WishEditorActivity::class.java)
                    .apply { wishId?.let { putExtra(WishEditorActivity.BUNDLE_WISH_ID, it) } }
        }
    }
}

class FriendsEditorProviderImpl @Inject constructor() : FriendsEditorProvider {
    override fun getFriendsEditorScreen(authFlow: Boolean?): SupportAppScreen {
        return object : SupportAppScreen() {
            override fun getScreenKey(): String = FriendsEditorActivity::class.java.name
            override fun getActivityIntent(context: Context): Intent =
                Intent(context, FriendsEditorActivity::class.java)
                    .apply {
                        authFlow?.let {
                            putExtra(FriendsEditorActivity.BUNDLE_AUTH_FLOW, it)
                        }
                    }
        }
    }
}

class EditProfileScreenProviderImpl @Inject constructor() : EditProfileScreenProvider {
    override fun getEditProfileScreen(authFlow: Boolean?): SupportAppScreen {
        return object : SupportAppScreen() {
            override fun getScreenKey(): String = EditProfileActivity::class.java.name
            override fun getActivityIntent(context: Context): Intent =
                Intent(context, EditProfileActivity::class.java)
                    .apply {
                        authFlow?.let {
                            putExtra(EditProfileActivity.BUNDLE_AUTH_FLOW, it)
                        }
                    }
        }
    }
}

class ChangePasswordScreenProviderImpl @Inject constructor() : ChangePasswordScreenProvider {
    override fun getChangePasswordScreen(): SupportAppScreen {
        return object : SupportAppScreen() {
            override fun getScreenKey(): String = ChangePasswordActivity::class.java.name
            override fun getActivityIntent(context: Context): Intent =
                Intent(context, ChangePasswordActivity::class.java)
        }
    }
}