package com.wishbox.di

import com.wishbox.di.module.RootScreenProvidersModule
import com.wishbox.core.di.api.RootScreensApi
import com.wishbox.core.di.scope.PerApplication
import dagger.Component

@PerApplication
@Component(
    modules = [
        RootScreenProvidersModule::class
    ]
)
interface RootScreensComponent : RootScreensApi {

    class Initializer private constructor() {
        companion object {
            fun init(): RootScreensApi =
                DaggerRootScreensComponent.create()
        }
    }
}