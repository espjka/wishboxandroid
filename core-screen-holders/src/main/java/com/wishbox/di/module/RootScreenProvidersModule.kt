package com.wishbox.di.module

import com.wishbox.*
import com.wishbox.core.di.screenproviders.*
import dagger.Binds
import dagger.Module

@Module
interface RootScreenProvidersModule {

    @Binds
    fun provideMainScreenProvider(impl: MainScreenProviderImpl): MainScreenProvider

    @Binds
    fun provideWishEditorScreenProvider(impl: WishEditorProviderImpl): WishEditorProvider

    @Binds
    fun provideEntranceScreenProvider(impl: EntranceScreenProviderImpl): EntranceScreenProvider

    @Binds
    fun provideFriendsEditorScreenProvider(impl: FriendsEditorProviderImpl): FriendsEditorProvider

    @Binds
    fun provideEditProfileScreenProvider(impl: EditProfileScreenProviderImpl): EditProfileScreenProvider

    @Binds
    fun provideChangePasswordScreenProvider(impl: ChangePasswordScreenProviderImpl): ChangePasswordScreenProvider
}
