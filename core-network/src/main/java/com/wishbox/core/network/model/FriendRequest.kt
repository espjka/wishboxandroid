package com.wishbox.core.network.model

import com.google.gson.annotations.SerializedName

/**
 * Created by Pak Evgeniy on 20/12/2018.
 */

data class RequestFriendshipRequest(@SerializedName("user") val user: UserId)
data class UserId(@SerializedName("id") val id: Long)

data class AcceptFriendshipRequest(@SerializedName("confirmed") val confirmed: Boolean = true)