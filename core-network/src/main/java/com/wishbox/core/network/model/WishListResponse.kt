package com.wishbox.core.network.model

import com.google.gson.annotations.SerializedName

/**
 * Created by Pak Evgeniy on 28/11/2018.
 */

data class WishListResponse(@SerializedName("results") val wishList: List<WishShortDTO>? = null)

data class WishShortDTO(
    @SerializedName("id") val id: Long,
    @SerializedName("user") val userId: Long,
    @SerializedName("name") val name: String,
    @SerializedName("deleted") val isDeleted: Boolean,
    @SerializedName("presented") val isPresented: Boolean,
    @SerializedName("main_image") val mainPicture: PictureDTO? = null
)

data class WishDetailDTO(
    @SerializedName("id") val id: Long,
    @SerializedName("user") val userId: Long,
    @SerializedName("name") val name: String,
    @SerializedName("deleted") val isDeleted: Boolean,
    @SerializedName("presented") val isPresented: Boolean,
    @SerializedName("link") val link: String? = null,
    @SerializedName("cost") val cost: String? = null,
    @SerializedName("comment") val comment: String? = null,
    @SerializedName("shop_address") val address: String? = null,
    @SerializedName("images") val images: List<PictureDTO>,
    @SerializedName("upload_links") val links: List<String>)