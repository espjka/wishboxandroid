package com.wishbox.core.network.model

import com.google.gson.annotations.SerializedName

/**
 * Created by Pak Evgeniy on 28/11/2018.
 */

data class FriendListResponse(@SerializedName("results") val wishList: List<FriendDTO>? = null)

data class FriendDTO(
    @SerializedName("id") val friendshipId: Long,
    @SerializedName("user") val user: UserDTO,
    @SerializedName("confirmed") val isFriend: Boolean
)

