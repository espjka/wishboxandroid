package com.wishbox.core.network.model

import com.google.gson.annotations.SerializedName

/**
 * Created by Pak Evgeniy on 20/12/2018.
 */

data class UserListResponse(@SerializedName("results") val userList: List<UserDTO>? = null)

data class UserDTO(
    @SerializedName("id") val id: Long,
    @SerializedName("first_name") val firstName: String? = null,
    @SerializedName("last_name") val lastName: String? = null,
    @SerializedName("confirmed") val confirmed: String? = null,
    @SerializedName("gender") val gender: String? = null,
    @SerializedName("patronymic") val patronymic: String? = null,
    @SerializedName("birth_date") val birthday: String? = null,
    @SerializedName("city") val city: ItemValueDTO? = null,
    @SerializedName("hobby") val hobby: ItemValueDTO? = null,
    @SerializedName("alcohol") val alcohol: ItemValueDTO? = null,
    @SerializedName("flowers") val flower: ItemValueDTO? = null,
    @SerializedName("sweets") val sweet: ItemValueDTO? = null,
    @SerializedName("friends_count") val friendAmount: Int? = null,
    @SerializedName("gifts_count") val wishAmount: Int? = null,
    @SerializedName("avatar") val avatar: PictureDTO? = null,
    @SerializedName("private") val private: Boolean? = null
)

data class ItemValueDTO(
    @SerializedName("id") val id: Long,
    @SerializedName("name") val name: String? = null
)