package com.wishbox.core.network.model

import com.google.gson.annotations.SerializedName

data class RegistrationResponse(
    @SerializedName("id")
    val userId: Long,
    val username: String,
    val password: String
)
