package com.wishbox.core.network.model

import com.google.gson.annotations.SerializedName

data class UserRequestBody(
    @SerializedName("first_name") val firstName: String? = null,
    @SerializedName("last_name") val lastName: String? = null,
    @SerializedName("confirmed") val confirmed: String? = null,
    @SerializedName("gender") val gender: String? = null,
    @SerializedName("patronymic") val patronymic: String? = null,
    @SerializedName("birth_date") val birthDay: String? = null,
    @SerializedName("city") val city: ItemValueRequestBody? = null,
    @SerializedName("hobby") val hobby: ItemValueRequestBody? = null,
    @SerializedName("alcohol") val alcohol: ItemValueRequestBody? = null,
    @SerializedName("flowers") val flower: ItemValueRequestBody? = null,
    @SerializedName("sweets") val sweet: ItemValueRequestBody? = null
)

data class ItemValueRequestBody(
    @SerializedName("id") val id: Long
)