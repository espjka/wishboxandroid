package com.wishbox.core.network

import com.wishbox.core.network.model.*
import com.wishbox.core.network.model.LoginBody
import com.wishbox.core.network.model.LoginResponse
import com.wishbox.core.network.model.TokenBody
import io.reactivex.Completable
import io.reactivex.Single
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.POST

interface WishBoxAuthApi {

    @POST("auth/login/")
    fun login(@Body body: LoginBody): Single<LoginResponse>

    @POST("auth/refresh/")
    fun getAccessToken(@Body body: TokenBody): Call<ResponseBody>

    @POST("user/registration/")
    fun signUp(@Body body: LoginBody): Single<RegistrationResponse>

    @POST("user/confirmation/")
    fun confirmPin(@Body body: ConfirmPinBody): Single<LoginResponse>

    @POST("user/confirmation/resend/")
    fun resendPin(@Body body: ResendPinBody): Completable

    @POST("user/password_recovery/send_code/")
    fun sendRecoveryCode(@Body body: PasswordRecovery.SendCodeBody): Completable

    @POST("user/password_recovery/")
    fun confirmRecoveryCode(@Body body: PasswordRecovery.ConfirmCodeBody): Single<PasswordRecovery.ConfirmationToken>

    @POST("user/password_recovery/reset/")
    fun resetPassword(@Body body: PasswordRecovery.ResetPasswordBody): Completable
}