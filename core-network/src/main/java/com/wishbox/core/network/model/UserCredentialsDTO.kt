package com.wishbox.core.network.model

import com.google.gson.annotations.SerializedName

data class UserCredentialsDTO(
    @SerializedName("id") val id: Long,
    @SerializedName("email") val email: String?,
    @SerializedName("phone") val phone: String?,
    @SerializedName("is_active") val isActive: Boolean,
    @SerializedName("confirmed") val confirmed: String?
)
