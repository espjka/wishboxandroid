package com.wishbox.core.network.model

import com.google.gson.annotations.SerializedName

data class ProfileItemValuesResponse(@SerializedName("results") val items: List<ItemValueDTO>? = null)