package com.wishbox.core.network.model

import com.google.gson.annotations.SerializedName

data class ResendPinBody(
    @SerializedName("user_id")
    val userId: Long
)

data class ConfirmPinBody(
    @SerializedName("user_id")
    val userId: Long,
    val code: String
)