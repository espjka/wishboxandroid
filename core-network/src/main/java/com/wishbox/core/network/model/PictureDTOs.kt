package com.wishbox.core.network.model

import com.google.gson.annotations.SerializedName

/**
 * Created by Pak Evgeniy on 22.12.2018
 */

data class UploadLink(@SerializedName("link") val url: String)

data class PictureKey(@SerializedName("key") val key: String)

data class PictureDTO(
    @SerializedName("key") val key: String,
    @SerializedName("links") val links: LinksDTO
)

data class LinksDTO(
    @SerializedName("small") val small: String,
    @SerializedName("medium") val medium: String,
    @SerializedName("big") val big: String
)