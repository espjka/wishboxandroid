package com.wishbox.core.network.di

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.sml.oauth2token.interceptor.OAuth2Interceptor
import com.wishbox.core.di.scope.PerApplication
import com.wishbox.core.network.*
import dagger.Module
import dagger.Provides
import io.reactivex.schedulers.Schedulers
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import pl.droidsonroids.retrofit2.JspoonConverterFactory
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

@Module(includes = [OAuthInterceptorModule::class])
class NetworkModule {

    @PerApplication
    @Provides
    fun provideGson(): Gson = GsonBuilder().serializeNulls().create()

    @Provides
    fun provideWishBoxApi(
        gson: Gson,
        oAuth2Interceptor: OAuth2Interceptor
    ): WishBoxApi {
        val httpClient = OkHttpClient.Builder()
            .apply {
                if (BuildConfig.DEBUG) {
                    addInterceptor(HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
                }
            }
            .addInterceptor(oAuth2Interceptor)
            .build()

        return Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create(gson))
            .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))
            .client(httpClient)
            .baseUrl(BuildConfig.BASE_URL)
            .build()
            .create(WishBoxApi::class.java)
    }

    @Provides
    fun provideMetaDataApi(): MetaApi {
        val httpClient = OkHttpClient.Builder()
            .apply {
                if (BuildConfig.DEBUG) {
                    addInterceptor(HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
                }
            }
            .build()

        return Retrofit.Builder()
            .addConverterFactory(JspoonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))
            .client(httpClient)
            .baseUrl(BuildConfig.BASE_URL)
            .build()
            .create(MetaApi::class.java)
    }

    @Provides
    fun provideUploadApi(): UploadApi =
        Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))
            .client(OkHttpClient.Builder().apply {
                if (BuildConfig.DEBUG) {
                    addInterceptor(HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.HEADERS))
                }
            }.build())
            .baseUrl(BuildConfig.BASE_URL)
            .build()
            .create(UploadApi::class.java)

    @Provides
    fun provideWishBoxAuthApi(): WishBoxAuthApi {
        val httpClient = OkHttpClient.Builder()
            .apply {
                if (BuildConfig.DEBUG) {
                    addInterceptor(HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
                }
            }
            .build()

        return Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))
            .client(httpClient)
            .baseUrl(BuildConfig.BASE_URL)
            .build()
            .create(WishBoxAuthApi::class.java)
    }
}