package com.wishbox.core.network.oauth

import com.sml.oauth2token.AUTHORIZATION_HEADER
import com.sml.oauth2token.header.OAuth2HeaderProvider

class OAuthProviderImpl : OAuth2HeaderProvider {

    companion object {
        const val TOKEN_TYPE = "JWT"
    }

    override fun provideHeaderValue(accessToken: String): String = "$TOKEN_TYPE $accessToken"

    override fun provideHeaderName(): String = AUTHORIZATION_HEADER

}