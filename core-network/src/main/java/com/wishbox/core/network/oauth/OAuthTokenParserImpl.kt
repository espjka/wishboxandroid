package com.wishbox.core.network.oauth

import com.google.gson.Gson
import com.google.gson.JsonObject
import com.sml.oauth2token.tokenParser.OAuth2TokenParser
import okhttp3.ResponseBody
import javax.inject.Inject

class OAuthTokenParserImpl @Inject constructor(private val gson: Gson) : OAuth2TokenParser {

    companion object {
        const val ACCESS_TOKEN_FIELD_NAME = "access_token"
        const val REFRESH_TOKEN_FIELD_NAME = "refresh_token"
    }

    override fun parseTokens(body: ResponseBody?): Pair<String?, String?>? =
        body?.let {
            val element = gson.fromJson(it.string(), JsonObject::class.java)
            val access: String? = element
                .get(ACCESS_TOKEN_FIELD_NAME)
                .asString

            val refresh: String? = element
                .get(REFRESH_TOKEN_FIELD_NAME)
                .asString

            access to refresh
        }
}