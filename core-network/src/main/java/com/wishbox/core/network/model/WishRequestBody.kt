package com.wishbox.core.network.model

import com.google.gson.annotations.SerializedName

data class WishRequestBody(
    val name: String,
    val link: String?,
    val cost: String?,
    val comment: String?,
    @SerializedName("shop_address")
    val shopAddress: String?
)
