package com.wishbox.core.network.model

import com.google.gson.annotations.SerializedName

object PasswordRecovery {
    data class SendCodeBody(
        @SerializedName("username")
        val userName: String
    )

    data class ConfirmCodeBody(
        @SerializedName("username")
        val userName: String,
        val code: String
    )

    data class ResetPasswordBody(
        val password: String,
        val token: String
    )

    data class ConfirmationToken(
        val token: String
    )
}
