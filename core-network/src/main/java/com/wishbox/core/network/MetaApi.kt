package com.wishbox.core.network

import com.wishbox.core.network.model.MetaDataDTO
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Url

/**
 * Created by Pak Evgeniy on 04.12.2018
 */

interface MetaApi {

    @GET
    fun getMetaData(@Url url: String): Single<MetaDataDTO>
}