package com.wishbox.core.network.model

import com.google.gson.annotations.SerializedName

data class LoginResponse(@SerializedName("user") val user: UserDTO,
                         @SerializedName("access_token") val accessToken: String,
                         @SerializedName("refresh_token") val refreshToken: String)