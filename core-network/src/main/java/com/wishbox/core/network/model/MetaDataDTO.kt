package com.wishbox.core.network.model

import pl.droidsonroids.jspoon.annotation.Selector

/**
 * Created by Pak Evgeniy on 04.12.2018
 */

class MetaDataDTO {
    @Selector(value = "meta[property=\"og:title\"]", attr = "content") var title: String? = null
    @Selector(value = "meta[property=\"og:image\"]", attr = "content") var imageLink: String? = null
}