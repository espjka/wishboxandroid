package com.wishbox.core.network.di

import android.content.SharedPreferences
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.sml.oauth2token.interceptor.ConfigOAuth2Interceptor
import com.sml.oauth2token.interceptor.OAuth2Interceptor
import com.sml.oauth2token.session.OAuth2Session
import com.sml.oauth2token.session.OAuth2SessionImpl
import com.sml.oauth2token.tokenParser.OAuth2TokenParser
import com.wishbox.core.di.scope.PerApplication
import com.wishbox.core.network.WishBoxAuthApi
import com.wishbox.core.network.model.TokenBody
import com.wishbox.core.network.oauth.OAuthTokenParserImpl
import dagger.Binds
import dagger.Module
import dagger.Provides

@Module
abstract class OAuthInterceptorModule {

    @Module
    companion object {

        @JvmStatic
        @Provides
        fun provideOAuthConfig(
            oAuth2Session: OAuth2Session,
            tokenParser: OAuth2TokenParser,
            api: WishBoxAuthApi
        ): ConfigOAuth2Interceptor =
            ConfigOAuth2Interceptor(
                session = oAuth2Session,
                newAccessTokenCall = { api.getAccessToken(TokenBody(it)) },
                tokenParser = tokenParser
            )

        @JvmStatic
        @Provides
        fun provideOAuthInterceptor(configOAuth2Interceptor: ConfigOAuth2Interceptor): OAuth2Interceptor =
            OAuth2Interceptor(configOAuth2Interceptor)

        @JvmStatic
        @Provides
        fun provideOAuthSession(pref: SharedPreferences): OAuth2Session = OAuth2SessionImpl(pref)

    }

    @Binds
    abstract fun provideOAuth2TokenParser(impl: OAuthTokenParserImpl): OAuth2TokenParser
}
