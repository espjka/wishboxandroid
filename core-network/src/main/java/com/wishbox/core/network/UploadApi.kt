package com.wishbox.core.network

import com.wishbox.core.network.model.PictureDTO
import io.reactivex.Single
import okhttp3.MultipartBody
import retrofit2.http.Multipart
import retrofit2.http.POST
import retrofit2.http.Part
import retrofit2.http.Url

/**
 * Created by Pak Evgeniy on 24/12/2018.
 */

interface UploadApi {

    @Multipart
    @POST
    fun uploadPicture(@Url url: String, @Part part: MultipartBody.Part): Single<PictureDTO>
}