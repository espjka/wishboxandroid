package com.wishbox.core.network

import com.wishbox.core.network.model.*
import io.reactivex.Completable
import io.reactivex.Single
import retrofit2.http.*

interface WishBoxApi {

    @GET("gift/")
    fun getWishList(
        @Query("limit") limit: Int,
        @Query("offset") offset: Int,
        @Query("user") id: Long? = null
    ): Single<WishListResponse>

    @GET("gift/{id}/")
    fun getWish(@Path("id") id: Long): Single<WishDetailDTO>

    @DELETE("gift/{id}/")
    fun deleteWish(@Path("id") id: Long): Completable

    @POST("gift/")
    fun createWish(@Body body: WishRequestBody): Single<WishDetailDTO>

    @PATCH("gift/{id}/")
    fun updateWish(@Path("id") id: Long, @Body wish: WishRequestBody): Single<WishDetailDTO>

    @GET("user/me/friends/")
    fun getMyFriendList(
        @Query("limit") limit: Int,
        @Query("offset") offset: Int,
        @Query("confirmed") confirmed: Boolean? = null
    ): Single<FriendListResponse>

    @GET("user/{id}/friends/")
    fun getFriendList(
        @Path("id") id: Long,
        @Query("limit") limit: Int,
        @Query("offset") offset: Int,
        @Query("confirmed") confirmed: Boolean? = null
    ): Single<FriendListResponse>

    @GET("user/")
    fun getUserList(
        @Query("limit") limit: Int,
        @Query("offset") offset: Int,
        @Query("exclude_friends") excludeFriends: Boolean = false
    ): Single<UserListResponse>

    @GET("user/{id}/")
    fun getUser(@Path("id") id: Long): Single<UserDTO>

    @GET("user/me/")
    fun getMyProfile(): Single<UserDTO>

    @PATCH("user/me/")
    fun updateProfile(@Body userRequestBody: UserRequestBody): Single<UserDTO>

    @POST("user/me/friends/")
    fun requestFriendship(@Body body: RequestFriendshipRequest): Single<FriendDTO>

    @PATCH("user/me/friends/{friendshipId}/")
    fun acceptFriendship(
        @Path("friendshipId") friendshipId: Long,
        @Body body: AcceptFriendshipRequest = AcceptFriendshipRequest()
    ): Single<FriendDTO>

    @DELETE("user/me/friends/{friendshipId}/")
    fun breakFriendship(@Path("friendshipId") friendshipId: Long): Completable

    @GET("user/me/avatar/upload_link/")
    fun getUploadLinkForAvatar(): Single<UploadLink>

    @GET("gift/{wishId}/images/upload_links/")
    fun getUploadLinkForWish(@Path("wishId") wishId: Long): Single<UploadLink>

    @POST("gift/{wishId}/images/main/")
    fun setMainPicture(@Path("wishId") wishId: Long, @Body pictureKey: PictureKey): Completable

    @HTTP(method = "DELETE", path = "gift/{wishId}/images/", hasBody = true)
    fun deletePicture(@Path("wishId") wishId: Long, @Body pictureKey: PictureKey): Completable

    @POST("user/password_change/")
    fun changePassword(@Body body: ChangePasswordBody): Completable

    @GET("dictionary/{type}/")
    fun getProfileItemValues(
        @Path("type") type: String,
        @Query("search") queryString: String?,
        @Query("limit") limit: Int,
        @Query("offset") offset: Int
    ): Single<ProfileItemValuesResponse>

}