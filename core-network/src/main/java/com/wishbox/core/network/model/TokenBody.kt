package com.wishbox.core.network.model

import com.google.gson.annotations.SerializedName

/**
 * Created by Pak Evgeniy on 07.12.2018
 */
data class TokenBody(@SerializedName("refresh_token") val refreshToken: String)