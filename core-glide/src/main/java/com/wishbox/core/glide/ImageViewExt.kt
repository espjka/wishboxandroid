package com.wishbox.core.glide

import android.widget.ImageView
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners

/**
 * Created by Pak Evgeniy on 08.12.2018
 */

const val IMAGE_CORNER_RADIUS_IN_PX = 20

fun ImageView.loadImage(model: Any?, placeholder: Int? = null) {
    GlideApp.with(context)
        .load(model)
        .diskCacheStrategy(DiskCacheStrategy.DATA)
        .transforms(RoundedCorners(IMAGE_CORNER_RADIUS_IN_PX), CenterCrop())
        .apply { placeholder?.let { placeholder(it) } }
        .into(this)
}