package com.wishbox.core.glide

import com.bumptech.glide.annotation.GlideModule
import com.bumptech.glide.module.AppGlideModule

/**
 * Created by Pak Evgeniy on 08.12.2018
 */

@GlideModule
class WishBoxGlideModule : AppGlideModule()