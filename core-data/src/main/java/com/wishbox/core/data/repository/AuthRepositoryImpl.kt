package com.wishbox.core.data.repository

import com.wishbox.core.data.util.ProfileManager
import com.wishbox.core.data.session.SessionManager
import com.wishbox.core.domain.repository.AuthRepository
import com.wishbox.core.network.WishBoxAuthApi
import com.wishbox.core.network.model.LoginBody
import io.reactivex.Completable
import javax.inject.Inject

class AuthRepositoryImpl @Inject constructor(
    private val profileManager: ProfileManager,
    private val api: WishBoxAuthApi,
    private val sessionManager: SessionManager
) : AuthRepository {

    override fun login(userName: String, password: String): Completable {
        return api.login(LoginBody(userName, password))
            .doOnSuccess(sessionManager::save)
            .doOnSuccess(profileManager::saveProfile)
            .ignoreElement()
    }
}