package com.wishbox.core.data.mapper

import com.smedialink.common.ext.orFalse
import com.smedialink.common.ext.orZero
import com.wishbox.core.db.model.UserDbModel
import com.wishbox.core.domain.entity.Gender
import com.wishbox.core.domain.entity.UserEntity
import com.wishbox.core.domain.parser.DateParser
import com.wishbox.core.network.model.UserDTO
import javax.inject.Inject

/**
 * Created by Pak Evgeniy on 08.12.2018
 */

class UserMapper @Inject constructor(
    private val itemValueMapper: ItemValueMapper,
    private val dateParser: DateParser,
    private val pictureMapper: PictureMapper
) {

    fun mapToDbModelList(from: List<UserDTO>): List<UserDbModel> =
        from.map { mapToDbModel(it) }

    fun mapToDbModel(from: UserDTO): UserDbModel =
        UserDbModel(
            id = from.id.orZero(),
            firstName = from.firstName,
            lastName = from.lastName,
            patronymic = from.patronymic,
            avatar = from.avatar?.let { pictureMapper.mapToDbModel(it) },
            birthday = from.birthday,
            gender = from.gender,
            city = itemValueMapper.mapItemValueToDb(from.city),
            hobby = itemValueMapper.mapItemValueToDb(from.hobby),
            alcohol = itemValueMapper.mapItemValueToDb(from.alcohol),
            flower = itemValueMapper.mapItemValueToDb(from.flower),
            sweet = itemValueMapper.mapItemValueToDb(from.sweet),
            friendAmount = from.friendAmount,
            wishAmount = from.wishAmount
        )

    fun mapToEntityList(from: List<UserDbModel>): List<UserEntity> =
        from.map { mapToEntity(it) }

    fun mapToEntity(from: UserDbModel): UserEntity =
        UserEntity(
            id = from.id.orZero(),
            firstName = from.firstName,
            lastName = from.lastName,
            patronymic = from.patronymic,
            avatar = from.avatar?.let { pictureMapper.mapToEntity(it) },
            isFriend = from.isFriend.orFalse(),
            friendshipId = from.friendshipId,
            gender = Gender.getGender(from.gender),
            birthday = dateParser.stringToDate(from.birthday, DateParser.Pattern.DATE),
            city = itemValueMapper.mapItemValueToEntity(from.city),
            hobby = itemValueMapper.mapItemValueToEntity(from.hobby),
            alcohol = itemValueMapper.mapItemValueToEntity(from.alcohol),
            flower = itemValueMapper.mapItemValueToEntity(from.flower),
            sweet = itemValueMapper.mapItemValueToEntity(from.sweet),
            friendAmount = from.friendAmount.orZero(),
            wishAmount = from.wishAmount.orZero()
        )
}