package com.wishbox.core.data.parser

import com.wishbox.core.domain.parser.DateParser
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject

class DateParserImpl @Inject constructor() : DateParser {

    private val formatter = SimpleDateFormat("", Locale.getDefault())

    override fun stringToDate(from: String?, pattern: DateParser.Pattern): Date? =
        try {
            from?.let { formatter.apply { applyPattern(pattern.pattern) }.parse(it) }
        } catch (exception: ParseException) {
            null
        }

    override fun dateToString(from: Date?, pattern: DateParser.Pattern): String? =
        from?.let { formatter.apply { applyPattern(pattern.pattern) }.format(it) }
}