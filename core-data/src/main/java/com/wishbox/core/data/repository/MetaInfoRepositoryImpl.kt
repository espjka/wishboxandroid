package com.wishbox.core.data.repository

import com.wishbox.core.data.mapper.MetaMapper
import com.wishbox.core.domain.repository.MetaInfoRepository
import com.wishbox.core.domain.entity.MetaEntity
import com.wishbox.core.network.MetaApi
import io.reactivex.Single
import javax.inject.Inject

/**
 * Created by Pak Evgeniy on 03/12/2018.
 */

class MetaInfoRepositoryImpl @Inject constructor(
    private val metaApi: MetaApi,
    private val mapper: MetaMapper
) : MetaInfoRepository {

    override fun getMetaInfo(url: String): Single<MetaEntity> =
        metaApi.getMetaData(url)
            .map { mapper.map(it) }
}