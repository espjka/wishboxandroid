package com.wishbox.core.data.mapper

import com.wishbox.core.domain.entity.UserCredentials
import com.wishbox.core.network.model.UserDTO
import javax.inject.Inject

class UserCredentialsMapper @Inject constructor() {

    fun map(from: UserDTO): UserCredentials {
        return UserCredentials(
            id = from.id,
            isConfirmed = from.confirmed.isNullOrBlank().not()
        )
    }
}