package com.wishbox.core.data.di.module

import android.content.SharedPreferences
import com.f2prateek.rx.preferences2.RxSharedPreferences
import com.wishbox.core.data.cleaner.DataCleanerImpl
import com.wishbox.core.data.manager.UploadPictureManager
import com.wishbox.core.data.manager.UploadPictureManagerImpl
import com.wishbox.core.data.parser.DateParserImpl
import com.wishbox.core.domain.cleaner.DataCleaner
import com.wishbox.core.domain.parser.DateParser
import dagger.Binds
import dagger.Module
import dagger.Provides

/**
 * Created by Pak Evgeniy on 29/11/2018.
 */

@Module
abstract class UtilModule {

    @Binds
    abstract fun provideDataCleaner(cleaner: DataCleanerImpl): DataCleaner

    @Binds
    abstract fun provideDateParser(dateParser: DateParserImpl): DateParser

    @Binds
    abstract fun provideUploadPictureManager(impl: UploadPictureManagerImpl): UploadPictureManager

    @Module
    companion object {
        @Provides
        @JvmStatic
        fun provideRxSharedPrefs(sharedPrefs: SharedPreferences): RxSharedPreferences =
            RxSharedPreferences.create(sharedPrefs)
    }
}