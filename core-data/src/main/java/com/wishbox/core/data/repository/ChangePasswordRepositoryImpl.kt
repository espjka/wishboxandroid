package com.wishbox.core.data.repository

import com.wishbox.core.domain.repository.ChangePasswordRepository
import com.wishbox.core.network.WishBoxApi
import com.wishbox.core.network.model.ChangePasswordBody
import io.reactivex.Completable
import javax.inject.Inject

class ChangePasswordRepositoryImpl @Inject constructor(
    private val api: WishBoxApi
) : ChangePasswordRepository {
    override fun changePassword(old: String, new: String): Completable =
            api.changePassword(ChangePasswordBody(old, new))
}