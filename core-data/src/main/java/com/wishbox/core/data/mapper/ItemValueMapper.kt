package com.wishbox.core.data.mapper

import com.wishbox.core.db.model.ItemValueDbModel
import com.wishbox.core.domain.entity.ItemValue
import com.wishbox.core.network.model.ItemValueDTO
import javax.inject.Inject

/**
 * Created by Pak Evgeniy on 08.12.2018
 */

class ItemValueMapper @Inject constructor() {

    fun mapItemValueToDb(value: ItemValueDTO?): ItemValueDbModel? =
        when {
            value?.name == null -> null
            else -> ItemValueDbModel(value.id, value.name!!)
        }

    fun mapItemValueToEntity(value: ItemValueDTO?): ItemValue? =
        when {
            value?.name == null -> null
            else -> ItemValue(value.id, value.name!!)
        }

    fun mapItemValuesToEntities(values: List<ItemValueDTO>): List<ItemValue> {
        return values.mapNotNull {
            mapItemValueToEntity(it)
        }
    }

    fun mapItemValueToEntity(value: ItemValueDbModel?): ItemValue? = value?.let { ItemValue(it.id, it.name) }
}