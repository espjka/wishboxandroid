package com.wishbox.core.data.mapper

import com.wishbox.core.domain.entity.MetaEntity
import com.wishbox.core.network.model.MetaDataDTO
import javax.inject.Inject

/**
 * Created by Pak Evgeniy on 04.12.2018
 */

class MetaMapper @Inject constructor() {

    fun map(from: MetaDataDTO): MetaEntity =
            MetaEntity(
                from.title,
                from.imageLink
            )
}