package com.wishbox.core.data.session

import com.sml.oauth2token.session.OAuth2Session
import com.wishbox.core.data.mapper.UserCredentialsMapper
import com.wishbox.core.network.model.LoginResponse
import javax.inject.Inject

class SessionManager @Inject constructor(
    private val userCredentialsMapper: UserCredentialsMapper,
    private val userSession: UserSession,
    private val oAuth2Session: OAuth2Session
) {

    fun save(loginResponse: LoginResponse) {
        userSession.userCredentials = userCredentialsMapper.map(loginResponse.user)
        oAuth2Session.accessToken = loginResponse.accessToken
        oAuth2Session.refreshToken = loginResponse.refreshToken
    }

}
