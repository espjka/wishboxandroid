package com.wishbox.core.data.repository

import com.wishbox.core.data.mapper.FriendMapper
import com.wishbox.core.data.mapper.UserMapper
import com.wishbox.core.db.dao.UserDao
import com.wishbox.core.domain.entity.UserEntity
import com.wishbox.core.domain.exception.LastPageException
import com.wishbox.core.domain.repository.UserRepository
import com.wishbox.core.network.WishBoxApi
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Single
import javax.inject.Inject

/**
 * Created by Pak Evgeniy on 08.12.2018
 */

class UserRepositoryImpl @Inject constructor(
    private val api: WishBoxApi,
    private val dao: UserDao,
    private val userMapper: UserMapper,
    private val friendMapper: FriendMapper
) : UserRepository {

    override fun getUser(id: Long, isFresh: Boolean): Single<UserEntity> {
        val userRequest = if (isFresh) {
            api.getUser(id)
                .map { userMapper.mapToDbModel(it) }
                .doOnSuccess { dao.updateDetailIfExists(it) }
                .onErrorResumeNext { dao.getUser(id) }
        } else {
            dao.getUser(id)
                .onErrorResumeNext { api.getUser(id).map { userMapper.mapToDbModel(it) } }
        }

        return userRequest.onErrorResumeNext { dao.getUser(id) }
            .map { userMapper.mapToEntity(it) }
    }

    override fun getMyNotFriendList(limit: Int, offset: Int): Single<List<UserEntity>> =
        api.getUserList(limit, offset, excludeFriends = true)
            .map { userMapper.mapToDbModelList(it.userList.orEmpty()) }
            .doOnSuccess { dao.updateListShortIfExists(it) }
            .map { userMapper.mapToEntityList(it) }

    override fun requestMyFriendList(limit: Int, offset: Int): Completable =
        api.getMyFriendList(limit, offset, true)
            .map { it.wishList.orEmpty() }
            .map { friendMapper.mapToDbModelList(it) }
            .doOnSuccess { if (offset == 0) dao.clearAndInsert(it) else dao.insertOrReplace(it) }
            .flatMap { if (it.size < limit) Single.error(LastPageException()) else Single.just(it) }
            .ignoreElement()

    override fun observeMyFriendList(): Flowable<List<UserEntity>> =
        dao.observeMyFriendList()
            .map { userMapper.mapToEntityList(it) }

    override fun getFriendList(userId: Long, limit: Int, offset: Int): Single<List<UserEntity>> =
        api.getFriendList(userId, limit, offset, true)
            .map { it.wishList.orEmpty() }
            .map { friendMapper.mapToDbModelList(it) }
            .doOnSuccess { dao.updateListShortIfExists(it) }
            .map { userMapper.mapToEntityList(it) }

    override fun observeUser(id: Long): Flowable<UserEntity> =
        dao.observeUser(id)
            .map { userMapper.mapToEntity(it) }

    override fun requestUser(id: Long): Completable =
        api.getUser(id)
            .map { userMapper.mapToDbModel(it) }
            .doOnSuccess { dao.insertOrReplace(it) }
            .ignoreElement()
}