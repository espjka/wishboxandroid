package com.wishbox.core.data.manager

import com.wishbox.core.network.UploadApi
import com.wishbox.core.network.model.PictureDTO
import io.reactivex.Single
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.io.File
import javax.inject.Inject

/**
 * Created by Pak Evgeniy on 26.12.2018
 */

interface UploadPictureManager {
    fun upload(url: String, picture: File): Single<PictureDTO>
}

class UploadPictureManagerImpl @Inject constructor(
    private val uploadApi: UploadApi
) : UploadPictureManager {

    companion object {
        private const val MEDIA_TYPE = "image/*"
        private const val PART_NAME = "file"
    }

    override fun upload(url: String, picture: File): Single<PictureDTO> =
        uploadApi.uploadPicture(url, createPart(picture))

    private fun createPart(file: File): MultipartBody.Part = MultipartBody.Part.createFormData(
        PART_NAME,
        file.name,
        RequestBody.create(MediaType.parse(MEDIA_TYPE), file)
    )
}