package com.wishbox.core.data.mapper

import com.smedialink.common.ext.orZero
import com.wishbox.core.domain.entity.Gender
import com.wishbox.core.domain.entity.ProfileEntity
import com.wishbox.core.domain.entity.UpdateProfileEntity
import com.wishbox.core.domain.parser.DateParser
import com.wishbox.core.network.model.ItemValueRequestBody
import com.wishbox.core.network.model.UserDTO
import com.wishbox.core.network.model.UserRequestBody
import javax.inject.Inject

/**
 * Created by Pak Evgeniy on 08.12.2018
 */

class ProfileMapper @Inject constructor(
    private val itemValueMapper: ItemValueMapper,
    private val dateParser: DateParser,
    private val pictureMapper: PictureMapper
) {

    fun mapToEntity(from: UserDTO): ProfileEntity =
        ProfileEntity(
            id = from.id,
            firstName = from.firstName,
            lastName = from.lastName,
            patronymic = from.patronymic,
            avatar = from.avatar?.let { pictureMapper.mapToEntity(it) },
            gender = Gender.getGender(from.gender),
            birthday = dateParser.stringToDate(from.birthday, DateParser.Pattern.DATE),
            city = itemValueMapper.mapItemValueToEntity(from.city),
            hobby = itemValueMapper.mapItemValueToEntity(from.hobby),
            alcohol = itemValueMapper.mapItemValueToEntity(from.alcohol),
            flower = itemValueMapper.mapItemValueToEntity(from.flower),
            sweet = itemValueMapper.mapItemValueToEntity(from.sweet),
            friendAmount = from.friendAmount.orZero(),
            wishAmount = from.wishAmount.orZero()
        )

    fun mapToDto(from: UpdateProfileEntity): UserRequestBody =
        UserRequestBody(
            firstName = from.firstName,
            lastName = from.lastName,
            patronymic = from.patronymic,
            gender = from.gender.value,
            birthDay = dateParser.dateToString(from.birthday, DateParser.Pattern.DATE),
            city = from.city?.id?.let { ItemValueRequestBody(it) },
            hobby = from.hobby?.id?.let { ItemValueRequestBody(it) },
            alcohol = from.alcohol?.id?.let { ItemValueRequestBody(it) },
            flower = from.flower?.id?.let { ItemValueRequestBody(it) },
            sweet = from.sweet?.id?.let { ItemValueRequestBody(it) }
        )
}