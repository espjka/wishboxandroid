package com.wishbox.core.data.repository

import com.wishbox.core.data.mapper.FriendMapper
import com.wishbox.core.db.dao.UserDao
import com.wishbox.core.domain.exception.UserDidntSentYouRequestException
import com.wishbox.core.domain.exception.UserNotYourFriendException
import com.wishbox.core.domain.repository.FriendshipRepository
import com.wishbox.core.network.WishBoxApi
import com.wishbox.core.network.model.RequestFriendshipRequest
import com.wishbox.core.network.model.UserId
import io.reactivex.Completable
import javax.inject.Inject

/**
 * Created by Pak Evgeniy on 19/12/2018.
 */

class FriendshipRepositoryImpl @Inject constructor(
    private val api: WishBoxApi,
    private val userDao: UserDao,
    private val friendMapper: FriendMapper
) : FriendshipRepository {

    override fun requestFriendship(id: Long): Completable =
        api.requestFriendship(RequestFriendshipRequest(UserId(id)))
            .map { friendMapper.mapToDbModel(it) }
            .doOnSuccess {
                it.isFriend = true  //remove this when private friendship will be ready on server side
                userDao.updateFriendship(id, it.friendshipId, it.isFriend)
            }
            .ignoreElement()

    override fun acceptFriendship(id: Long): Completable =
        userDao.getUser(id)
            .flatMapCompletable { user ->
                user.friendshipId
                    ?.let { id ->
                        api.acceptFriendship(id)
                            .map { friendMapper.mapToDbModel(it) }
                            .doOnSuccess {
                                userDao.updateFriendship(
                                    id,
                                    it.friendshipId,
                                    it.isFriend
                                )
                            }
                            .ignoreElement()
                    }
                    ?: Completable.error(UserDidntSentYouRequestException())
            }

    override fun breakFriendship(id: Long): Completable =
        userDao.getUser(id)
            .flatMapCompletable {
                it.friendshipId
                    ?.let {
                        api.breakFriendship(it)
                            .doOnComplete { userDao.updateFriendship(id) }
                    }
                    ?: Completable.error(UserNotYourFriendException())
            }
}