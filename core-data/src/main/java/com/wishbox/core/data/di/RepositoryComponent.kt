package com.wishbox.core.data.di

import com.wishbox.core.network.di.NetworkModule
import com.wishbox.core.data.di.module.RepositoryModule
import com.wishbox.core.data.di.module.UtilModule
import com.wishbox.core.db.di.DbModule
import com.wishbox.core.di.api.AppApi
import com.wishbox.core.di.api.RepositoryApi
import com.wishbox.core.di.scope.PerApplication
import dagger.Component

@PerApplication
@Component(
    dependencies = [
        AppApi::class
    ],
    modules = [
        RepositoryModule::class,
        NetworkModule::class,
        DbModule::class,
        UtilModule::class
    ]
)
interface RepositoryComponent : RepositoryApi {

    @Component.Builder
    interface Builder {

        fun api(api: AppApi): Builder

        fun build(): RepositoryComponent
    }

    class Initializer private constructor() {
        companion object {
            fun init(appApi: AppApi): RepositoryApi =
                DaggerRepositoryComponent.builder()
                    .api(appApi)
                    .build()
        }
    }
}