package com.wishbox.core.data.repository

import com.wishbox.core.data.mapper.PictureMapper
import com.wishbox.core.data.mapper.WishMapper
import com.wishbox.core.data.mapper.WishRequestBodyMapper
import com.wishbox.core.db.dao.PictureDao
import com.wishbox.core.db.dao.WishDao
import com.wishbox.core.db.model.WishWithMainPicture
import com.wishbox.core.db.model.WishWithPictures
import com.wishbox.core.domain.entity.WishEntity
import com.wishbox.core.domain.entity.WishRequestParams
import com.wishbox.core.domain.entity.WishWithLinks
import com.wishbox.core.domain.exception.LastPageException
import com.wishbox.core.domain.repository.WishRepository
import com.wishbox.core.network.WishBoxApi
import com.wishbox.core.network.model.PictureDTO
import com.wishbox.core.network.model.WishShortDTO
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Single
import javax.inject.Inject

/**
 * Created by Pak Evgeniy on 28/11/2018.
 */

class WishRepositoryImpl @Inject constructor(
    private val api: WishBoxApi,
    private val dao: WishDao,
    private val pictureDao: PictureDao,
    private val mapper: WishMapper,
    private val pictureMapper: PictureMapper,
    private val wishRequestBodyMapper: WishRequestBodyMapper
) : WishRepository {

    override fun getWish(id: Long): Single<WishEntity> =
        dao.getWish(id)
            .map { WishWithPictures(it, pictureDao.getPictures(id)) }
            .map { mapper.map(it) }

    override fun updateWish(id: Long, params: WishRequestParams): Completable =
        api.updateWish(id, wishRequestBodyMapper.map(params))
            .map { mapper.map(it) }
            .doOnSuccess { dao.updateDetail(it) }
            .ignoreElement()

    override fun requestWishList(userId: Long, limit: Int, offset: Int): Completable =
        api.getWishList(limit, offset, userId)
            .map { it.wishList.orEmpty() }
            .doOnSuccess { handleCacheShort(userId, offset == 0, it) }
            .flatMap { if (it.size < limit) Single.error(LastPageException()) else Single.just(it) }
            .ignoreElement()

    private fun handleCacheShort(userId: Long, needToDrop: Boolean, list: List<WishShortDTO>) {
        val wishes = list.map { wish ->
            wish.mainPicture?.let {
                pictureDao.updateMainPicture(
                    wish.id,
                    pictureMapper.map(it, wish.id, true)
                )
            }
            mapper.map(wish)
        }
        if (needToDrop) {
            dao.clearAndInsert(userId, wishes)
        } else {
            dao.updateShortList(wishes)
        }
    }

    override fun observeWishList(userId: Long): Flowable<List<WishEntity>> =
        dao.observeWishList(userId)
            .map { list ->
                list.map {
                    WishWithMainPicture(
                        it,
                        it.mainPictureKey?.let { key -> pictureDao.getPicture(key) })
                }
            }
            .map { mapper.mapToEntityList(it) }

    override fun observeWish(id: Long): Flowable<WishEntity> =
        Flowable.merge(wish(id), pictures(id))
            .map { mapper.map(it) }

    private fun wish(id: Long): Flowable<WishWithPictures> =
        dao.observeWish(id).map { WishWithPictures(it, pictureDao.getPictures(id)) }

    private fun pictures(id: Long): Flowable<WishWithPictures> =
        pictureDao.observePictures(id).map { list ->
            dao.getWishModel(id)?.let { WishWithPictures(it, list) }
                ?: WishWithPictures(pictures = list)
        }

    override fun requestWish(id: Long): Completable =
        api.getWish(id)
            .doOnSuccess { handlePicturesCache(id, it.images) }
            .map { mapper.map(it) }
            .doOnSuccess { dao.updateDetail(it) }
            .ignoreElement()

    private fun handlePicturesCache(wishId: Long, pictures: List<PictureDTO>) {
        val dbList = pictures.mapIndexed { index, wish ->
            pictureMapper.map(wish, wishId, index == 0)
        }
        pictureDao.upsert(dbList)
    }

    override fun deleteWish(id: Long): Completable =
        api.deleteWish(id)
            .doOnComplete { dao.deleteById(id) }

    override fun createWish(params: WishRequestParams): Single<WishWithLinks> =
        api.createWish(wishRequestBodyMapper.map(params))
            .map { Pair(mapper.map(it), it.links) }
            .doOnSuccess { dao.insertOrReplace(it.first) }
            .map { WishWithLinks(it.first.id, it.second) }

    override fun updateMainPicture(id: Long, key: String?): Completable =
        Completable.fromAction {
            dao.getWishModel(id)
                ?.let {
                    it.mainPictureKey = key
                    dao.upsert(it)
                }
        }
}