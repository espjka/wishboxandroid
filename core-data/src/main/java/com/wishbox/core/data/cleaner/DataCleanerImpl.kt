package com.wishbox.core.data.cleaner

import com.sml.oauth2token.session.OAuth2Session
import com.wishbox.core.data.session.UserSession
import com.wishbox.core.data.util.ProfileManager
import com.wishbox.core.db.WishBoxDatabase
import com.wishbox.core.domain.cleaner.DataCleaner
import io.reactivex.Completable
import javax.inject.Inject

/**
 * Created by Pak Evgeniy on 29/11/2018.
 */

class DataCleanerImpl @Inject constructor(
    private val db: WishBoxDatabase,
    private val userSession: UserSession,
    private val oAuth2Session: OAuth2Session,
    private val profileManager: ProfileManager
) : DataCleaner {

    override fun clear(): Completable =
        Completable.fromAction {
            db.clear()
            profileManager.clear()
            userSession.clear()
            oAuth2Session.clearToken()
        }
}