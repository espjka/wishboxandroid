package com.wishbox.core.data.repository

import com.wishbox.core.data.util.ProfileManager
import com.wishbox.core.data.session.SessionManager
import com.wishbox.core.domain.repository.PinRepository
import com.wishbox.core.network.WishBoxAuthApi
import com.wishbox.core.network.model.ConfirmPinBody
import com.wishbox.core.network.model.ResendPinBody
import io.reactivex.Completable
import javax.inject.Inject

class PinRepoImpl @Inject constructor(
    private val authApi: WishBoxAuthApi,
    private val sessionManager: SessionManager,
    private val profileManager: ProfileManager
) : PinRepository {

    override fun confirm(userId: Long, code: String): Completable {
        val body = ConfirmPinBody(
            userId = userId,
            code = code
        )

        return authApi.confirmPin(body)
            .doOnSuccess(sessionManager::save)
            .doOnSuccess(profileManager::saveProfile)
            .ignoreElement()
    }

    override fun resend(userId: Long): Completable {
        val body = ResendPinBody(
            userId = userId
        )

        return authApi.resendPin(body)
    }

}