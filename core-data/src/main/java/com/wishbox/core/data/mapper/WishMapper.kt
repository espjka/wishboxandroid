package com.wishbox.core.data.mapper

import com.smedialink.common.ext.orZero
import com.wishbox.core.db.model.WishDbModel
import com.wishbox.core.db.model.WishWithMainPicture
import com.wishbox.core.db.model.WishWithPictures
import com.wishbox.core.domain.entity.WishEntity
import com.wishbox.core.network.model.WishDetailDTO
import com.wishbox.core.network.model.WishShortDTO
import javax.inject.Inject

/**
 * Created by Pak Evgeniy on 28/11/2018.
 */

class WishMapper @Inject constructor(
    private val pictureMapper: PictureMapper
) {

    fun map(short: WishShortDTO): WishDbModel =
        WishDbModel(
            id = short.id,
            userId = short.userId.orZero(),
            name = short.name,
            mainPictureKey = short.mainPicture?.key
        )

    fun map(detail: WishDetailDTO): WishDbModel =
        WishDbModel(
            id = detail.id,
            userId = detail.userId.orZero(),
            link = detail.link,
            name = detail.name,
            comment = detail.comment,
            address = detail.address,
            cost = detail.cost
        )

    fun mapToEntityList(from: List<WishWithMainPicture>): List<WishEntity> =
        from.map { map(it) }

    fun map(from: WishWithMainPicture): WishEntity =
        WishEntity(
            id = from.wish.id.orZero(),
            name = from.wish.name.orEmpty(),
            link = from.wish.link,
            comment = from.wish.comment,
            address = from.wish.address,
            cost = from.wish.cost,
            pictures = from.mainPicture?.let { listOf(pictureMapper.map(it)) }
        )

    fun map(from: WishWithPictures): WishEntity =
        WishEntity(
            id = from.wish.id.orZero(),
            name = from.wish.name.orEmpty(),
            link = from.wish.link,
            comment = from.wish.comment,
            address = from.wish.address,
            cost = from.wish.cost,
            pictures = pictureMapper.mapList(from.pictures)
        )
}