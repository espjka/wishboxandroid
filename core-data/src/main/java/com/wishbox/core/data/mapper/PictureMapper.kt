package com.wishbox.core.data.mapper

import com.wishbox.core.db.model.AvatarDbModel
import com.wishbox.core.db.model.PictureDbModel
import com.wishbox.core.domain.entity.AvatarEntity
import com.wishbox.core.domain.entity.PictureEntity
import com.wishbox.core.network.model.PictureDTO
import javax.inject.Inject

/**
 * Created by Pak Evgeniy on 22.12.2018
 */

class PictureMapper @Inject constructor() {

    fun map(from: PictureDTO, wishId: Long, isMain: Boolean): PictureDbModel =
            PictureDbModel(
                key = from.key,
                attachId = wishId,
                isMain = isMain,
                small = from.links.small,
                medium = from.links.medium,
                big = from.links.big
            )

    fun mapToDbModel(from: PictureDTO): AvatarDbModel =
            AvatarDbModel(
                small = from.links.small,
                medium = from.links.medium,
                big = from.links.big
            )

    fun mapToEntity(from: AvatarDbModel): AvatarEntity =
        AvatarEntity(
            small = from.small,
            medium = from.medium,
            big = from.big
        )

    fun mapToEntity(from: PictureDTO): AvatarEntity =
        AvatarEntity(
            small = from.links.small,
            medium = from.links.medium,
            big = from.links.big
        )

    fun mapList(from: List<PictureDbModel>): List<PictureEntity> =
            from.map { map(it) }

    fun map(from: PictureDbModel): PictureEntity =
            PictureEntity(
                key = from.key,
                isMain = from.isMain,
                small = from.small,
                medium = from.medium,
                big = from.big
            )
}