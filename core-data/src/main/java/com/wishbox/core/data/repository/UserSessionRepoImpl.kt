package com.wishbox.core.data.repository

import com.sml.oauth2token.session.OAuth2Session
import com.wishbox.core.data.session.UserSession
import com.wishbox.core.domain.entity.UserCredentials
import com.wishbox.core.domain.repository.UserSessionRepository
import javax.inject.Inject

class UserSessionRepoImpl @Inject constructor(
    private val userSession: UserSession,
    private val oAuth2Session: OAuth2Session
) : UserSessionRepository {

    override var userCredentials: UserCredentials?
        get() = userSession.userCredentials
        set(value) {
            userSession.userCredentials = value
        }

    override var userId: Long?
        get() = userSession.userCredentials?.id
        set(value) {
            val profile = userSession.userCredentials
            value?.let {
                userSession.userCredentials = profile?.copy(id = value) ?: UserCredentials(
                    id = value,
                    isConfirmed = false
                )
            }
        }

    override val isLoggedIn: Boolean
        get() = oAuth2Session.accessToken != null && oAuth2Session.refreshToken != null

    override val isConfirmed: Boolean
        get() = userSession.userCredentials?.isConfirmed ?: false
}