package com.wishbox.core.data.mapper

import com.smedialink.common.ext.orZero
import com.wishbox.core.db.model.UserDbModel
import com.wishbox.core.domain.entity.Gender
import com.wishbox.core.domain.entity.UserEntity
import com.wishbox.core.domain.parser.DateParser
import com.wishbox.core.network.model.FriendDTO
import javax.inject.Inject

/**
 * Created by Pak Evgeniy on 08.12.2018
 */

class FriendMapper @Inject constructor(
    private val itemValueMapper: ItemValueMapper,
    private val dateParser: DateParser,
    private val pictureMapper: PictureMapper
) {

    fun mapToDbModelList(from: List<FriendDTO>): List<UserDbModel> =
        from.map { mapToDbModel(it) }

    fun mapToDbModel(from: FriendDTO): UserDbModel =
        UserDbModel(
            id = from.user.id.orZero(),
            firstName = from.user.firstName,
            lastName = from.user.lastName,
            patronymic = from.user.patronymic,
            avatar = from.user.avatar?.let { pictureMapper.mapToDbModel(it) },
            isFriend = from.isFriend,
            friendshipId = from.friendshipId,
            birthday = from.user.birthday,
            gender = from.user.gender,
            city = itemValueMapper.mapItemValueToDb(from.user.city),
            hobby = itemValueMapper.mapItemValueToDb(from.user.hobby),
            alcohol = itemValueMapper.mapItemValueToDb(from.user.alcohol),
            flower = itemValueMapper.mapItemValueToDb(from.user.flower),
            sweet = itemValueMapper.mapItemValueToDb(from.user.sweet),
            friendAmount = from.user.friendAmount,
            wishAmount = from.user.wishAmount
        )

    fun mapToEntityList(from: List<FriendDTO>): List<UserEntity> =
        from.map { mapToEntity(it) }

    fun mapToEntity(from: FriendDTO): UserEntity =
        UserEntity(
            id = from.user.id.orZero(),
            firstName = from.user.firstName,
            lastName = from.user.lastName,
            patronymic = from.user.patronymic,
            avatar = from.user.avatar?.let { pictureMapper.mapToEntity(it) },
            isFriend = from.isFriend,
            friendshipId = from.friendshipId,
            gender = Gender.getGender(from.user.gender),
            birthday = dateParser.stringToDate(from.user.birthday, DateParser.Pattern.DATE),
            city = itemValueMapper.mapItemValueToEntity(from.user.city),
            hobby = itemValueMapper.mapItemValueToEntity(from.user.hobby),
            alcohol = itemValueMapper.mapItemValueToEntity(from.user.alcohol),
            flower = itemValueMapper.mapItemValueToEntity(from.user.flower),
            sweet = itemValueMapper.mapItemValueToEntity(from.user.sweet),
            friendAmount = from.user.friendAmount.orZero(),
            wishAmount = from.user.wishAmount.orZero()
        )
}