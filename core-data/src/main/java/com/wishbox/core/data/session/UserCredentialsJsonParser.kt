package com.wishbox.core.data.session

import com.google.gson.Gson
import com.smedialink.common.delegates.sharedpreference.JsonParser
import com.smedialink.common.delegates.sharedpreference.asParser
import com.wishbox.core.domain.entity.UserCredentials
import javax.inject.Inject

class UserCredentialsJsonParser @Inject constructor(private val gson: Gson) : JsonParser<UserCredentials> by gson.asParser()