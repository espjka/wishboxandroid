package com.wishbox.core.data.util

import android.content.SharedPreferences
import com.f2prateek.rx.preferences2.Preference
import com.f2prateek.rx.preferences2.RxSharedPreferences
import com.google.gson.Gson
import com.smedialink.common.delegates.sharedpreference.JsonParser
import com.smedialink.common.delegates.sharedpreference.asParser
import com.smedialink.common.delegates.sharedpreference.delegate
import com.wishbox.core.data.mapper.ProfileMapper
import com.wishbox.core.di.scope.PerApplication
import com.wishbox.core.domain.entity.AvatarEntity
import com.wishbox.core.domain.entity.ProfileEntity
import com.wishbox.core.domain.exception.UserNotLoggedInException
import com.wishbox.core.network.WishBoxApi
import com.wishbox.core.network.model.LoginResponse
import io.reactivex.Completable
import io.reactivex.Observable
import javax.inject.Inject

/**
 * Created by Pak Evgeniy on 20/12/2018.
 */

@PerApplication
class ProfileManager @Inject constructor(
    sharedPreferences: SharedPreferences,
    jsonParser: ProfileJsonParser,
    private val api: WishBoxApi,
    private val mapper: ProfileMapper,
    private val profileRxPrefConverter: ProfileRxPrefConverter,
    private val rxPreferences: RxSharedPreferences
) {

    var currentProfile: ProfileEntity? by sharedPreferences.delegate(jsonParser)

    fun saveProfile(response: LoginResponse) {
        currentProfile = mapper.mapToEntity(response.user)
    }

    fun updateAvatar(avatar: AvatarEntity) {
        currentProfile = currentProfile?.copy(avatar = avatar)
    }

    fun requestProfile(): Completable =
        api.getMyProfile()
            .map { mapper.mapToEntity(it) }
            .doOnSuccess { currentProfile = it }
            .ignoreElement()

    fun observeProfile(): Observable<ProfileEntity> {
        return currentProfile?.let {
            rxPreferences.getObject(ProfileEntity::class.java.name, it, profileRxPrefConverter)
                .asObservable()
                .startWith(it)
        } ?: Observable.error(UserNotLoggedInException())
    }

    fun clear() {
        currentProfile = null
    }
}

class ProfileJsonParser @Inject constructor(private val gson: Gson) :
    JsonParser<ProfileEntity> by gson.asParser()

class ProfileRxPrefConverter @Inject constructor(private val gson: Gson) :
    Preference.Converter<ProfileEntity> by gson.asConverter()

inline fun <reified T : Any> Gson.asConverter(): Preference.Converter<T> {
    return object : Preference.Converter<T> {

        override fun deserialize(serialized: String): T =
            fromJson(serialized, T::class.java)

        override fun serialize(value: T): String =
            toJson(value)
    }
}