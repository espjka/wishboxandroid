package com.wishbox.core.data.repository

import com.wishbox.core.data.manager.UploadPictureManager
import com.wishbox.core.data.mapper.ItemValueMapper
import com.wishbox.core.data.mapper.PictureMapper
import com.wishbox.core.data.mapper.ProfileMapper
import com.wishbox.core.data.util.ProfileManager
import com.wishbox.core.domain.entity.*
import com.wishbox.core.domain.exception.ProfileNotExistException
import com.wishbox.core.domain.repository.ProfileRepository
import com.wishbox.core.network.WishBoxApi
import io.reactivex.BackpressureStrategy
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Single
import java.io.File
import javax.inject.Inject

/**
 * Created by Pak Evgeniy on 20/12/2018.
 */

class ProfileRepositoryImpl @Inject constructor(
    private val profileManager: ProfileManager,
    private val profileMapper: ProfileMapper,
    private val itemValueMapper: ItemValueMapper,
    private val api: WishBoxApi,
    private val uploadPictureManager: UploadPictureManager,
    private val pictureMapper: PictureMapper
) : ProfileRepository {

    override val profile: ProfileEntity? = profileManager.currentProfile

    override fun requestProfile(): Completable =
        profileManager.requestProfile()

    override fun observeProfile(): Flowable<ProfileEntity> =
        profileManager.observeProfile()
            .toFlowable(BackpressureStrategy.BUFFER)

    override fun checkExistence(): Completable {
        return Completable.fromAction {
            val profile = profileManager.currentProfile
            if (profile?.firstName == null
                || profile.lastName == null
                || profile.gender == Gender.NONE
                || profile.city == null
            ) {
                throw ProfileNotExistException()
            }
        }
    }

    override fun uploadAvatar(file: File): Single<AvatarEntity> =
        api.getUploadLinkForAvatar()
            .flatMap { uploadPictureManager.upload(it.url, file) }
            .map { pictureMapper.mapToEntity(it)  }
            .doOnSuccess { profileManager.updateAvatar(it) }

    override fun updateProfile(profile: UpdateProfileEntity): Single<ProfileEntity> {
        return Single.fromCallable { profileMapper.mapToDto(profile) }
            .flatMap { api.updateProfile(it) }
            .map(profileMapper::mapToEntity)
            .doOnSuccess { profileManager.currentProfile = it }
    }

    override fun requestProfileItemValues(
        type: ProfileItemValueType,
        queryString: String?,
        limit: Int,
        offset: Int
    ): Single<List<ItemValue>> =
        api.getProfileItemValues(type.toString(), queryString, limit, offset)
            .map { itemValueMapper.mapItemValuesToEntities(it.items.orEmpty()) }

}
