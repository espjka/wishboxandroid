package com.wishbox.core.data.mapper

import com.wishbox.core.domain.entity.WishRequestParams
import com.wishbox.core.network.model.WishRequestBody
import javax.inject.Inject

class WishRequestBodyMapper @Inject constructor() {

    fun map(params: WishRequestParams) =
        WishRequestBody(
            name = params.name,
            link = params.link,
            cost = params.cost,
            comment = params.comment,
            shopAddress = params.shopAddress
        )
}