package com.wishbox.core.data.repository

import com.wishbox.core.domain.repository.ResetPasswordRepository
import com.wishbox.core.network.WishBoxAuthApi
import com.wishbox.core.network.model.PasswordRecovery
import io.reactivex.Completable
import io.reactivex.Single
import javax.inject.Inject

class RecoveryPasswordRepositoryImpl @Inject constructor(
    private val authApi: WishBoxAuthApi
) : ResetPasswordRepository {

    override fun sendResetCode(userName: String): Completable {
        return authApi.sendRecoveryCode(
            PasswordRecovery.SendCodeBody(
                userName
            )
        )
    }

    override fun confirmCode(username: String, code: String): Single<String> {
        return authApi.confirmRecoveryCode(
            PasswordRecovery.ConfirmCodeBody(
                userName = username,
                code = code
            )
        ).map(PasswordRecovery.ConfirmationToken::token)
    }

    override fun resetPassword(password: String, token: String): Completable {
        return authApi.resetPassword(
            PasswordRecovery.ResetPasswordBody(
                password = password,
                token = token
            )
        )
    }

}