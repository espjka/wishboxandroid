package com.wishbox.core.data.session

import android.content.SharedPreferences
import com.smedialink.common.delegates.sharedpreference.delegate
import com.wishbox.core.di.scope.PerApplication
import com.wishbox.core.domain.entity.UserCredentials
import javax.inject.Inject

@PerApplication
class UserSession @Inject constructor(
    sharedPreferences: SharedPreferences,
    jsonParser: UserCredentialsJsonParser
) {
    var userCredentials: UserCredentials? by sharedPreferences.delegate(jsonParser)

    fun clear() {
        userCredentials = null
    }
}