package com.wishbox.core.data.di.module

import com.wishbox.core.data.repository.*
import com.wishbox.core.di.scope.PerApplication
import com.wishbox.core.domain.repository.*
import dagger.Binds
import dagger.Module

@Module
interface RepositoryModule {

    @Binds
    fun provideAuthRepository(repository: AuthRepositoryImpl): AuthRepository

    @Binds
    fun provideWishRepository(repository: WishRepositoryImpl): WishRepository

    @Binds
    fun provideMetaRepository(repository: MetaInfoRepositoryImpl): MetaInfoRepository

    @Binds
    fun provideUserRepository(repository: UserRepositoryImpl): UserRepository

    @Binds
    fun provideUserRegistrationRepository(impl: UserRegistrationRepoImpl): UserRegistrationRepository

    @Binds
    fun providePinRepository(impl: PinRepoImpl): PinRepository

    @PerApplication
    @Binds
    fun provideUserSessionRepository(impl: UserSessionRepoImpl): UserSessionRepository

    @Binds
    fun provideRecoveryPasswordRepository(impl: RecoveryPasswordRepositoryImpl): ResetPasswordRepository

    @Binds
    fun provideFriendshipRepository(impl: FriendshipRepositoryImpl): FriendshipRepository

    @Binds
    fun provideProfileRepository(impl: ProfileRepositoryImpl): ProfileRepository

    @Binds
    fun providePictureRepository(impl: WishPictureRepositoryImpl): PictureRepository

    @Binds
    fun provideChangePasswordRepository(impl: ChangePasswordRepositoryImpl): ChangePasswordRepository
}
