package com.wishbox.core.data.repository

import com.wishbox.core.domain.repository.UserRegistrationRepository
import com.wishbox.core.network.WishBoxAuthApi
import com.wishbox.core.network.model.LoginBody
import io.reactivex.Single
import javax.inject.Inject

class UserRegistrationRepoImpl @Inject constructor(
    private val authApi: WishBoxAuthApi
) : UserRegistrationRepository {

    override fun register(userName: String, password: String): Single<Long> {
        val body = LoginBody(
            username = userName,
            password = password
        )

        return authApi.signUp(body).map { it.userId }
    }

}