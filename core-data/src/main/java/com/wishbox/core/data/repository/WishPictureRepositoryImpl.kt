package com.wishbox.core.data.repository

import com.wishbox.core.data.manager.UploadPictureManager
import com.wishbox.core.data.mapper.PictureMapper
import com.wishbox.core.db.dao.PictureDao
import com.wishbox.core.domain.entity.PictureEntity
import com.wishbox.core.domain.repository.PictureRepository
import com.wishbox.core.network.WishBoxApi
import com.wishbox.core.network.model.PictureKey
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Single
import java.io.File
import javax.inject.Inject

/**
 * Created by Pak Evgeniy on 22.12.2018
 */

class WishPictureRepositoryImpl @Inject constructor(
    private val api: WishBoxApi,
    private val uploadPictureManager: UploadPictureManager,
    private val pictureDao: PictureDao,
    private val pictureMapper: PictureMapper
) : PictureRepository {

    override fun observePictures(wishId: Long): Flowable<List<PictureEntity>> =
        pictureDao.observePictures(wishId)
            .map { pictureMapper.mapList(it) }

    override fun initialUpload(
        wishId: Long,
        links: List<String>,
        localPaths: List<String>
    ): Single<String> {
        var key: String? = null

        val uploads = localPaths.mapIndexed { index, path ->
            val isMain = index == 0
            uploadPictureManager.upload(links[index], File(path))
                .map { pictureMapper.map(it, wishId, isMain) }
                .doOnSuccess { pictureDao.upsert(it) }
                .flatMapCompletable {
                    if (isMain) {
                        key = it.key
                        makePictureMain(wishId, it.key)
                    } else {
                        Completable.complete()
                    }
                }
        }

        return Completable.merge(uploads)
            .toSingle { key ?: "" }
    }

    override fun getMainPictureKey(wishId: Long): String? = pictureDao.getMainPicture(wishId)?.key

    override fun getFristPictureKey(wishId: Long): String? = pictureDao.getPictures(wishId).takeIf { it.isNotEmpty() }?.get(0)?.key

    override fun uploadPicture(wishId: Long, file: File): Single<String> =
        api.getUploadLinkForWish(wishId)
            .flatMap { uploadPictureManager.upload(it.url, file) }
            .map { pictureMapper.map(it, wishId, false) }
            .doOnSuccess { pictureDao.upsert(it) }
            .map { it.key }

    override fun deletePicture(wishId: Long, key: String): Completable =
        api.deletePicture(wishId, PictureKey(key))
            .doOnComplete { pictureDao.deleteByKey(key) }

    override fun makePictureMain(wishId: Long, key: String): Completable =
        api.setMainPicture(wishId, PictureKey(key))
            .doOnComplete { pictureDao.updateMainPicture(wishId, key) }
}