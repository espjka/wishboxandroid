package com.wishbox.feature.changePassword

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.smedialink.common.base.BaseViewModel
import com.smedialink.common.delegates.PropertyChangedDelegate
import com.smedialink.common.delegates.rx.disposableDelegate
import com.smedialink.common.ext.applySchedulers
import com.smedialink.common.ext.reduceState
import com.wishbox.feature.changePassword.domain.ChangePasswordUseCase
import com.wishbox.feature.changePassword.errorHandler.ChangePasswordErrorHandler
import io.reactivex.disposables.Disposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import ru.terrakok.cicerone.Router
import javax.inject.Inject

/**
 * Created by Pak Evgeniy on 24/12/2018.
 */

class ChangePasswordViewModel @Inject constructor(
    private val router: Router,
    private val changePasswordUseCase: ChangePasswordUseCase,
    private val errorHandler: ChangePasswordErrorHandler,
    private val messageHandler: MessageHandler
) : BaseViewModel() {

    private var disposable: Disposable? by disposableDelegate()

    private val internalState: MutableLiveData<ChangePasswordState> = MutableLiveData()

    val state: LiveData<ChangePasswordState> = internalState

    init {
        internalState.value = ChangePasswordState.initial()
    }

    var oldPassword: String? by PropertyChangedDelegate("") {
        internalState.reduceState {
            it.copy(errorState = it.errorState?.copy(oldPasswordTextError = null))
        }
    }

    var newPassword: String? by PropertyChangedDelegate("") {
        internalState.reduceState {
            it.copy(errorState = it.errorState?.copy(newPasswordTextError = null))
        }
    }

    var newRepeatPassword: String? by PropertyChangedDelegate("") {
        internalState.reduceState {
            it.copy(errorState = it.errorState?.copy(newRepeatPasswordTextError = null))
        }
    }

    fun navigateBack() {
        router.exit()
    }

    fun changePassword() {
        val params = ChangePasswordUseCase.Params(
            oldPassword = oldPassword ?: "",
            newPassword = newPassword ?: "",
            newRepeatPassword = newRepeatPassword ?: ""
        )

        disposable = changePasswordUseCase.invoke(params)
            .applySchedulers()
            .doOnSubscribe {
                internalState.value = ChangePasswordState.loading()
            }
            .subscribeBy(
                onComplete = {
                    val event = messageHandler.invoke(MessageHandler.MessageType.CHANGE_SUCCESS)
                    internalState.value = ChangePasswordState.success(message = event)
                },
                onError = {
                    internalState.reduceState { state ->
                        val error = errorHandler.handle(state.errorState, it)
                        ChangePasswordState.error(error)
                    }
                }
            ).addTo(disposables)
    }
}