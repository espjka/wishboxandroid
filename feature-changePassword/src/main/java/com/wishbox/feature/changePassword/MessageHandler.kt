package com.wishbox.feature.changePassword

import com.smedialink.common.events.MessageEvent
import com.wishbox.core.presentation.resources.ResourceProvider
import javax.inject.Inject

/**
 * Created by Pak Evgeniy on 10.01.2019
 */

class MessageHandler @Inject constructor(
    private val resourceProvider: ResourceProvider
) {

    operator fun invoke(type: MessageType): MessageEvent = MessageEvent(resourceProvider.getString(type.stringRes))

    enum class MessageType(val stringRes: Int) {
        CHANGE_SUCCESS(R.string.change_password_success)
    }
}