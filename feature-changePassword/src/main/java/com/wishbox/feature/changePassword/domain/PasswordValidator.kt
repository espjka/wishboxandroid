package com.wishbox.feature.changePassword.domain

import com.wishbox.feature.changePassword.errorHandler.PasswordType
import com.wishbox.feature.changePassword.errorHandler.ValidationException
import io.reactivex.Completable
import java.util.regex.Pattern
import javax.inject.Inject

class PasswordValidator @Inject constructor() {

    companion object {
        const val REGEXP_PASSWORD =
            "^(?=.*[a-zA-Z!@#\$%^&*_+\\/=\\-?.,])[a-zA-Za-zA-Z!@#\$%^&*_+\\/=\\-?.,\\d]{6,}\$"
    }

    fun validate(value: String, type: PasswordType): Completable =
        if (Pattern.matches(REGEXP_PASSWORD, value)) Completable.complete()
        else Completable.error(ValidationException.InvalidPasswordException(type))
}