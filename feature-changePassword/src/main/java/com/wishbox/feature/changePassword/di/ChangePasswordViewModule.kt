package com.wishbox.feature.changePassword.di

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.wishbox.core.di.mapkeys.ViewModelKey
import com.wishbox.feature.changePassword.ChangePasswordActivity
import com.wishbox.feature.changePassword.ChangePasswordViewModel
import com.wishbox.feature.changePassword.MessageHandler
import com.wishbox.feature.changePassword.R
import com.wishbox.feature.changePassword.domain.ChangePasswordUseCase
import com.wishbox.feature.changePassword.errorHandler.ChangePasswordErrorHandler
import dagger.Module
import dagger.Provides
import dagger.multibindings.IntoMap
import ru.terrakok.cicerone.Navigator
import ru.terrakok.cicerone.Router
import ru.terrakok.cicerone.android.support.SupportAppNavigator

/**
 * Created by Pak Evgeniy on 26/11/2018.
 */

@Module
abstract class ChangePasswordViewModule {

    @Module
    companion object {

        @Provides
        @JvmStatic
        fun provideNavigator(
            activity: ChangePasswordActivity
        ): Navigator =
            SupportAppNavigator(
                activity,
                R.id.rootLayout
            )

        @Provides
        @JvmStatic
        @IntoMap
        @ViewModelKey(ChangePasswordViewModel::class)
        fun provideViewModel(
            router: Router,
            messageHandler: MessageHandler,
            changePasswordUseCase: ChangePasswordUseCase,
            errorHandler: ChangePasswordErrorHandler
        ): ViewModel =
            ChangePasswordViewModel(
                router = router,
                messageHandler = messageHandler,
                changePasswordUseCase = changePasswordUseCase,
                errorHandler = errorHandler
            )

        @Provides
        @JvmStatic
        fun provideChangePasswordViewModel(
            activity: ChangePasswordActivity,
            factory: ViewModelProvider.Factory
        ): ChangePasswordViewModel =
            ViewModelProviders.of(activity, factory).get(ChangePasswordViewModel::class.java)
    }
}