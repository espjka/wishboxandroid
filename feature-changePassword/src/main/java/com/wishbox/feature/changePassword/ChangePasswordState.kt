package com.wishbox.feature.changePassword

import com.smedialink.common.error.ErrorState
import com.smedialink.common.events.ErrorEvent
import com.smedialink.common.events.MessageEvent

data class ChangePasswordState(
    val isLoading: Boolean = false,
    val errorState: ChangePasswordErrorState? = null,
    val messageEvent: MessageEvent? = null,
    val isSuccess: Boolean = false
) {
    companion object {
        fun initial() = ChangePasswordState()
        fun loading() = ChangePasswordState(isLoading = true)
        fun error(error: ChangePasswordErrorState) = ChangePasswordState(errorState = error)
        fun success(message: MessageEvent) = ChangePasswordState(messageEvent = message, isSuccess = true)
    }
}

data class ChangePasswordErrorState(
    override val event: ErrorEvent?,
    val oldPasswordTextError: String?,
    val newPasswordTextError: String?,
    val newRepeatPasswordTextError: String?
) : ErrorState(event)

