package com.wishbox.feature.changePassword.errorHandler

object ValidationException {
    class InvalidPasswordException(val type: PasswordType) : Exception()
    object OldPasswordsNotMatchException : Exception()
    object NewAndOldPasswordsMatchException : Exception()
}

enum class PasswordType {
    OLD,
    NEW,
    NEW_REPEAT
}