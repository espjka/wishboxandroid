package com.wishbox.feature.changePassword.domain

import com.wishbox.core.domain.repository.ChangePasswordRepository
import com.wishbox.feature.changePassword.errorHandler.PasswordType
import io.reactivex.Completable
import javax.inject.Inject

/**
 * Created by Pak Evgeniy on 26/11/2018.
 */

class ChangePasswordUseCase @Inject constructor(
    private val resetPasswordRepository: ChangePasswordRepository,
    private val passwordMatchValidator: PasswordMatchValidator,
    private val passwordNotMatchValidator: PasswordNotMatchValidator,
    private val passwordValidator: PasswordValidator
) {

    operator fun invoke(params: Params): Completable {
        return passwordValidator.validate(params.oldPassword, PasswordType.OLD)
            .andThen(passwordValidator.validate(params.newPassword, PasswordType.NEW))
            .andThen(passwordValidator.validate(params.newRepeatPassword, PasswordType.NEW_REPEAT))
            .andThen(passwordMatchValidator.validate(params.newPassword, params.newRepeatPassword))
            .andThen(passwordNotMatchValidator.validate(params.oldPassword, params.newPassword))
            .andThen(resetPasswordRepository.changePassword(params.oldPassword, params.newPassword))
    }

    data class Params(
        val oldPassword: String,
        val newPassword: String,
        val newRepeatPassword: String)
}