package com.wishbox.feature.changePassword.di

import com.wishbox.core.di.api.InjectorApi
import com.wishbox.core.di.api.RouterApi
import com.wishbox.core.di.module.GlobalRouterModule
import com.wishbox.core.di.module.ViewModelModule
import com.wishbox.core.di.provider.InjectorApiProvider
import com.wishbox.core.di.scope.PerActivity
import com.wishbox.feature.changePassword.ChangePasswordActivity
import dagger.BindsInstance
import dagger.Component

/**
 * Created by Pak Evgeniy on 26/11/2018.
 */

@PerActivity
@Component(
    dependencies = [
        InjectorApi::class
    ],
    modules = [
        GlobalRouterModule::class,
        ViewModelModule::class,
        ChangePasswordViewModule::class
    ]
)
interface ChangePasswordViewComponent : RouterApi {

    fun inject(activity: ChangePasswordActivity)

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun bind(activity: ChangePasswordActivity): Builder

        fun api(api: InjectorApi): Builder

        fun build(): ChangePasswordViewComponent
    }

    companion object {
        fun init(activity: ChangePasswordActivity): ChangePasswordViewComponent {
            val appApi = (activity.application as InjectorApiProvider).injectorApi

            return DaggerChangePasswordViewComponent.builder()
                .bind(activity)
                .api(appApi)
                .build()
        }
    }
}