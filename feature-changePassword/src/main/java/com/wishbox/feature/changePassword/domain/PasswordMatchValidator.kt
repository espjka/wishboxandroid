package com.wishbox.feature.changePassword.domain

import com.wishbox.feature.changePassword.errorHandler.ValidationException
import io.reactivex.Completable
import javax.inject.Inject

/**
 * Created by Pak Evgeniy on 17.12.2018
 */

class PasswordMatchValidator @Inject constructor() {
    fun validate(firstPass: String, secondPass: String): Completable =
        if (firstPass == secondPass) Completable.complete()
        else Completable.error(ValidationException.OldPasswordsNotMatchException)
}