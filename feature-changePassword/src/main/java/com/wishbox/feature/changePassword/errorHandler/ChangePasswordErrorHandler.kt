package com.wishbox.feature.changePassword.errorHandler

import com.smedialink.common.events.ErrorEvent
import com.wishbox.core.di.util.Logger
import com.wishbox.core.presentation.resources.ErrorTextProvider
import com.wishbox.core.presentation.resources.ResourceProvider
import com.wishbox.feature.changePassword.ChangePasswordErrorState
import com.wishbox.feature.changePassword.R
import javax.inject.Inject
import retrofit2.HttpException

class ChangePasswordErrorHandler @Inject constructor(
    private val logger: Logger,
    private val resourceProvider: ResourceProvider,
    private val errorTextProvider: ErrorTextProvider
) {

    private val invalidDataFormatString: String by lazy { resourceProvider.getString(R.string.invalid_data_format) }

    fun handle(
        prev: ChangePasswordErrorState?,
        throwable: Throwable
    ): ChangePasswordErrorState {
        logger.error(throwable)
        return when {
            throwable is HttpException && throwable.code() == 400 -> ChangePasswordErrorState(
                event = ErrorEvent(resourceProvider.getString(R.string.unable_change_password)),
                oldPasswordTextError = prev?.oldPasswordTextError,
                newPasswordTextError = prev?.newPasswordTextError,
                newRepeatPasswordTextError = prev?.newRepeatPasswordTextError
            )
            throwable is ValidationException.InvalidPasswordException -> ChangePasswordErrorState(
                event = prev?.event,
                oldPasswordTextError = if (throwable.type == PasswordType.OLD) invalidDataFormatString else prev?.oldPasswordTextError,
                newPasswordTextError = if (throwable.type == PasswordType.NEW) invalidDataFormatString else prev?.newPasswordTextError,
                newRepeatPasswordTextError = if (throwable.type == PasswordType.NEW_REPEAT) invalidDataFormatString else prev?.newRepeatPasswordTextError
            )
            throwable is ValidationException.OldPasswordsNotMatchException -> ChangePasswordErrorState(
                event = prev?.event,
                oldPasswordTextError = prev?.oldPasswordTextError,
                newPasswordTextError = prev?.newPasswordTextError,
                newRepeatPasswordTextError = resourceProvider.getString(R.string.passwords_not_match)
            )
            throwable is ValidationException.NewAndOldPasswordsMatchException -> ChangePasswordErrorState(
                event = prev?.event,
                oldPasswordTextError = prev?.oldPasswordTextError,
                newPasswordTextError = resourceProvider.getString(R.string.old_and_new_password_match),
                newRepeatPasswordTextError = prev?.newRepeatPasswordTextError
            )
            else -> ChangePasswordErrorState(
                event = errorTextProvider.provideErrorEvent(throwable),
                oldPasswordTextError = prev?.oldPasswordTextError,
                newPasswordTextError = prev?.newPasswordTextError,
                newRepeatPasswordTextError = prev?.newRepeatPasswordTextError
            )
        }
    }

}