package com.wishbox.feature.changePassword

import com.smedialink.common.events.handle
import com.smedialink.common.ext.observe
import com.smedialink.common.ext.onTextChanged
import com.smedialink.common.ext.trimmedString
import com.smedialink.common.util.StateHolder
import com.wishbox.core.di.api.RouterApi
import com.wishbox.core.di.provider.RouterApiProvider
import com.wishbox.core.presentation.base.BaseActivity
import com.wishbox.core.presentation.errorHandling.delegate.ToastDelegate
import com.wishbox.core.presentation.loading.ProgressLoadingDelegate
import com.wishbox.feature.changePassword.di.ChangePasswordViewComponent
import kotlinx.android.synthetic.main.activity_change_password.*
import javax.inject.Inject

/**
 * Created by Pak Evgeniy on 24/12/2018.
 */

class ChangePasswordActivity : BaseActivity(), RouterApiProvider {

    private val component by lazy { ChangePasswordViewComponent.init(this) }

    override val routerApi: RouterApi
        get() = component

    @Inject
    lateinit var toastDelegate: ToastDelegate

    @Inject
    lateinit var viewModel: ChangePasswordViewModel

    private var stateHolder = StateHolder<ChangePasswordState>()

    private val progressLoadingDelegate: ProgressLoadingDelegate by lazy {
        return@lazy ProgressLoadingDelegate(this, "Загрузка...")
    }

    override fun injectDependencies() {
        component.inject(this)
    }

    override fun layoutRes(): Int = R.layout.activity_change_password

    override fun viewCreated() {
        etOldPassword.onTextChanged { _, _, _, _ ->
            viewModel.oldPassword = etOldPassword.trimmedString()
        }
        etNewPassword.onTextChanged { _, _, _, _ ->
            viewModel.newPassword = etNewPassword.trimmedString()
        }
        etNewRepeatPassword.onTextChanged { _, _, _, _ ->
            viewModel.newRepeatPassword = etNewRepeatPassword.trimmedString()
        }
        toolbar.getLeftButton().setOnClickListener { viewModel.navigateBack() }
        buttonChangePassword.setOnClickListener { viewModel.changePassword() }
        viewModel.state.observe(this, this::render)
    }

    private fun render(state: ChangePasswordState) {
        stateHolder = stateHolder.newState(state)

        stateHolder.apply {

            state.messageEvent?.handle {
                toastDelegate.show(it.text, ToastDelegate.ToastGravity.TOP, ToastDelegate.ToastType.MESSAGE)
            }

            diff({ isSuccess }) {
                if (it) {
                    etOldPassword.setText("")
                    etNewPassword.setText("")
                    etNewRepeatPassword.setText("")
                }
            }

            diff({ isLoading }) { progressLoadingDelegate.showLoading(it) }

            state.errorState?.event?.handle {
                toastDelegate.show(it.errorText, ToastDelegate.ToastGravity.TOP)
            }

            diffNullable({ errorState?.oldPasswordTextError }) { etOldPassword.showError(it) }
            diffNullable({ errorState?.newPasswordTextError }) { etNewPassword.showError(it) }
            diffNullable({ errorState?.newRepeatPasswordTextError }) {
                etNewRepeatPassword.showError(it)
            }
        }
    }
}