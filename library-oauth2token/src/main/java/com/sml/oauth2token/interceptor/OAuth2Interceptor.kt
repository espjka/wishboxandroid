package com.sml.oauth2token.interceptor

import com.sml.oauth2token.CODE_OK
import com.sml.oauth2token.UNAUTHORIZED_ERROR_CODE
import com.sml.oauth2token.exception.RefreshTokenExpiredException
import com.sml.oauth2token.header.OAuth2HeaderProvider
import com.sml.oauth2token.header.OAuth2HeaderProviderImpl
import com.sml.oauth2token.session.OAuth2Session
import com.sml.oauth2token.tokenParser.OAuth2TokenParser
import okhttp3.Interceptor
import okhttp3.Response
import okhttp3.ResponseBody
import retrofit2.Call
import java.io.IOException


class OAuth2Interceptor(private val config: ConfigOAuth2Interceptor) : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response? {
        val response = requestWithToken(chain)
        if (response.code() != config.unAuthorizedCode) {
            return response
        }
        val previousToken = config.session.accessToken
        synchronized(this) {
            if (previousToken != config.session.accessToken) return requestWithToken(chain)
            return requestNewAccessToken()?.run {
                when (code()) {
                    UNAUTHORIZED_ERROR_CODE -> throw RefreshTokenExpiredException()
                    CODE_OK -> {
                        val tokens = config.tokenParser.parseTokens(body())
                        config.session.accessToken = tokens?.first
                        config.session.refreshToken = tokens?.second
                        requestWithToken(chain)
                    }
                    else -> response
                }
            } ?: response
        }
    }

    @Throws(IOException::class)
    private fun requestNewAccessToken(): retrofit2.Response<ResponseBody>? {
        config.session.refreshToken?.let {
            return config.newAccessTokenCall.invoke(it).execute()
        }
        return null
    }

    @Throws(IOException::class)
    private fun requestWithToken(chain: Interceptor.Chain): Response {
        val request = chain.request()
                .newBuilder()
                .apply { config.session.accessToken?.let { header(config.headerProvider.provideHeaderName(), config.headerProvider.provideHeaderValue(it)) } }
                .build()
        return chain.proceed(request)
    }
}

data class ConfigOAuth2Interceptor(
    val session: OAuth2Session,
    val newAccessTokenCall: (String) -> Call<ResponseBody>,
    val tokenParser: OAuth2TokenParser,
    val unAuthorizedCode: Int = UNAUTHORIZED_ERROR_CODE,
    val headerProvider: OAuth2HeaderProvider = OAuth2HeaderProviderImpl()
)