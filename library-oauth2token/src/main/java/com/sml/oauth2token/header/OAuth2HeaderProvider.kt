package com.sml.oauth2token.header

import com.sml.oauth2token.ACCESS_TOKEN_TYPE
import com.sml.oauth2token.AUTHORIZATION_HEADER

interface OAuth2HeaderProvider {

    fun provideHeaderName(): String

    fun provideHeaderValue(accessToken: String): String
}


class OAuth2HeaderProviderImpl : OAuth2HeaderProvider {

    override fun provideHeaderName() = AUTHORIZATION_HEADER

    override fun provideHeaderValue(accessToken: String) = "$ACCESS_TOKEN_TYPE $accessToken"
}