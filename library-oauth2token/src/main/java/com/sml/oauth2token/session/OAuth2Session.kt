package com.sml.oauth2token.session

import android.content.SharedPreferences
import kotlin.properties.ReadWriteProperty
import kotlin.reflect.KProperty

interface OAuth2Session {
    var accessToken: String?
    var refreshToken: String?

    fun clearToken() {
        accessToken = null
        refreshToken = null
    }
}

class OAuth2SessionImpl(sharedPref: SharedPreferences) : OAuth2Session {
    override var accessToken: String? by PrefDelegate(PrefKey.ACCESS_TOKEN, sharedPref)
    override var refreshToken: String? by PrefDelegate(PrefKey.REFRESH_TOKEN, sharedPref)
}

private class PrefDelegate<T>(private val key: PrefKey,
                              private val sharedPref: SharedPreferences) : ReadWriteProperty<T, String?> {

    override fun getValue(thisRef: T, property: KProperty<*>): String? =
            sharedPref.getString(key.key, null)

    override fun setValue(thisRef: T, property: KProperty<*>, value: String?) {
        sharedPref.edit()
                .apply {
                    if (value != null) putString(key.key, value)
                    else remove(key.key)
                }
                .apply()
    }
}

private enum class PrefKey(val key: String) {
    ACCESS_TOKEN("PREF_ACCESS_TOKEN"),
    REFRESH_TOKEN("PREF_REFRESH_TOKEN")
}