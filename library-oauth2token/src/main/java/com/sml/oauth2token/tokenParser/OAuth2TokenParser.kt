package com.sml.oauth2token.tokenParser

import okhttp3.ResponseBody

/**
 * Created by Pak Evgeniy on 03.10.2018.
 */

/**
 * Парсер access accessToken'a из [ResponseBody]
 */
interface OAuth2TokenParser {
    fun parseTokens(body: ResponseBody?): Pair<String?, String?>?
}