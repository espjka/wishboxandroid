package com.sml.oauth2token.exception

import java.lang.Exception


/**
 * Ошибка, означающая, что refresh токен не является валидным
 */
class RefreshTokenExpiredException(message: String? = null) : Exception(message)