package com.wishbox.feature.addFriends

import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.SimpleItemAnimator
import com.smedialink.common.delegates.bundle.bundleBoolean
import com.smedialink.common.events.handle
import com.smedialink.common.ext.gone
import com.smedialink.common.ext.observe
import com.smedialink.common.ext.visible
import com.smedialink.common.recyclerview.itemDecoration.SpaceItemDecoration
import com.wishbox.core.presentation.base.BaseFragment
import com.wishbox.core.presentation.errorHandling.delegate.ToastDelegate
import com.wishbox.core.presentation.loading.ProgressLoadingDelegate
import com.wishbox.core.presentation.navigation.BackButtonListener
import com.wishbox.core.presentation.pagination.Paginator
import com.wishbox.feature.addFriends.adapter.AddFriendsAdapter
import com.wishbox.feature.addFriends.di.AddFriendsViewComponent
import kotlinx.android.synthetic.main.fragment_add_friends.*
import javax.inject.Inject


/**
 * Created by Pak Evgeniy on 16.12.2018
 */

class AddFriendsFragment : BaseFragment<AddFriendsViewModel>(), BackButtonListener {

    @Inject
    lateinit var errorDelegate: ToastDelegate

    @Inject
    lateinit var adapter: AddFriendsAdapter

    @Inject
    lateinit var paginator: Paginator

    var isAuthFlow: Boolean? by bundleBoolean()
        private set

    companion object {
        fun newInstance(isAuthFlow: Boolean? = null): AddFriendsFragment =
            AddFriendsFragment()
                .apply { this.isAuthFlow = isAuthFlow }
    }

    private val progressLoadingDelegate: ProgressLoadingDelegate by lazy {
        return@lazy ProgressLoadingDelegate(context!!, "Запрос...")
    }

    override fun injectDependencies() {
        AddFriendsViewComponent.Initializer.inject(this)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        with(recyclerFriends) {
            adapter = this@AddFriendsFragment.adapter
            layoutManager = LinearLayoutManager(this@AddFriendsFragment.context)
            paginator.withRecyclerView(this, adapter?.itemCount == 0)
            val offset = resources.getDimension(R.dimen.margin_small)
            addItemDecoration(
                SpaceItemDecoration(
                    SpaceItemDecoration.Offset(offset.toInt(), 0),
                    SpaceItemDecoration.each()
                )
            )
            (itemAnimator as? SimpleItemAnimator)?.supportsChangeAnimations = false
        }

        layoutRefresh.setOnRefreshListener { viewModel.requestFriendList(true) }

        textRetry.setOnClickListener { viewModel.requestFriendList() }

        toolbar.getLeftButton().setOnClickListener { viewModel.navigateBack() }

        if (isAuthFlow == true) setupForAuthFlow()
        else toolbar.getRightButton().gone()

        viewModel.getState().observe(this, this::render)
    }

    private fun setupForAuthFlow() {
        toolbar.getLeftButton().gone()
        toolbar.getRightButton().visible()
        buttonNext.visible()
        toolbar.getRightButton().setOnClickListener { viewModel.openMainScreen() }
        buttonNext.setOnClickListener { viewModel.openMainScreen() }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        paginator.onSaveInstanceState(outState)
    }

    override fun onViewStateRestored(savedInstanceState: Bundle?) {
        super.onViewStateRestored(savedInstanceState)
        paginator.onViewStateRestored(savedInstanceState)
    }

    override fun onBackPressed(): Boolean {
        viewModel.navigateBack()
        return true
    }

    override fun layoutRes(): Int = R.layout.fragment_add_friends

    private fun render(state: AddFriendsState) {
        progressLoadingDelegate.showLoading(state.isDialogLoading)
        if (state.hasLoadPage) paginator.hasLoadPage()
        layoutRefresh.isRefreshing = state.isInitialLoading
        state.friendList.let { adapter.items = it }
        if (state.emptyListError) layoutError.visible() else layoutError.gone()
        state.event?.handle { errorDelegate.show(it.errorText) }
    }
}