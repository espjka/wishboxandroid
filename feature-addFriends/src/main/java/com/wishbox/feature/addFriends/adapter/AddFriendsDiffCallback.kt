package com.wishbox.feature.addFriends.adapter

import androidx.recyclerview.widget.DiffUtil
import javax.inject.Inject

/**
 * Created by Pak Evgeniy on 16.12.2018
 */

class AddFriendsDiffCallback @Inject constructor() :
    DiffUtil.ItemCallback<AddFriendsAdapterViewType>() {

    override fun areItemsTheSame(
        oldItem: AddFriendsAdapterViewType,
        newItem: AddFriendsAdapterViewType
    ): Boolean {
        if (oldItem.javaClass != newItem.javaClass) return false
        return when (oldItem) {
            is AddFriendsAdapterViewType.User -> oldItem.id == (newItem as AddFriendsAdapterViewType.User).id
            AddFriendsAdapterViewType.Loading -> true
            AddFriendsAdapterViewType.Error -> true
        }
    }

    override fun areContentsTheSame(
        oldItem: AddFriendsAdapterViewType,
        newItem: AddFriendsAdapterViewType
    ): Boolean = oldItem == newItem
}