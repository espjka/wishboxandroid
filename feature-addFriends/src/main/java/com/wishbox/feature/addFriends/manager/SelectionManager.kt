package com.wishbox.feature.addFriends.manager

import com.wishbox.feature.addFriends.adapter.AddFriendsAdapterViewType
import javax.inject.Inject

/**
 * Created by Pak Evgeniy on 17.12.2018
 */

class SelectionManager @Inject constructor() {

    private val selectionSet: MutableSet<Long> = mutableSetOf()

    fun handleSelection(id: Long, data: List<AddFriendsAdapterViewType>): List<AddFriendsAdapterViewType> {
        val outData = data.toMutableList()
        val index = data.indexOfFirst { it is AddFriendsAdapterViewType.User && it.id == id }
        if (index != -1) {
            val oldUser = data[index] as AddFriendsAdapterViewType.User
            val newUser = AddFriendsAdapterViewType.User(
                id = oldUser.id,
                name = oldUser.name,
                avatarUrl = oldUser.avatarUrl,
                checkedState = !oldUser.checkedState
            )
            with (outData) {
                removeAt(index)
                add(index, newUser)
            }
            when (newUser.checkedState) {
                AddFriendsAdapterViewType.User.CheckedState.CHECKED -> selectionSet.add(id)
                AddFriendsAdapterViewType.User.CheckedState.UNCHECKED -> selectionSet.remove(id)
            }
        }
        return outData
    }
}