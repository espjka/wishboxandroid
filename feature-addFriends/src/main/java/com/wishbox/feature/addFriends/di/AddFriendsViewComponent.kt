package com.wishbox.feature.addFriends.di

import com.wishbox.core.di.api.FeatureProviderApi
import com.wishbox.core.di.api.InjectorApi
import com.wishbox.core.di.module.GlobalRouterModule
import com.wishbox.core.di.module.ViewModelModule
import com.wishbox.core.di.provider.FeatureProviderApiProvider
import com.wishbox.core.di.provider.InjectorApiProvider
import com.wishbox.core.di.scope.PerFragment
import com.wishbox.core.di.util.lookupApiProvider
import com.wishbox.feature.addFriends.AddFriendsFragment
import dagger.BindsInstance
import dagger.Component

/**
 * Created by Pak Evgeniy on 16.12.2018
 */

@PerFragment
@Component(
    dependencies = [
        InjectorApi::class,
        FeatureProviderApi::class
    ],
    modules = [
        GlobalRouterModule::class,
        ViewModelModule::class,
        AddFriendsViewModule::class
    ]
)
interface AddFriendsViewComponent {

    fun inject(fragment: AddFriendsFragment)

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun bind(fragment: AddFriendsFragment): Builder

        fun api(api: InjectorApi): Builder

        fun api(api: FeatureProviderApi): Builder

        fun build(): AddFriendsViewComponent
    }

    class Initializer private constructor() {
        companion object {

            fun inject(fragment: AddFriendsFragment) {
                val injectorProvider = fragment.lookupApiProvider<InjectorApiProvider>()
                val featureProviderProvider = fragment.lookupApiProvider<FeatureProviderApiProvider>()

                DaggerAddFriendsViewComponent.builder()
                    .api(injectorProvider.injectorApi)
                    .api(featureProviderProvider.featureProviderApi)
                    .bind(fragment)
                    .build()
                    .inject(fragment)
            }
        }
    }
}