package com.wishbox.feature.addFriends.domain

import com.wishbox.core.domain.entity.UserEntity
import com.wishbox.core.domain.repository.UserRepository
import com.wishbox.core.presentation.pagination.Paginator
import io.reactivex.Single
import javax.inject.Inject

/**
 * Created by Pak Evgeniy on 16.12.2018
 */

class GetNotFriendsUseCase @Inject constructor(
    private val repository: UserRepository
) {

    var offset = 0

    operator fun invoke(dropOffset: Boolean = false): Single<List<UserEntity>> {
        var prevOffset: Int? = null
        if (dropOffset) {
            prevOffset = offset
            offset = 0
        }
        return repository.getMyNotFriendList(Paginator.LIMIT, offset)
            .doOnSuccess { offset += Paginator.LIMIT }
            .doOnError { prevOffset?.let { offset = prevOffset } }
    }
}