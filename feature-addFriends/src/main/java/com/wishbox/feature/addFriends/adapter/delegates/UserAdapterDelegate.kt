package com.wishbox.feature.addFriends.adapter.delegates

import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.hannesdorfmann.adapterdelegates4.AbsListItemAdapterDelegate
import com.smedialink.common.ext.inflate
import com.wishbox.core.glide.loadImage
import com.wishbox.feature.addFriends.R
import com.wishbox.feature.addFriends.adapter.AddFriendsAdapterViewType
import kotlinx.android.synthetic.main.item_user.view.*

/**
 * Created by Pak Evgeniy on 16.12.2018
 */

class UserAdapterDelegate(
    private val userClick: (AddFriendsAdapterViewType.User) -> Unit,
    private val requestFriendshipClick: (AddFriendsAdapterViewType.User) -> Unit,
    private val cancelFriendshipRequestClick: (AddFriendsAdapterViewType.User) -> Unit
) :
    AbsListItemAdapterDelegate<AddFriendsAdapterViewType.User, AddFriendsAdapterViewType, UserAdapterDelegate.ViewHolder>(){

    override fun onCreateViewHolder(parent: ViewGroup): ViewHolder =
        ViewHolder(parent.inflate(R.layout.item_user), userClick, requestFriendshipClick, cancelFriendshipRequestClick)

    override fun isForViewType(
        item: AddFriendsAdapterViewType,
        items: MutableList<AddFriendsAdapterViewType>,
        position: Int
    ): Boolean = item is AddFriendsAdapterViewType.User

    override fun onBindViewHolder(
        item: AddFriendsAdapterViewType.User,
        holder: ViewHolder,
        payloads: MutableList<Any>
    ) {
        holder.bind(item)
    }

    class ViewHolder(
        view: View,
        private val userClick: (AddFriendsAdapterViewType.User) -> Unit,
        private val requestFriendshipClick: (AddFriendsAdapterViewType.User) -> Unit,
        private val cancelFriendshipRequestClick: (AddFriendsAdapterViewType.User) -> Unit
    ) : RecyclerView.ViewHolder(view) {

        fun bind(item: AddFriendsAdapterViewType.User) =
            with(itemView) {
                buttonAdd.setBackgroundResource(item.checkedState.bgResId)
                buttonAdd.setImageResource(item.checkedState.iconResId)
                imageCheck.visibility = item.checkedState.selectIconVisibility
                imageAvatar.loadImage(item.avatarUrl, placeholder = R.drawable.placeholder_avatar)
                imageAvatar.setOnClickListener { userClick.invoke(item) }
                itemView.setOnClickListener { userClick.invoke(item) }
                buttonAdd.setOnClickListener { handleClick(item) }
                textName.text = item.name
            }

        private fun handleClick(item: AddFriendsAdapterViewType.User) {
            when (item.checkedState) {
                AddFriendsAdapterViewType.User.CheckedState.CHECKED -> cancelFriendshipRequestClick.invoke(item)
                AddFriendsAdapterViewType.User.CheckedState.UNCHECKED -> requestFriendshipClick.invoke(item)
            }
        }
    }
}