package com.wishbox.feature.addFriends.di

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.smedialink.common.error.ErrorHandler
import com.wishbox.core.di.mapkeys.ViewModelKey
import com.wishbox.core.di.screenproviders.FeatureProvider
import com.wishbox.core.presentation.errorHandling.MainErrorHandler
import com.wishbox.core.presentation.pagination.Paginator
import com.wishbox.feature.addFriends.AddFriendsFragment
import com.wishbox.feature.addFriends.AddFriendsViewModel
import com.wishbox.feature.addFriends.adapter.AddFriendsAdapter
import com.wishbox.feature.addFriends.adapter.AddFriendsDiffCallback
import com.wishbox.feature.addFriends.domain.AddFriendsUseCaseFacade
import com.wishbox.feature.addFriends.manager.SelectionManager
import com.wishbox.feature.addFriends.mapper.UserViewMapper
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.multibindings.IntoMap
import ru.terrakok.cicerone.Router

/**
 * Created by Pak Evgeniy on 16.12.2018
 */

@Module
abstract class AddFriendsViewModule {

    @Binds
    abstract fun provideErrorHandler(impl: MainErrorHandler): ErrorHandler

    @Module
    companion object {
        @Provides
        @IntoMap
        @ViewModelKey(AddFriendsViewModel::class)
        @JvmStatic
        fun provideViewModel(
            router: Router,
            mapper: UserViewMapper,
            errorHandler: ErrorHandler,
            useCaseFacade: AddFriendsUseCaseFacade,
            selectionManager: SelectionManager,
            featureProvider: FeatureProvider
        ): ViewModel =
            AddFriendsViewModel(
                router = router,
                useCaseFacade = useCaseFacade,
                errorHandler = errorHandler,
                mapper = mapper,
                selectionManager = selectionManager,
                featureProvider = featureProvider
            )

        @JvmStatic
        @Provides
        fun provideFriendListViewModel(
            fragment: AddFriendsFragment,
            factory: ViewModelProvider.Factory
        ): AddFriendsViewModel =
            ViewModelProviders.of(fragment, factory).get(AddFriendsViewModel::class.java)

        @Provides
        @JvmStatic
        fun provideAdapter(
            diffCallback: AddFriendsDiffCallback,
            viewModel: AddFriendsViewModel
        ): AddFriendsAdapter =
            AddFriendsAdapter(
                diffCallback = diffCallback,
                userClick = { viewModel.openProfile(it.id) },
                requestFriendshipClick = { viewModel.requestFriendship(it.id) },
                cancelOrDeleteClick = { viewModel.cancelOrDeleteFriendship(it.id) },
                errorClick = { viewModel.requestFriendList() }
            )

        @Provides
        @JvmStatic
        fun providePaginationScrollDelegate(viewModel: AddFriendsViewModel): Paginator =
            Paginator { viewModel.requestFriendList() }
    }
}