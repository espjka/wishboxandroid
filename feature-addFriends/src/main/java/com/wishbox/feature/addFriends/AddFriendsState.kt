package com.wishbox.feature.addFriends

import com.smedialink.common.error.ErrorState
import com.smedialink.common.events.ErrorEvent
import com.wishbox.feature.addFriends.adapter.AddFriendsAdapterViewType

/**
 * Created by Pak Evgeniy on 16.12.2018
 */

data class AddFriendsState(
    val friendList: List<AddFriendsAdapterViewType> = emptyList(),
    val isEmpty: Boolean = false,
    val isInitialLoading: Boolean = false,
    val isDialogLoading: Boolean = false,
    val hasLoadPage: Boolean = false,
    val event: ErrorEvent? = null,
    val emptyListError: Boolean = false
)

sealed class AddFriendsStateChanges {
    class Loading(val isForceLoading: Boolean = false, val isPermanentLoading: Boolean = false) :
        AddFriendsStateChanges()

    class Data(
        val friendList: List<AddFriendsAdapterViewType>,
        val hasLoadPage: Boolean? = null,
        val isForceLoading: Boolean = false
    ) : AddFriendsStateChanges()

    class Error(val error: ErrorState, val isPagination: Boolean = true) :
        AddFriendsStateChanges()

    class Update(val friendList: List<AddFriendsAdapterViewType>) : AddFriendsStateChanges()
}

fun AddFriendsStateChanges.Update.reduceState(prevState: AddFriendsState): AddFriendsState =
    prevState.copy(friendList = friendList, isDialogLoading = false)

fun AddFriendsStateChanges.Data.reduceState(prevState: AddFriendsState): AddFriendsState =
    AddFriendsState(
        friendList = if (!isForceLoading) prevState.friendList.toMutableList()
            .apply {
                remove(AddFriendsAdapterViewType.Loading)
                addAll(friendList)
            } else friendList,
        isEmpty = friendList.isEmpty() && prevState.emptyListError.not(),
        emptyListError = friendList.isEmpty() && prevState.emptyListError,
        hasLoadPage = hasLoadPage ?: prevState.hasLoadPage
    )

fun AddFriendsStateChanges.Error.reduceState(prevState: AddFriendsState): AddFriendsState =
    AddFriendsState(
        friendList = prevState.friendList.toMutableList()
            .apply {
                remove(AddFriendsAdapterViewType.Loading)
                if (isPagination && isNotEmpty() && !contains(AddFriendsAdapterViewType.Error)) {
                    add(AddFriendsAdapterViewType.Error)
                }
            },
        event = error.event,
        emptyListError = prevState.friendList.isNullOrEmpty()
    )

fun AddFriendsStateChanges.Loading.reduceState(prevState: AddFriendsState): AddFriendsState =
    when {
        isPermanentLoading ->
            prevState.copy(isDialogLoading = true,
                hasLoadPage = false,
                friendList = prevState.friendList.toMutableList()
                    .apply {
                        remove(AddFriendsAdapterViewType.Loading)
                    })
        isForceLoading || prevState.friendList.isNullOrEmpty() ->
            prevState.copy(isInitialLoading = true,
                hasLoadPage = false,
                friendList = prevState.friendList.toMutableList()
                    .apply {
                        remove(AddFriendsAdapterViewType.Loading)
                    })
        else ->
            prevState.copy(friendList = prevState.friendList.toMutableList()
                .apply {
                    remove(AddFriendsAdapterViewType.Error)
                    if (!contains(AddFriendsAdapterViewType.Loading)) {
                        add(AddFriendsAdapterViewType.Loading)
                    }
                })
    }