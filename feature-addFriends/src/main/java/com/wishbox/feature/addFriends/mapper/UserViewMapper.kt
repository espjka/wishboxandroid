package com.wishbox.feature.addFriends.mapper

import android.content.Context
import com.wishbox.core.domain.entity.UserEntity
import com.wishbox.feature.addFriends.R
import com.wishbox.feature.addFriends.adapter.AddFriendsAdapterViewType
import javax.inject.Inject

/**
 * Created by Pak Evgeniy on 16.12.2018
 */

class UserViewMapper @Inject constructor(
    private val context: Context
) {

    fun mapToViewTypeList(from: List<UserEntity>): List<AddFriendsAdapterViewType> =
        from.map { mapToViewType(it) }

    private fun mapToViewType(from: UserEntity): AddFriendsAdapterViewType =
        AddFriendsAdapterViewType.User(
            id = from.id,
            name = manageName(from.firstName, from.lastName),
            avatarUrl = from.avatar?.small
        )

    private fun manageName(firstName: String?, lastName: String?): String =
        "${firstName.orEmpty()} ${lastName.orEmpty()}"
            .takeIf { it.isNotBlank() } ?: context.getString(R.string.not_specified)
}