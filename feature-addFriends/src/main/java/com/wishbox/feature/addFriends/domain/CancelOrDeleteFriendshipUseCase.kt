package com.wishbox.feature.addFriends.domain

import com.wishbox.core.domain.repository.FriendshipRepository
import com.wishbox.core.domain.repository.UserRepository
import io.reactivex.Completable
import javax.inject.Inject

/**
 * Created by Pak Evgeniy on 20/12/2018.
 */

class CancelOrDeleteFriendshipUseCase @Inject constructor(
    private val friendshipRepository: FriendshipRepository,
    private val userRepository: UserRepository
) {

    operator fun invoke(id: Long): Completable =
            userRepository.getUser(id)
                .flatMapCompletable { friendshipRepository.breakFriendship(id) }
}