package com.wishbox.feature.addFriends.adapter

import com.hannesdorfmann.adapterdelegates4.AsyncListDifferDelegationAdapter
import com.wishbox.core.presentation.pagination.PaginationErrorAdapterDelegate
import com.wishbox.core.presentation.pagination.PaginationProgressAdapterDelegate
import com.wishbox.feature.addFriends.adapter.delegates.UserAdapterDelegate
import javax.inject.Inject


/**
 * Created by Pak Evgeniy on 16.12.2018
 */

class AddFriendsAdapter @Inject constructor(
    diffCallback: AddFriendsDiffCallback,
    userClick: (user: AddFriendsAdapterViewType.User) -> Unit,
    requestFriendshipClick: (user: AddFriendsAdapterViewType.User) -> Unit,
    cancelOrDeleteClick: (user: AddFriendsAdapterViewType.User) -> Unit,
    errorClick: () -> Unit
) : AsyncListDifferDelegationAdapter<AddFriendsAdapterViewType>(diffCallback) {

    init {
        delegatesManager
            .addDelegate(UserAdapterDelegate(userClick, requestFriendshipClick, cancelOrDeleteClick))
            .addDelegate(PaginationProgressAdapterDelegate<AddFriendsAdapterViewType.Loading, AddFriendsAdapterViewType> { it is AddFriendsAdapterViewType.Loading })
            .addDelegate(
                PaginationErrorAdapterDelegate<AddFriendsAdapterViewType.Error, AddFriendsAdapterViewType>(
                    isForViewType = { it is AddFriendsAdapterViewType.Error },
                    errorClick = errorClick
                )
            )
    }
}