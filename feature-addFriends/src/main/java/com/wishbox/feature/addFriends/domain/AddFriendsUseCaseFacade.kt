package com.wishbox.feature.addFriends.domain

import com.wishbox.core.domain.entity.UserEntity
import io.reactivex.Completable
import io.reactivex.Single
import javax.inject.Inject

/**
 * Created by Pak Evgeniy on 09.01.2019
 */

class AddFriendsUseCaseFacade @Inject constructor(
    private val getNotFriends: GetNotFriendsUseCase,
    private val requestFriendship: RequestFriendshipUseCase,
    private val cancelOrDeleteFriendshipUseCase: CancelOrDeleteFriendshipUseCase
) {

    fun getNotFriends(dropOffset: Boolean = false): Single<List<UserEntity>> {
        return getNotFriends.invoke(dropOffset)
    }

    fun requestFriendship(id: Long): Completable = requestFriendship.invoke(id)

    fun cancelOrDeleteFriendship(id: Long): Completable = cancelOrDeleteFriendshipUseCase.invoke(id)
}