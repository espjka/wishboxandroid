package com.wishbox.feature.addFriends.adapter

import android.os.Parcelable
import android.view.View
import com.wishbox.feature.addFriends.R
import kotlinx.android.parcel.Parcelize

/**
 * Created by Pak Evgeniy on 16.12.2018
 */

sealed class AddFriendsAdapterViewType {

    @Parcelize
    data class User(
        val id: Long,
        val name: String,
        val avatarUrl: String? = null,
        var checkedState: CheckedState = CheckedState.UNCHECKED
    ) : AddFriendsAdapterViewType(), Parcelable {

        enum class CheckedState(
            val iconResId: Int,
            val bgResId: Int,
            val selectIconVisibility: Int
        ) {
            CHECKED(R.drawable.ic_cross, R.drawable.bg_button_small_red, View.VISIBLE),
            UNCHECKED(R.drawable.ic_plus, R.drawable.bg_button_circle_active_small, View.GONE);

            operator fun not(): CheckedState =
                when (this) {
                    CHECKED -> UNCHECKED
                    UNCHECKED -> CHECKED
                }
        }
    }

    object Loading : AddFriendsAdapterViewType()

    object Error : AddFriendsAdapterViewType()
}