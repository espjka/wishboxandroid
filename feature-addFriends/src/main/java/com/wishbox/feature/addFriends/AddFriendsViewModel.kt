package com.wishbox.feature.addFriends

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.smedialink.common.base.BaseViewModel
import com.smedialink.common.delegates.rx.disposableDelegate
import com.smedialink.common.error.ErrorHandler
import com.smedialink.common.ext.applySchedulers
import com.wishbox.core.di.screenproviders.FeatureProvider
import com.wishbox.core.presentation.pagination.Paginator.Companion.LIMIT
import com.wishbox.feature.addFriends.adapter.AddFriendsAdapterViewType
import com.wishbox.feature.addFriends.domain.AddFriendsUseCaseFacade
import com.wishbox.feature.addFriends.manager.SelectionManager
import com.wishbox.feature.addFriends.mapper.UserViewMapper
import io.reactivex.disposables.Disposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import ru.terrakok.cicerone.Router
import javax.inject.Inject

/**
 * Created by Pak Evgeniy on 16.12.2018
 */

class AddFriendsViewModel @Inject constructor(
    private val router: Router,
    private val mapper: UserViewMapper,
    private val errorHandler: ErrorHandler,
    private val useCaseFacade: AddFriendsUseCaseFacade,
    private val selectionManager: SelectionManager,
    private val featureProvider: FeatureProvider
) : BaseViewModel() {

    private val state: MutableLiveData<AddFriendsState> by lazy { MutableLiveData<AddFriendsState>() }

    private var getNotFriendListDisposable: Disposable? by disposableDelegate()
    private var requestFriendshipDisposable: Disposable? by disposableDelegate()
    private var cancelOrDeleteDisposable: Disposable? by disposableDelegate()

    fun getState(): LiveData<AddFriendsState> = state

    fun requestFriendList(needToDrop: Boolean = false) {
        getNotFriendListDisposable = useCaseFacade.getNotFriends(needToDrop)
            .map { mapper.mapToViewTypeList(it) }
            .applySchedulers()
            .doOnSubscribe { reduceState(AddFriendsStateChanges.Loading(needToDrop)) }
            .subscribeBy(
                onError = {
                    reduceState(
                        AddFriendsStateChanges.Error(
                            errorHandler.handle(it),
                            !needToDrop
                        )
                    )
                },
                onSuccess = {
                    reduceState(
                        AddFriendsStateChanges.Data(
                            it,
                            it.size == LIMIT,
                            needToDrop
                        )
                    )
                }
            ).addTo(disposables)
    }

    fun openProfile(id: Long) {
        router.navigateTo(featureProvider.getProfile(id))
    }

    fun requestFriendship(id: Long) {
        requestFriendshipDisposable = useCaseFacade.requestFriendship(id)
            .applySchedulers()
            .doOnSubscribe { reduceState(AddFriendsStateChanges.Loading(isPermanentLoading = true)) }
            .subscribeBy(
                onError = { reduceState(AddFriendsStateChanges.Error(errorHandler.handle(it), false)) },
                onComplete = { updateStatus(id) }
            ).addTo(disposables)
    }

    fun cancelOrDeleteFriendship(id: Long) {
        cancelOrDeleteDisposable = useCaseFacade.cancelOrDeleteFriendship(id)
            .applySchedulers()
            .doOnSubscribe { reduceState(AddFriendsStateChanges.Loading(isPermanentLoading = true)) }
            .subscribeBy(
                onError = { reduceState(AddFriendsStateChanges.Error(errorHandler.handle(it), false)) },
                onComplete = { updateStatus(id) }
            ).addTo(disposables)
    }

    private fun updateStatus(id: Long) {
        //state.value?.friendList?.let { reduceState(AddFriendsStateChanges.Update(selectionManager.handleSelection(id, it))) }
        state.value?.friendList?.let {
            it.toMutableList()
                .apply {
                    val index = indexOfFirst { (it as? AddFriendsAdapterViewType.User)?.id == id }
                    if (index != -1) {
                        val nextList = it.toMutableList()
                            .apply { removeAt(index) }
                        reduceState(AddFriendsStateChanges.Update(nextList))
                    }
                }
        }
    }

    private fun reduceState(changes: AddFriendsStateChanges) {
        val prevState = state.value ?: AddFriendsState()
        state.value = when (changes) {
            is AddFriendsStateChanges.Data -> changes.reduceState(prevState)
            is AddFriendsStateChanges.Error -> changes.reduceState(prevState)
            is AddFriendsStateChanges.Loading -> changes.reduceState(prevState)
            is AddFriendsStateChanges.Update -> changes.reduceState(prevState)
        }
    }

    fun navigateBack() {
        router.exit()
    }

    fun openMainScreen() {
        router.newRootScreen(featureProvider.getMain())
    }
}