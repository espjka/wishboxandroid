/*
 * Created by Konstantin Tskhovrebov (aka @terrakok)
 */

package ru.terrakok.cicerone;

import ru.terrakok.cicerone.commands.Command;

/**
 * BaseRouter is an abstract class to implement high-level navigation.
 * Extend it to add needed transition methods.
 */
public abstract class BaseRouter {
    private CommandBuffer commandBuffer;

    BaseRouter() {
        this.commandBuffer = new CommandBuffer();
    }

    CommandBuffer getCommandBuffer() {
        return commandBuffer;
    }

    public void setNavigatorHolder(NavigatorHolder navigatorHolder) {
        if (navigatorHolder instanceof CommandBuffer) {
            commandBuffer = (CommandBuffer) navigatorHolder;
        }
    }

    /**
     * Sends navigation command array to {@link CommandBuffer}.
     *
     * @param commands navigation command array to execute
     */
    public void executeCommands(Command... commands) {
        commandBuffer.executeCommands(commands);
    }
}
