package com.wishbox.core.db.converter

import androidx.room.TypeConverter
import com.wishbox.core.db.model.ItemValueDbModel

/**
 * Created by Pak Evgeniy on 20/12/2018.
 */

class ItemValueConverter {
    @TypeConverter fun fromValue(value: ItemValueDbModel?): String? = fromJson(value)
    @TypeConverter fun toValue(valueString: String?): ItemValueDbModel? = toJson(valueString)
}