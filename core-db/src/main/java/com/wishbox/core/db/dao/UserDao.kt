package com.wishbox.core.db.dao

import androidx.room.Dao
import androidx.room.Query
import androidx.room.Transaction
import com.wishbox.core.db.model.UserDbModel
import io.reactivex.Flowable
import io.reactivex.Single

/**
 * Created by Pak Evgeniy on 29/11/2018.
 */

@Dao
abstract class UserDao : CrudDao<UserDbModel>(tableName = UserDbModel.TABLE_NAME) {

    @Query("SELECT * FROM user WHERE id = :id")
    abstract fun getUserModel(id: Long): UserDbModel?

    @Query("SELECT * FROM user WHERE id = :id")
    abstract fun getUser(id: Long): Single<UserDbModel>

    @Query("SELECT * FROM user WHERE id = :id")
    abstract fun observeUser(id: Long): Flowable<UserDbModel>

    @Transaction
    open fun clearAndInsert(list: List<UserDbModel>) {
        deleteAll()
        insertOrReplace(list)
    }

    @Transaction
    open fun updateListShortIfExists(list: List<UserDbModel>) {
        list.forEach { new ->
            getUserModel(new.id)
                ?.run {
                    new.friendAmount = friendAmount
                    new.wishAmount = wishAmount
                    new.friendshipId = friendshipId
                    new.isFriend = isFriend
                    new.gender = gender
                    new.birthday = birthday
                    new.city = city
                    new.hobby = hobby
                    new.alcohol = alcohol
                    new.flower = flower
                    new.sweet = sweet
                    upsert(new)
                }
        }
    }

    @Transaction
    open fun updateDetailIfExists(new: UserDbModel) {
        getUserModel(new.id)
            ?.run {
                new.isFriend = isFriend
                new.friendshipId = friendshipId
                upsert(new)
            }
    }

    @Query("SELECT * FROM user WHERE isFriend = 1")
    abstract fun observeMyFriendList(): Flowable<List<UserDbModel>>

    @Query("DELETE FROM user WHERE id = :id")
    abstract fun deleteById(id: Long)

    @Transaction
    open fun updateFriendship(id: Long, friendshipId: Long? = null, isFriend: Boolean? = null) {
        getUserModel(id)?.let {
            it.friendshipId = friendshipId
            it.isFriend = isFriend
            update(it)
        }
    }
}