package com.wishbox.core.db.model

import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.wishbox.core.db.model.UserDbModel.Companion.TABLE_NAME

/**
 * Created by Pak Evgeniy on 08.12.2018
 */

@Entity(tableName = TABLE_NAME)
data class UserDbModel(
    @PrimaryKey var id: Long,
    var firstName: String? = null,
    var lastName: String? = null,
    var patronymic: String? = null,
    var friendshipId: Long? = null,
    var isFriend: Boolean? = null,
    var gender: String? = null,
    var birthday: String? = null,
    var city: ItemValueDbModel? = null,
    var hobby: ItemValueDbModel? = null,
    var alcohol: ItemValueDbModel? = null,
    var flower: ItemValueDbModel? = null,
    var sweet: ItemValueDbModel? = null,
    var friendAmount: Int? = null,
    var wishAmount: Int? = null,
    @Embedded var avatar: AvatarDbModel? = null
) {
    companion object {
        const val TABLE_NAME = "user"
    }
}

data class AvatarDbModel(
    var small: String = "",
    var medium: String = "",
    var big: String = ""
)

data class ItemValueDbModel(
    val id: Long,
    val name: String
)