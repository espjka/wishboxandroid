package com.wishbox.core.db.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.wishbox.core.db.model.WishDbModel.Companion.TABLE_NAME

/**
 * Created by Pak Evgeniy on 29/11/2018.
 */

@Entity(tableName = TABLE_NAME)
data class WishDbModel(
    @PrimaryKey var id: Long = 0,
    var userId: Long = 0,
    var link: String? = null,
    var name: String? = null,
    var comment: String? = null,
    var address: String? = null,
    var cost: String? = null,
    var mainPictureKey: String? = null
) {
    companion object {
        const val TABLE_NAME = "wish"
    }
}

data class WishWithMainPicture(
    var wish: WishDbModel = WishDbModel(),
    var mainPicture: PictureDbModel? = null
)

data class WishWithPictures(
    var wish: WishDbModel = WishDbModel(),
    var pictures: List<PictureDbModel> = emptyList()
)