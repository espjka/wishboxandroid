package com.wishbox.core.db

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.wishbox.core.db.converter.ItemValueConverter
import com.wishbox.core.db.dao.PictureDao
import com.wishbox.core.db.dao.UserDao
import com.wishbox.core.db.dao.WishDao
import com.wishbox.core.db.model.PictureDbModel
import com.wishbox.core.db.model.UserDbModel
import com.wishbox.core.db.model.WishDbModel

/**
 * Created by Pak Evgeniy on 08.12.2018
 */

@Database(
    entities = [
        WishDbModel::class,
        UserDbModel::class,
        PictureDbModel::class
    ],
    version = 1,
    exportSchema = false
)
@TypeConverters(
    value = [
        ItemValueConverter::class
    ]
)
abstract class WishBoxDatabase : RoomDatabase() {

    companion object {
        const val DB_NAME = "wish_box_db"
    }

    abstract fun getWishDao(): WishDao
    abstract fun getUserDao(): UserDao
    abstract fun getPictureDao(): PictureDao

    fun clear() {
        getWishDao().deleteAll()
        getUserDao().deleteAll()
        getPictureDao().deleteAll()
    }
}