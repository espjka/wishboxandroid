package com.wishbox.core.db.dao

import androidx.room.Dao
import androidx.room.Query
import androidx.room.Transaction
import com.wishbox.core.db.model.WishDbModel
import io.reactivex.Flowable
import io.reactivex.Single

/**
 * Created by Pak Evgeniy on 29/11/2018.
 */

@Dao
abstract class WishDao : CrudDao<WishDbModel>(tableName = WishDbModel.TABLE_NAME) {

    @Query("SELECT * FROM wish WHERE id = :id")
    abstract fun getWish(id: Long): Single<WishDbModel>

    @Query("SELECT * FROM wish WHERE id = :id")
    abstract fun getWishModel(id: Long): WishDbModel?

    @Query("DELETE FROM wish WHERE userId = :userId")
    abstract fun deleteAllByUserId(userId: Long)

    @Transaction
    open fun clearAndInsert(userId: Long, list: List<WishDbModel>) {
        deleteAllByUserId(userId)
        insertOrReplace(list)
    }

    @Transaction
    open fun updateShortList(wishes: List<WishDbModel>) {
        wishes.forEach { updateShort(it) }
    }

    @Transaction
    open fun updateShort(wish: WishDbModel) {
        getWishModel(wish.id)?.let {
            wish.comment = it.comment
            wish.address = it.address
            wish.cost = it.cost
            wish.link = it.link
        }
        upsert(wish)
    }

    @Transaction
    open fun updateDetail(wish: WishDbModel) {
        getWishModel(wish.id)?.let {
            wish.mainPictureKey = it.mainPictureKey
        }
        upsert(wish)
    }

    @Query("SELECT * FROM wish WHERE id = :id")
    abstract fun observeWish(id: Long): Flowable<WishDbModel>

    @Query("SELECT * FROM wish WHERE wish.userId = :userId")
    abstract fun observeWishList(userId: Long?): Flowable<List<WishDbModel>>

    @Query("DELETE FROM wish WHERE id = :id")
    abstract fun deleteById(id: Long)
}