package com.wishbox.core.db.di

import android.content.Context
import androidx.room.Room
import com.wishbox.core.db.WishBoxDatabase
import com.wishbox.core.db.dao.PictureDao
import com.wishbox.core.db.dao.UserDao
import com.wishbox.core.db.dao.WishDao
import com.wishbox.core.di.scope.PerApplication
import dagger.Module
import dagger.Provides

/**
 * Created by Pak Evgeniy on 29/11/2018.
 */

@Module
class DbModule {

    @PerApplication
    @Provides
    fun provideDb(context: Context): WishBoxDatabase =
        Room.databaseBuilder(context, WishBoxDatabase::class.java, WishBoxDatabase.DB_NAME)
            .fallbackToDestructiveMigration()
            .build()

    @PerApplication
    @Provides
    fun provideWishDao(db: WishBoxDatabase): WishDao = db.getWishDao()

    @PerApplication
    @Provides
    fun provideUserDao(db: WishBoxDatabase): UserDao = db.getUserDao()

    @PerApplication
    @Provides
    fun providePictureDao(db: WishBoxDatabase): PictureDao = db.getPictureDao()
}