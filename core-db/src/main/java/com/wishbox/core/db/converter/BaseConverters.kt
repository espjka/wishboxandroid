package com.wishbox.core.db.converter

import com.google.gson.reflect.TypeToken

/**
 * Created by Pak Evgeniy on 29.12.2018
 */

inline fun <reified T> fromJson(value: T?): String? {
    if (value == null) {
        return null
    }
    val type = object : TypeToken<T>() {

    }.type
    return GsonProvider.gson.toJson(value, type)
}

inline fun <reified T> toJson(valueString: String?): T? {
    if (valueString == null) {
        return null
    }
    val type = object : TypeToken<T>() {

    }.type
    return GsonProvider.gson.fromJson(valueString, type)
}