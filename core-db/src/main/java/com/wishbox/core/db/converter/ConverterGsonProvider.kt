package com.wishbox.core.db.converter

import com.google.gson.Gson
import com.google.gson.GsonBuilder

/**
 * Created by Pak Evgeniy on 06.10.2018
 */

object GsonProvider {
    val gson: Gson by lazy { GsonBuilder().create() }
}