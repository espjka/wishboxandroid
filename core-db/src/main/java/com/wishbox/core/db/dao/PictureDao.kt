package com.wishbox.core.db.dao

import androidx.room.Dao
import androidx.room.Query
import androidx.room.Transaction
import com.wishbox.core.db.model.PictureDbModel
import io.reactivex.Flowable

/**
 * Created by Pak Evgeniy on 22.12.2018
 */

@Dao
abstract class PictureDao : CrudDao<PictureDbModel>(tableName = PictureDbModel.TABLE_NAME) {

    @Query("SELECT * FROM picture WHERE attachId = :wishId AND isMain = 1")
    abstract fun getMainPicture(wishId: Long): PictureDbModel?

    @Query("SELECT * FROM picture WHERE `key` = :key")
    abstract fun getPicture(key: String): PictureDbModel?

    @Query("SELECT * FROM picture WHERE attachId = :wishId ORDER BY isMain DESC")
    abstract fun getPictures(wishId: Long): List<PictureDbModel>

    @Query("SELECT * FROM picture WHERE attachId = :wishId ORDER BY isMain DESC")
    abstract fun observePictures(wishId: Long): Flowable<List<PictureDbModel>>

    @Transaction
    open fun updateMainPicture(wishId: Long, newMain: PictureDbModel) {
        getMainPicture(wishId)
            ?.also {
                it.isMain = false
                upsert(it)
            }
        upsert(newMain)
    }

    @Transaction
    open fun updateMainPicture(wishId: Long, newMainKey: String) {
        getMainPicture(wishId)
            ?.also {
                it.isMain = false
                upsert(it)
            }
        getPicture(newMainKey)?.let {
            it.isMain = true
            upsert(it)
        }
    }

    @Query("DELETE FROM picture WHERE `key` = :key")
    abstract fun deleteByKey(key: String)
}