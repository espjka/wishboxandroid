package com.wishbox.core.db.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.wishbox.core.db.model.PictureDbModel.Companion.TABLE_NAME

/**
 * Created by Pak Evgeniy on 22.12.2018
 */

@Entity(tableName = TABLE_NAME)
data class PictureDbModel(
    @PrimaryKey var key: String = "",
    var attachId: Long = 0,
    var isMain: Boolean = false,
    var small: String = "",
    var medium: String = "",
    var big: String = ""
) {
    companion object {
        const val TABLE_NAME = "picture"
    }
}