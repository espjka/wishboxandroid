package com.wishbox.holder.main

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.smedialink.common.base.BaseViewModel
import com.wishbox.holder.main.navigation.MainSelection
import javax.inject.Inject

/**
 * Created by Pak Evgeniy on 27/11/2018.
 */

class MainViewModel @Inject constructor() : BaseViewModel() {

    var initialSelection: MainSelection? = null

    private val selectionState: MutableLiveData<MainSelection> by lazy {
        MutableLiveData<MainSelection>().apply { value = initialSelection ?: MainSelection.WISHES }
    }

    fun getSelectionState(): LiveData<MainSelection> = selectionState

    fun showScreen(selection: MainSelection) {
        if (selectionState.value == selection) return
        selectionState.value = selection
    }
}