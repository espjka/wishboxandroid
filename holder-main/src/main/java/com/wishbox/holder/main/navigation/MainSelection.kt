package com.wishbox.holder.main.navigation

import androidx.annotation.IdRes
import androidx.fragment.app.Fragment
import com.wishbox.core.di.screenproviders.FeatureProvider
import com.wishbox.holder.main.R
import com.wishbox.holder.main.container.ContainerFragment
import ru.terrakok.cicerone.android.support.SupportAppScreen

/**
 * Created by Pak Evgeniy on 27/11/2018.
 */

enum class MainSelection(@IdRes val resId: Int) {
    CALENDAR(R.id.imageCalendar),
    WISHES(R.id.imageGift),
    FRIENDS(R.id.imageHuman),
    MENU(R.id.imageMenu)
}

fun MainSelection.getScreen(featureProvider: FeatureProvider): SupportAppScreen = when (this) {
    MainSelection.CALENDAR -> featureProvider.getCalendar()
    MainSelection.WISHES -> featureProvider.getWishList()
    MainSelection.FRIENDS -> featureProvider.getFriendList()
    MainSelection.MENU -> featureProvider.getMenu()
}

class ContainerScreen(private val selection: MainSelection) : SupportAppScreen() {
    fun getSelectionName(): String = selection.name
    override fun getFragment(): Fragment = ContainerFragment.newInstance(selection)
}