package com.wishbox.holder.main.di

import com.wishbox.core.di.api.FeatureProviderApi
import com.wishbox.core.di.provider.InjectorApiProvider
import com.wishbox.core.di.api.InjectorApi
import com.wishbox.core.di.module.ViewModelModule
import com.wishbox.core.di.scope.PerActivity
import com.wishbox.holder.main.MainActivity
import dagger.BindsInstance
import dagger.Component

/**
 * Created by Pak Evgeniy on 26/11/2018.
 */

@PerActivity
@Component(
    dependencies = [
        InjectorApi::class
    ],
    modules = [
        ViewModelModule::class,
        MainViewModule::class
    ]
)
interface MainViewComponent : FeatureProviderApi {

    fun inject(activity: MainActivity)

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun bind(activity: MainActivity): Builder

        fun api(api: InjectorApi): Builder

        fun build(): MainViewComponent
    }

    class Initializer private constructor() {
        companion object {

            fun init(activity: MainActivity): MainViewComponent {
                val appApi = (activity.application as InjectorApiProvider).injectorApi

                return DaggerMainViewComponent.builder()
                    .bind(activity)
                    .api(appApi)
                    .build()
            }
        }
    }
}