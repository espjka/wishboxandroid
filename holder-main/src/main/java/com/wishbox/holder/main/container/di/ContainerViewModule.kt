package com.wishbox.holder.main.container.di

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.wishbox.core.di.mapkeys.ViewModelKey
import com.wishbox.core.di.qualifier.Internal
import com.wishbox.core.di.scope.PerFragment
import com.wishbox.core.di.screenproviders.FeatureProvider
import com.wishbox.holder.main.R
import com.wishbox.holder.main.container.ContainerFragment
import com.wishbox.holder.main.container.ContainerNavigator
import com.wishbox.holder.main.container.ContainerViewModel
import com.wishbox.holder.main.navigation.MainSelection
import com.wishbox.holder.main.navigation.getScreen
import dagger.Module
import dagger.Provides
import dagger.multibindings.IntoMap
import ru.terrakok.cicerone.Cicerone
import ru.terrakok.cicerone.Navigator
import ru.terrakok.cicerone.NavigatorHolder
import ru.terrakok.cicerone.Router
import ru.terrakok.cicerone.android.support.SupportAppScreen

/**
 * Created by Pak Evgeniy on 01.12.2018
 */

@Module
class ContainerViewModule {

    @Provides
    fun provideInitialScreen(fragment: ContainerFragment,
                             featureProvider: FeatureProvider): SupportAppScreen =
        MainSelection.values()[fragment.arguments?.getInt(ContainerFragment.BUNDLE_SELECTION)
            ?: throw IllegalArgumentException("MainSelection not specified")].getScreen(featureProvider)

    @PerFragment
    @Provides
    @Internal
    fun provideCicerone(): Cicerone<Router> = Cicerone.create()

    @Provides
    fun provideRouter(@Internal cicerone: Cicerone<Router>): Router = cicerone.router

    @Provides
    @Internal
    fun provideNavigatorHolder(@Internal cicerone: Cicerone<Router>): NavigatorHolder =
        cicerone.navigatorHolder

    @Internal
    @Provides
    fun provideNavigator(fragment: ContainerFragment): Navigator =
        ContainerNavigator(
            fragment.activity!!,
            fragment.childFragmentManager,
            R.id.container
        )

    @Provides
    @IntoMap
    @ViewModelKey(ContainerViewModel::class)
    fun provideViewModel(
        router: Router,
        initialScreen: SupportAppScreen
    ): ViewModel = ContainerViewModel(router, initialScreen)

    @Provides
    fun provideContainerViewModel(
        fragment: ContainerFragment,
        factory: ViewModelProvider.Factory
    ): ContainerViewModel =
        ViewModelProviders.of(fragment, factory).get(ContainerViewModel::class.java)
}