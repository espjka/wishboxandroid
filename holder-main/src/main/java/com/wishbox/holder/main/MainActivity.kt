package com.wishbox.holder.main

import android.content.Context
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import com.wishbox.core.di.api.FeatureProviderApi
import com.wishbox.core.di.provider.FeatureProviderApiProvider
import com.wishbox.core.presentation.navigation.NavigationBackHelper
import com.wishbox.holder.main.di.MainViewComponent
import com.wishbox.holder.main.navigation.ContainerScreen
import com.wishbox.holder.main.navigation.MainNavigator
import com.wishbox.holder.main.navigation.MainSelection
import kotlinx.android.synthetic.main.activity_main.*
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper
import javax.inject.Inject

/**
 * Created by Pak Evgeniy on 26/11/2018.
 */

class MainActivity : AppCompatActivity(), FeatureProviderApiProvider {

    private val component by lazy { MainViewComponent.Initializer.init(this) }

    override val featureProviderApi: FeatureProviderApi
        get() = component

    companion object {
        const val BUNDLE_SCREEN = "BUNDLE_SCREEN"
    }

    @Inject
    lateinit var navigationBackHelper: NavigationBackHelper

    @Inject
    lateinit var navigator: MainNavigator

    @Inject
    lateinit var viewModel: MainViewModel

    override fun attachBaseContext(newBase: Context) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase))
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        injectDependencies()
        super.onCreate(savedInstanceState)
        setContentView(layoutRes())
        savedInstanceState?.getInt(BUNDLE_SCREEN)
            ?.let { viewModel.initialSelection = MainSelection.values()[it] }
        viewCreated()
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)
        outState?.putInt(BUNDLE_SCREEN, viewModel.getSelectionState().value?.ordinal ?: 0)
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle?) {
        super.onRestoreInstanceState(savedInstanceState)
        viewModel.showScreen(
            MainSelection.values()[savedInstanceState?.getInt(
                BUNDLE_SCREEN
            ) ?: 0])
    }

    override fun onBackPressed() {
        if (navigationBackHelper.onBackPressedHandled().not()) super.onBackPressed()
    }

    private fun injectDependencies() {
        component.inject(this)
    }

    private fun layoutRes(): Int = R.layout.activity_main

    private fun viewCreated() {
        layoutCalendar.setOnClickListener { viewModel.showScreen(MainSelection.CALENDAR) }
        layoutGift.setOnClickListener { viewModel.showScreen(MainSelection.WISHES) }
        layoutHuman.setOnClickListener { viewModel.showScreen(MainSelection.FRIENDS) }
        layoutMenu.setOnClickListener { viewModel.showScreen(MainSelection.MENU) }

        viewModel.getSelectionState().observe(this, Observer { updateSelection(it) })
    }

    private fun updateSelection(selection: MainSelection) {
        imageCalendar.isActivated = false
        imageGift.isActivated = false
        imageHuman.isActivated = false
        imageMenu.isActivated = false
        findViewById<View>(selection.resId).isActivated = true
        navigator.showScreen(ContainerScreen(selection))
    }
}