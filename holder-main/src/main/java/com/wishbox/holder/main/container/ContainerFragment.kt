package com.wishbox.holder.main.container

import android.os.Bundle
import com.wishbox.core.di.api.RouterApi
import com.wishbox.core.di.provider.RouterApiProvider
import com.wishbox.core.di.qualifier.Internal
import com.wishbox.core.presentation.base.BaseFragment
import com.wishbox.core.presentation.navigation.BackButtonListener
import com.wishbox.holder.main.R
import com.wishbox.holder.main.container.di.ContainerViewComponent
import com.wishbox.holder.main.navigation.MainSelection
import ru.terrakok.cicerone.Navigator
import ru.terrakok.cicerone.NavigatorHolder
import javax.inject.Inject


/**
 * Created by Pak Evgeniy on 30.11.2018
 */

class ContainerFragment : BaseFragment<ContainerViewModel>(), RouterApiProvider,
    BackButtonListener {

    override fun onBackPressed(): Boolean =
        (childFragmentManager.findFragmentById(R.id.container) as? BackButtonListener)?.onBackPressed() == true

    private val component by lazy { ContainerViewComponent.Initializer.init(this) }

    override val routerApi: RouterApi
        get() = component

    @Inject
    @field:Internal
    lateinit var navigatorHolder: NavigatorHolder

    @Inject
    @field:Internal
    lateinit var navigator: Navigator

    companion object {

        const val BUNDLE_SELECTION = "BUNDLE_SELECTION"

        fun newInstance(selection: MainSelection): ContainerFragment =
            ContainerFragment()
                .apply {
                    arguments = Bundle().apply { putInt(BUNDLE_SELECTION, selection.ordinal) }
                }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        if (childFragmentManager.findFragmentById(R.id.container) == null) {
            viewModel.openInitialScreen()
        }
    }

    override fun onResume() {
        super.onResume()
        navigatorHolder.setNavigator(navigator)
    }

    override fun onPause() {
        navigatorHolder.removeNavigator()
        super.onPause()
    }

    override fun injectDependencies() {
        component.inject(this)
        viewModel.attachNavigatorHolder(navigatorHolder)
    }

    override fun layoutRes(): Int = R.layout.fragment_container
}