package com.wishbox.holder.main.di

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.wishbox.core.di.mapkeys.ViewModelKey
import com.wishbox.core.di.screenproviders.FeatureProvider
import com.wishbox.holder.main.MainActivity
import com.wishbox.holder.main.MainViewModel
import com.wishbox.holder.main.navigation.MainNavigator
import com.wishbox.core.presentation.navigation.NavigationBackHelper
import com.wishbox.holder.main.navigation.MainFeatureProvider
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.multibindings.IntoMap

/**
 * Created by Pak Evgeniy on 26/11/2018.
 */

@Module
abstract class MainViewModule {

    @Binds
    abstract fun provideFeatureProvider(imple: MainFeatureProvider): FeatureProvider

    @Module
    companion object {

        @Provides
        @JvmStatic
        fun provideNavigatorBackHelper(activity: MainActivity): NavigationBackHelper =
            NavigationBackHelper(activity)

        @Provides
        @JvmStatic
        fun provideNavigator(activity: MainActivity): MainNavigator =
            MainNavigator(activity)

        @Provides
        @JvmStatic
        @IntoMap
        @ViewModelKey(MainViewModel::class)
        fun provideViewModel(): ViewModel = MainViewModel()

        @Provides
        @JvmStatic
        fun provideMainViewModel(
            activity: MainActivity,
            factory: ViewModelProvider.Factory
        ): MainViewModel = ViewModelProviders.of(activity, factory).get(MainViewModel::class.java)
    }
}