package com.wishbox.holder.main.container

import com.smedialink.common.base.BaseViewModel
import ru.terrakok.cicerone.NavigatorHolder
import ru.terrakok.cicerone.Router
import ru.terrakok.cicerone.android.support.SupportAppScreen
import javax.inject.Inject

/**
 * Created by Pak Evgeniy on 01.12.2018
 */

class ContainerViewModel @Inject constructor(
    private val router: Router,
    private val initialScreen: SupportAppScreen
) : BaseViewModel() {

    fun openInitialScreen() {
        router.replaceScreen(initialScreen)
    }

    fun attachNavigatorHolder(navigatorHolder: NavigatorHolder) {
        router.setNavigatorHolder(navigatorHolder)
    }
}