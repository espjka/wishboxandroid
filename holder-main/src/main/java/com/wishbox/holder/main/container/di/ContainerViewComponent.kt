package com.wishbox.holder.main.container.di

import com.wishbox.core.di.api.FeatureProviderApi
import com.wishbox.core.di.api.InjectorApi
import com.wishbox.core.di.api.RouterApi
import com.wishbox.core.di.module.ViewModelModule
import com.wishbox.core.di.provider.FeatureProviderApiProvider
import com.wishbox.core.di.provider.InjectorApiProvider
import com.wishbox.core.di.scope.PerFragment
import com.wishbox.holder.main.container.ContainerFragment
import dagger.BindsInstance
import dagger.Component

/**
 * Created by Pak Evgeniy on 01.12.2018
 */

@PerFragment
@Component(
    dependencies = [
        InjectorApi::class,
        FeatureProviderApi::class
    ],
    modules = [
        ContainerViewModule::class,
        ViewModelModule::class
    ]
)
interface ContainerViewComponent : RouterApi {

    fun inject(fragment: ContainerFragment)

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun bind(fragment: ContainerFragment): Builder

        fun bind(api: InjectorApi): Builder

        fun bind(api: FeatureProviderApi): Builder

        fun build(): ContainerViewComponent
    }

    class Initializer private constructor() {
        companion object {

            fun init(fragment: ContainerFragment): ContainerViewComponent {
                val appApi = (fragment.activity!!.application as InjectorApiProvider).injectorApi
                val featureProviderApi = (fragment.activity as FeatureProviderApiProvider).featureProviderApi

                return DaggerContainerViewComponent.builder()
                    .bind(fragment)
                    .bind(appApi)
                    .bind(featureProviderApi)
                    .build()
            }
        }
    }
}