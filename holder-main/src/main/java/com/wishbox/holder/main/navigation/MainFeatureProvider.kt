package com.wishbox.holder.main.navigation

import androidx.fragment.app.Fragment
import com.wishbox.core.di.screenproviders.*
import com.wishbox.feature.friendList.FriendListFragment
import com.wishbox.feature.menu.MenuFragment
import com.wishbox.feature.profile.ProfileFragment
import com.wishbox.feature.wishDetail.WishDetailFragment
import com.wishbox.feature.wishList.WishListFragment
import ru.terrakok.cicerone.android.support.SupportAppScreen
import javax.inject.Inject

/**
 * Created by Pak Evgeniy on 12.12.2018
 */

class MainFeatureProvider @Inject constructor(
    private val changePasswordScreenProvider: ChangePasswordScreenProvider,
    private val entranceScreenProvider: EntranceScreenProvider,
    private val editProfileScreenProvider: EditProfileScreenProvider,
    private val friendsEditorProvider: FriendsEditorProvider,
    private val wishEditorProvider: WishEditorProvider
) : FeatureProvider {

    override fun getFriendList(userId: Long?): SupportAppScreen =
        object : SupportAppScreen() {
            override fun getScreenKey(): String = FriendListFragment::class.java.name
            override fun getFragment(): Fragment = FriendListFragment.newInstance(userId)
        }

    override fun getWishDetail(wishId: Long, userId: Long?): SupportAppScreen =
        object : SupportAppScreen() {
            override fun getScreenKey(): String = WishDetailFragment::class.java.name
            override fun getFragment(): Fragment = WishDetailFragment.newInstance(wishId, userId)
        }

    override fun getWishList(userId: Long?): SupportAppScreen =
        object : SupportAppScreen() {
            override fun getScreenKey(): String = WishListFragment::class.java.name
            override fun getFragment(): Fragment = WishListFragment.newInstance(userId)
        }

    override fun getCalendar(): SupportAppScreen =
        object : SupportAppScreen() {
            override fun getScreenKey(): String = Fragment::class.java.name
            override fun getFragment(): Fragment = Fragment()
        }

    override fun getMenu(): SupportAppScreen =
        object : SupportAppScreen() {
            override fun getScreenKey(): String = MenuFragment::class.java.name
            override fun getFragment(): Fragment = MenuFragment()
        }

    override fun getProfile(id: Long?): SupportAppScreen =
        object : SupportAppScreen() {
            override fun getScreenKey(): String = ProfileFragment::class.java.name
            override fun getFragment(): Fragment = ProfileFragment.newInstance(id)
        }

    override fun getChangePassword(): SupportAppScreen {
        return changePasswordScreenProvider.getChangePasswordScreen()
    }

    override fun getEntrance(): SupportAppScreen {
        return entranceScreenProvider.getEntranceScreen()
    }

    override fun getEditProfile(): SupportAppScreen {
        return editProfileScreenProvider.getEditProfileScreen(false)
    }

    override fun getFriendsEditor(): SupportAppScreen {
        return friendsEditorProvider.getFriendsEditorScreen(false)
    }

    override fun getWishEditInfo(wishId: Long?): SupportAppScreen {
        return wishEditorProvider.getWishEditorScreen(wishId)
    }
}