package com.wishbox.holder.main.navigation

import androidx.fragment.app.Fragment
import com.wishbox.holder.main.MainActivity
import com.wishbox.holder.main.R
import javax.inject.Inject

/**
 * Created by Pak Evgeniy on 26/11/2018.
 */

class MainNavigator @Inject constructor(private val activity: MainActivity) {

    fun showScreen(screen: ContainerScreen) {
        val fm = activity.supportFragmentManager
        var currentFragment: Fragment? = null
        val fragments = fm.fragments
        for (f in fragments) {
            if (f.isVisible) {
                currentFragment = f
                break
            }
        }
        val newFragment = fm.findFragmentByTag(screen.getSelectionName())

        if (currentFragment != null && newFragment != null && currentFragment === newFragment) return

        val transaction = fm.beginTransaction()
        if (newFragment == null) {
            transaction.add(R.id.container, screen.fragment, screen.getSelectionName())
        }

        if (currentFragment != null) {
            transaction.hide(currentFragment)
        }

        if (newFragment != null) {
            transaction.show(newFragment)
        }

        transaction.commitNow()
    }
}