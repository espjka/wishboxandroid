package com.wishbox.holder.main.container

import android.content.Intent
import android.net.Uri
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.FragmentManager
import com.wishbox.core.presentation.navigation.OpenUrl
import com.wishbox.holder.main.R
import ru.terrakok.cicerone.android.support.SupportAppNavigator
import ru.terrakok.cicerone.commands.Command
import javax.inject.Inject

/**
 * Created by Pak Evgeniy on 05.12.2018
 */

class ContainerNavigator @Inject constructor(
    activity: FragmentActivity,
    fragmentManager: FragmentManager,
    containerId: Int
) : SupportAppNavigator(activity, fragmentManager, containerId) {

    override fun applyCommand(command: Command?) {
        when (command) {
            is OpenUrl -> openUrl(command.url)
            else -> super.applyCommand(command)
        }
    }

    private fun openUrl(url: String) {
        val intent = Intent(Intent.ACTION_VIEW, Uri.parse(url))
        activity.startActivity(
            Intent.createChooser(intent, activity.getString(R.string.choose_browser))
        )
    }
}