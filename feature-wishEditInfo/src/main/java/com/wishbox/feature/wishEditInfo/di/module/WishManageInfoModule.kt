package com.wishbox.feature.wishEditInfo.di.module

import android.content.ClipboardManager
import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.smedialink.common.error.ErrorHandler
import com.wishbox.core.di.mapkeys.ViewModelKey
import com.wishbox.core.di.screenproviders.FeatureProvider
import com.wishbox.core.domain.repository.WishRepository
import com.wishbox.core.domain.validator.Validator
import com.wishbox.core.presentation.pictures.PicturesAdapter
import com.wishbox.core.presentation.pictures.PicturesManager
import com.wishbox.feature.wishEditInfo.WishManageInfoFragment
import com.wishbox.feature.wishEditInfo.WishManageInfoViewModel
import com.wishbox.feature.wishEditInfo.di.qualifier.WishManageInfo
import com.wishbox.feature.wishEditInfo.domain.usecase.GetMetaDataUseCase
import com.wishbox.feature.wishEditInfo.domain.usecase.WishCreateUseCase
import com.wishbox.feature.wishEditInfo.domain.usecase.WishUpdateUseCase
import com.wishbox.feature.wishEditInfo.domain.validator.UrlValidator
import com.wishbox.feature.wishEditInfo.error.WishManageInfoErrorHandler
import com.wishbox.feature.wishEditInfo.mapper.WishManageViewMapper
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.multibindings.IntoMap
import ru.terrakok.cicerone.Router

@Module
abstract class WishManageInfoModule {

    @WishManageInfo
    @Binds
    abstract fun provideErrorHandler(impl: WishManageInfoErrorHandler): ErrorHandler

    @Binds
    abstract fun provideUrlValidator(impl: UrlValidator): Validator

    @Module
    companion object {

        @JvmStatic
        @Provides
        fun provideWishId(fragment: WishManageInfoFragment): Long? =
            fragment.wishId?.takeIf { it != 0L }

        @JvmStatic
        @Provides
        fun providePagerAdapter(fragment: WishManageInfoFragment): PicturesAdapter =
            PicturesAdapter(fragment.context!!)

        @JvmStatic
        @Provides
        fun provideClipboardManager(context: Context): ClipboardManager =
            context.getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager

        @JvmStatic
        @Provides
        @IntoMap
        @ViewModelKey(WishManageInfoViewModel::class)
        fun provideViewModel(
            wishId: Long?,
            wishCreateUseCase: WishCreateUseCase,
            wishUpdateUseCase: WishUpdateUseCase,
            mapper: WishManageViewMapper,
            getMetaDataUseCase: GetMetaDataUseCase,
            repository: WishRepository,
            router: Router,
            @WishManageInfo errorHandler: ErrorHandler,
            featureProvider: FeatureProvider,
            picturesManager: PicturesManager
        ): ViewModel = WishManageInfoViewModel(
            wishId,
            wishCreateUseCase,
            wishUpdateUseCase,
            { repository.observeWish(it) },
            getMetaDataUseCase,
            mapper,
            errorHandler,
            router,
            featureProvider,
            picturesManager
        )

        @JvmStatic
        @Provides
        fun provideWishCreateViewModel(
            fragment: WishManageInfoFragment,
            factory: ViewModelProvider.Factory
        ): WishManageInfoViewModel = ViewModelProviders.of(fragment, factory).get(
            WishManageInfoViewModel::class.java
        )
    }
}
