package com.wishbox.feature.wishEditInfo.domain.validator

import com.wishbox.core.domain.validator.Validator
import com.wishbox.feature.wishEditInfo.domain.exception.NotUrlException
import io.reactivex.Completable
import java.util.regex.Pattern
import javax.inject.Inject

/**
 * Created by Pak Evgeniy on 12.12.2018
 */

class UrlValidator @Inject constructor() : Validator {

    companion object {
        private const val REGEX_URL =
            "\\b(https?|ftp|file)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]"
    }

    override fun validate(value: String): Completable =
        if (!value.isEmpty() && Pattern.compile(REGEX_URL).matcher(value).matches()) Completable.complete()
        else Completable.error(NotUrlException())
}