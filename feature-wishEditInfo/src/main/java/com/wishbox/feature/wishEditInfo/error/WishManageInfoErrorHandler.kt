package com.wishbox.feature.wishEditInfo.error

import com.wishbox.core.presentation.errorHandling.MainErrorHandler
import com.wishbox.core.presentation.resources.ResourceProvider
import com.wishbox.feature.wishEditInfo.R
import com.wishbox.feature.wishEditInfo.domain.exception.NotUrlException
import com.wishbox.feature.wishEditInfo.domain.exception.WishNameValidationException
import javax.inject.Inject

class WishManageInfoErrorHandler @Inject constructor(
    resourceProvider: ResourceProvider
) : MainErrorHandler(resourceProvider) {

    init {
        addError(WishNameValidationException::class.java) { createDefaultState(R.string.wish_create_name_validation_error)  }
        addError(NotUrlException::class.java) { createDefaultState(R.string.error_not_valid_url) }
    }
}
