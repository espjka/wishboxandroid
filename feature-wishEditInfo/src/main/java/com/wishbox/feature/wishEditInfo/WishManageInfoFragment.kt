package com.wishbox.feature.wishEditInfo

import android.content.ClipDescription
import android.content.ClipboardManager
import android.os.Bundle
import android.view.View
import com.smedialink.common.delegates.bundle.bundleLong
import com.smedialink.common.delegates.bundle.bundleString
import com.smedialink.common.events.handle
import com.smedialink.common.ext.*
import com.smedialink.common.util.StateHolder
import com.wishbox.core.presentation.base.BaseDialog
import com.wishbox.core.presentation.base.BaseFragment
import com.wishbox.core.presentation.errorHandling.delegate.ToastDelegate
import com.wishbox.core.presentation.loading.ProgressLoadingDelegate
import com.wishbox.core.presentation.navigation.BackButtonListener
import com.wishbox.core.presentation.pictures.PicturesAdapter
import com.wishbox.feature.wishEditInfo.custom.DecimalDigitsInputFilter
import com.wishbox.feature.wishEditInfo.custom.WishBoxTextWatcher
import com.wishbox.feature.wishEditInfo.di.WishManageInfoComponent
import com.wishbox.feature.wishEditInfo.model.WishManageState
import com.wishbox.feature.wishmanage.wishManageInfo.helper.NewUrlListener
import kotlinx.android.synthetic.main.fragment_wish_manage_info.*
import kotlinx.android.synthetic.main.partial_wish_edit_info_photo.view.*
import javax.inject.Inject

class WishManageInfoFragment : BaseFragment<WishManageInfoViewModel>(), BackButtonListener,
    NewUrlListener {

    companion object {
        fun newInstance(wishId: Long? = null, url: String? = null): WishManageInfoFragment =
            WishManageInfoFragment()
                .apply {
                    this.wishId = wishId
                    this.url = url
                }
    }

    var wishId: Long? by bundleLong()
    private var url: String? by bundleString()

    private var stateHolder = StateHolder<WishManageState>()

    private val progressLoadingDelegate: ProgressLoadingDelegate by lazy {
        return@lazy ProgressLoadingDelegate(context!!, "Сохранение...")
    }

    @Inject
    lateinit var clipboardManager: ClipboardManager

    @Inject
    lateinit var errorDelegate: ToastDelegate

    @Inject
    lateinit var picturesAdapter: PicturesAdapter

    override fun injectDependencies() {
        WishManageInfoComponent.Initializer.inject(this)
    }

    override fun layoutRes() = R.layout.fragment_wish_manage_info

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        stateHolder = StateHolder() // need to refresh state holder, cause it is not recreated when we navigate back
        initUi()
    }

    override fun onBackPressed(): Boolean {
        viewModel.closeScreen()
        return true
    }

    override fun onNewUrl(url: String) {
        showMetaInsertDialog(url)
    }

    private fun initUi() {
        toolbar.getLeftButton().setOnClickListener { viewModel.closeScreen() }
        btnCreate.setOnClickListener { manageWish() }
        toolbar.getRightButton().setOnClickListener { manageWish() }

        viewModel.uiModel.observe(this, this::render)
        setupEditWish()

        etName.addTextChangedListener(WishBoxTextWatcher(etName))
        etName.onTextChanged { s, _, _, _ -> viewModel.name = s.toString().trim() }
        etAddress.addTextChangedListener(WishBoxTextWatcher(etAddress))
        etAddress.onTextChanged { s, _, _, _ -> viewModel.address = s.toString().trim() }
        etCost.addTextChangedListener(WishBoxTextWatcher(etCost))
        etCost.onTextChanged { s, _, _, _ -> viewModel.cost = s.toString().trim() }
        etLink.addTextChangedListener(WishBoxTextWatcher(etLink))
        etLink.onTextChanged { s, _, _, _ -> viewModel.link = s.toString().trim() }
        etComment.addTextChangedListener(WishBoxTextWatcher(etComment))
        etComment.onTextChanged { s, _, _, _ -> viewModel.comment = s.toString().trim() }

        with(includeLayoutPictures) {
            imageAdd.setOnClickListener { viewModel.openManagePicturesScreen() }
            viewPagerPictures.adapter = picturesAdapter
            tabPageIndicator.setupWithViewPager(viewPagerPictures, true)
        }

        includeLayoutEmptyPicture.setOnClickListener { viewModel.openManagePicturesScreen() }

        etCost.filters = arrayOf(DecimalDigitsInputFilter(9, 2))

        etLink.setOnLongClickListener {
            handlePasteLink()
            true
        }

        url?.let { showMetaInsertDialog(it) }
    }

    private fun setupEditWish() {
        wishId?.takeIf { it > 0 }?.let {
            btnCreate.setText(R.string.wish_save)
            toolbar.getTitleText().setText(R.string.wish_edit)
            toolbar.getRightButton().visible()
            btnCreate.gone()
        } ?: run {
            toolbar.getRightButton().gone()
        }
    }

    private fun manageWish() {
        viewModel.manageWish()
    }

    private fun handlePasteLink() {
        if (clipboardManager.primaryClipDescription?.hasMimeType(ClipDescription.MIMETYPE_TEXT_PLAIN) == true) {
            clipboardManager.primaryClip
                ?.getItemAt(0)
                ?.text
                ?.toString()
                ?.also { showMetaInsertDialog(it) }
        }
    }

    private fun showMetaInsertDialog(url: String) {
        BaseDialog.show(
            fragmentManager = childFragmentManager,
            title = getString(R.string.insert_meta_data_title),
            description = getString(R.string.insert_meta_data_description),
            positiveText = getString(R.string.insert),
            okClick = { viewModel.handleMetaDataInsert(url) }
        )
    }

    private fun render(state: WishManageState) {
        stateHolder = stateHolder.newState(state)

        with (stateHolder) {

            diff({ isLoading }) { progressLoadingDelegate.showLoading(it) }

            diffNullable({ wish?.address }) { etAddress.fillText(it) }
            diffNullable({ wish?.link }) { etLink.fillText(it) }
            diffNullable({ wish?.cost }) { etCost.fillText(it) }
            diffNullable({ wish?.comment }) { etComment.fillText(it) }
            diffNullable({ wish?.name }) { etName.fillText(it) }
        }

        if (state.wish?.pictures?.isNotEmpty() == true) {
            picturesAdapter.urls = state.wish.pictures
            includeLayoutEmptyPicture.gone()
            includeLayoutPictures.visible()
        } else {
            includeLayoutEmptyPicture.visible()
            includeLayoutPictures.gone()
        }

        state.error?.handle { errorDelegate.show(it.errorText) }
    }
}
