package com.wishbox.feature.wishEditInfo.mapper

import com.wishbox.core.domain.entity.MetaEntity
import com.wishbox.core.domain.entity.WishEntity
import com.wishbox.feature.wishEditInfo.model.WishManageUiModel
import javax.inject.Inject

/**
 * Created by Pak Evgeniy on 04.12.2018
 */

class WishManageViewMapper @Inject constructor() {

    fun map(from: WishEntity): WishManageUiModel =
        WishManageUiModel(
            name = from.name,
            link = from.link,
            address = from.address,
            comment = from.comment,
            cost = from.cost,
            pictures = from.pictures?.map { it.big }.orEmpty()
        )

    fun mapPictures(prev: WishManageUiModel?, pictures: List<String>): WishManageUiModel =
            prev?.copy(pictures = pictures) ?: WishManageUiModel(pictures = pictures)

    fun map(prev: WishManageUiModel?, from: MetaEntity, url: String): WishManageUiModel =
        WishManageUiModel(
            name = prev?.name ?: from.title,
            link = url,
            address = prev?.address,
            comment = prev?.comment,
            cost = prev?.cost,
            pictures = prev?.pictures.orEmpty()
        )
}