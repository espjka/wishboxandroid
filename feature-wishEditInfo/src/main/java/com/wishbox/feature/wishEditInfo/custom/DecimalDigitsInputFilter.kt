package com.wishbox.feature.wishEditInfo.custom

import android.text.InputFilter
import android.text.Spanned

class DecimalDigitsInputFilter(
    private val digitsBeforeDot: Int,
    private val digitsAfterDot: Int
) : InputFilter {

    companion object {
        const val DOT = '.'
    }

    override fun filter(
        source: CharSequence,
        start: Int,
        end: Int,
        dest: Spanned,
        dstart: Int,
        dend: Int
    ): CharSequence? {

        var dotPos = -1
        val len = dest.length
        for (i in 0 until len) {
            val c = dest[i]
            if (c == DOT) {
                dotPos = i
                break
            }
        }

        when {
            dotPos == dstart && dend != len && source.isEmpty() -> return DOT.toString()
            dotPos < 0 -> {
                if (len >= digitsBeforeDot && source != DOT.toString()) {
                    return ""
                }
            }
            dend <= dotPos -> {
                if (dotPos >= digitsBeforeDot) {
                    return ""
                }
            }
            else -> {
                if (len - dotPos > digitsAfterDot) {
                    return ""
                }
            }
        }

        return null
    }
}