package com.wishbox.feature.wishEditInfo.di

import com.wishbox.core.di.api.FeatureProviderApi
import com.wishbox.core.di.api.InjectorApi
import com.wishbox.core.di.api.RouterApi
import com.wishbox.core.di.module.ViewModelModule
import com.wishbox.core.di.provider.FeatureProviderApiProvider
import com.wishbox.core.di.provider.InjectorApiProvider
import com.wishbox.core.di.provider.RouterApiProvider
import com.wishbox.core.di.scope.PerFragment
import com.wishbox.core.di.util.lookupApiProvider
import com.wishbox.core.presentation.di.PictureManagerApi
import com.wishbox.core.presentation.di.PictureManagerProvider
import com.wishbox.feature.wishEditInfo.WishManageInfoFragment
import com.wishbox.feature.wishEditInfo.di.module.WishManageInfoModule
import dagger.BindsInstance
import dagger.Component

@PerFragment
@Component(
    dependencies = [
        InjectorApi::class,
        RouterApi::class,
        FeatureProviderApi::class,
        PictureManagerApi::class
    ],
    modules = [
        WishManageInfoModule::class,
        ViewModelModule::class
    ]
)
interface WishManageInfoComponent {

    fun inject(fragment: WishManageInfoFragment)

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun bind(fragment: WishManageInfoFragment): Builder

        fun api(api: InjectorApi): Builder

        fun api(api: RouterApi): Builder

        fun api(api: FeatureProviderApi): Builder

        fun api(api: PictureManagerApi): Builder

        fun build(): WishManageInfoComponent
    }

    class Initializer {
        companion object {
            fun inject(fragment: WishManageInfoFragment) {
                val injectorProvider = fragment.lookupApiProvider<InjectorApiProvider>()
                val routerProvider = fragment.lookupApiProvider<RouterApiProvider>()
                val featureProviderProvider = fragment.lookupApiProvider<FeatureProviderApiProvider>()
                val pictureManagerProvider = fragment.lookupApiProvider<PictureManagerProvider>()

                DaggerWishManageInfoComponent.builder()
                    .bind(fragment)
                    .api(injectorProvider.injectorApi)
                    .api(routerProvider.routerApi)
                    .api(featureProviderProvider.featureProviderApi)
                    .api(pictureManagerProvider.pictureManagerApi)
                    .build()
                    .inject(fragment)
            }
        }
    }
}