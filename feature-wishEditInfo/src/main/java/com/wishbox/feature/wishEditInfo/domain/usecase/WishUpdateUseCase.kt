package com.wishbox.feature.wishEditInfo.domain.usecase

import com.wishbox.core.domain.entity.WishRequestParams
import com.wishbox.core.domain.repository.WishRepository
import com.wishbox.feature.wishEditInfo.domain.exception.WishNameValidationException
import io.reactivex.Completable
import io.reactivex.Single
import javax.inject.Inject

class WishUpdateUseCase @Inject constructor(
    private val wishRepository: WishRepository
) {
    operator fun invoke(id: Long, params: WishRequestParams): Completable =
        Single.fromCallable { params.name.isNotEmpty() }
            .flatMapCompletable {
                if (!it) Completable.error(WishNameValidationException())
                else wishRepository.updateWish(id, params)
            }
}