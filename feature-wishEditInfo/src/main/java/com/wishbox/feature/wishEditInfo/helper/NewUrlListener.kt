package com.wishbox.feature.wishmanage.wishManageInfo.helper

/**
 * Created by Pak Evgeniy on 07.12.2018
 */

interface NewUrlListener {
    fun onNewUrl(url: String)
}