package com.wishbox.feature.wishEditInfo.domain.exception

/**
 * Created by Pak Evgeniy on 12.12.2018
 */

class NotUrlException : Exception()