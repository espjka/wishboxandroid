package com.wishbox.feature.wishEditInfo.domain.usecase

import com.wishbox.core.domain.entity.WishRequestParams
import com.wishbox.core.domain.repository.PictureRepository
import com.wishbox.core.domain.repository.WishRepository
import com.wishbox.feature.wishEditInfo.domain.exception.WishNameValidationException
import io.reactivex.Completable
import io.reactivex.Single
import javax.inject.Inject

class WishCreateUseCase @Inject constructor(
    private val wishRepository: WishRepository,
    private val pictureRepository: PictureRepository
) {
    operator fun invoke(
        params: WishRequestParams,
        localPictures: List<String> = emptyList()
    ): Completable =
        Single.fromCallable { params.name.isNotEmpty() }
            .flatMap {
                if (!it) Single.error(WishNameValidationException())
                else wishRepository.createWish(params)
            }.flatMapCompletable { wish ->
                pictureRepository.initialUpload(wish.id, wish.links, localPictures)
                    .flatMapCompletable { wishRepository.updateMainPicture(wish.id, it) }
            }
}