package com.wishbox.feature.wishEditInfo

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.smedialink.common.base.BaseViewModel
import com.smedialink.common.delegates.rx.disposableDelegate
import com.smedialink.common.error.ErrorHandler
import com.smedialink.common.ext.applySchedulers
import com.wishbox.core.di.screenproviders.FeatureProvider
import com.wishbox.core.domain.entity.WishEntity
import com.wishbox.core.domain.entity.WishRequestParams
import com.wishbox.core.presentation.pictures.PicturesManager
import com.wishbox.feature.wishEditInfo.domain.usecase.GetMetaDataUseCase
import com.wishbox.feature.wishEditInfo.domain.usecase.WishCreateUseCase
import com.wishbox.feature.wishEditInfo.domain.usecase.WishUpdateUseCase
import com.wishbox.feature.wishEditInfo.mapper.WishManageViewMapper
import com.wishbox.feature.wishEditInfo.model.WishManageState
import com.wishbox.feature.wishEditInfo.model.WishManageStateChanges
import com.wishbox.feature.wishEditInfo.model.WishManageUiModel
import io.reactivex.Flowable
import io.reactivex.disposables.Disposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import ru.terrakok.cicerone.Router

class WishManageInfoViewModel(
    private val wishId: Long?,
    private val createWishUseCase: WishCreateUseCase,
    private val updateWishUseCase: WishUpdateUseCase,
    getWish: (id: Long) -> Flowable<WishEntity>,
    private val getMetaData: GetMetaDataUseCase,
    private val mapper: WishManageViewMapper,
    private val errorHandler: ErrorHandler,
    private val router: Router,
    private val featureProvider: FeatureProvider,
    private val pictureManager: PicturesManager
) : BaseViewModel() {

    private var manageWishDisposable: Disposable? by disposableDelegate()
    private var observeWishDisposable: Disposable? by disposableDelegate()
    private var metaDataDisposable: Disposable? by disposableDelegate()

    private val internalUiModel: MutableLiveData<WishManageState> = MutableLiveData()

    var name: String? = null
    var link: String? = null
    var cost: String? = null
    var comment: String? = null
    var address: String? = null
    var pictures: List<String>? = null

    init {
        internalUiModel.value = WishManageState.initial()

        observeWishDisposable = if (wishId != null) {
            getWish.invoke(wishId)
                .map { mapper.map(it) }
        } else {
            pictureManager.observePictures()
                .map { mapper.mapPictures(internalUiModel.value?.wish, it) }
        }.applySchedulers()
            .doOnSubscribe { reduceState(WishManageStateChanges.Loading) }
            .subscribeBy(
                onError = { reduceState(WishManageStateChanges.Error(errorHandler.handle(it))) },
                onNext = { reduceState(WishManageStateChanges.Data(it)) }
            ).addTo(disposables)
    }

    val uiModel: LiveData<WishManageState> = internalUiModel

    fun manageWish() {
        val params = WishRequestParams(
            name.orEmpty(),
            link,
            cost,
            comment,
            address
        )

        reduceState(
            WishManageStateChanges.Data(
                WishManageUiModel(
                    name = name,
                    link = link,
                    cost = cost,
                    comment = comment,
                    address = address,
                    pictures = uiModel.value?.wish?.pictures.orEmpty()
                )
            )
        )

        manageWishDisposable = (wishId?.let { updateWishUseCase(it, params) } ?: createWishUseCase(
            params,
            pictureManager.pictures
        ))
            .applySchedulers()
            .doOnSubscribe { reduceState(WishManageStateChanges.Loading) }
            .subscribeBy(
                onError = { reduceState(WishManageStateChanges.Error(errorHandler.handle(it))) },
                onComplete = {
                    internalUiModel.value = WishManageState.initial()
                    closeScreen()
                }
            ).addTo(disposables)
    }

    fun handleMetaDataInsert(url: String) {
        reduceState(
            WishManageStateChanges.Data(
                WishManageUiModel(
                    name = name,
                    link = link,
                    cost = cost,
                    comment = comment,
                    address = address,
                    pictures = uiModel.value?.wish?.pictures.orEmpty()
                )
            )
        )
        val prev = internalUiModel.value?.wish
        if (prev?.name?.isNotEmpty() == true && prev.pictures.size == PicturesManager.MAX_PICTURES_AMOUNT) {
            reduceState(WishManageStateChanges.Data(prev.copy(link = url)))
        } else {
            metaDataDisposable = getMetaData.invoke(url)
                .map { mapper.map(internalUiModel.value?.wish, it, url) }
                .applySchedulers()
                .doOnSubscribe { reduceState(WishManageStateChanges.Loading) }
                .subscribeBy(
                    onError = { reduceState(WishManageStateChanges.Error(errorHandler.handle(it))) },
                    onSuccess = { reduceState(WishManageStateChanges.Data(it)) }
                ).addTo(disposables)
        }
    }

    fun openManagePicturesScreen() {
        router.navigateTo(featureProvider.getManagePictures(wishId))
    }

    fun closeScreen() {
        router.exit()
    }

    private fun reduceState(changes: WishManageStateChanges) {
        val prevState = internalUiModel.value ?: WishManageState()
        internalUiModel.value = when (changes) {
            WishManageStateChanges.Loading -> prevState.copy(isLoading = true)
            is WishManageStateChanges.Data -> WishManageState(wish = changes.wish)
            is WishManageStateChanges.Error -> {
                prevState.copy(
                    error = changes.errorState.event,
                    isLoading = false
                )
            }
        }
    }
}
