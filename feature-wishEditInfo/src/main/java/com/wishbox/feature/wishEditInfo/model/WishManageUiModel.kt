package com.wishbox.feature.wishEditInfo.model

/**
 * Created by Pak Evgeniy on 04.12.2018
 */

data class WishManageUiModel(
    val name: String? = null,
    val link: String? = null,
    val comment: String? = null,
    val address: String? = null,
    val cost: String? = null,
    val pictures: List<String> = emptyList()
)
