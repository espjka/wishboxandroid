package com.wishbox.feature.wishEditInfo.domain.exception

import java.lang.Exception

class WishNameValidationException : Exception()
