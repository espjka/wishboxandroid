package com.wishbox.feature.wishEditInfo.custom

import android.text.Editable
import android.text.TextWatcher
import android.widget.EditText
import androidx.annotation.DrawableRes
import com.wishbox.feature.wishEditInfo.R

class WishBoxTextWatcher(private val editText: EditText) : TextWatcher {
    override fun afterTextChanged(s: Editable?) {

    }

    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
    }

    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
        val state = s.getState()
        editText.elevation = state.elevation
        editText.setBackgroundResource(state.backgroundRes)
    }

    fun CharSequence?.getState(): State = if (isNullOrBlank()) State.EMPTY else State.FILLED

    enum class State(val elevation: Float, @DrawableRes val backgroundRes: Int) {
        EMPTY(0f, R.drawable.wish_box_edit_bg),
        FILLED(8F, R.drawable.wish_box_edit_text_bg_filled)
    }

}