package com.wishbox.feature.wishEditInfo.model

import com.smedialink.common.error.ErrorState
import com.smedialink.common.events.ErrorEvent

data class WishManageState(
    val isLoading: Boolean = false,
    val error: ErrorEvent? = null,
    val wish: WishManageUiModel? = null
) {
    companion object {
        fun initial() = WishManageState(false, null, WishManageUiModel())
    }
}

sealed class WishManageStateChanges {
    object Loading : WishManageStateChanges()
    class Data(val wish: WishManageUiModel) : WishManageStateChanges()
    class Error(val errorState: ErrorState) : WishManageStateChanges()
}