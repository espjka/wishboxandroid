package com.wishbox.feature.wishEditInfo.domain.usecase

import com.wishbox.core.domain.entity.MetaEntity
import com.wishbox.core.domain.repository.MetaInfoRepository
import com.wishbox.feature.wishEditInfo.domain.exception.NotUrlException
import com.wishbox.feature.wishEditInfo.domain.validator.UrlValidator
import io.reactivex.Single
import javax.inject.Inject

/**
 * Created by Pak Evgeniy on 12.12.2018
 */

class GetMetaDataUseCase @Inject constructor(
    private val wishRepository: MetaInfoRepository,
    private val urlValidator: UrlValidator
) {

    operator fun invoke(url: String): Single<MetaEntity> =
        urlValidator.validate(url)
            .andThen(wishRepository.getMetaInfo(url))
}