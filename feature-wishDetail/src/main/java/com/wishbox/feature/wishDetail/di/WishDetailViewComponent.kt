package com.wishbox.feature.wishDetail.di

import com.wishbox.core.di.api.FeatureProviderApi
import com.wishbox.core.di.api.InjectorApi
import com.wishbox.core.di.api.RouterApi
import com.wishbox.core.di.module.ViewModelModule
import com.wishbox.core.di.provider.FeatureProviderApiProvider
import com.wishbox.core.di.provider.InjectorApiProvider
import com.wishbox.core.di.provider.RouterApiProvider
import com.wishbox.core.di.scope.PerFragment
import com.wishbox.core.di.util.lookupApiProvider
import com.wishbox.feature.wishDetail.WishDetailFragment
import dagger.BindsInstance
import dagger.Component

/**
 * Created by Pak Evgeniy on 26/11/2018.
 */

@PerFragment
@Component(
    dependencies = [
        InjectorApi::class,
        FeatureProviderApi::class,
        RouterApi::class
    ],
    modules = [
        ViewModelModule::class,
        WishDetailViewModule::class
    ]
)
interface WishDetailViewComponent {

    fun inject(fragment: WishDetailFragment)

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun bind(fragment: WishDetailFragment): Builder

        fun api(api: InjectorApi): Builder

        fun api(api: RouterApi): Builder

        fun api(api: FeatureProviderApi): Builder

        fun build() : WishDetailViewComponent
    }

    class Initializer private constructor() {
        companion object {

            fun init(fragment: WishDetailFragment) {
                val injectorProvider = fragment.lookupApiProvider<InjectorApiProvider>()
                val routerProvider = fragment.lookupApiProvider<RouterApiProvider>()
                val featureProviderProvider = fragment.lookupApiProvider<FeatureProviderApiProvider>()

                DaggerWishDetailViewComponent.builder()
                    .api(injectorProvider.injectorApi)
                    .api(routerProvider.routerApi)
                    .api(featureProviderProvider.featureProviderApi)
                    .bind(fragment)
                    .build()
                    .inject(fragment)
            }
        }
    }
}