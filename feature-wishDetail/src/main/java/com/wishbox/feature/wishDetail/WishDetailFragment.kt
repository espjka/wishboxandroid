package com.wishbox.feature.wishDetail

import android.app.Dialog
import android.os.Bundle
import android.view.View
import com.smedialink.common.delegates.bundle.bundleLong
import com.smedialink.common.events.handle
import com.smedialink.common.ext.gone
import com.smedialink.common.ext.observe
import com.smedialink.common.ext.visible
import com.wishbox.core.glide.loadImage
import com.wishbox.core.presentation.base.BaseBottomSheetDialog
import com.wishbox.core.presentation.base.BaseDialog
import com.wishbox.core.presentation.base.BaseFragment
import com.wishbox.core.presentation.errorHandling.delegate.ToastDelegate
import com.wishbox.core.presentation.navigation.BackButtonListener
import com.wishbox.core.presentation.pictures.PicturesAdapter
import com.wishbox.feature.wishDetail.di.WishDetailViewComponent
import com.wishbox.feature.wishDetail.model.WishState
import kotlinx.android.synthetic.main.fragment_wish_detail.*
import javax.inject.Inject

/**
 * Created by Pak Evgeniy on 26/11/2018.
 */

class WishDetailFragment : BaseFragment<WishDetailViewModel>(), BackButtonListener {

    @Inject
    lateinit var errorDelegate: ToastDelegate

    @Inject
    lateinit var picturesAdapter: PicturesAdapter

    var wishId: Long? by bundleLong()
        private set

    var userId: Long? by bundleLong()
        private set

    companion object {
        fun newInstance(wishId: Long, userId: Long? = null): WishDetailFragment = WishDetailFragment()
            .apply {
                this.wishId = wishId
                this.userId = userId
            }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.getState().observe(this, this::render)
        viewModel.observeWish()

        setupForNotMineWishes()

        buttonLink.setOnClickListener { viewModel.openLink() }
        toolbar.getLeftButton().setOnClickListener { viewModel.navigateBack() }
        toolbar.getRightButton().setOnClickListener {
            BaseBottomSheetDialog.show(
                childFragmentManager,
                getString(R.string.edit),
                getString(R.string.delete),
                { dialog ->
                    viewModel.navigateToEdit()
                    dialog.dismiss()
                },
                { dialog -> showDeleteDialog(dialog) }
            )
        }

        pagerPhotos.adapter = picturesAdapter
        tabPageIndicator.setupWithViewPager(pagerPhotos, true)
    }

    private fun showDeleteDialog(bottomSheetDialog: Dialog) {
        viewModel.getState().value?.wish?.run {
            BaseDialog.show(
                fragmentManager = childFragmentManager,
                title = getString(R.string.delete_wish_question, name),
                description = if (isCommentSpecified) comment else null,
                positiveText = getString(R.string.delete),
                okClick = {
                    viewModel.deleteWish()
                    bottomSheetDialog.dismiss()
                }
            )
        }
    }

    override fun onBackPressed(): Boolean {
        viewModel.navigateBack()
        return true
    }

    override fun injectDependencies() {
        WishDetailViewComponent.Initializer.init(this)
    }

    override fun layoutRes(): Int = R.layout.fragment_wish_detail

    private fun render(state: WishState) {
        state.wish?.let {
            if (it.photoUrls.isNotEmpty()) {
                picturesAdapter.urls = it.photoUrls
                imageEmptyPhoto.gone()
            } else {
                picturesAdapter.urls = emptyList()
                imageEmptyPhoto.visible()
            }
            textTitle.text = it.name
            textPrice.text = it.cost
            textAddress.text = it.address
            textComment.text = it.comment
            imageEmptyPhoto.loadImage(
                null,
                placeholder = R.drawable.placeholder_wish
            )
            if (it.link.isNullOrBlank()) buttonLink.gone() else buttonLink.visible()
        }
        state.userName?.let { toolbar.getDescriptionText().text = it }
        state.error?.handle { errorDelegate.show(it.errorText) }
    }

    private fun setupForNotMineWishes() {
        if (userId.takeIf { it != 0L } != null) {
            with (toolbar) {
                getTitleText().text = getString(R.string.title_wishes)
                getDescriptionText().visible()
                toolbar.getRightButton().gone()
            }
        }
    }
}