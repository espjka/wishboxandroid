package com.wishbox.feature.wishDetail.model

/**
 * Created by Pak Evgeniy on 04/12/2018.
 */

data class WishDetailUiModel(
    val id: Long,
    val name: String,
    val link: String? = null,
    val comment: String,
    val address: String,
    val cost: String,
    val isCommentSpecified: Boolean,
    val photoUrls: List<String> = emptyList()
)