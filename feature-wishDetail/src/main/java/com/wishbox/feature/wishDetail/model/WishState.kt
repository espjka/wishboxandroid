package com.wishbox.feature.wishDetail.model

import com.smedialink.common.error.ErrorState
import com.smedialink.common.events.ErrorEvent

/**
 * Created by Pak Evgeniy on 30.11.2018
 */

data class WishState(
    val wish: WishDetailUiModel? = null,
    val error: ErrorEvent? = null,
    val isLoading: Boolean = false,
    val userName: String? = null
)

sealed class WishStateChanges {
    object Loading : WishStateChanges()
    class Data(val wish: WishDetailUiModel) : WishStateChanges()
    class Error(val error: ErrorState) : WishStateChanges()
    class UserName(val userName: String) : WishStateChanges()
}