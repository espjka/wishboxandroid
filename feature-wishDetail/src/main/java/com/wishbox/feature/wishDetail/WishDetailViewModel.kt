package com.wishbox.feature.wishDetail

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.smedialink.common.base.BaseViewModel
import com.smedialink.common.delegates.rx.disposableDelegate
import com.smedialink.common.error.ErrorHandler
import com.wishbox.core.di.qualifier.UserId
import com.wishbox.core.di.qualifier.WishId
import com.wishbox.core.di.screenproviders.FeatureProvider
import com.wishbox.core.domain.entity.UserEntity
import com.wishbox.core.domain.entity.WishEntity
import com.wishbox.core.presentation.navigation.openUrl
import com.wishbox.feature.wishDetail.mapper.UserViewMapper
import com.wishbox.feature.wishDetail.mapper.WishDetailUiMapper
import com.wishbox.feature.wishDetail.model.WishState
import com.wishbox.feature.wishDetail.model.WishStateChanges
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import ru.terrakok.cicerone.Router
import javax.inject.Inject

/**
 * Created by Pak Evgeniy on 26/11/2018.
 */

class WishDetailViewModel @Inject constructor(
    private val router: Router,
    @WishId private val wishId: Long,
    @UserId private val userId: Long?,
    private val mapper: WishDetailUiMapper,
    private val userMapper: UserViewMapper,
    private val errorHandler: ErrorHandler,
    private val getUser: (userId: Long) -> Single<UserEntity>,
    private val featureProvider: FeatureProvider,
    private val requestWish: (id: Long) -> Completable,
    private val observeWish: (id: Long) -> Flowable<WishEntity>,
    private val deleteWish: (id: Long) -> Completable
) : BaseViewModel() {

    private val state: MutableLiveData<WishState> by lazy { MutableLiveData<WishState>() }

    private var requestDisposable: Disposable? by disposableDelegate()
    private var observeDisposable: Disposable? by disposableDelegate()
    private var deleteDisposable: Disposable? by disposableDelegate()
    private var getUserDisposable: Disposable? by disposableDelegate()

    fun getState(): LiveData<WishState> = state

    init {
        requestWish()
        requestUser()
    }

    private fun requestWish() {
        requestDisposable = requestWish.invoke(wishId)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { reduceState(WishStateChanges.Loading) }
            .subscribeBy(
                onError = { reduceState(WishStateChanges.Error(errorHandler.handle(it))) }
            ).addTo(disposables)
    }

    private fun requestUser() {
        getUserDisposable = userId?.let(getUser)
            ?.map { userMapper.mapName(it) }
            ?.subscribeOn(Schedulers.io())
            ?.observeOn(AndroidSchedulers.mainThread())
            ?.subscribeBy(
                onError = { reduceState(WishStateChanges.Error(errorHandler.handle(it))) },
                onSuccess = { reduceState(WishStateChanges.UserName(it)) }
            )
            ?.addTo(disposables)
    }

    fun observeWish() {
        observeDisposable = observeWish.invoke(wishId)
            .map { WishStateChanges.Data(mapper.map(it)) }
            .cast(WishStateChanges::class.java)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeBy(
                onError = { reduceState(WishStateChanges.Error(errorHandler.handle(it))) },
                onNext = { reduceState(it) }
            ).addTo(disposables)
    }

    fun navigateBack() {
        router.exit()
    }

    private fun reduceState(changes: WishStateChanges) {
        val prevState = state.value ?: WishState()
        state.value = when (changes) {
            WishStateChanges.Loading -> prevState.copy(isLoading = true)
            is WishStateChanges.Data -> WishState(
                wish = changes.wish
            )
            is WishStateChanges.Error -> prevState.copy(
                error = changes.error.event,
                isLoading = false
            )
            is WishStateChanges.UserName -> prevState.copy(userName = changes.userName)
        }
    }

    fun navigateToEdit() {
        router.navigateTo(featureProvider.getWishEditInfo(wishId))
    }

    fun deleteWish() {
        deleteDisposable = deleteWish.invoke(wishId)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeBy(
                onError = { reduceState(WishStateChanges.Error(errorHandler.handle(it))) },
                onComplete = { router.exit() }
            ).addTo(disposables)
    }

    fun openLink() {
        state.value?.wish?.link?.takeIf { it.isNotBlank() }?.let { router.openUrl(it) }
    }
}