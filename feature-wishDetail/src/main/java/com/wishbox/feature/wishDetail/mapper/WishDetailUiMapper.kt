package com.wishbox.feature.wishDetail.mapper

import com.wishbox.core.domain.entity.WishEntity
import com.wishbox.core.presentation.resources.ResourceProvider
import com.wishbox.feature.wishDetail.R
import com.wishbox.feature.wishDetail.model.WishDetailUiModel
import javax.inject.Inject

/**
 * Created by Pak Evgeniy on 04/12/2018.
 */

class WishDetailUiMapper @Inject constructor(
    private val resourceProvider: ResourceProvider
) {

    private val notSpecifiedInfoString: String by lazy { resourceProvider.getString(R.string.not_specified) }

    fun map(from: WishEntity): WishDetailUiModel =
        WishDetailUiModel(
            id = from.id,
            name = from.name,
            link = from.link,
            comment = from.comment?.takeIf { it.isNotBlank() } ?: notSpecifiedInfoString,
            address = from.address?.takeIf { it.isNotBlank() } ?: notSpecifiedInfoString,
            cost = from.cost?.takeIf { it.isNotBlank() } ?: notSpecifiedInfoString,
            isCommentSpecified = from.comment?.isNotBlank() == true,
            photoUrls = from.pictures?.map { it.medium }.orEmpty()
        )
}