package com.wishbox.feature.wishDetail.di

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.smedialink.common.error.ErrorHandler
import com.wishbox.core.di.mapkeys.ViewModelKey
import com.wishbox.core.di.qualifier.UserId
import com.wishbox.core.di.qualifier.WishId
import com.wishbox.core.di.screenproviders.FeatureProvider
import com.wishbox.core.domain.repository.UserRepository
import com.wishbox.core.domain.repository.WishRepository
import com.wishbox.core.presentation.errorHandling.MainErrorHandler
import com.wishbox.core.presentation.pictures.PicturesAdapter
import com.wishbox.feature.wishDetail.WishDetailFragment
import com.wishbox.feature.wishDetail.WishDetailViewModel
import com.wishbox.feature.wishDetail.mapper.UserViewMapper
import com.wishbox.feature.wishDetail.mapper.WishDetailUiMapper
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.multibindings.IntoMap
import ru.terrakok.cicerone.Router

/**
 * Created by Pak Evgeniy on 26/11/2018.
 */

@Module
abstract class WishDetailViewModule {

    @Binds
    abstract fun provideErrorHandler(impl: MainErrorHandler): ErrorHandler

    @Module
    companion object {

        @JvmStatic
        @Provides
        fun providePagerAdapter(fragment: WishDetailFragment): PicturesAdapter =
            PicturesAdapter(fragment.context!!)

        @Provides
        @JvmStatic
        @WishId
        fun provideWishId(fragment: WishDetailFragment): Long =
            fragment.wishId ?: throw IllegalArgumentException("wishId not specified")

        @Provides
        @JvmStatic
        @UserId
        fun provideUserId(fragment: WishDetailFragment): Long? = fragment.userId.takeIf { it != 0L }

        @Provides
        @IntoMap
        @ViewModelKey(WishDetailViewModel::class)
        @JvmStatic
        fun provideViewModel(
            router: Router,
            @WishId wishId: Long,
            @UserId userId: Long?,
            userMapper: UserViewMapper,
            errorHandler: ErrorHandler,
            mapper: WishDetailUiMapper,
            repository: WishRepository,
            userRepository: UserRepository,
            featureProvider: FeatureProvider
        ): ViewModel =
            WishDetailViewModel(
                router = router,
                wishId = wishId,
                userId = userId,
                errorHandler = errorHandler,
                featureProvider = featureProvider,
                mapper = mapper,
                userMapper = userMapper,
                getUser = { userRepository.getUser(it) },
                requestWish = { repository.requestWish(it) },
                observeWish = { repository.observeWish(it) },
                deleteWish = { repository.deleteWish(it) }
            )

        @Provides
        @JvmStatic
        fun provideWishDetailViewModel(
            fragment: WishDetailFragment,
            factory: ViewModelProvider.Factory
        ): WishDetailViewModel =
            ViewModelProviders.of(fragment, factory).get(WishDetailViewModel::class.java)
    }


}